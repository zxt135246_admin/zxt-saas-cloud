# zxt-saas-cloud

#### 项目介绍
文案系统 微服务架构(saas微服务版本)

主页(域名访问)：http://paofudu.cn/static/page/login.html（域名未备案暂停使用）

主页(外网ip访问)：http://101.35.246.250/static/page/login.html

#### 软件架构
微服务 高可用、高并发开发部署


#### 部署

1.  华为云+腾讯云双服务器部署
2.  nginx、redis集群、mysql主从部署、nacos集群、rocketmq集群
3.  docker/k8s 网关中心、用户中心、文案中心、运营中心集群式部署
4.  阿里云远程Maven仓库部署
5.  OSS、SMS、MAIL、微信公众号等


#### 模块介绍

1.  zxt-bean 通用bean，通用工具类，swagger等
2.  zxt-core-cloud 项目核心包 项目环境，JVM参数数值，maven编译打包插件配置，Maven远程仓库地址配置
3.  zxt-parent-cloud 项目父工程，项目模块统一管理，依赖版本统一控制
4.  zxt-starter-cloud springCloud封装，nacos注册发现和配置中心配置和服务共享配置
5.  zxt-starter-web 日志系统，web配置，日志拦截器，ThreadLocal拦截器，防重复提交拦截器，统一异常处理,undertow容器配置
6.  zxt-starter-redis 缓存配置封装，注解aop解决缓存击穿、穿透、雪崩等问题
7.  zxt-starter-lock 基于redis配置的分布式锁封装，策略模式实现集群或单机配置
8.  zxt-starter-feign feign调用封装，重写feign请求头
9.  zxt-starter-database db封装，多数据源，主从配置，读写分离，druid数据源监控，mybatis配置、字段自动填充，标识雪花算法生成
10. zxt-starter-rocketmq rocketmq封装，mq信息的监控和存储
11. zxt-gateway 网关中心，权限统一控制，token认证，ws控制，项目跨域
12. zxt-user 用户中心，用户相关管理，微信公众号对外
13. zxt-content 文案中心，相册中心，文案系统相关
14. zxt-operation 运营中心，oss,sms,mail,mq相关管理

#### 参与

1.  架构骨架搭建（全模块从0搭建）
2.  系统权限设计（请求流转，数据传输）
3.  系统服务部署（docker，k8s，部署应用，部署服务）
4.  开发流程梳理（controller->service->manager->mapper->xml）

``` lua
项目结构举例
zxt-operation
├── zxt-operation-api      -- 对外提供接口
└── zxt-operation-service   -- 业务逻辑层
```

### 强制要求：
```lua
1、接收参数在api项目下req包中，用DTO类
2、输出对象在api项目下resp包中，用VO类
3、表名、类名使用英文
4、数据库规范：http://wiki.staples.cn/pages/viewpage.action?pageId=2917514
```

### git规范

## 分支
```lua
master：当前线上分支
dev：发布分支
feature-日期：新功能分支
bug-fix-日期：修复bug分支
```

## 提交
```lua
feat:新功能
fix:修补bug
docs:文档
style:格式
refactor:重构
```