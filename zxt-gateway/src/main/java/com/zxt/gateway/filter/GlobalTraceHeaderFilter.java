package com.zxt.gateway.filter;


import com.zxt.bean.constants.TraceConstant;
import com.zxt.bean.utils.UuIdUtils;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.net.URI;
import java.util.Locale;

/**
 * 网关全局添加TraceID
 */
@Component
public class GlobalTraceHeaderFilter implements GlobalFilter, Ordered {

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        // 给请求及响应设置TraceID，但如果是webSocket请求时  无需设置响应的TraceID 否则会将长链接断开
        String traceID = UuIdUtils.randomUUID(Boolean.FALSE).toUpperCase();
        exchange.getRequest().mutate().header(TraceConstant.TRACE_ID_HEADER, traceID).build();
        URI uri = exchange.getRequest().getURI();
        // webSocket请求 不设置响应头
        if (uri.getPath().toLowerCase(Locale.ROOT).contains("-ws")) {
            return chain.filter(exchange);
        }
        return chain.filter(exchange).then(Mono.fromRunnable(() -> exchange.getResponse().getHeaders().add(TraceConstant.TRACE_ID_HEADER, traceID)));
    }

    @Override
    public int getOrder() {
        return 0;
    }
}

