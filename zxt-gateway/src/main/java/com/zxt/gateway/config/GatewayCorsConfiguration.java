package com.zxt.gateway.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.reactive.CorsWebFilter;
import org.springframework.web.cors.reactive.UrlBasedCorsConfigurationSource;
import org.springframework.web.util.pattern.PathPatternParser;

@Configuration
public class GatewayCorsConfiguration {


    /**
     * 整个项目的跨域都在这里
     *
     * @return
     */
    @Bean
    public CorsWebFilter corsWebFilter() {
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource(new PathPatternParser());

        CorsConfiguration gatewayCorsConfiguration = new CorsConfiguration();

        //1.配置跨域
        gatewayCorsConfiguration.addAllowedHeader("*");
        gatewayCorsConfiguration.addAllowedMethod("*");
        gatewayCorsConfiguration.addAllowedOrigin("*");
        gatewayCorsConfiguration.setAllowCredentials(true);

        source.registerCorsConfiguration("/**", gatewayCorsConfiguration);
        return new CorsWebFilter(source);
    }
}
