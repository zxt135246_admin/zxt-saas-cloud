package com.zxt.gateway.factory;

import com.alibaba.fastjson.JSON;
import com.zxt.bean.constants.ApiConstant;
import com.zxt.bean.model.UserInfoVO;
import com.zxt.bean.result.Result;
import com.zxt.bean.result.ResultCode;
import com.zxt.bean.utils.CollectionsUtil;
import com.zxt.bean.utils.ip.IpUtil;
import com.zxt.bean.utils.LocalDateUtil;
import com.zxt.bean.utils.StringUtils;
import com.zxt.gateway.utils.ResponseUtil;
import com.zxt.redis.utils.UserUtil;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;
import org.springframework.util.PathMatcher;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLEncoder;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;
import java.util.Objects;


/**
 * 权限
 * 1.验证是否登录
 * <p>从header/cookie中获取token</p>
 * 2.验证是否有相应的api权限
 */
@Component
@Slf4j
@RefreshScope
public class AuthGatewayFilterFactory extends AbstractGatewayFilterFactory<AuthGatewayFilterFactory.Config> {

    @Value("${ignore.base.path:''}")
    private String ignoreBasePath;

    private static final String REQUEST_TIME_BEGIN = "asgRequestTimeBegin";

    public AuthGatewayFilterFactory() {
        super(Config.class);
    }

    PathMatcher pathMatcher = new AntPathMatcher();

    public GatewayFilter apply(Config config) {

        return (exchange, chain) -> {

            // 当前时间
            LocalDateTime now = LocalDateTime.now();

            ServerHttpRequest request = exchange.getRequest();
            URI uri = request.getURI();
            log.info("uri={}", uri);
            // 请求路径
            String path = uri.getPath();

            // 忽略文档接口
            if (ignoreDoc(path)) {
                return chain.filter(exchange);
            }


            String ip = IpUtil.getIp(request);
            // 获取token
            String token = getToken(request, ApiConstant.HEADER_KEY);

            // 忽略验证登录的接口
            if (loginIgnorePath(config, path)) {
                UserInfoVO userInfoVO = null;
                if (StringUtils.isNotBlank(token)) {
                    userInfoVO = UserUtil.getUser(token);
                }
                return addHeaderInfo(exchange, chain, now, request, path, ip, userInfoVO, "IgnoreLoginRecord");
            }

            if (StringUtils.isBlank(token)) {
                // 未登录
                return ResponseUtil.returnAuthFail(exchange, Result.newError(ResultCode.NO_LOGGED_IN));
            }

            UserInfoVO userInfoVO = UserUtil.getUser(token);
            if (Objects.nonNull(userInfoVO.getIsVisitor()) && userInfoVO.getIsVisitor()) {
                //游客无法访问
                return ResponseUtil.returnAuthFail(exchange, Result.newError(ResultCode.VISITOR_USER));
            }

            // 根据token获取用户信息
            log.info("根据token获取用户信息token={},result={}", token, JSON.toJSONString(userInfoVO));
            if (userInfoVO == null) {
                return ResponseUtil.returnAuthFail(exchange, Result.newError(ResultCode.SESSION_INVALID_ERROR));
            }

            // 设置用户信息
            UserUtil.expire(userInfoVO);

            return addHeaderInfo(exchange, chain, now, request, path, ip, userInfoVO, uri + " AuthRecord");
        };
    }


    private Mono<Void> addHeaderInfo(ServerWebExchange exchange, GatewayFilterChain chain, LocalDateTime now, ServerHttpRequest request,
                                     String path, String ip, UserInfoVO userInfoVO, String info) {
        ServerHttpRequest serverHttpRequest = null;
        request.mutate().build();
        ServerWebExchange build = exchange.mutate().request(serverHttpRequest).build();
        build.getAttributes().put(REQUEST_TIME_BEGIN, LocalDateTime.now());

        String userStr = JSON.toJSONString(userInfoVO);
        String encode;
        try {
            encode = URLEncoder.encode(userStr, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            log.error(e.getMessage(), e);
            encode = userStr;
        }
        exchange.getRequest().mutate().header(ApiConstant.USER_HEADER_KEY, encode).build();

        return chain.filter(build).then(
                Mono.fromRunnable(() -> {
                    LocalDateTime time = build.getAttribute(REQUEST_TIME_BEGIN);
                    Opt opt = new Opt();
                    if (!Objects.isNull(userInfoVO)) {
                        opt.userId = userInfoVO.getId();
//                        opt.userCode = userInfoVO.getUserName();
                        opt.userName = userInfoVO.getNickName();
                    }
                    opt.ip = ip;
                    opt.path = path;
                    opt.requestTime = LocalDateUtil.format(now, "yyyy-MM-dd HH:mm:ss");
                    if (time != null) {
                        opt.processingTime = (System.currentTimeMillis() - LocalDateTime.now().toInstant(ZoneOffset.of("+8")).toEpochMilli()) + "ms";
                        opt.startTime = LocalDateUtil.format(now, "yyyy-MM-dd HH:mm:ss");
                    }
                    // 记录请求日志
                    log.info("{}:{}", info, JSON.toJSONString(opt));
                })
        );
    }


    private String getToken(ServerHttpRequest request, String key) {
        HttpHeaders headers = request.getHeaders();

        // 从header中获取token
        List<String> authorizations = headers.get(key);
        String value = null;
        if (CollectionsUtil.isNotEmpty(authorizations)) {
            value = authorizations.get(0);
        }
        return value;
    }

    /**
     * 忽略非必须登录的链接
     *
     * @param config
     * @param path
     * @return
     */
    private boolean loginIgnorePath(Config config, String path) {
        String loginIgnorePath = config.loginIgnorePath;
        if (StringUtils.isNotBlank(loginIgnorePath)) {
            String[] pathArray = loginIgnorePath.split("\\|");
            for (int i = 0; i < pathArray.length; i++) {
                if (this.pathMatcher.match(pathArray[i], path)) {
                    return true;
                }
            }
        }
        return false;
    }


    /**
     * 忽略文档链接
     *
     * @param path
     * @return
     */
    private boolean ignoreDoc(String path) {
        if (StringUtils.isBlank(ignoreBasePath)) {
            return false;
        }
        String[] pathArray = ignoreBasePath.split("\\|");
        for (int i = 0; i < pathArray.length; i++) {
            if (this.pathMatcher.match(pathArray[i], path)) {
                return true;
            }
        }

        return false;
    }


    class Opt {
        public Long userId;
        public String userCode;
        public String userName;
        public String ip;
        public String path;
        // 请求时间
        public String requestTime;
        // 验证通过后时间
        public String startTime;
        // 处理时间
        public String processingTime;

    }


    @Data
    public static class Config {
        // 忽略验证登录的接口
        private String loginIgnorePath;
        // 忽略验证权限的接口
        private String authIgnorePath;
    }

}
