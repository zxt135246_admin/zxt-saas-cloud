package com.zxt.redis.utils;

import com.alibaba.fastjson.JSON;
import com.zxt.bean.constants.ApiConstant;
import com.zxt.bean.constants.RedisKeyUser;
import com.zxt.bean.constants.UserConstant;
import com.zxt.bean.model.UserInfoVO;
import com.zxt.bean.utils.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * @author zxt
 */
@Component
@Slf4j
public class UserUtil {

    // 令牌自定义标识
    public static String HEADER;

    private static final String LOG_INFO = "user redis ";

    /**
     * 获取登录凭证
     *
     * @return 登录凭证
     */
    public static String getToken() {
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        if (Objects.isNull(requestAttributes)) {
            return null;
        }
        HttpServletRequest request = getCurrentRequest();
        String token = request.getHeader(ApiConstant.HEADER_KEY);
        if (StringUtils.isNotEmpty(token)) {
            log.info(LOG_INFO + "token from header {}", ApiConstant.HEADER_KEY);
            return token;
        }
        return null;
    }

    /**
     * 获取登录用户基本信息
     *
     * @return
     */
    public static UserInfoVO getUser(String token) {
        Long startTime = System.currentTimeMillis();
        String tokenStr = token;
        if (StringUtils.isEmpty(token)) {
            tokenStr = getToken();
        }
        if (StringUtils.isBlank(tokenStr)) {
            log.info(LOG_INFO + "token is null");
            return null;
        }
        UserInfoVO userInfoVO = getUserInfoByToken(tokenStr);
        log.info(LOG_INFO + "get user info from redis end,user:{},timeUsed:{}ms", JSON.toJSONString(userInfoVO), System.currentTimeMillis() - startTime);
        return userInfoVO;
    }

    private static UserInfoVO getUserInfoByToken(String token) {
        log.info(LOG_INFO + "token={}", token);
        if (StringUtils.isNotEmpty(token)) {
            // 获取用户唯一标识
            Object object = RedisUtil.get(RedisKeyUser.getUserKey(token));
            if (Objects.nonNull(object)) {
                String userTokenStr = String.valueOf(object);
                if (StringUtils.isNotEmpty(userTokenStr)) {
                    return JSON.parseObject(userTokenStr, UserInfoVO.class);
                }
            }
        }
        return null;
    }

    /**
     * 设置用户信息
     *
     * @param userInfoVO
     */
    public static String setRedisUser(UserInfoVO userInfoVO) {
        if (StringUtils.isEmpty(userInfoVO.getToken())) {
            // 设置登录凭证
            String token = (UUID.randomUUID().toString() + UUID.randomUUID().toString()).replace("-", "").toLowerCase();
            userInfoVO.setToken(token);
        }
        // 当前时间
        LocalDateTime now = LocalDateTime.now();
        // 超时时间
        long timeoutHours = getTimeoutHours();
        LocalDateTime timeout = now.plusHours(timeoutHours);
        userInfoVO.setTimeout(timeout);
        // 设置用户缓存
        String userKey = RedisKeyUser.getUserKey(userInfoVO.getToken());
        // token 用户信息
        String userInfoStr = JSON.toJSONString(userInfoVO);
        RedisUtil.set(userKey, userInfoStr, TimeUnit.HOURS, timeoutHours);
        return userInfoVO.getToken();
    }

    /**
     * 重设token过期时间
     *
     * @param userInfoVO 用户信息
     */
    public static void expire(UserInfoVO userInfoVO) {
        if (Objects.isNull(userInfoVO)) {
            return;
        }
        long timeoutHours = getTimeoutHours();

        LocalDateTime now = LocalDateTime.now();
        Duration between = Duration.between(now, userInfoVO.getTimeout());
        long hours = between.toHours();
        log.info("hours:{},timeoutHours:{}", hours, timeoutHours);
        if (hours <= (timeoutHours / 2)) {
            log.info("用户token即将过期，重设token");
            // 当前时间距离过期时间还差一半的时间才重设
            setRedisUser(userInfoVO);
        }

    }

    /**
     * 获取超时时间，单位：小时
     *
     * @return
     */
    private static long getTimeoutHours() {
        //设置token的有效时间为2小时
        long timeout = 2;
        return timeout;
    }


    private static HttpServletRequest getCurrentRequest() {
        return RequestContextHolder.currentRequestAttributes() == null ? null : ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
    }

    /**
     * 获取请求token
     *
     * @param request
     * @return token
     */
    public static String getToken(HttpServletRequest request) {
        String token = request.getHeader(HEADER);
        if (StringUtils.isNotEmpty(token) && token.startsWith(UserConstant.TOKEN_PREFIX)) {
            token = token.replace(UserConstant.TOKEN_PREFIX, "");
        }
        return token;
    }

    /**
     * 是否超级管理员
     *
     * @param userInfoVO
     * @return
     */
    public static Boolean isSuperAdmin(UserInfoVO userInfoVO) {
        if (Objects.isNull(userInfoVO)) {
            return Boolean.FALSE;
        }
        return userInfoVO.getUserName().equals("superAdmin");
    }
}
