package com.zxt.redis.annotation;

import java.lang.annotation.*;

/**
 * 自定义缓存注解，可以夹在方法上标记缓存方法的返回
 *
 * @author zxt
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface CustomCache {


    String key() default "";

    /**
     * 过期时间/分钟
     *
     * @return
     */
    int expirationTime() default 60;

    /**
     * 是否同步 用来解决
     * 缓存击穿:大量并发查询同一个正好过期的数据
     * 可以开启同步解决
     *
     * @return
     */
    boolean sync() default false;

}
