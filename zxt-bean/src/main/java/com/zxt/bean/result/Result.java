package com.zxt.bean.result;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

@ApiModel(
        value = "Rest result",
        description = "请求结果"
)
public class Result<T> implements Serializable {
    private static final long serialVersionUID = -4696898674758052311L;
    @ApiModelProperty("结果代码")
    private Integer code;
    @ApiModelProperty("错误说明")
    private String message;
    @ApiModelProperty("链路id")
    private String traceId;
    @ApiModelProperty("结果对象")
    private T data;
    @ApiModelProperty("操作标识")
    private Boolean success;
    @ApiModelProperty("当前时间戳")
    private Long timestamp;

    public Result(int code, String message, boolean success, T data) {
        this.code = ResultCode.SUCCESS.code;
        this.message = "";
        this.traceId = "";
        this.success = Boolean.TRUE;
        this.setCode(code);
        this.setMessage(message);
        this.setSuccess(success);
        this.setData(data);
    }

    public Result(int code, String message, T data) {
        this.code = ResultCode.SUCCESS.code;
        this.message = "";
        this.traceId = "";
        this.success = Boolean.TRUE;
        this.setCode(code);
        this.setMessage(message);
        this.setSuccess(code == 200);
        this.setData(data);
    }

    public Result() {
        this.code = ResultCode.SUCCESS.code;
        this.message = "";
        this.traceId = "";
        this.success = Boolean.TRUE;
    }

    public static <T> Result<T> custom(int code, String message) {
        return new Result(code, message, (Object)null);
    }

    public static <T> Result<T> custom(int code, String message, T data) {
        return new Result(code, message, data);
    }

    public static <T> Result<T> newInstance(ResultCode resultCode, boolean success, T value) {
        return new Result(resultCode.code, resultCode.desc, success, value);
    }

    public static <T> Result<T> newSuccess() {
        return new Result(ResultCode.SUCCESS.code, ResultCode.SUCCESS.desc, true, (Object)null);
    }

    public static <T> Result<T> newSuccess(T value) {
        return new Result(ResultCode.SUCCESS.code, ResultCode.SUCCESS.desc, true, value);
    }

    public static <T> Result<T> newSuccess(T data, String message) {
        return new Result(ResultCode.SUCCESS.code, message, true, data);
    }

    public static <T> Result<T> newError(String message) {
        Result<T> result = new Result(ResultCode.COMMON_MESSAGE.code, message, false, (Object)null);
        return result;
    }

    public static <T> Result<T> newError(ResultCode errorCode) {
        return new Result(errorCode.code, errorCode.desc, false, (Object)null);
    }

    public static <T> Result<T> newError(int code, String message, Object... args) {
        Result<T> r = new Result(code, "", false, (Object)null);
        r.setFormatMessage(message, args);
        return r;
    }

    public static <T> Result<T> newError(ResultCode resultCode, Object... args) {
        Result<T> r = new Result(resultCode.getCode(), "", false, (Object)null);
        r.setFormatMessage(resultCode.getDesc(), args);
        return r;
    }

    public Result<T> setErrorCode(ResultCode errorCode) {
        if (errorCode == null) {
            return null;
        } else {
            this.code = errorCode.code;
            this.message = errorCode.desc;
            this.success = false;
            return this;
        }
    }

    public Result<T> setErrorCode(ResultCode errorCode, Object... args) {
        if (errorCode == null) {
            return null;
        } else {
            this.code = errorCode.code;
            this.setFormatMessage(errorCode.getDesc(), args);
            this.success = false;
            return this;
        }
    }

    public void setMessageStatus(ResultCode resultCode, Object... args) {
        this.setCode(resultCode.getCode());
        this.setFormatMessage(resultCode.getDesc(), args);
    }

    public void setFormatMessage(String message, Object... args) {
        if (args != null && args.length != 0) {
            this.setMessage(String.format(message, args));
        } else {
            this.setMessage(message);
        }

    }

    public Long getTimestamp() {
        return this.timestamp == null ? System.currentTimeMillis() : this.timestamp;
    }

    public Integer getCode() {
        return this.code;
    }

    public String getMessage() {
        return this.message;
    }

    public String getTraceId() {
        return this.traceId;
    }

    public T getData() {
        return this.data;
    }

    public Boolean getSuccess() {
        return this.success;
    }

    public void setCode(final Integer code) {
        this.code = code;
    }

    public void setMessage(final String message) {
        this.message = message;
    }

    public void setTraceId(final String traceId) {
        this.traceId = traceId;
    }

    public void setData(final T data) {
        this.data = data;
    }

    public void setSuccess(final Boolean success) {
        this.success = success;
    }

    public void setTimestamp(final Long timestamp) {
        this.timestamp = timestamp;
    }
}