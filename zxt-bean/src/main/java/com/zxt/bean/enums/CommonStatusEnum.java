package com.zxt.bean.enums;

/**
 * @Comments: 状态
 */
public enum CommonStatusEnum {
	CREATE                 ((byte) 0, "新建"),
	ENABLE                 ((byte) 1, "启用"),
	DISABLE                ((byte) 2, "停用"),
	EXPIRE                 ((byte) 3, "过期"),
	;

	private byte value;
	private String desc;

	CommonStatusEnum(byte value, String desc){
		this.value = value;
		this.desc = desc;
	}

    public byte getValue() {
        return value;
    }

    public String getDesc() {
		return desc;
	}

	public static boolean exists(Byte status) {
        if (status == null) {
            return false;
        }
        byte s = status.byteValue();
        return exists(s);
    }

    public static boolean exists(byte s) {
        for (CommonStatusEnum element : CommonStatusEnum.values()) {
            if (element.value == s) {
                return true;
            }
        }
        return false;
    }

    public boolean equal(Byte val) {
        return val == null ? false : val.byteValue() == this.value;
    }

	public static String getDescByValue(Byte value) {
		if (value == null) {
			return "";
		}
		for (CommonStatusEnum element : CommonStatusEnum.values()) {
			if (element.value == value.byteValue()) {
				return element.desc;
			}
		}
		return "";
	}
}
