package com.zxt.bean.enums;

/**
 * 限流类型
 *
 * @author zxt
 */
public enum LimitTypeEnums {
    /**
     * 默认策略全局限流
     */
    DEFAULT,

    /**
     * 根据请求者IP进行限流
     */
    IP
}
