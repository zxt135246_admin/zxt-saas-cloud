package com.zxt.bean.annotation;

import java.lang.annotation.*;

/**
 * 参考：https://blog.csdn.net/ANewProgrammer/article/details/123404617
 */
@Documented
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface ExcelSelected {
    /**
     * 固定下拉内容
     */
    String[] source() default {};

    /**
     * 设置下拉框的起始行，默认为第二行
     */
    int firstRow() default 1;

    /**
     * 设置下拉框的结束行，默认为最后一行
     */
    int lastRow() default 0x10000;

    /**
     * 是否允许输入下拉框之外的值
     */
    boolean isUnanimous() default false;
}
