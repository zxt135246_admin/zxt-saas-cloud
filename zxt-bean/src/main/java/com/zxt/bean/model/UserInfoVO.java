package com.zxt.bean.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.zxt.bean.convertor.LongJsonDeserializer;
import com.zxt.bean.convertor.LongJsonSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString(callSuper = true)
@ApiModel("用户登录信息")
public class UserInfoVO implements Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 1L;

    /**
     * token
     */
    @ApiModelProperty("token")
    private String token;

    /**
     * 超时时间
     */
    @ApiModelProperty("超时时间")
    private LocalDateTime timeout;

    /**
     * 用户id
     */
    @ApiModelProperty("用户id")
    @JsonSerialize(using = LongJsonSerializer.class)
    @JsonDeserialize(using = LongJsonDeserializer.class)
    private Long id;

    /**
     * 用户昵称
     */
    @ApiModelProperty("用户昵称")
    private String nickName;

    /**
     * 用户名
     */
    private String userName;

    /**
     * 手机号
     */
    private String phone;

    @ApiModelProperty("头像")
    private String avatarUrl;

    @ApiModelProperty("会员成长数值")
    private Integer growthValue;

    @ApiModelProperty("下一级需要的会员成长数值")
    private Integer nextGrowthValue;

    @ApiModelProperty("会员等级描述")
    private String levelDesc;

    private Boolean isAdmin;

    @ApiModelProperty("是游客")
    private Boolean isVisitor;

    /**
     * 权限列表
     */
    private Set<String> perms;


}
