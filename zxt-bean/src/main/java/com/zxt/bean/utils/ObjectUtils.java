package com.zxt.bean.utils;


import java.lang.reflect.Field;

public class ObjectUtils {

    /**
     * 1.去除对象中string类型的前后空格
     * 2.将string类型为空串的置为null
     *
     * @param o
     */
    public static void removeStringSpaces(Object o) {
        if (o == null) {
            return;
        }
        try {
            Class<?> aClass = o.getClass();
            Field[] fields = aClass.getDeclaredFields();
            for (Field field : fields) {
                field.setAccessible(Boolean.TRUE);
                if (field.getType().getName().equals("java.lang.String")) {
                    Object value = field.get(o);
                    if (value == null) {

                    } else if ("".equals(value)) {
                        field.set(o, null);
                    } else {
                        String trim = value.toString().trim();
                        if ("".equals(trim)) {
                            field.set(o, null);
                        } else {
                            field.set(o, value.toString().trim());
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
