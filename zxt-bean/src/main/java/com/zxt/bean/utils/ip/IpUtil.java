package com.zxt.bean.utils.ip;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;

import com.zxt.bean.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.server.reactive.ServerHttpRequest;

public class IpUtil {
    private static final Logger log = LoggerFactory.getLogger(IpUtil.class);

    public IpUtil() {
    }

    public static String getIp(HttpServletRequest request) {
        String ip = null;

        try {
            ip = request.getHeader("x-forwarded-for");
            if (StringUtils.isEmpty(ip) || "unknown".equalsIgnoreCase(ip)) {
                ip = request.getHeader("Proxy-Client-IP");
            }

            if (StringUtils.isEmpty(ip) || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
                ip = request.getHeader("WL-Proxy-Client-IP");
            }

            if (StringUtils.isEmpty(ip) || "unknown".equalsIgnoreCase(ip)) {
                ip = request.getHeader("HTTP_CLIENT_IP");
            }

            if (StringUtils.isEmpty(ip) || "unknown".equalsIgnoreCase(ip)) {
                ip = request.getHeader("HTTP_X_FORWARDED_FOR");
            }

            if (StringUtils.isEmpty(ip) || "unknown".equalsIgnoreCase(ip)) {
                ip = request.getRemoteAddr();
            }
        } catch (Exception var3) {
            log.error("IPUtils ERROR ", var3);
        }

        if (StringUtils.isNotEmpty(ip) && ip.length() > 15 && ip.indexOf(",") > 0) {
            ip = ip.substring(0, ip.indexOf(","));
        }

        return ip;
    }

    public static String getIp(ServerHttpRequest request) {
        Map<String, String> headers = request.getHeaders().toSingleValueMap();
        String ip = (String)headers.get("x-forwarded-for");
        log.debug("get ip from x-forwarded-for {}", ip);
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = (String)headers.get("X-Forwarded-For");
            log.debug("get ip from X-Forwarded-For {}", ip);
        }

        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = (String)headers.get("Proxy-Client-IP");
            log.debug("get ip from Proxy-Client-IP {}", ip);
        }

        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = (String)headers.get("WL-Proxy-Client-IP");
            log.debug("get ip from WL-Proxy-Client-IP {}", ip);
        }

        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getRemoteAddress().getHostString();
            log.debug("get ip from request.getRemoteAddress().getHostString() {}", ip);
            if (ip.equals("127.0.0.1")) {
                InetAddress inet = null;

                try {
                    inet = InetAddress.getLocalHost();
                    log.debug("get ip from InetAddress.getLocalHost() {}", ip);
                } catch (UnknownHostException var5) {
                    log.error(var5.getMessage(), var5);
                }

                ip = inet.getHostAddress();
                log.debug("get ip from inet.getHostAddress() {}", ip);
            }
        }

        if (StringUtils.isNotEmpty(ip) && ip.length() > 15 && ip.indexOf(",") > 0) {
            ip = ip.substring(0, ip.indexOf(","));
        }

        return ip;
    }

    public static boolean internalIp(String ip) {
        byte[] addr = textToNumericFormatV4(ip);
        return internalIp(addr) || "127.0.0.1".equals(ip);
    }

    private static boolean internalIp(byte[] addr) {
        if (!StringUtils.isEmpty(addr) && addr.length >= 2) {
            byte b0 = addr[0];
            byte b1 = addr[1];
            boolean SECTION_1 = true;
            boolean SECTION_2 = true;
            boolean SECTION_3 = true;
            boolean SECTION_4 = true;
            boolean SECTION_5 = true;
            boolean SECTION_6 = true;
            switch (b0) {
                case -84:
                    if (b1 >= 16 && b1 <= 31) {
                        return true;
                    }
                case -64:
                    switch (b1) {
                        case -88:
                            return true;
                    }
                default:
                    return false;
                case 10:
                    return true;
            }
        } else {
            return true;
        }
    }

    /**
     * 获取IP地址
     *
     * @return 本地IP地址
     */
    public static String getHostIp() {
        try {
            return InetAddress.getLocalHost().getHostAddress();
        } catch (UnknownHostException e) {
        }
        return "127.0.0.1";
    }

    /**
     * 获取主机名
     *
     * @return 本地主机名
     */
    public static String getHostName() {
        try {
            return InetAddress.getLocalHost().getHostName();
        } catch (UnknownHostException e) {
        }
        return "未知";
    }

    public static byte[] textToNumericFormatV4(String text) {
        if (text.length() == 0) {
            return null;
        } else {
            byte[] bytes = new byte[4];
            String[] elements = text.split("\\.", -1);

            try {
                long l;
                int i;
                switch (elements.length) {
                    case 1:
                        l = Long.parseLong(elements[0]);
                        if (l < 0L || l > 4294967295L) {
                            return null;
                        }

                        bytes[0] = (byte)((int)(l >> 24 & 255L));
                        bytes[1] = (byte)((int)((l & 16777215L) >> 16 & 255L));
                        bytes[2] = (byte)((int)((l & 65535L) >> 8 & 255L));
                        bytes[3] = (byte)((int)(l & 255L));
                        break;
                    case 2:
                        l = (long)Integer.parseInt(elements[0]);
                        if (l >= 0L && l <= 255L) {
                            bytes[0] = (byte)((int)(l & 255L));
                            l = (long)Integer.parseInt(elements[1]);
                            if (l < 0L || l > 16777215L) {
                                return null;
                            }

                            bytes[1] = (byte)((int)(l >> 16 & 255L));
                            bytes[2] = (byte)((int)((l & 65535L) >> 8 & 255L));
                            bytes[3] = (byte)((int)(l & 255L));
                            break;
                        }

                        return null;
                    case 3:
                        for(i = 0; i < 2; ++i) {
                            l = (long)Integer.parseInt(elements[i]);
                            if (l < 0L || l > 255L) {
                                return null;
                            }

                            bytes[i] = (byte)((int)(l & 255L));
                        }

                        l = (long)Integer.parseInt(elements[2]);
                        if (l < 0L || l > 65535L) {
                            return null;
                        }

                        bytes[2] = (byte)((int)(l >> 8 & 255L));
                        bytes[3] = (byte)((int)(l & 255L));
                        break;
                    case 4:
                        for(i = 0; i < 4; ++i) {
                            l = (long)Integer.parseInt(elements[i]);
                            if (l < 0L || l > 255L) {
                                return null;
                            }

                            bytes[i] = (byte)((int)(l & 255L));
                        }

                        return bytes;
                    default:
                        return null;
                }

                return bytes;
            } catch (NumberFormatException var6) {
                return null;
            }
        }
    }
}

