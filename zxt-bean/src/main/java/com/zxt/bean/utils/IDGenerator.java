package com.zxt.bean.utils;


import com.baomidou.mybatisplus.core.incrementer.IdentifierGenerator;

import java.util.ArrayList;
import java.util.List;

/**
 * id生成策略
 *
 * @author zxt
 */
public class IDGenerator implements IdentifierGenerator {
    TwitterSnowFlake twitterSnowFlake;

    public IDGenerator() {
        this.twitterSnowFlake = new TwitterSnowFlake();
    }

    public IDGenerator(Long idWorker) {
        this.twitterSnowFlake = new TwitterSnowFlake(idWorker);
    }

    public Number nextId(Object entity) {
        return this.twitterSnowFlake.next();
    }

    public Long genId() {
        return this.twitterSnowFlake.next();
    }

    public List<Long> genId(int count) {
        List<Long> pks = new ArrayList(count);

        for (int i = 0; i < count; ++i) {
            pks.add(this.twitterSnowFlake.next());
        }

        return pks;
    }
}
