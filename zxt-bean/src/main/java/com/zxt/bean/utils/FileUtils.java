package com.zxt.bean.utils;

import com.zxt.bean.model.MultipartFileDTO;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.entity.ContentType;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;

@Slf4j
public class FileUtils {


    public static void deleteFile(File file) {
        /*File del = new File(file.toURI());*/
        file.delete();
    }

    public static File multipartFileToFile(MultipartFile multipartFile) {
        try {
            String directoryPath = "./file/images";
            mkdirDirectory(directoryPath);
            InputStream ins = multipartFile.getInputStream();
            String filePath = "./file/images/" + UuIdUtils.randomUUID(Boolean.TRUE) + ".png";
            File file = new File(filePath);
            inputStreamToFile(ins, file);
            return file;
        } catch (Exception e) {
            log.error("MultipartFile转File失败，异常原因:{}", e);
        }
        return null;
    }

    private static void inputStreamToFile(InputStream ins, File file) {
        try {
            OutputStream os = new FileOutputStream(file);
            int bytesRead = 0;
            byte[] buffer = new byte[8192];
            while ((bytesRead = ins.read(buffer, 0, 8192)) != -1) {
                os.write(buffer, 0, bytesRead);
            }
            os.close();
            ins.close();
        } catch (Exception e) {
            log.error("InputStream转File失败，异常原因:{}", e);
        }
    }

    public static MultipartFile fileToMultipartFile(File file) {
        MultipartFile multipartFile = null;
        try {
            FileInputStream fileInputStream = new FileInputStream(file);
            multipartFile = new MultipartFileDTO(file.getName(), file.getName(),
                    ContentType.APPLICATION_OCTET_STREAM.toString(), fileInputStream);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return multipartFile;
    }

    /**
     * 添加水印
     * 参考：https://mp.weixin.qq.com/s/KSrybz6PCUFgjEXbC3svTw
     *
     * @param image
     * @throws IOException
     */
    public static void addWatermark(Image image) throws IOException {
        String watermarkContent = "z x t";
        //获取图片的宽
        int width = image.getWidth(null);
        //获取图片的高
        int height = image.getHeight(null);
        System.out.println("图片的宽:" + width);
        System.out.println("图片的高:" + height);
        BufferedImage bufImg = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        // 加水印
        //创建画笔
        Graphics2D g = bufImg.createGraphics();
        //srcImg 为上面获取到的原始图片的图片对象
        g.drawImage(image, 0, 0, width, height, null);
        //根据图片的背景设置水印颜色
        g.setColor(new Color(255, 255, 255, 128));
        //设置字体  画笔字体样式为微软雅黑，加粗，文字大小为60pt
        /**
         * Font.PLAIN（普通）
         * Font.BOLD（加粗）
         * Font.ITALIC（斜体）
         * Font.BOLD+Font.ITALIC（粗斜体）
         */
        g.setFont(new Font("微软雅黑", Font.ITALIC, 35));
        //设置水印的坐标
        //int x=200;
        //int y=200;
        int x = (width - getWatermarkLength(watermarkContent, g)) / 2;
        int y = height / 2;
        //画出水印 第一个参数是水印内容，第二个参数是x轴坐标，第三个参数是y轴坐标
        g.drawString(watermarkContent, x, y);
        g.dispose();

        String directoryPath = "./file/qr";
        mkdirDirectory(directoryPath);

        //待存储的地址
        String tarImgPath = "./file/qr/" + UuIdUtils.randomUUID(Boolean.TRUE) + ".png";
        // 输出图片
        FileOutputStream outImgStream = new FileOutputStream(tarImgPath);
        ImageIO.write(bufImg, "png", outImgStream);
        System.out.println("添加水印完成");
        outImgStream.flush();
        outImgStream.close();
    }

    /**
     * 目录不存在就创建
     *
     * @param directoryPath
     */
    public static void mkdirDirectory(String directoryPath) {
        File directory = new File(directoryPath);
        //如果没有该目录进行创建
        directory.mkdirs();
    }

    /**
     * 获取水印文字的长度
     *
     * @param waterMarkContent
     * @param g
     * @return
     */
    private static int getWatermarkLength(String waterMarkContent, Graphics2D g) {
        return g.getFontMetrics(g.getFont()).charsWidth(waterMarkContent.toCharArray(), 0, waterMarkContent.length());
    }
}
