package com.zxt.bean.utils;


import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;
import java.util.Date;
import java.util.Objects;

public class LocalDateUtil {
    public static final String FORMAT_DATE = "yyyy-MM-dd";
    public static final String FORMAT_DATETIME = "yyyy-MM-dd HH:mm:ss";

    public LocalDateUtil() {
    }

    public static LocalDateTime toLocalDateTime(Date date) {
        return LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault());
    }

    public static LocalDateTime toLocalDateTime(Date date, ZoneId zone) {
        return LocalDateTime.ofInstant(date.toInstant(), zone);
    }

    public static LocalDate toLocalDate(Date date) {
        return LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault()).toLocalDate();
    }

    public static LocalDate toLocalDate(Date date, ZoneId zone) {
        return LocalDateTime.ofInstant(date.toInstant(), zone).toLocalDate();
    }

    public static LocalTime toLocalTime(Date date) {
        return LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault()).toLocalTime();
    }

    public static LocalTime toLocalTime(Date date, ZoneId zone) {
        return LocalDateTime.ofInstant(date.toInstant(), zone).toLocalTime();
    }

    public static LocalDateTime toLocalTime(Long timestamp) {
        return Objects.isNull(timestamp) ? null : (new Date(timestamp)).toInstant().atOffset(ZoneOffset.of("+8")).toLocalDateTime();
    }

    public static Long toLocalTime(LocalDateTime dateTime) {
        return Objects.isNull(dateTime) ? null : LocalDateTime.now().toInstant(ZoneOffset.of("+8")).toEpochMilli();
    }

    public static Date toDate(LocalDateTime dateTime) {
        return Date.from(dateTime.atZone(ZoneId.systemDefault()).toInstant());
    }

    public static Date toDate(LocalDateTime dateTime, ZoneId zone) {
        return Date.from(dateTime.atZone(zone).toInstant());
    }

    public static Date toDate(LocalDate date) {
        return Date.from(date.atStartOfDay(ZoneId.systemDefault()).toInstant());
    }

    public static Date toDate(LocalDate date, ZoneId zone) {
        return Date.from(date.atStartOfDay(zone).toInstant());
    }

    public static Date toDate(LocalTime time) {
        LocalDateTime dateTime = LocalDateTime.of(LocalDate.now(), time);
        return Date.from(dateTime.atZone(ZoneId.systemDefault()).toInstant());
    }

    public static Date toDate(LocalTime time, ZoneId zone) {
        LocalDateTime dateTime = LocalDateTime.of(LocalDate.now(), time);
        return Date.from(dateTime.atZone(zone).toInstant());
    }

    public static String format(LocalDate date) {
        return date.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
    }

    public static String format(LocalDate date, String format) {
        return date.format(DateTimeFormatter.ofPattern(format));
    }

    public static String format(LocalDateTime dateTime) {
        return dateTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
    }

    public static String format(LocalDateTime dateTime, String format) {
        return dateTime.format(DateTimeFormatter.ofPattern(format));
    }

    public static LocalDate parse(String dateStr, String format) {
        return LocalDate.parse(dateStr, DateTimeFormatter.ofPattern(format));
    }

    public static LocalDateTime parseTime(String datetimeStr, String format) {
        return LocalDateTime.parse(datetimeStr, DateTimeFormatter.ofPattern(format));
    }
    /**
     * 获取一天最早时间
     *
     * @return
     */
    public static LocalDateTime getEarliestOfDay() {
        return LocalDateTime.of(LocalDate.now(), LocalTime.MIN);
    }

    /**
     * 获取一天最晚时间
     *
     * @return
     */
    public static LocalDateTime getLatestOfDay() {
        return LocalDateTime.of(LocalDate.now(), LocalTime.MAX);
    }

    /**
     * 获取周最早时间
     *
     * @return
     */
    public static LocalDateTime getEarliestOfWeek() {
        return getEarliestOfDay().with(DayOfWeek.MONDAY);
    }

    /**
     * 获取周最晚时间
     *
     * @return
     */
    public static LocalDateTime getLatestOfWeek() {
        return getLatestOfDay().with(DayOfWeek.SUNDAY);
    }

    /**
     * 获取月最早时间
     *
     * @return
     */
    public static LocalDateTime getEarliestOfMonth() {
        return getEarliestOfDay().with(TemporalAdjusters.firstDayOfMonth());
    }

    /**
     * 获取月最晚时间
     *
     * @return
     */
    public static LocalDateTime getLatestOfMonth() {
        return getLatestOfDay().with(TemporalAdjusters.lastDayOfMonth());
    }

    /**
     * 获取年最早时间
     *
     * @return
     */
    public static LocalDateTime getEarliestOfYear() {
        return getEarliestOfDay().with(TemporalAdjusters.firstDayOfYear());
    }


    /**
     * 获取年最晚时间
     *
     * @return
     */
    public static LocalDateTime getLatestOfYear() {
        return getLatestOfDay().with(TemporalAdjusters.lastDayOfYear());
    }

    /**
     * 获取一天最早时间
     *
     * @return
     */
    public static LocalDateTime getEarliestOfDayByDate(LocalDate date) {
        return LocalDateTime.of(date, LocalTime.MIN);
    }

    /**
     * 获取一天最晚时间
     *
     * @return
     */
    public static LocalDateTime getLatestOfDayByData(LocalDate date) {
        return LocalDateTime.of(date, LocalTime.MAX);
    }
}
