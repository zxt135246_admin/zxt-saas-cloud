package com.zxt.bean.utils;

import org.slf4j.MDC;

/**
 * @author zxt
 */
public class LogUtils {

    public static String getTraceId() {
        return MDC.get("traceId");
    }

    public static void putAsyncTraceId(String traceId) {
        MDC.put("traceId", "Async-" + traceId);
    }

    public static void putJobTraceId() {
        MDC.put("traceId", "Job-" + UuIdUtils.randomUUID(Boolean.TRUE));
    }
}
