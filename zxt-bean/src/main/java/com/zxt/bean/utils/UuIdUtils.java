package com.zxt.bean.utils;

import cn.hutool.core.lang.UUID;

public class UuIdUtils {

    /**
     * @param isSimple 是否去除横杠
     * @return
     */
    public static String randomUUID(Boolean isSimple) {
        String uuidStr = UUID.randomUUID().toString();
        if (isSimple) {
            uuidStr = uuidStr.replaceAll("-", "");
        }
        return uuidStr;
    }
}
