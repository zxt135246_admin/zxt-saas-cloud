package com.zxt.bean.utils;


import com.zxt.bean.exeption.ServiceException;
import com.zxt.bean.function.BranchHandle;
import com.zxt.bean.function.ThrowExceptionFunction;
import com.zxt.bean.result.ResultCode;

/**
 * @author zxt
 */
public class TrueUtils {

    /**
     * 如果参数为true抛出异常
     *
     * @param b
     * @return
     **/

    public static ThrowExceptionFunction isTrue(boolean b) {
        ThrowExceptionFunction result = (errorMessage) -> {
            if (b) {
                throw new ServiceException(ResultCode.COMMON_MESSAGE, errorMessage);
            }
        };
        return result;
    }

    /**
     * 参数为true或false时，分别进行不同的操作
     *
     * @param b
     * @return
     **/
    public static BranchHandle isTrueOrFalse(boolean b) {
        return (trueHandle, falseHandle) -> {
            if (b) {
                trueHandle.run();
            } else {
                falseHandle.run();
            }
        };
    }
}
