package com.zxt.bean.utils;


import java.util.List;
import java.util.Objects;
import java.util.Random;

import org.apache.skywalking.apm.toolkit.trace.TraceContext;
import org.springframework.util.AntPathMatcher;

public class StringUtils extends org.apache.commons.lang3.StringUtils {
    public static final String NUMBERS_LETTER = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    public static final String NUMBERS_EXCLUDE = "23456789";
    public static final String EXCLUDE_UPPER = "abcdefghjkmnpqrstuvwxyzABCDEFGHJKMNPQRSTUVWXYZ";
    public static final String NUMBERS = "0123456789";
    public static final String CAPITAL_LETTERS_NUMBERS = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private static Random random = new Random();
    public static final String EMPTY = "";

    public StringUtils() {
    }

    public static boolean isEmpty(Object str) {
        return str == null || "".equals(str);
    }

    public static boolean isNotEmpty(Object str) {
        return !isEmpty(str);
    }

    public static boolean hasLength(String str) {
        return str != null && !str.isEmpty();
    }

    public static boolean hasText(String str) {
        return hasLength(str) && containsText(str);
    }

    private static boolean containsText(CharSequence str) {
        int strLen = str.length();

        for (int i = 0; i < strLen; ++i) {
            if (!Character.isWhitespace(str.charAt(i))) {
                return true;
            }
        }

        return false;
    }

    public static boolean isBlank(final CharSequence cs) {
        int strLen;
        if (cs != null && (strLen = cs.length()) != 0) {
            for (int i = 0; i < strLen; ++i) {
                if (!Character.isWhitespace(cs.charAt(i))) {
                    return false;
                }
            }

            return true;
        } else {
            return true;
        }
    }

    public static boolean isNotBlank(final CharSequence cs) {
        return !isBlank(cs);
    }

    public static String getRandomString(int length) {
        return randomString(length, "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ");
    }

    public static String getRandomString(int length, String source) {
        return isBlank(source) ? "" : randomString(length, source);
    }

    private static String randomString(int length, String source) {
        if (length <= 0) {
            return "";
        } else {
            StringBuffer stringBuffer = new StringBuffer(length);

            for (int i = 0; i < length; ++i) {
                int number = random.nextInt(source.length());
                stringBuffer.append(source.charAt(number));
            }

            return stringBuffer.toString();
        }
    }

    public static String toUpperCaseFirstOne(String s) {
        return Character.isUpperCase(s.charAt(0)) ? s : Character.toUpperCase(s.charAt(0)) + s.substring(1);
    }

    public static int getRandomContinuousNumber(int start, int end) {
        return start >= end ? start : end - (new Random()).nextInt(end - start + 1);
    }

    public static String trim(String str) {
        return str == null ? null : str.trim();
    }

    public static int length(final CharSequence cs) {
        return cs == null ? 0 : cs.length();
    }

    public static String capitalize(final String str) {
        int strLen = length(str);
        if (strLen == 0) {
            return str;
        } else {
            int firstCodepoint = str.codePointAt(0);
            int newCodePoint = Character.toTitleCase(firstCodepoint);
            if (firstCodepoint == newCodePoint) {
                return str;
            } else {
                int[] newCodePoints = new int[strLen];
                int outOffset = 0;
                newCodePoints[outOffset++] = newCodePoint;

                int codepoint;
                for (int inOffset = Character.charCount(firstCodepoint); inOffset < strLen; inOffset += Character.charCount(codepoint)) {
                    codepoint = str.codePointAt(inOffset);
                    newCodePoints[outOffset++] = codepoint;
                }

                return new String(newCodePoints, 0, outOffset);
            }
        }
    }

    public static String getTraceId() {
        try {
            String traceId = TraceContext.traceId();
            return !isBlank(traceId) && !Objects.equals(traceId, "N/A") ? traceId : System.currentTimeMillis() + getRandomString(4, "0123456789");
        } catch (Exception var1) {
            return System.currentTimeMillis() + getRandomString(4, "0123456789");
        }
    }

    public static String trimToEmpty(final String str) {
        return str == null ? "" : str.trim();
    }

    /**
     * 查找指定字符串是否匹配指定字符串列表中的任意一个字符串
     *
     * @param str  指定字符串
     * @param strs 需要检查的字符串数组
     * @return 是否匹配
     */
    public static boolean matches(String str, List<String> strs) {
        if (isEmpty(str) || CollectionsUtil.isEmpty(strs)) {
            return false;
        }
        for (String pattern : strs) {
            if (isMatch(pattern, str)) {
                return true;
            }
        }
        return false;
    }

    public static boolean isMatch(String pattern, String url) {
        AntPathMatcher matcher = new AntPathMatcher();
        return matcher.match(pattern, url);
    }
}

