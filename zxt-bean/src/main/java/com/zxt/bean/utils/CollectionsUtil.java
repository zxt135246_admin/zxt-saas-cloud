package com.zxt.bean.utils;//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CollectionsUtil {
    public CollectionsUtil() {
    }

    public static boolean isEmpty(Collection<?> collection) {
        return collection == null || collection.isEmpty();
    }

    public static boolean isNotEmpty(Collection<?> collection) {
        return !isEmpty(collection);
    }

    public static <T> boolean isEmpty(T[] array) {
        return array == null || array.length == 0;
    }

    public static <T> boolean isNotEmpty(T[] array) {
        return !isEmpty(array);
    }

    public static boolean isEmpty(Map<?, ?> map) {
        return map == null || map.isEmpty();
    }

    public static boolean isNotEmpty(Map<?, ?> map) {
        return !isEmpty(map);
    }

    public static <T> List<T> asList(T... list) {
        return (List) (isEmpty(list) ? new ArrayList() : (List) Stream.of(list).collect(Collectors.toList()));
    }

    public static String toString(Collection<?> collection, CharSequence delimiter) {
        if (isEmpty(collection)) {
            return null;
        } else {
            StringJoiner joiner = new StringJoiner(delimiter);
            Iterator var3 = collection.iterator();

            while (var3.hasNext()) {
                Object object = var3.next();
                joiner.add(String.valueOf(object));
            }

            return joiner.toString();
        }
    }

    public static <T> BigDecimal sum(List<T> cols, String propName, Class<T> clazz) {
        if (isEmpty((Collection) cols)) {
            return null;
        } else {
            BigDecimal result = BigDecimal.ZERO;

            try {
                BigDecimal value;
                for (Iterator var4 = cols.iterator(); var4.hasNext(); result = result.add(value)) {
                    T c = (T) var4.next();
                    Field field = clazz.getDeclaredField(propName);
                    field.setAccessible(true);
                    value = (BigDecimal) field.get(c);
                }

                return result;
            } catch (Exception var8) {
                return null;
            }
        }
    }

    public static <T> List<List<T>> split(Collection<T> collection, int size) {
        final List<List<T>> result = new ArrayList<>();
        if (isEmpty(collection)) {
            return result;
        }

        ArrayList<T> subList = new ArrayList<>(size);
        for (T t : collection) {
            if (subList.size() >= size) {
                result.add(subList);
                subList = new ArrayList<>(size);
            }
            subList.add(t);
        }
        result.add(subList);
        return result;
    }
}
