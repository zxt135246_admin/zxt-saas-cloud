package com.zxt.bean.exeption;


import com.zxt.bean.result.ResultCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

public class ServiceException extends RuntimeException {
    private static final Logger log = LoggerFactory.getLogger(ServiceException.class);
    private static final long serialVersionUID = 1L;
    private static Map<Integer, String> SERVICE_EXCEPTION_MAP = null;
    private static final String SERVICE_EXCEPTION_FILE = "META-INF/exception_def.properties";
    private Integer code;
    private String message;

    public ServiceException(Integer code) {
        this.code = code;
        this.message = this.matchMessage(code);
    }

    public ServiceException(Integer code, String message) {
        this.code = code;
        this.message = message;
        if (this.message == null || "".equals(this.message)) {
            this.message = this.matchMessage(code);
        }

    }

    public ServiceException(Integer code, String message, Object... args) {
        super(message);
        this.code = code;
        if (args != null && args.length != 0) {
            this.message = String.format(message, args);
        } else {
            this.message = message;
        }

    }

    public ServiceException(ResultCode resultCode) {
        this(resultCode.getCode(), resultCode.getDesc());
    }

    public ServiceException(ResultCode resultCode, Object... args) {
        this(resultCode.getCode(), resultCode.getDesc(), args);
    }

    private synchronized void loadExceptionDef() {
        log.info("load exception definition");
        SERVICE_EXCEPTION_MAP = new ConcurrentHashMap();

        try {
            Enumeration<URL> exceptionDefs = ClassLoader.getSystemResources("META-INF/exception_def.properties");

            while(exceptionDefs.hasMoreElements()) {
                URL exceptionDef = (URL)exceptionDefs.nextElement();
                log.info("load exception definition from {}", exceptionDef);
                InputStream in = new BufferedInputStream(exceptionDef.openStream());
                Properties properties = new Properties();
                properties.load(new InputStreamReader(in, StandardCharsets.UTF_8));
                Iterator var5 = properties.stringPropertyNames().iterator();

                while(var5.hasNext()) {
                    String name = (String)var5.next();
                    Integer eCode = null;

                    try {
                        eCode = Integer.valueOf(name);
                    } catch (Exception var9) {
                        log.warn("{} not a int code!", name);
                    }

                    if (eCode != null) {
                        SERVICE_EXCEPTION_MAP.put(eCode, properties.getProperty(name));
                    }
                }
            }
        } catch (IOException var10) {
            log.error("load exception definition error", var10);
        }

        log.info("all service exception {}", SERVICE_EXCEPTION_MAP);
    }

    public String matchMessage(Integer code) {
        if (SERVICE_EXCEPTION_MAP == null) {
            this.loadExceptionDef();
        }

        String msg = (String)SERVICE_EXCEPTION_MAP.get(code);
        if (msg == null || "".equals(msg)) {
            log.warn("undefined exception message! code = {}", code);
            msg = "undefined exception message!";
        }

        return msg;
    }

    public Integer getCode() {
        return this.code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
        if ("".equals(this.message) || this.message == null) {
            this.message = this.matchMessage(this.code);
        }

    }

    public ServiceException() {
    }

    public ServiceException(String message) {
        this.code = ResultCode.COMMON_MESSAGE.getCode();
        this.message = message;
    }

    public ServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    public ServiceException(Throwable cause) {
        super(cause);
    }
}
