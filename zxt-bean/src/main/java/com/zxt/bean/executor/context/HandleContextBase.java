package com.zxt.bean.executor.context;

import lombok.Data;

@Data
public class HandleContextBase<T> {

    private T params;

    /**
     * 校验的id
     */
    private Long checkId;

}
