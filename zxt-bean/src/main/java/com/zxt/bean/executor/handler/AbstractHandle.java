package com.zxt.bean.executor.handler;

import com.zxt.bean.executor.context.HandleContextBase;
import lombok.Getter;

@Getter
public abstract class AbstractHandle<T> {

    private final String identity;

    private AbstractHandle<T> beforeHandle = null;

    /**
     * handler标识
     *
     * @param identity
     */
    public AbstractHandle(String identity) {
        this.identity = identity;
    }

    public AbstractHandle(String identity, AbstractHandle<T> beforeHandle) {
        this.identity = identity;
        this.beforeHandle = beforeHandle;
    }

    /**
     * 运行
     *
     * @param handleContext
     * @return
     * @throws Exception
     */
    public abstract boolean run(HandleContextBase handleContext);


    /**
     * 是否放行通过，返回true直接通过
     *
     * @param handleContext
     * @return
     */
    public abstract boolean accept(HandleContextBase handleContext);

}
