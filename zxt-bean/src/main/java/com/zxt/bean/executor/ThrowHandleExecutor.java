package com.zxt.bean.executor;

import com.zxt.bean.executor.context.HandleContextBase;
import com.zxt.bean.executor.handler.AbstractHandle;
import com.zxt.bean.executor.strategy.AbstractHandleStrategy;
import com.zxt.bean.utils.CollectionsUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.Assert;

import java.util.List;

/**
 * 异常直接抛出的执行方式
 */
@Slf4j
public class ThrowHandleExecutor {
    private AbstractHandleStrategy baseCompanyInsureHandleStrategy;

    private HandleContextBase companyInsureHandleContextBase;

    public ThrowHandleExecutor() {
    }

    public ThrowHandleExecutor(AbstractHandleStrategy baseValidationRuleStrategy) {
        this.baseCompanyInsureHandleStrategy = baseValidationRuleStrategy;
    }

    public ThrowHandleExecutor withHandleStrategy(AbstractHandleStrategy handleStrategy) {
        Assert.notNull(handleStrategy, "the param of 'handleStrategy' can not be null");
        return new ThrowHandleExecutor(handleStrategy);
    }

    public ThrowHandleExecutor applyContext(HandleContextBase companyInsureHandleContextBase) {
        Assert.notNull(companyInsureHandleContextBase, "the param of 'companyInsureHandleContextBase' can not be null");
        this.companyInsureHandleContextBase = companyInsureHandleContextBase;
        return this;
    }

    /**
     * 执行器开始执行
     *
     * @return 上下文
     */
    public HandleContextBase execute() {
        /**
         * 非空判断
         */
        Assert.notNull(baseCompanyInsureHandleStrategy, "未选择策略模式");
        Assert.notNull(companyInsureHandleContextBase, "未初始化上下文");

        /**
         * 获取当前策略的所有Handler
         */
        List<AbstractHandle> handles = baseCompanyInsureHandleStrategy.getHandles();
        if (CollectionsUtil.isEmpty(handles)) {
            log.info("当前策略没有handler可执行，策略标识: {} ", baseCompanyInsureHandleStrategy.getHandleStrategyIdentify());
            //为空直接返回上下文
            return this.companyInsureHandleContextBase;
        }
        for (AbstractHandle handle : handles) {
            executeHandle(handle, companyInsureHandleContextBase);
        }
        return this.companyInsureHandleContextBase;
    }

    /**
     * 默认执行逻辑
     *
     * @param companyInsureHandle            执行的handle
     * @param companyInsureHandleContextBase 上下文内容
     * @return
     */
    private void executeHandle(AbstractHandle companyInsureHandle, HandleContextBase companyInsureHandleContextBase) {
        /**
         * 是否直接通过
         */
        if (!companyInsureHandle.accept(companyInsureHandleContextBase)) {
            log.info("handler被放行，跳过执行，handler标识:{}", companyInsureHandle.getIdentity());
            return;
        }
        log.info("handler 执行中 handler标识:{}", companyInsureHandle.getIdentity());
        /**
         * 当前handler是否有前置handler
         */
        if (companyInsureHandle.getBeforeHandle() != null) {
            /**
             * 执行前置handler
             */
            executeHandle(companyInsureHandle.getBeforeHandle(), companyInsureHandleContextBase);
        }
        companyInsureHandle.run(companyInsureHandleContextBase);
    }
}
