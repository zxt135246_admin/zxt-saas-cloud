package com.zxt.bean.executor.strategy;

import com.google.common.collect.Lists;
import com.zxt.bean.executor.handler.AbstractHandle;
import lombok.Getter;
import org.springframework.util.Assert;

import java.util.List;

@Getter
public abstract class AbstractHandleStrategy {

    private final String handleStrategyIdentify;

    /**
     * 策略对应的所有handle
     */
    private final List<AbstractHandle> handles = Lists.newLinkedList();

    protected AbstractHandleStrategy(String handleStrategyIdentify) {
        this.handleStrategyIdentify = handleStrategyIdentify;
    }

    public AbstractHandleStrategy withHandle(AbstractHandle handle) {
        Assert.notNull(handles, "handle为空");
        handles.add(handle);
        return this;
    }

   /*public AbstractHandleStrategy excludeHandle(AbstractHandle handle) {
        Assert.notNull(handles, "handle为空");
        handles.remove(handle);
        return this;
    }*/

    public AbstractHandleStrategy withHandles(List<AbstractHandle> handleList) {
        Assert.notEmpty(handleList, "handle列表为空");
        this.handles.addAll(handleList);
        return this;
    }
}
