package com.zxt.bean.function;

/**
 * 抛异常接口
 *
 * @author zxt
 */
@FunctionalInterface
public interface ThrowExceptionFunction {

    /**
     * 抛出异常信息
     *
     * @param message
     */
    void throwMessage(String message);
}
