package com.zxt.bean.config;

import org.springframework.context.annotation.ComponentScan;

/**
 * @author zxt
 */
//扫描注入其他bean
@ComponentScan(value = "com.zxt.bean")
public class BeanAutoConfiguration {

}
