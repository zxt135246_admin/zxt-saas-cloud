package com.zxt.bean.constants;

public interface ApiConstant {
    String HEADER_KEY = "Authorization";
    String USER_HEADER_KEY = "Zxt-User";
}
