package com.zxt.bean.constants;

public interface UserConstant {

    /**
     * 令牌前缀
     */
    String TOKEN_PREFIX = "Bearer ";

}
