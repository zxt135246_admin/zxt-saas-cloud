package com.zxt.bean.constants;

public interface MqConstant {

    @Deprecated
    String OUTPUT = "output";
    @Deprecated
    String INPUT = "input";


    String SUBJECT = "subject";
    String CONSUME = "consume";
    String DASH = "-";
}
