package com.zxt.bean.constants;

/**
 * zxt
 */
public interface ServiceNameConstant {
    String ZXT_CONTENT = "zxt-content";
    String ZXT_USER = "zxt-user";
    String ZXT_OPERATION = "zxt-operation";
}
