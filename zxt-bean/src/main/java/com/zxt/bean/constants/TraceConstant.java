package com.zxt.bean.constants;

public interface TraceConstant {
    String TRACE_ID_HEADER = "x-traceId-header";
    String LOG_TRACE_ID = "traceId";
}