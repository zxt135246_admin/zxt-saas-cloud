package com.zxt.bean.query;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

@ApiModel("分页输入参数基类")
public class QueryDTO implements Serializable {
    private static final long serialVersionUID = 1L;
    @ApiModelProperty("第几页")
    private int pageNo = 1;
    @ApiModelProperty("每页多少数据")
    private int pageSize = 10;
    @ApiModelProperty("排序")
    private String orderBy;
    @ApiModelProperty("指定返回哪些字段")
    private String returnFields;

    public QueryDTO() {
    }

    public int getPageNo() {
        return this.pageNo;
    }

    public int getPageSize() {
        return this.pageSize;
    }

    public String getOrderBy() {
        return this.orderBy;
    }

    public String getReturnFields() {
        return this.returnFields;
    }

    public void setPageNo(final int pageNo) {
        this.pageNo = pageNo;
    }

    public void setPageSize(final int pageSize) {
        this.pageSize = pageSize;
    }

    public void setOrderBy(final String orderBy) {
        this.orderBy = orderBy;
    }

    public void setReturnFields(final String returnFields) {
        this.returnFields = returnFields;
    }

    @Override
    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof QueryDTO)) {
            return false;
        } else {
            QueryDTO other = (QueryDTO) o;
            if (!other.canEqual(this)) {
                return false;
            } else if (this.getPageNo() != other.getPageNo()) {
                return false;
            } else if (this.getPageSize() != other.getPageSize()) {
                return false;
            } else {
                label40:
                {
                    Object this$orderBy = this.getOrderBy();
                    Object other$orderBy = other.getOrderBy();
                    if (this$orderBy == null) {
                        if (other$orderBy == null) {
                            break label40;
                        }
                    } else if (this$orderBy.equals(other$orderBy)) {
                        break label40;
                    }

                    return false;
                }

                Object this$returnFields = this.getReturnFields();
                Object other$returnFields = other.getReturnFields();
                if (this$returnFields == null) {
                    if (other$returnFields != null) {
                        return false;
                    }
                } else if (!this$returnFields.equals(other$returnFields)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof QueryDTO;
    }

    @Override
    public int hashCode() {
        Boolean PRIME = true;
        int result = 1;
        result = result * 59 + this.getPageNo();
        result = result * 59 + this.getPageSize();
        Object $orderBy = this.getOrderBy();
        result = result * 59 + ($orderBy == null ? 43 : $orderBy.hashCode());
        Object $returnFields = this.getReturnFields();
        result = result * 59 + ($returnFields == null ? 43 : $returnFields.hashCode());
        return result;
    }

    @Override
    public String toString() {
        return "QueryDTO(pageNo=" + this.getPageNo() + ", pageSize=" + this.getPageSize() + ", orderBy=" + this.getOrderBy() + ", returnFields=" + this.getReturnFields() + ")";
    }
}
