package com.zxt.bean.query;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.List;

@ApiModel("分页输出结果基类")
public class QueryResultVO<E> implements Serializable {
    private static final long serialVersionUID = 8189778548888809110L;
    @ApiModelProperty("第几页")
    private long pageNo;
    private boolean hasNextPage = true;
    @ApiModelProperty("每页多少数据")
    private long pageSize;
    @ApiModelProperty("总页数")
    private long totalPages;
    @ApiModelProperty("总记录数")
    private long totalRecords;
    @ApiModelProperty("返回记录列表")
    private List<E> records;

    public boolean getHasNextPage() {
        if (this.totalRecords > this.pageNo * this.pageSize) {
            this.setHasNextPage(true);
            return true;
        } else {
            this.setHasNextPage(false);
            return false;
        }
    }

    public void setHasNextPage(boolean hasNextPage) {
        this.hasNextPage = hasNextPage;
    }

    public long getPageNo() {
        return this.pageNo;
    }

    public long getPageSize() {
        return this.pageSize;
    }

    public long getTotalPages() {
        return this.totalPages;
    }

    public long getTotalRecords() {
        return this.totalRecords;
    }

    public List<E> getRecords() {
        return this.records;
    }

    public void setPageNo(final long pageNo) {
        this.pageNo = pageNo;
    }

    public void setPageSize(final long pageSize) {
        this.pageSize = pageSize;
    }

    public void setTotalPages(final long totalPages) {
        this.totalPages = totalPages;
    }

    public void setTotalRecords(final long totalRecords) {
        this.totalRecords = totalRecords;
    }

    public void setRecords(final List<E> records) {
        this.records = records;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof QueryResultVO)) {
            return false;
        } else {
            QueryResultVO<?> other = (QueryResultVO) o;
            if (!other.canEqual(this)) {
                return false;
            } else if (this.getPageNo() != other.getPageNo()) {
                return false;
            } else if (this.getHasNextPage() != other.getHasNextPage()) {
                return false;
            } else if (this.getPageSize() != other.getPageSize()) {
                return false;
            } else if (this.getTotalPages() != other.getTotalPages()) {
                return false;
            } else if (this.getTotalRecords() != other.getTotalRecords()) {
                return false;
            } else {
                Object this$records = this.getRecords();
                Object other$records = other.getRecords();
                if (this$records == null) {
                    if (other$records != null) {
                        return false;
                    }
                } else if (!this$records.equals(other$records)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof QueryResultVO;
    }

    public int hashCode() {
        int result = 1;
        long $pageNo = this.getPageNo();
        result = result * 59 + (int) ($pageNo >>> 32 ^ $pageNo);
        result = result * 59 + (this.getHasNextPage() ? 79 : 97);
        long $pageSize = this.getPageSize();
        result = result * 59 + (int) ($pageSize >>> 32 ^ $pageSize);
        long $totalPages = this.getTotalPages();
        result = result * 59 + (int) ($totalPages >>> 32 ^ $totalPages);
        long $totalRecords = this.getTotalRecords();
        result = result * 59 + (int) ($totalRecords >>> 32 ^ $totalRecords);
        Object $records = this.getRecords();
        result = result * 59 + ($records == null ? 43 : $records.hashCode());
        return result;
    }

    public QueryResultVO() {
    }

    public QueryResultVO(final long pageNo, final boolean hasNextPage, final long pageSize, final long totalPages, final long totalRecords, final List<E> records) {
        this.pageNo = pageNo;
        this.hasNextPage = hasNextPage;
        this.pageSize = pageSize;
        this.totalPages = totalPages;
        this.totalRecords = totalRecords;
        this.records = records;
    }

    public String toString() {
        return "QueryResultVO(super=" + super.toString() + ", pageNo=" + this.getPageNo() + ", hasNextPage=" + this.getHasNextPage() + ", pageSize=" + this.getPageSize() + ", totalPages=" + this.getTotalPages() + ", totalRecords=" + this.getTotalRecords() + ", records=" + this.getRecords() + ")";
    }
}
