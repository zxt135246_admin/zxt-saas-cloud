package com.zxt.bean.convertor;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class LongJsonDeserializer extends JsonDeserializer<Long> {
    private static final Logger log = LoggerFactory.getLogger(LongJsonDeserializer.class);

    public LongJsonDeserializer() {
    }

    @Override
    public Long deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        String value = jsonParser.getText();

        try {
            return value == null ? null : Long.parseLong(value);
        } catch (NumberFormatException var5) {
            log.error("解析长整形错误", var5);
            return null;
        }
    }
}
