package com.zxt.bean.convertor;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;

/**
 * @author zxt
 */
public class LongJsonSerializer extends JsonSerializer<Long> {
    public LongJsonSerializer() {
    }

    @Override
    public void serialize(Long value, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException, JsonProcessingException {
        String text = value == null ? null : String.valueOf(value);
        if (text != null) {
            jsonGenerator.writeString(text);
        }

    }
}
