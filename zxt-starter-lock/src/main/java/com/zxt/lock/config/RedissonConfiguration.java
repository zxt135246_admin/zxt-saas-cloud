package com.zxt.lock.config;

import com.alibaba.fastjson.JSON;
import com.zxt.lock.RedissonLock;
import com.zxt.lock.RedissonManager;
import com.zxt.lock.properties.RedissonProperties;
import org.redisson.Redisson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;

/**
 * @author zxt
 * redisson自动配置
 */
@Configuration
@ConditionalOnClass({Redisson.class})
@EnableConfigurationProperties({RedissonProperties.class})
public class RedissonConfiguration {
    private static final Logger log = LoggerFactory.getLogger(RedissonConfiguration.class);

    public RedissonConfiguration() {
    }

    @Bean
    @ConditionalOnMissingBean
    @Order(2)
    public RedissonLock redissonLock(RedissonManager redissonManager) {
        RedissonLock redissonLock = new RedissonLock();
        redissonLock.setRedissonManager(redissonManager);
        log.info("RedissonLock组装完毕");
        return redissonLock;
    }

    @Bean
    @ConditionalOnMissingBean
    @Order(1)
    public RedissonManager redissonManager(RedissonProperties redissonProperties) {
        RedissonManager redissonManager = new RedissonManager(redissonProperties);
        log.info("RedissonManager组装完毕,连接信息={}", JSON.toJSONString(redissonProperties));
        return redissonManager;
    }
}

