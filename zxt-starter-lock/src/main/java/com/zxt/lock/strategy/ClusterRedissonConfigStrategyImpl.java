package com.zxt.lock.strategy;


import com.zxt.bean.utils.StringUtils;
import com.zxt.lock.constant.Constant;
import com.zxt.lock.properties.RedissonProperties;
import org.redisson.config.Config;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * @author 集群模式
 */
public class ClusterRedissonConfigStrategyImpl implements RedissonConfigStrategy {
    private static final Logger log = LoggerFactory.getLogger(ClusterRedissonConfigStrategyImpl.class);

    public ClusterRedissonConfigStrategyImpl() {
    }

    public Config createRedissonConfig(RedissonProperties redissonProperties) {
        Config config = new Config();

        try {
            List<String> nodes = redissonProperties.getCluster().getNodes();
            String password = redissonProperties.getPassword();

            for (int i = 0; i < nodes.size(); ++i) {
                config.useClusterServers().addNodeAddress(new String[]{Constant.REDIS_CONNECTION_PREFIX.getValue() + (String) nodes.get(i)});
                if (StringUtils.isNotBlank(password)) {
                    config.useClusterServers().setPassword(password);
                }
            }

            log.info("初始化[cluster]方式Config,redisAddress:" + nodes);
        } catch (Exception var6) {
            log.error("cluster Redisson init error", var6);
        }

        return config;
    }
}