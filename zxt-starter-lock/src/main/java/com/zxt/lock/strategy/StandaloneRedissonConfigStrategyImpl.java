package com.zxt.lock.strategy;


import com.zxt.bean.utils.StringUtils;
import com.zxt.lock.constant.Constant;
import com.zxt.lock.properties.RedissonProperties;
import org.redisson.config.Config;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 单机模式
 */
public class StandaloneRedissonConfigStrategyImpl implements RedissonConfigStrategy {
    private static final Logger log = LoggerFactory.getLogger(StandaloneRedissonConfigStrategyImpl.class);

    public StandaloneRedissonConfigStrategyImpl() {
    }

    public Config createRedissonConfig(RedissonProperties redissonProperties) {
        Config config = new Config();

        try {
            String address = Constant.REDIS_CONNECTION_PREFIX.getValue() + redissonProperties.getHost() + ":" + redissonProperties.getPort();
            config.useSingleServer().setAddress(address);
            String password = redissonProperties.getPassword();
            if (StringUtils.isNotBlank(password)) {
                config.useSingleServer().setPassword(password);
            }

            log.info("初始化[standalone]方式Config,redisAddress:" + address);
        } catch (Exception var5) {
            log.error("standalone Redisson init error", var5);
        }

        return config;
    }
}
