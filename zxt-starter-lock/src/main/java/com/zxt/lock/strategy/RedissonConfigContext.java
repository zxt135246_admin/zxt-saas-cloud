package com.zxt.lock.strategy;

import com.zxt.lock.properties.RedissonProperties;
import org.redisson.config.Config;

public class RedissonConfigContext {
    private final RedissonConfigStrategy redissonConfigStrategy;

    public Config createRedissonConfig(RedissonProperties redissonProperties) {
        return this.redissonConfigStrategy.createRedissonConfig(redissonProperties);
    }

    public RedissonConfigContext(final RedissonConfigStrategy redissonConfigStrategy) {
        this.redissonConfigStrategy = redissonConfigStrategy;
    }
}
