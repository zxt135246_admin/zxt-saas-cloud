package com.zxt.lock.strategy;

import com.zxt.lock.properties.RedissonProperties;
import org.redisson.config.Config;

/**
 * 策略模式
 */
public interface RedissonConfigStrategy {

    /**
     * 策略模式创建RedissonConfig
     *
     * @param redissonProperties
     * @return
     */
    Config createRedissonConfig(RedissonProperties redissonProperties);
}