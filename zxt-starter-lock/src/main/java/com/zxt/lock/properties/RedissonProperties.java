package com.zxt.lock.properties;

import org.springframework.boot.autoconfigure.data.redis.RedisProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(
        prefix = "spring.redis"
)
public class RedissonProperties {
    private String type;
    private int database = 0;
    private String password;
    private String host;
    private String port;
    private RedisProperties.Cluster cluster;

    public RedissonProperties() {
    }

    public String getType() {
        return this.type;
    }

    public int getDatabase() {
        return this.database;
    }

    public String getPassword() {
        return this.password;
    }

    public String getHost() {
        return this.host;
    }

    public String getPort() {
        return this.port;
    }

    public RedisProperties.Cluster getCluster() {
        return this.cluster;
    }

    public void setType(final String type) {
        this.type = type;
    }

    public void setDatabase(final int database) {
        this.database = database;
    }

    public void setPassword(final String password) {
        this.password = password;
    }

    public void setHost(final String host) {
        this.host = host;
    }

    public void setPort(final String port) {
        this.port = port;
    }

    public void setCluster(final RedisProperties.Cluster cluster) {
        this.cluster = cluster;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof RedissonProperties)) {
            return false;
        } else {
            RedissonProperties other = (RedissonProperties)o;
            if (!other.canEqual(this)) {
                return false;
            } else if (this.getDatabase() != other.getDatabase()) {
                return false;
            } else {
                label73: {
                    Object this$type = this.getType();
                    Object other$type = other.getType();
                    if (this$type == null) {
                        if (other$type == null) {
                            break label73;
                        }
                    } else if (this$type.equals(other$type)) {
                        break label73;
                    }

                    return false;
                }

                Object this$password = this.getPassword();
                Object other$password = other.getPassword();
                if (this$password == null) {
                    if (other$password != null) {
                        return false;
                    }
                } else if (!this$password.equals(other$password)) {
                    return false;
                }

                label59: {
                    Object this$host = this.getHost();
                    Object other$host = other.getHost();
                    if (this$host == null) {
                        if (other$host == null) {
                            break label59;
                        }
                    } else if (this$host.equals(other$host)) {
                        break label59;
                    }

                    return false;
                }

                Object this$port = this.getPort();
                Object other$port = other.getPort();
                if (this$port == null) {
                    if (other$port != null) {
                        return false;
                    }
                } else if (!this$port.equals(other$port)) {
                    return false;
                }

                Object this$cluster = this.getCluster();
                Object other$cluster = other.getCluster();
                if (this$cluster == null) {
                    if (other$cluster != null) {
                        return false;
                    }
                } else if (!this$cluster.equals(other$cluster)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof RedissonProperties;
    }

    public int hashCode() {
        Boolean PRIME = true;
        int result = 1;
        result = result * 59 + this.getDatabase();
        Object $type = this.getType();
        result = result * 59 + ($type == null ? 43 : $type.hashCode());
        Object $password = this.getPassword();
        result = result * 59 + ($password == null ? 43 : $password.hashCode());
        Object $host = this.getHost();
        result = result * 59 + ($host == null ? 43 : $host.hashCode());
        Object $port = this.getPort();
        result = result * 59 + ($port == null ? 43 : $port.hashCode());
        Object $cluster = this.getCluster();
        result = result * 59 + ($cluster == null ? 43 : $cluster.hashCode());
        return result;
    }

    public String toString() {
        return "RedissonProperties(type=" + this.getType() + ", database=" + this.getDatabase() + ", password=" + this.getPassword() + ", host=" + this.getHost() + ", port=" + this.getPort() + ", cluster=" + this.getCluster() + ")";
    }
}
