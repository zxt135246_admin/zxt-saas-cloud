package com.zxt.lock;

import com.google.common.base.Preconditions;
import com.zxt.lock.constant.RedisConnectionType;
import com.zxt.lock.properties.RedissonProperties;
import com.zxt.lock.strategy.ClusterRedissonConfigStrategyImpl;
import com.zxt.lock.strategy.RedissonConfigContext;
import com.zxt.lock.strategy.StandaloneRedissonConfigStrategyImpl;
import org.redisson.Redisson;
import org.redisson.config.Config;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 分布式锁管理
 *
 * @author zxt
 */
public class RedissonManager {
    private static final Logger log = LoggerFactory.getLogger(RedissonManager.class);
    private Config config = new Config();
    private Redisson redisson = null;

    public RedissonManager() {
    }

    public RedissonManager(RedissonProperties redissonProperties) {
        try {
            this.config = RedissonManager.RedissonConfigFactory.getInstance().createConfig(redissonProperties);
            this.redisson = (Redisson) Redisson.create(this.config);
        } catch (Exception var3) {
            log.error("Redisson init error", var3);
            throw new IllegalArgumentException("please input correct configurations,connectionType must in standalone/sentinel/cluster/masterslave");
        }
    }

    public Redisson getRedisson() {
        return this.redisson;
    }

    static class RedissonConfigFactory {
        private static volatile RedissonConfigFactory factory = null;
        private Config config = new Config();

        private RedissonConfigFactory() {
        }

        public static RedissonConfigFactory getInstance() {
            if (factory == null) {
                Class var0 = Object.class;
                synchronized (Object.class) {
                    if (factory == null) {
                        factory = new RedissonConfigFactory();
                    }
                }
            }

            return factory;
        }

        Config createConfig(RedissonProperties redissonProperties) {
            Preconditions.checkNotNull(redissonProperties);
            Preconditions.checkNotNull(redissonProperties.getType(), "redisson.lock.server.password cannot be NULL");
            Preconditions.checkNotNull(redissonProperties.getDatabase(), "redisson.lock.server.database cannot be NULL");
            String connectionType = redissonProperties.getType();
            RedissonConfigContext redissonConfigContext = null;
            if (connectionType.equals(RedisConnectionType.CLUSTER.getType())) {
                Preconditions.checkNotNull(redissonProperties.getCluster().getNodes(), "spring.redis.cluster.nodes cannot be NULL!");
                redissonConfigContext = new RedissonConfigContext(new ClusterRedissonConfigStrategyImpl());
            } else {
                if (!connectionType.equals(RedisConnectionType.STANDALONE.getType())) {
                    throw new IllegalArgumentException("创建Redisson连接Config失败！当前连接方式:" + connectionType);
                }

                Preconditions.checkNotNull(redissonProperties.getHost(), "spring.redis.host cannot be NULL!");
                Preconditions.checkNotNull(redissonProperties.getPort(), "spring.redis.port cannot be NULL!");
                redissonConfigContext = new RedissonConfigContext(new StandaloneRedissonConfigStrategyImpl());
            }

            return redissonConfigContext.createRedissonConfig(redissonProperties);
        }
    }
}
