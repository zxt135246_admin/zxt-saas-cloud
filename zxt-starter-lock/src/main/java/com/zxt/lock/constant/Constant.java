package com.zxt.lock.constant;

public enum Constant {
    REDIS_CONNECTION_PREFIX("redis://", "Redis地址配置前缀");

    private final String value;
    private final String desc;

    public String getValue() {
        return this.value;
    }

    public String getDesc() {
        return this.desc;
    }

    private Constant(final String value, final String desc) {
        this.value = value;
        this.desc = desc;
    }
}

