package com.zxt.lock.constant;

public enum RedisConnectionType {
    CLUSTER("cluster", "集群方式"),
    STANDALONE("standalone", "单机方式");

    private final String type;
    private final String desc;

    public String getType() {
        return this.type;
    }

    public String getDesc() {
        return this.desc;
    }

    private RedisConnectionType(final String type, final String desc) {
        this.type = type;
        this.desc = desc;
    }
}
