package com.zxt.lock.annotation;

import java.lang.annotation.*;

/**
 * 分布式锁注解，可以加在类或方法上，被调用时会通过aop加分布式锁
 *
 * @author zxt
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface DistributedLock {
    /**
     * 锁名称、redis的key
     *
     * @return
     */
    String value() default "distributed-lock-redisson";

    /**
     * 最大等待时间
     *
     * @return
     */
    int waitTime() default 10;

    /**
     * 占锁时间
     *
     * @return
     */
    int leaseTime() default 100;
}
