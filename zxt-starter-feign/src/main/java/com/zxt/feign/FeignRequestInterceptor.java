package com.zxt.feign;

import com.zxt.bean.utils.ServletUtil;
import com.zxt.bean.utils.StringUtils;
import feign.RequestInterceptor;
import feign.RequestTemplate;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;
import java.util.Objects;


/**
 * @author zxt
 * <p>
 * feign接口的调用因为是封装了全新的请求头，会导致原请求头数据丢失
 * 所以我们这个类是为了拦截原请求头，重写feign的请求头保证需要的数据不丢失
 */
@Component
public class FeignRequestInterceptor implements RequestInterceptor {
    public FeignRequestInterceptor() {
    }

    public void apply(RequestTemplate requestTemplate) {
        HttpServletRequest httpServletRequest = ServletUtil.getRequest();
        if (!Objects.isNull(httpServletRequest)) {
            Map<String, String> headers = ServletUtil.getHeaders(httpServletRequest);
            String user = (String) headers.get("Zxt-User");
            if (StringUtils.isNotEmpty(user)) {
                requestTemplate.header("Zxt-User", new String[]{user});
            }
        }

    }
}
