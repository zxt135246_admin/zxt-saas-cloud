package com.zxt.feign.config;

import com.zxt.feign.FeignRequestInterceptor;
import feign.RequestInterceptor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author zxt feign的自动配置
 */
@Configuration
@ConditionalOnWebApplication
@ConditionalOnClass({FeignRequestInterceptor.class})
public class FeignAutoConfiguration {
    public FeignAutoConfiguration() {
    }

    @Bean
    @ConditionalOnMissingBean({FeignRequestInterceptor.class})
    public RequestInterceptor requestInterceptor() {
        return new FeignRequestInterceptor();
    }
}
