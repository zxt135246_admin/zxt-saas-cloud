package com.zxt.cloud.interceptor.config;


import com.zxt.cloud.interceptor.LogHeaderFeignInterceptor;
import com.zxt.cloud.interceptor.properties.LogHeaderInterceptorProperties;
import feign.RequestInterceptor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author zxt
 */
@Configuration
@EnableConfigurationProperties({LogHeaderInterceptorProperties.class})
@ConditionalOnWebApplication
@ConditionalOnClass({LogHeaderFeignInterceptor.class})
@ConditionalOnProperty(
        prefix = "saas.log.trace",
        value = {"enable"},
        matchIfMissing = true
)
public class LogHeaderInterceptorAutoConfiguration {
    public LogHeaderInterceptorAutoConfiguration() {
    }

    @Bean
    @ConditionalOnMissingBean({LogHeaderFeignInterceptor.class})
    public RequestInterceptor currentUserRequestInterceptor() {
        return new LogHeaderFeignInterceptor();
    }
}