package com.zxt.cloud.interceptor.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(
        prefix = "saas.log.trace"
)
public class LogHeaderInterceptorProperties {
    private boolean enable = true;

    public LogHeaderInterceptorProperties() {
    }

    public boolean isEnable() {
        return this.enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }
}
