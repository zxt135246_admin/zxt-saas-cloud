package com.zxt.cloud.interceptor;

import feign.RequestInterceptor;
import feign.RequestTemplate;
import org.slf4j.MDC;

public class LogHeaderFeignInterceptor implements RequestInterceptor {
    public LogHeaderFeignInterceptor() {
    }

    public void apply(RequestTemplate template) {
        template.header("x-traceId-header", new String[]{MDC.get("traceId")});
    }
}
