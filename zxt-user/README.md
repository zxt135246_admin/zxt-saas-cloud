## zxt-user

``` lua
zxt-user
├── zxt-user-api      -- 对外提供接口
└── zxt-user-service  -- 业务逻辑层
```

### 强制要求：
```lua
1、接收参数在api项目下req包中，用DTO类
2、输出对象在api项目下resp包中，用VO类
3、表名、类名使用英文
```

### git规范

## 分支
```lua
master：当前线上分支
dev：发布分支
feature-日期：新功能分支
bug-fix-日期：修复bug分支
```

## 提交
```lua
feat:新功能
fix:修补bug
docs:文档
style:格式
refactor:重构
```
