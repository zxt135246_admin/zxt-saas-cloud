package com.zxt.user.controller;

import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.StrUtil;
import com.zxt.bean.exeption.ServiceException;
import com.zxt.bean.result.Result;
import com.zxt.user.enums.MenuTypeEnum;
import com.zxt.user.model.SysMenu;
import com.zxt.user.service.SysMenuService;
import com.zxt.web.controller.CommonController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
 * 系统菜单
 *
 * @author zxt
 */
@Slf4j
@RestController
@RequestMapping("/sys/menu")
@AllArgsConstructor
@Api(tags = "系统菜单管理接口")
public class SysMenuController extends CommonController {

    private final SysMenuService sysMenuService;


    @GetMapping("/nav")
    @ApiOperation(value = "获取用户所拥有的菜单和权限", notes = "获取用户所拥有的菜单和权限")
    public Result<Map<Object, Object>> nav() {
        List<SysMenu> menuList = sysMenuService.listMenuByUserId(getUser());
        return Result.newSuccess(MapUtil.builder().put("menuList", menuList).put("authorities", getUser().getPerms()).build());
    }

    @GetMapping("/table")
    @ApiOperation(value = "获取全部菜单列表", notes = "获取全部菜单列表")
    public Result<List<SysMenu>> table() {
        List<SysMenu> sysMenuList = sysMenuService.listMenuAndBtn();
        return Result.newSuccess(sysMenuList);
    }

    /**
     * 所有菜单列表(用于新建、修改角色时 获取菜单的信息)
     */
    @GetMapping("/list")
    @ApiOperation(value = "获取用户所拥有的菜单(不包括按钮)", notes = "通过登陆用户的userId获取用户所拥有的菜单和权限")
    public Result<List<SysMenu>> list() {
        List<SysMenu> sysMenuList = sysMenuService.listSimpleMenuNoButton();
        return Result.newSuccess(sysMenuList);
    }


    @PostMapping
    @ApiOperation(value = "保存菜单", notes = "保存菜单")
    public Result<Void> save(@RequestBody SysMenu menu) {
        //数据校验
        verifyForm(menu);
        sysMenuService.save(menu);
        return Result.newSuccess();
    }

    /**
     * 验证参数是否正确
     */
    private void verifyForm(SysMenu menu) {

        if (menu.getType() == MenuTypeEnum.MENU.getValue()) {
            if (StrUtil.isBlank(menu.getUrl())) {
                throw new ServiceException("菜单URL不能为空");
            }
        }
        if (Objects.equals(menu.getMenuId(), menu.getParentId())) {
            throw new ServiceException("自己不能是自己的上级");
        }

        //上级菜单类型
        int parentType = MenuTypeEnum.CATALOG.getValue();
        if (menu.getParentId() != 0) {
            SysMenu parentMenu = sysMenuService.getById(menu.getParentId());
            parentType = parentMenu.getType();
        }

        //目录、菜单
        if (menu.getType() == MenuTypeEnum.CATALOG.getValue() ||
                menu.getType() == MenuTypeEnum.MENU.getValue()) {
            if (parentType != MenuTypeEnum.CATALOG.getValue()) {
                throw new ServiceException("上级菜单只能为目录类型");
            }
            return;
        }

        //按钮
        if (menu.getType() == MenuTypeEnum.BUTTON.getValue()) {
            if (parentType != MenuTypeEnum.MENU.getValue()) {
                throw new ServiceException("上级菜单只能为菜单类型");
            }
        }
    }


}
