package com.zxt.user.controller;

import com.zxt.bean.query.QueryResultVO;
import com.zxt.bean.result.Result;
import com.zxt.user.dto.MallDTO;
import com.zxt.user.dto.MallQueryDTO;
import com.zxt.user.dto.UpdateStatusDTO;
import com.zxt.user.service.MallService;
import com.zxt.user.vo.MallVO;
import com.zxt.web.controller.CommonController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;

@Slf4j
@RequestMapping("mall")
@RestController
@AllArgsConstructor
@Api(tags = "商城管理接口")
public class MallController extends CommonController {
    private MallService mallService;

    @ApiModelProperty(value = "分页查询商城列表", notes = "分页查询商城列表")
    @PostMapping("query")
    public Result<QueryResultVO<MallVO>> query(@RequestBody MallQueryDTO dto) {
        return Result.newSuccess(mallService.queryMall(dto));
    }

    @ApiModelProperty(value = "新增/修改商城", notes = "新增/修改商城")
    @PostMapping("addOrUpdate")
    public Result addOrUpdate(@RequestBody MallDTO dto) {
        if (Objects.isNull(dto.getId())) {
            mallService.addMall(dto);
        } else {
            mallService.updateMall(dto);
        }
        return Result.newSuccess();
    }

    @ApiModelProperty(value = "修改商城状态", notes = "修改商城状态")
    @PostMapping("updateStatus")
    public Result updateStatus(@RequestBody UpdateStatusDTO dto) {
        mallService.updateStatus(dto);
        return Result.newSuccess();
    }

    @ApiModelProperty(value = "获取商城详情", notes = "获取商城详情")
    @GetMapping("get/{id}")
    public Result<MallVO> get(@ApiParam("商城id") @PathVariable Long id) {
        return Result.newSuccess(mallService.getMall(id));
    }

}