package com.zxt.user.controller;

import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.zxt.bean.model.UserInfoVO;
import com.zxt.bean.result.Result;
import com.zxt.bean.utils.CollectionsUtil;
import com.zxt.redis.utils.UserUtil;
import com.zxt.user.dto.LoginBodyDTO;
import com.zxt.user.dto.m.LoginBodyMDTO;
import com.zxt.user.model.SysMenu;
import com.zxt.user.service.SysMenuService;
import com.zxt.user.service.UserService;
import com.zxt.user.vo.LoginBodyVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@RequestMapping("login")
@RestController
@AllArgsConstructor
@Api(tags = "登录管理接口")
public class LoginController {

    private UserService userService;
    private SysMenuService sysMenuService;

    @ApiOperation(value = "登录", notes = "登录")
    @PostMapping("adminLogin")
    public Result<String> list(@RequestBody LoginBodyDTO dto) {
        // 生成令牌
        UserInfoVO userInfoVO = userService.login(dto);
        if (userInfoVO.getIsAdmin()) {
            List<SysMenu> sysMenus = sysMenuService.listMenuAndBtn();
            userInfoVO.setPerms(getAuthoritieList(sysMenus));
        }
        log.info("用户中台登录：{}", JSON.toJSONString(userInfoVO));
        UserUtil.setRedisUser(userInfoVO);
        return Result.newSuccess(userInfoVO.getToken());
    }

    private Set<String> getAuthoritieList(List<SysMenu> menuList) {
        if (CollectionsUtil.isEmpty(menuList)) {
            return new HashSet<>();
        }
        List<String> permsList = menuList.stream().map(SysMenu::getPerms).collect(Collectors.toList());
        Set<String> authoritieList = permsList.stream().flatMap((perms) -> {
                    if (StrUtil.isBlank(perms)) {
                        return null;
                    }
                    return Arrays.stream(perms.trim().split(StrUtil.COMMA));
                }
        ).collect(Collectors.toSet());
        return authoritieList;
    }
}
