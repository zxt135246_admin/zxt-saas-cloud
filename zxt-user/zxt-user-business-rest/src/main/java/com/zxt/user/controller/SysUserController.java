package com.zxt.user.controller;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.zxt.bean.model.UserInfoVO;
import com.zxt.bean.result.Result;
import com.zxt.bean.utils.BeanUtils;
import com.zxt.redis.utils.UserUtil;
import com.zxt.user.service.UserService;
import com.zxt.user.vo.UserVO;
import com.zxt.web.container.UserContainer;
import com.zxt.web.controller.CommonController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

/**
 * @author zxt
 */
@Slf4j
@RequestMapping("sys/user")
@RestController
@AllArgsConstructor
@Api(tags = "用户管理接口")
public class SysUserController extends CommonController {

    private UserService userService;

    @ApiOperation(value = "获取用户信息", notes = "获取用户信息")
    @GetMapping("info")
    public Result<UserInfoVO> get() {
        UserInfoVO user = UserUtil.getUser(null);
        UserVO userVO = userService.getById(user.getId());
        UserInfoVO userInfoVO = BeanUtils.beanCopy(userVO, UserInfoVO.class);
        UserContainer.putUserInfo(userInfoVO);
        return Result.newSuccess(userInfoVO);
    }

}