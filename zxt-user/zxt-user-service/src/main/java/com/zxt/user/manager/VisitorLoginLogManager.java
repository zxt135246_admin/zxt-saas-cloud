package com.zxt.user.manager;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zxt.user.dao.VisitorLoginLogMapper;
import com.zxt.user.model.VisitorLoginLog;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * 游客登录日志管理
 *
 * @author zxt
 */
@Slf4j
@Service
public class VisitorLoginLogManager extends ServiceImpl<VisitorLoginLogMapper, VisitorLoginLog> {

}
