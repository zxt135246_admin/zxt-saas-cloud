package com.zxt.user.service.impl;

import com.zxt.bean.enums.CommonStatusEnum;
import com.zxt.bean.exeption.ServiceException;
import com.zxt.bean.model.UserInfoVO;
import com.zxt.bean.result.ResultCode;
import com.zxt.bean.utils.*;
import com.zxt.content.api.vo.ContentApiVO;
import com.zxt.operation.api.dto.SendEmailApiDTO;
import com.zxt.operation.api.dto.SystemNewsApiDTO;
import com.zxt.operation.api.enums.NewsTypeEnum;
import com.zxt.redis.annotation.CustomCache;
import com.zxt.user.clients.ContentClient;
import com.zxt.user.clients.OperationClient;
import com.zxt.user.dto.RegisterBodyDTO;
import com.zxt.user.dto.LoginBodyDTO;
import com.zxt.user.dto.VisitorLoginDTO;
import com.zxt.user.enums.FriendTypeEnum;
import com.zxt.user.manager.*;
import com.zxt.user.model.*;
import com.zxt.user.service.UserService;
import com.zxt.user.vo.UserContentAssetVO;
import com.zxt.user.vo.UserMallAssetsVO;
import com.zxt.user.vo.UserVO;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author zxt
 */
@Slf4j
@Service
@AllArgsConstructor
public class UserServiceImpl implements UserService {

    private final PasswordEncoderUtil passwordEncoderUtil;
    private final UserManager userManager;
    private final ContentClient contentClient;
    private final OperationClient operationClient;
    private final UserConcernManager userConcernManager;
    private final UserAccountRecordManager userAccountRecordManager;
    private final GradeManager gradeManager;
    private final VisitorLoginLogManager visitorLoginLogManager;
    private final UserIntegralManager userIntegralManager;
    private final UserIntegralDetailManager userIntegralDetailManager;

    @Override
    public UserInfoVO login(LoginBodyDTO dto) {
        User user = userManager.getByUserName(dto.getUsername());
        if (Objects.isNull(user)) {
            // 用户不存在
            throw new ServiceException(ResultCode.LOGIN_ERROR);
        }
        boolean matches = passwordEncoderUtil.matches(dto.getPassword(), user.getUserPwd());
        if (!matches) {
            throw new ServiceException(ResultCode.USER_PWD_ERROR);
        }
        if (CommonStatusEnum.DISABLE.equal(user.getUserStatus())) {
            throw new ServiceException(ResultCode.USER_DISABLE_ERROR);
        }
        return BeanUtils.beanCopy(user, UserInfoVO.class);
    }


    @Override
    public UserInfoVO quickLogin(String openId) {
        User user = userManager.getByWxOpenId(openId);
        if (Objects.isNull(user)) {
            throw new ServiceException(ResultCode.COMMON_MESSAGE, "用户不存在，请重新登录");
        }
        if (CommonStatusEnum.DISABLE.equal(user.getUserStatus())) {
            throw new ServiceException(ResultCode.USER_DISABLE_ERROR);
        }
        return BeanUtils.beanCopy(user, UserInfoVO.class);
    }

    @Override
    public void updateById(User user) {
        userManager.updateById(user);
    }

    @Override
    public UserVO getById(Long id) {
        User user = userManager.getById(id);
        if (Objects.isNull(user)) {
            throw new ServiceException(ResultCode.COMMON_MESSAGE, "用户不存在");
        }
        UserVO userVO = BeanUtils.beanCopy(user, UserVO.class);

        Grade grade = gradeManager.getByValueNum(user.getGrowthValue());

        if (Objects.nonNull(grade)) {
            //设置会员等级描述
            userVO.setLevelDesc(grade.getGradeName());
            //设置当前等级到下一级还需的成长数值
            userVO.setNextGrowthValue(grade.getEndGrowthValue() - userVO.getGrowthValue());
        } else {
            userVO.setLevelDesc("未知");
        }

        return userVO;
    }

    @Override
    public UserContentAssetVO getUserAsset(Long userId) {
        //查询用户作品
        List<ContentApiVO> contentApiVOList = contentClient.getPrivateContentByUserId(userId);
        contentApiVOList = Optional.ofNullable(contentApiVOList).orElse(new ArrayList<>());

        UserContentAssetVO userContentAssetVO = new UserContentAssetVO();
        //设置作品数量
        userContentAssetVO.setOpusNum(contentApiVOList.size());
        //TODO 设置喜欢数量
        userContentAssetVO.setLikeNum(0);
        //设置总热度
        userContentAssetVO.setTotalHeatNum(contentApiVOList.stream().mapToInt(ContentApiVO::getContentHeat).sum());
        List<UserConcern> userConcernList = userConcernManager.getUserAll(userId);
        userContentAssetVO.setFenSiNum(0);
        userContentAssetVO.setConcernNum(0);
        if (CollectionsUtil.isNotEmpty(userConcernList)) {
            //设置关注数量
            Long concernCount = userConcernList.stream().filter((userConcern) -> {
                return userConcern.getUserId().equals(userId);
            }).count();
            userContentAssetVO.setConcernNum(concernCount.intValue());

            //设置粉丝数量
            Long fenSiCount = userConcernList.stream().filter((userConcern) -> {
                return userConcern.getConcernUserId().equals(userId);
            }).count();
            userContentAssetVO.setFenSiNum(fenSiCount.intValue());
        }

        return userContentAssetVO;
    }

    @Override
    @CustomCache
    public List<UserVO> getAllUser() {
        List<User> userList = userManager.list();
        //根据会员成长值排序
        userList = userList.stream().sorted(Comparator.comparing(User::getGrowthValue).reversed()).collect(Collectors.toList());
        return BeanUtils.beanCopy(userList, UserVO.class);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT, rollbackFor = Exception.class)
    public Boolean register(RegisterBodyDTO dto) {
        User existUser = userManager.getByUserNameOrPhoneOrEmailOrNickName(dto.getUserName(), dto.getPhone(), dto.getEmail(), dto.getNickName());
        if (Objects.nonNull(existUser)) {
            TrueUtils.isTrue(existUser.getUserName().equals(dto.getUserName())).throwMessage("用户名已存在");
            TrueUtils.isTrue(existUser.getPhone().equals(dto.getPhone())).throwMessage("手机号已存在");
            TrueUtils.isTrue(existUser.getEmail().equals(dto.getEmail())).throwMessage("邮箱已存在");
            TrueUtils.isTrue(existUser.getNickName().equals(dto.getNickName())).throwMessage("昵称已存在");
        }
        User user = BeanUtils.beanCopy(dto, User.class);
        user.setUserStatus(CommonStatusEnum.ENABLE.getValue());
        user.setGrowthValue(0);
        user.setIsAdmin(Boolean.FALSE);
        user.setUserPwd(passwordEncoderUtil.encode(user.getUserPwd()));
        Boolean isTrue = userManager.save(user);
        TrueUtils.isTrue(!isTrue).throwMessage("注册失败");

        //记录用户平台账号密码
        UserAccountRecord accountRecord = new UserAccountRecord();
        accountRecord.setUserId(user.getId());
        accountRecord.setPlatformName("文案系统");
        accountRecord.setUserName(dto.getUserName());
        accountRecord.setUserPwd(dto.getUserPwd());
        userAccountRecordManager.save(accountRecord);

        SystemNewsApiDTO newsApiDTO = new SystemNewsApiDTO();
        newsApiDTO.setUserId(user.getId());
        newsApiDTO.setNewsType(NewsTypeEnum.SYSTEM.getValue());
        newsApiDTO.setContent("欢迎您加入文案系统~");
        newsApiDTO.setTraceId(LogUtils.getTraceId());
        newsApiDTO.setDataId(user.getId().toString());
        newsApiDTO.setIsRead(Boolean.FALSE);
        operationClient.saveSystemNews(newsApiDTO);

        SendEmailApiDTO sendEmailApiDTO = new SendEmailApiDTO();
        sendEmailApiDTO.setToEmail(dto.getEmail());
        sendEmailApiDTO.setSubject("文案系统账号开通成功~");
        StringBuilder builder = new StringBuilder();
        builder.append("登录链接: http://101.35.246.250/static/page/login.html\n");
        builder.append("登录账号: " + dto.getUserName() + "\n");
        builder.append("登录密码: " + dto.getUserPwd() + "\n");
        builder.append("友情提示!\n\n");
        builder.append("账号密码有误请自认倒霉，感谢合作");
        sendEmailApiDTO.setContent(builder.toString());
        operationClient.sendEmail(sendEmailApiDTO);
        return isTrue;
    }

    @Override
    public List<UserVO> getByIds(List<Long> userIds) {
        if (CollectionsUtil.isEmpty(userIds)) {
            return new ArrayList<>();
        }
        return BeanUtils.beanCopy(userManager.listByIds(userIds), UserVO.class);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT, rollbackFor = Exception.class)
    public void concern(Long userId, Long concernUserId) {
        UserConcern userConcern = userConcernManager.getByUserIdAndConcernUserId(userId, concernUserId);

        if (Objects.nonNull(userConcern)) {
            throw new ServiceException(ResultCode.COMMON_MESSAGE, "已经关注过了哦");
        }

        UserConcern newUserConcern = new UserConcern();
        newUserConcern.setUserId(userId);
        newUserConcern.setConcernUserId(concernUserId);
        userConcernManager.save(newUserConcern);

        User user = userManager.getById(userId);

        SystemNewsApiDTO apiDTO = new SystemNewsApiDTO();
        apiDTO.setUserId(concernUserId);
        apiDTO.setTraceId(LogUtils.getTraceId());
        apiDTO.setIsRead(Boolean.FALSE);
        apiDTO.setDataId(userId.toString());
        apiDTO.setNewsType(NewsTypeEnum.CONCERN.getValue());
        apiDTO.setContent(user.getNickName() + " 关注了你!");
        operationClient.saveSystemNews(apiDTO);
    }

    @Override
    public List<UserVO> getMutualConcernUser(Long userId) {
        List<User> userList = userConcernManager.getMutualConcernUser(userId);
        return BeanUtils.beanCopy(userList, UserVO.class);
    }

    @Override
    public void updateNickName(Long userId, String nickName) {

        TrueUtils.isTrue(StringUtils.isBlank(nickName)).throwMessage("用户昵称不能为空");

        TrueUtils.isTrue(nickName.length() < 2 || nickName.length() > 6).throwMessage("用户昵称必须是2到6位");

        User user = userManager.getByNickName(nickName);
        TrueUtils.isTrue(Objects.nonNull(user)).throwMessage("昵称已经存在");

        userManager.updateNickName(userId, nickName);
    }

    @Override
    public List<UserVO> getFriendList(Byte friendType, Long userId) {

        List<Long> userIds = new ArrayList<>();
        if (FriendTypeEnum.CONCERN.equal(friendType)) {
            //查询关注
            List<UserConcern> userConcernList = userConcernManager.getByUserIdOrConcernUserId(userId, null);
            userIds = userConcernList.stream().map(UserConcern::getConcernUserId).collect(Collectors.toList());
        } else if (FriendTypeEnum.FENSI.equal(friendType)) {
            //查询粉丝
            List<UserConcern> userConcernList = userConcernManager.getByUserIdOrConcernUserId(null, userId);
            userIds = userConcernList.stream().map(UserConcern::getUserId).collect(Collectors.toList());
        } else if (FriendTypeEnum.MUTUAL_CONCERN.equal(friendType)) {
            //查询互关
            List<User> userList = userConcernManager.getMutualConcernUser(userId);
            userIds = userList.stream().map(User::getId).collect(Collectors.toList());
        }

        if (CollectionsUtil.isEmpty(userIds)) {
            return new ArrayList<>();
        }

        return BeanUtils.beanCopy(userManager.listByIds(userIds), UserVO.class);
    }

    @Override
    public void visitorLogin(VisitorLoginDTO visitorLoginDTO) {
        VisitorLoginLog visitorLoginLog = BeanUtils.beanCopy(visitorLoginDTO, VisitorLoginLog.class);
        visitorLoginLogManager.save(visitorLoginLog);
    }

    @Override
    public List<UserMallAssetsVO> getMallAssets(Long userId) {
        List<UserMallAssetsVO> mallAssetList = new ArrayList<>();
        //设置积分余额
        mallAssetList.add(getUserIntegralAssets(userId));
        //设置消费金额
        mallAssetList.add(getSpentAmountAssets(userId));
        return mallAssetList;
    }

    private UserMallAssetsVO getSpentAmountAssets(Long userId) {
        UserMallAssetsVO mallAssetsVO = new UserMallAssetsVO();
        UserIntegralDetail orderReduction = userIntegralDetailManager.getOrderReduction(userId);
        mallAssetsVO.setAssetItemName("消费金额");
        mallAssetsVO.setAssetsNumber(orderReduction.getIntegral());
        return mallAssetsVO;
    }

    private UserMallAssetsVO getUserIntegralAssets(Long userId) {
        UserMallAssetsVO mallAssetsVO = new UserMallAssetsVO();
        UserIntegral userIntegral = userIntegralManager.getByUserId(userId);
        mallAssetsVO.setAssetItemValueId(userIntegral.getId());
        mallAssetsVO.setAssetItemName("账号积分余额");
        mallAssetsVO.setAssetsNumber(userIntegral.getAvailableIntegral());
        return mallAssetsVO;
    }
}
