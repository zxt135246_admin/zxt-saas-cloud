package com.zxt.user.model;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString(callSuper = true)
@TableName("sc_user_integral_detail")
public class UserIntegralDetail {

    public static final LambdaQueryWrapper<UserIntegralDetail> gw() {
        return new LambdaQueryWrapper<>();
    }


    /**
     * id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

	/**
	 * 会员积分id
	 */
	@TableField(value = "user_integral_id")
	private Long userIntegralId;

    /**
     * 会员id
     */
    @TableField(value = "user_id")
    private Long userId;

    /**
     * 积分
     */
    @TableField(value = "integral")
    private BigDecimal integral;

    /**
     * 积分来源 1.增加 2.扣除 3.退还
     */
    @TableField(value = "integral_source")
    private Byte integralSource;

    @TableField(value = "order_code")
    private String orderCode;

    /**
     * 操作原因
     */
    @TableField(value = "operation_reason")
    private String operationReason;

    /**
     * 扣减标识1.增加 2.扣除
     */
    @TableField(value = "deduction_flag")
    private Byte deductionFlag;

    /**
     * 流水号
     */
    @TableField(value = "flow_no")
    private String flowNo;


    /**
     * 创建时间
     */
    @TableField(value = "create_time")
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    @TableField(value = "update_time")
    private LocalDateTime updateTime;

    /**
     * 是否删除
     */
    @TableLogic
    @TableField(value = "is_deleted")
    private Boolean isDeleted;


    /**
     * -------------------------------------------------
     * 上面字段由工具自动生成，请在下面添加扩充字段
     * -------------------------------------------------
     */


}