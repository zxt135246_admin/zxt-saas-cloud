package com.zxt.user.manager;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zxt.user.dao.SysMenuMapper;
import com.zxt.user.model.SysMenu;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;


@Slf4j
@Service
public class SysMenuManager extends ServiceImpl<SysMenuMapper, SysMenu> {

    private SysMenuMapper sysMenuMapper;

    public List<SysMenu> listMenu() {
        LambdaQueryWrapper<SysMenu> wrapper = SysMenu.gw();
        wrapper.ne(SysMenu::getType, 2);
        wrapper.orderByAsc(SysMenu::getOrderNum);
        return list(wrapper);
    }
}
