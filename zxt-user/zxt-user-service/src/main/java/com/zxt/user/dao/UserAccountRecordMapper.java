package com.zxt.user.dao;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zxt.user.model.UserAccountRecord;

/**
 * @author zxt
 */
public interface UserAccountRecordMapper extends BaseMapper<UserAccountRecord> {
}
