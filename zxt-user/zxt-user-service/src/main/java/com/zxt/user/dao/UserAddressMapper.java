package com.zxt.user.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zxt.user.model.UserAddress;

/**
 * 数据持久层-地址信息管理类
 */
public interface UserAddressMapper extends BaseMapper<UserAddress> {


}