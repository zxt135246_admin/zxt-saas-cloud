package com.zxt.user.utils.wxUtils;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Data;

import java.io.Serializable;

/**
 * @author Lym
 * @version 1.0
 * @date 2022/11/8 10:38
 * @description 微信被动回复信息-基础 bean
 */
@Data
public abstract class WxOutMsgBaseBean implements Serializable {

    /**
     * 开发者微信号
     */
    @XStreamAlias("ToUserName")
    protected String toUserName;

    /**
     * 发送方帐号（一个OpenID）
     */
    @XStreamAlias("FromUserName")
    protected String fromUserName;

    /**
     * 消息创建时间 （整型）
     */
    @XStreamAlias("CreateTime")
    protected Long createTime;

    /**
     * 消息类型: MSG_TYPE
     */
    @XStreamAlias("MsgType")
    protected String msgType;

    /**
     * 转化为 XML 字符串
     *
     * @return XML 字符串
     */
    public abstract String toXml();
}
