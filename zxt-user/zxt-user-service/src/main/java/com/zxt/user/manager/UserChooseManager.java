package com.zxt.user.manager;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zxt.user.dao.UserChooseMapper;
import com.zxt.user.model.UserChoose;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class UserChooseManager extends ServiceImpl<UserChooseMapper, UserChoose> {


    public UserChoose getByOpenId(String wxOpenId) {
        LambdaQueryWrapper<UserChoose> wrapper = UserChoose.gw();
        wrapper.eq(UserChoose::getOpenId, wxOpenId);
        return getOne(wrapper);
    }

    public void deleteByOpenId(String wxOpenId) {
        LambdaQueryWrapper<UserChoose> wrapper = UserChoose.gw();
        wrapper.eq(UserChoose::getOpenId, wxOpenId);
        remove(wrapper);
    }
}
