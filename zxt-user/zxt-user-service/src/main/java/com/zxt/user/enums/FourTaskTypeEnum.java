package com.zxt.user.enums;

/**
 * @description 四象限任务类型 1.紧急且重要 2.不紧急且重要 3.紧急且不重要 4,不紧急且不重要
 */
public enum FourTaskTypeEnum {

    URGENT_AND_IMPORTANT         ((byte) 1, "紧急且重要"),
    NOURGENT_AND_IMPORTANT          ((byte) 2, "不紧急且重要"),
    URGENT_AND_NOIMPORTANT          ((byte) 3, "紧急且不重要"),
    NOURGENT_AND_NOIMPORTANT          ((byte) 4, "紧急且不重要"),

    ;

    private byte value;
    private String desc;

    FourTaskTypeEnum(byte value, String desc){
        this.value = value;
        this.desc = desc;
    }

    public byte getValue() {
        return value;
    }

    public String getDesc() {
        return desc;
    }

    public static boolean exists(Byte status) {
        if (status == null) {
            return false;
        }
        byte s = status.byteValue();
        return exists(s);
    }

    public static boolean exists(byte s) {
        for (FourTaskTypeEnum element : FourTaskTypeEnum.values()) {
            if (element.value == s) {
                return true;
            }
        }
        return false;
    }

    public boolean equal(Byte val) {
        return val == null ? false : val.byteValue() == this.value;
    }

    public static String getDescByValue(Byte value) {
        if (value == null) {
            return "";
        }
        for (FourTaskTypeEnum element : FourTaskTypeEnum.values()) {
            if (element.value == value.byteValue()) {
                return element.desc;
            }
        }
        return "";
    }
}
