package com.zxt.user.utils;

import java.util.List;

/**
 * 字符串脱敏
 */
public class StrDesensitizationUtils {

    public static void main(String[] args) {
        //身份证加密前 420315199704043440
        //身份证加密后 420***********344*
        System.out.println(encryptIdCord("420315199704043440"));
    }

    /**
     * 加密前  abcdefg
     * 加密后  a*****g
     *
     * @param str
     * @return
     */
    public static String encryptContent(String str) {
        return str.replaceAll("(?<=\\S{1})\\S(?=\\S{1})", "*");
    }

    /**
     * 前2后3加密
     * 加密前  18512220519
     * 加密后  18******519
     *
     * @param str
     * @return
     */
    public static String encryptTwoThreeStr(String str) {
        if (str.length() > 5) {
            int i = str.length() - 5;
            String xing = "";
            for (int i1 = 0; i1 < i; i1++) {
                xing = xing + "*";
            }
            str = str.replaceFirst(str.substring(2, str.length() - 3), xing);
            return str;
        }
        return encryptContent(str);
    }

    /**
     * 加密前  ab
     * 加密后  a*
     *
     * @param str
     * @return
     */
    public static String encryptShortContent(String str) {
        return str.replaceAll("(?<=\\S{1})\\S", "*");
    }

    public static String encryptName(String str) {
        if (str.length() > 2) {
            return encryptContent(str);
        } else {
            return encryptShortContent(str);
        }
    }

    /**
     * 加密前 18530850965
     * 加密后 185****0965
     *
     * @param str
     * @return
     */
    public static String encryptMobile(String str) {
        return str.replaceAll("(\\d{3})\\d{4}(\\d{4})", "$1****$2");
    }

    /**
     * 加密前 420315199704043440
     * 加密后 420***********344*
     *
     * @param str
     * @return
     */
    public static String encryptIdCord(String str) {
        return str.replaceAll("(\\d{3})\\d{11}(\\d{3})\\d{1}", "$1***********$2*");
    }


    /**
     * 根据原名称生成复制的名称
     * 活动 -> 活动3
     *
     * @param oldName  现在的名称 例:活动
     * @param hasNames 存在的名称 例:活动1,活动2
     * @return 活动n
     */
    public static String getCopyName(String oldName, List<String> hasNames) {
        String newName = null;
        for (int i = 1; i <= 99; i++) {
            newName = oldName + i;
            if (!hasNames.contains(newName)) {
                break;
            }
        }
        return newName;
    }
}
