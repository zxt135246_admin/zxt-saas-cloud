package com.zxt.user.model;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author lanhai
 */
@Data
@TableName("sc_user_time_mail")
public class UserTimeMail implements Serializable {

    public static final LambdaQueryWrapper<UserTimeMail> gw() {
        return new LambdaQueryWrapper<>();
    }

    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;

    @TableField(value = "user_id")
    private Long userId;

    @TableField(value = "send_time")
    private LocalDateTime sendTime;

    @TableField(value = "send_mail")
    private String sendMail;

    @TableField(value = "content_title")
    private String contentTitle;

    @TableField(value = "content")
    private String content;

    @TableField(value = "content_salt")
    private String contentSalt;

    @TableField(value = "is_send_arrive")
    private Boolean isSendArrive;

    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    @TableLogic
    @TableField(value = "is_deleted")
    private Boolean isDeleted;

}