package com.zxt.user.service;

import com.zxt.bean.model.UserInfoVO;
import com.zxt.user.model.SysMenu;

import java.util.List;

/**
 * 菜单管理
 *
 * @author lgh
 */
public interface SysMenuService {
    /**
     * 获取用户菜单列表
     *
     * @param userInfoVO 用户
     * @return 菜单列表
     */
    List<SysMenu> listMenuByUserId(UserInfoVO userInfoVO);

    /**
     * 查询所有菜单和按钮
     *
     * @return
     */
    List<SysMenu> listMenuAndBtn();

    /**
     * 简易版获取
     *
     * @return
     */
    List<SysMenu> listSimpleMenuNoButton();



    SysMenu getById(Long id);

    void save(SysMenu menu);
}
