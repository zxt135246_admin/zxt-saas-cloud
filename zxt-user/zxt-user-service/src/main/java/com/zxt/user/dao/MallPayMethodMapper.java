package com.zxt.user.dao;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zxt.user.model.MallPayMethod;

/**
 * @author zxt
 */
public interface MallPayMethodMapper extends BaseMapper<MallPayMethod> {
}
