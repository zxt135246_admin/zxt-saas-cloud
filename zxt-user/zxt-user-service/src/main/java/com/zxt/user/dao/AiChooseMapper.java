package com.zxt.user.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zxt.user.model.AiChoose;

/**
 * @author zxt
 */
public interface AiChooseMapper extends BaseMapper<AiChoose> {
}
