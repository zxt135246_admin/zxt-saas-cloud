package com.zxt.user.model;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.time.LocalDateTime;

/**
 * 游客登录日志
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString(callSuper = true)
@TableName(value = "sc_visitor_login_log", autoResultMap = true)
public class VisitorLoginLog {
    public static final LambdaQueryWrapper<VisitorLoginLog> gw() {
        return new LambdaQueryWrapper<>();
    }

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 登录ip
     */
    @TableField(value = "login_ip")
    private String loginIp;

    /**
     * 登录token
     */
    @TableField(value = "login_token")
    private String loginToken;

    /**
     * 登录时间
     */
    @TableField(value = "login_date")
    private LocalDateTime loginDate;


    /**
     * 创建时间
     */
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    /**
     * 是否删除
     */
    @TableLogic
    @TableField(value = "is_deleted")
    private Boolean isDeleted;

}
