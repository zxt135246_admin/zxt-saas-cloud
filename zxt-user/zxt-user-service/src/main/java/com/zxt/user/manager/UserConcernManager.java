package com.zxt.user.manager;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zxt.user.dao.UserConcernMapper;
import com.zxt.user.model.User;
import com.zxt.user.model.UserConcern;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Slf4j
@Service
public class UserConcernManager extends ServiceImpl<UserConcernMapper, UserConcern> {

    /**
     * 根据用户id和关注用户id获取
     *
     * @param userId
     * @param concernUserId
     * @return
     */
    public UserConcern getByUserIdAndConcernUserId(Long userId, Long concernUserId) {
        LambdaQueryWrapper<UserConcern> wrapper = UserConcern.gw();
        wrapper.eq(UserConcern::getUserId, userId);
        wrapper.eq(UserConcern::getConcernUserId, concernUserId);
        return getOne(wrapper);
    }

    /**
     * 根据用户id或关注用户id获取列表
     *
     * @param userId
     * @param concernUserId
     * @return
     */
    public List<UserConcern> getByUserIdOrConcernUserId(Long userId, Long concernUserId) {
        LambdaQueryWrapper<UserConcern> wrapper = UserConcern.gw();
        if (Objects.nonNull(userId)) {
            wrapper.eq(UserConcern::getUserId, userId);
        }
        if (Objects.nonNull(concernUserId)) {
            wrapper.eq(UserConcern::getConcernUserId, concernUserId);
        }
        return list(wrapper);
    }

    /**
     * 获取用户关注的所有 和 关注用户的所有
     *
     * @param userId
     * @return
     */
    public List<UserConcern> getUserAll(Long userId) {
        LambdaQueryWrapper<UserConcern> wrapper = UserConcern.gw();
        wrapper.eq(UserConcern::getUserId, userId);
        wrapper.or();
        wrapper.eq(UserConcern::getConcernUserId, userId);
        return list(wrapper);
    }

    /**
     * 获取用户的粉丝列表
     *
     * @param concernUserId
     * @return
     */
    public List<UserConcern> getByConcernUserId(Long concernUserId) {
        LambdaQueryWrapper<UserConcern> wrapper = UserConcern.gw();
        wrapper.eq(UserConcern::getConcernUserId, concernUserId);
        return list(wrapper);
    }

    public List<User> getMutualConcernUser(Long userId) {
        return baseMapper.getMutualConcernUser(userId);
    }
}
