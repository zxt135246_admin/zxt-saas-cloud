package com.zxt.user.dao;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zxt.user.model.PayMethod;

/**
 * @author zxt
 * 支付方式
 */
public interface PayMethodMapper extends BaseMapper<PayMethod> {


}
