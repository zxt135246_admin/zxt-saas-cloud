package com.zxt.user.utils.wxUtils;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author Lym
 * @version 1.0
 * @date 2022/11/8 11:47
 * @description 被动回复微信内容格式：文本消息
 */
@Data
@EqualsAndHashCode(callSuper = true)
@XStreamAlias("xml")
public class WxTextMsgBean extends WxOutMsgBaseBean {

    /**
     * 回复的消息内容（换行：在content中能够换行，微信客户端就支持换行显示）
     */
    @XStreamAlias("Content")
    private String content;

    /**
     * 转化为 XML 字符串
     *
     * @return XML 字符串
     */
    @Override
    public String toXml() {
        return XmlUtil.beanToXml(this);
    }

    /**
     * 初始化
     *
     * @param toUser     接收者，openID
     * @param fromUser   发送者
     * @param createTime 时间戳
     * @param content    文本信息
     * @return {@link WxTextMsgBean}
     */
    public static WxTextMsgBean getInstance(String toUser, String fromUser, Long createTime, String content) {
        WxTextMsgBean msgBean = new WxTextMsgBean();
        msgBean.setToUserName(toUser);
        msgBean.setFromUserName(fromUser);
        msgBean.setMsgType("text");
        msgBean.setCreateTime(createTime);
        msgBean.setContent(content);
        return msgBean;
    }
}
