package com.zxt.user.enums;

/**
 * @Comments: 积分来源 1.增加 2.扣除
 */
public enum IntegralSourceEnum {
    ADD((byte) 1, "增加"),
    REDUCTION((byte) 2, "扣除"),
    ;

    private byte value;
    private String desc;

    IntegralSourceEnum(byte value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public byte getValue() {
        return value;
    }

    public String getDesc() {
        return desc;
    }

    public static boolean exists(Byte status) {
        if (status == null) {
            return false;
        }
        byte s = status.byteValue();
        return exists(s);
    }

    public static boolean exists(byte s) {
        for (IntegralSourceEnum element : IntegralSourceEnum.values()) {
            if (element.value == s) {
                return true;
            }
        }
        return false;
    }

    public boolean equal(Byte val) {
        return val == null ? false : val.byteValue() == this.value;
    }

    public static String getDescByValue(Byte value) {
        if (value == null) {
            return "";
        }
        for (IntegralSourceEnum element : IntegralSourceEnum.values()) {
            if (element.value == value.byteValue()) {
                return element.desc;
            }
        }
        return "";
    }
}
