package com.zxt.user.manager;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zxt.user.dao.GradeMapper;
import com.zxt.user.model.Grade;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class GradeManager extends ServiceImpl<GradeMapper, Grade> {

    /**
     * 根据等级数值获取对应的等级
     *
     * @param growthValue
     * @return
     */
    public Grade getByValueNum(Integer growthValue) {
        LambdaQueryWrapper<Grade> wrapper = Grade.gw();
        wrapper.ge(Grade::getEndGrowthValue, growthValue);
        wrapper.le(Grade::getStartGrowthValue, growthValue);
        return getOne(wrapper);
    }
}
