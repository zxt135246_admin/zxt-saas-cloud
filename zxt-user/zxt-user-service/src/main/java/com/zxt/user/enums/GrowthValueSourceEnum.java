package com.zxt.user.enums;

/**
 * @Comments: 成长值来源 1.签到
 */
public enum GrowthValueSourceEnum {
	CHECK_INS                 ((byte) 1, "签到"),
	PUBLISH_CONTENT                 ((byte) 2, "发布文案"),
	;

	private byte value;
	private String desc;

	GrowthValueSourceEnum(byte value, String desc){
		this.value = value;
		this.desc = desc;
	}

    public byte getValue() {
        return value;
    }

    public String getDesc() {
		return desc;
	}

	public static boolean exists(Byte status) {
        if (status == null) {
            return false;
        }
        byte s = status.byteValue();
        return exists(s);
    }

    public static boolean exists(byte s) {
        for (GrowthValueSourceEnum element : GrowthValueSourceEnum.values()) {
            if (element.value == s) {
                return true;
            }
        }
        return false;
    }

    public boolean equal(Byte val) {
        return val == null ? false : val.byteValue() == this.value;
    }

	public static String getDescByValue(Byte value) {
		if (value == null) {
			return "";
		}
		for (GrowthValueSourceEnum element : GrowthValueSourceEnum.values()) {
			if (element.value == value.byteValue()) {
				return element.desc;
			}
		}
		return "";
	}
}
