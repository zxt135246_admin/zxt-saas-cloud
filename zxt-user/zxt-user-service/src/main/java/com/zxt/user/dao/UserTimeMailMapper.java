package com.zxt.user.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zxt.user.model.UserTimeMail;

/**
 * @author zxt
 */
public interface UserTimeMailMapper extends BaseMapper<UserTimeMail> {
}
