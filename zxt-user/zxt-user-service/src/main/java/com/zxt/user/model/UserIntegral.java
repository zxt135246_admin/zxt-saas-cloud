package com.zxt.user.model;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString(callSuper = true)
@TableName("sc_user_integral")
public class UserIntegral {

    public static final LambdaQueryWrapper<UserIntegral> gw() {
        return new LambdaQueryWrapper<>();
    }


    /**
     * id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;


    /**
     * 会员id
     */
    @TableField(value = "user_id")
    private Long userId;

    /**
     * 总积分
     */
    @TableField(value = "total_integral")
    private BigDecimal totalIntegral;

    /**
     * 可用积分
     */
    @TableField(value = "available_integral")
    private BigDecimal availableIntegral;

    /**
     * 已使用积分
     */
    @TableField(value = "used_integral")
    private BigDecimal usedIntegral;

    /**
     * 创建时间
     */
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;


    /**
     * -------------------------------------------------
     * 上面字段由工具自动生成，请在下面添加扩充字段
     * -------------------------------------------------
     */

}
