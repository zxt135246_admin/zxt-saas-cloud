package com.zxt.user.manager;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zxt.user.dao.UserTimeMailMapper;
import com.zxt.user.model.UserTimeMail;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

/**
 * 游客登录日志管理
 *
 * @author zxt
 */
@Slf4j
@Service
public class UserTimeMailManager extends ServiceImpl<UserTimeMailMapper, UserTimeMail> {


    public List<UserTimeMail> getNeedSendList() {
        LambdaQueryWrapper<UserTimeMail> wrapper = UserTimeMail.gw();
        wrapper.eq(UserTimeMail::getIsSendArrive, Boolean.FALSE);
        wrapper.le(UserTimeMail::getSendTime, LocalDateTime.now());
        return list(wrapper);
    }

    public void updateSendArrive(Long id) {
        LambdaUpdateWrapper<UserTimeMail> wrapper = new LambdaUpdateWrapper<>();
        wrapper.set(UserTimeMail::getIsSendArrive, Boolean.TRUE);
        wrapper.eq(UserTimeMail::getIsSendArrive, Boolean.FALSE);
        wrapper.eq(UserTimeMail::getId, id);
        update(wrapper);
    }


    public List<UserTimeMail> getUserNotSendMail(Long userId) {
        LambdaQueryWrapper<UserTimeMail> wrapper = UserTimeMail.gw();
        wrapper.eq(UserTimeMail::getUserId, userId);
        wrapper.eq(UserTimeMail::getIsSendArrive, Boolean.FALSE);
        return list(wrapper);
    }
}
