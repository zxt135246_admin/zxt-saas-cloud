package com.zxt.user.service;

public interface UserGradeDetailService {

    /**
     * 是否签到过
     *
     * @param userId
     * @return
     */
    Boolean isCheckIns(Long userId);

    /**
     * 每日签到
     *
     * @param userId
     */
    void checkIns(Long userId);

    /**
     * 发布文案增加成长值
     */
    void addContentGrade(Long contentId);
}
