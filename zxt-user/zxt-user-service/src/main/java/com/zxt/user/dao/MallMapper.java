package com.zxt.user.dao;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zxt.user.model.Mall;

/**
 * @author zxt
 */
public interface MallMapper extends BaseMapper<Mall> {
}
