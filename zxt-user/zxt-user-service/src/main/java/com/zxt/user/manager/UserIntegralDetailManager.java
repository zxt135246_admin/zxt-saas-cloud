package com.zxt.user.manager;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zxt.user.dao.UserIntegralDetailMapper;
import com.zxt.user.enums.DeductionFlagEnum;
import com.zxt.user.model.UserIntegralDetail;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class UserIntegralDetailManager extends ServiceImpl<UserIntegralDetailMapper, UserIntegralDetail> {

    /**
     * 查询订单扣减详情
     *
     * @param orderCode
     * @return
     */
    public UserIntegralDetail getByOrderCode(String orderCode) {
        LambdaQueryWrapper<UserIntegralDetail> wrapper = UserIntegralDetail.gw();
        wrapper.eq(UserIntegralDetail::getOrderCode, orderCode);
        wrapper.eq(UserIntegralDetail::getDeductionFlag, DeductionFlagEnum.REDUCTION.getValue());
        return getOne(wrapper);
    }

    public UserIntegralDetail getOrderReduction(Long userId) {
        QueryWrapper<UserIntegralDetail> wrapper = new QueryWrapper<>();
        wrapper.select("sum(integral) integral")
                .lambda()
                .isNotNull(UserIntegralDetail::getOrderCode)
                .eq(UserIntegralDetail::getDeductionFlag, DeductionFlagEnum.REDUCTION.getValue())
                .eq(UserIntegralDetail::getUserId, userId);
        return getOne(wrapper);
    }
}
