package com.zxt.user.constants;

import com.zxt.bean.utils.StringUtils;

import java.util.Objects;

/**
 * @author zxt
 */
public class RedisKeyUser {

    private static final String USER = "user:";


    /**
     * 登录用户 redis key
     *
     * @param token
     * @return
     */
    public static String getUserKey(String token) {
        if (StringUtils.isEmpty(token)) {
            return "";
        }
        StringBuilder sbBuilder = new StringBuilder(80);
        sbBuilder.append(USER);
        sbBuilder.append("token:");
        sbBuilder.append(token);
        return sbBuilder.toString();
    }

    public static String getCodeKey(String key) {
        if (StringUtils.isEmpty(key)) {
            return "";
        }
        StringBuilder sbBuilder = new StringBuilder(80);
        sbBuilder.append(USER);
        sbBuilder.append("captcha:code:");
        sbBuilder.append(key);
        return sbBuilder.toString();
    }

    public static String getLock(String key, Object dataId) {
        if (StringUtils.isEmpty(key)) {
            return "";
        }
        StringBuilder builder = new StringBuilder(80);
        builder.append(USER);
        builder.append("lock:key:");
        builder.append(key);
        if (Objects.nonNull(dataId)) {
            builder.append("dataId:");
            builder.append(dataId);
        }
        return builder.toString();
    }
}
