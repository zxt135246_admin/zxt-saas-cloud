package com.zxt.user.manager;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zxt.bean.utils.StringUtils;
import com.zxt.user.dao.MallMapper;
import com.zxt.user.dto.MallQueryDTO;
import com.zxt.user.model.Mall;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Slf4j
@Service
public class MallManager extends ServiceImpl<MallMapper, Mall> {

    public Page<Mall> queryMall(MallQueryDTO dto) {
        Page<Mall> page = new Page<>();
        page.setCurrent(dto.getPageNo());
        page.setSize(dto.getPageSize());
        LambdaQueryWrapper<Mall> queryWrapper = Mall.gw();
        if (StringUtils.isNotBlank(dto.getMallNameLike())) {
            queryWrapper.like(Mall::getMallName, dto.getMallNameLike());
        }
        if (Objects.nonNull(dto.getCommonStatus())) {
            queryWrapper.eq(Mall::getCommonStatus, dto.getCommonStatus());
        }
        queryWrapper.orderByDesc(Mall::getCreateTime);
        return page(page, queryWrapper);
    }

    /**
     * 修改商城状态
     *
     * @param id
     * @param commonStatus
     */
    public void updateStatus(Long id, Byte commonStatus) {
        LambdaUpdateWrapper<Mall> updateWrapper = new LambdaUpdateWrapper();
        updateWrapper.set(Mall::getCommonStatus, commonStatus);
        updateWrapper.eq(Mall::getId, id);
        update(updateWrapper);
    }
}
