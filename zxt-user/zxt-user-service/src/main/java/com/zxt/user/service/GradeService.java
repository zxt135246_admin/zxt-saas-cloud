package com.zxt.user.service;

import com.zxt.user.vo.GradeVO;

import java.util.List;

public interface GradeService {
    /**
     * 获取等级列表
     *
     * @return
     */
    List<GradeVO> list();

    /**
     * 获取当前成长值所属的等级
     *
     * @param growthValue
     * @return
     */
    GradeVO getGradeByValueNum(Integer growthValue);
}
