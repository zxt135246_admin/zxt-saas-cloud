package com.zxt.user.manager;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zxt.user.dao.UserIntegralMapper;
import com.zxt.user.model.UserIntegral;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Slf4j
@Service
public class UserIntegralManager extends ServiceImpl<UserIntegralMapper, UserIntegral> {


    /**
     * 根据用户id获取
     *
     * @param userId
     * @return
     */
    public UserIntegral getByUserId(Long userId) {
        LambdaQueryWrapper<UserIntegral> wrapper = UserIntegral.gw();
        wrapper.eq(UserIntegral::getUserId, userId);
        return getOne(wrapper);
    }


    public boolean addIntegral(Long id, BigDecimal integral) {
        LambdaUpdateWrapper<UserIntegral> updateWrapper = new LambdaUpdateWrapper<>();
        updateWrapper.setSql("total_integral = total_integral +" + integral)
                .setSql("available_integral = available_integral + " + integral)
                .eq(UserIntegral::getId, id);
        return update(updateWrapper);
    }

    public boolean deductionIntegral(Long id, BigDecimal integral) {
        LambdaUpdateWrapper<UserIntegral> updateWrapper = new LambdaUpdateWrapper<>();
        updateWrapper.setSql("used_integral = used_integral +" + integral)
                .setSql("available_integral = available_integral - " + integral)
                .eq(UserIntegral::getId, id);
        return update(updateWrapper);
    }
}
