package com.zxt.user.model;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.zxt.database.config.VarcharEncrypTypeHandler;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.apache.ibatis.type.JdbcType;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString(callSuper = true)
@TableName(value = "sc_user", autoResultMap = true)
public class User {
    public static final LambdaQueryWrapper<User> gw() {
        return new LambdaQueryWrapper<>();
    }

    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;

    /**
     * 账号
     */
    @TableField(value = "user_name")
    private String userName;

    /**
     * phone
     */
    @TableField(value = "phone", typeHandler = VarcharEncrypTypeHandler.class)
    private String phone;

    @TableField(value = "avatar_url")
    private String avatarUrl;

    /**
     * 会员成长数值
     */
    @TableField(value = "growth_value")
    private Integer growthValue;

    /**
     * 昵称
     */
    @TableField(value = "nick_name")
    private String nickName;

    /**
     * 密码
     */
    @TableField(value = "user_pwd")
    private String userPwd;

    /**
     * 邮箱
     */
    @TableField(value = "email", typeHandler = VarcharEncrypTypeHandler.class)
    private String email;

    @TableField(value = "wx_open_id")
    private String wxOpenId;

    /**
     * 是否超级管理员
     */
    @TableField(value = "is_admin")
    private Boolean isAdmin;

    /**
     * 帐号状态（0新建 1启用 2停用）
     */
    @TableField(value = "user_status")
    private Byte userStatus;

    @TableField(value = "login_ip")
    private String loginIp;

    @TableField(value = "login_date")
    private LocalDateTime loginDate;

    /**
     * 创建时间
     */
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    /**
     * 是否删除
     */
    @TableLogic
    @TableField(value = "is_deleted")
    private Boolean isDeleted;

}
