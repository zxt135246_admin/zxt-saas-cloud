package com.zxt.user.service;

import com.zxt.user.vo.PayMethodVO;

import java.util.List;

public interface PayMethodService {

    /**
     * 获取支付方式
     *
     * @param payMethodId
     * @return
     */
    PayMethodVO getById(Long payMethodId);

    /**
     * 获取启用支付方式列表
     *
     * @return
     */
    List<PayMethodVO> getEnableList();
}
