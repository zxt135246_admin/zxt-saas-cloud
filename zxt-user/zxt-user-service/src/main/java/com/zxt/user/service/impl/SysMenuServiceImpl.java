package com.zxt.user.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.zxt.bean.model.UserInfoVO;
import com.zxt.user.manager.SysMenuManager;
import com.zxt.user.manager.UserManager;
import com.zxt.user.model.*;
import com.zxt.user.service.SysMenuService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service
@AllArgsConstructor
public class SysMenuServiceImpl implements SysMenuService {

    private final SysMenuManager sysMenuManager;

    @Override
    public List<SysMenu> listMenuByUserId(UserInfoVO userInfoVO) {
        List<SysMenu> sysMenuList = new ArrayList<>();
        if (userInfoVO.getIsAdmin()) {
            sysMenuList=  sysMenuManager.listMenu();
        }

        Map<Long, List<SysMenu>> sysMenuLevelMap = sysMenuList.stream()
                .sorted(Comparator.comparing(SysMenu::getOrderNum))
                .collect(Collectors.groupingBy(SysMenu::getParentId));

        // 一级菜单
        List<SysMenu> rootMenu = sysMenuLevelMap.get(0L);
        if (CollectionUtil.isEmpty(rootMenu)) {
            return Collections.emptyList();
        }
        // 二级菜单
        for (SysMenu sysMenu : rootMenu) {
            sysMenu.setList(sysMenuLevelMap.get(sysMenu.getMenuId()));
        }
        return rootMenu;
    }

    @Override
    public List<SysMenu> listMenuAndBtn() {
        return sysMenuManager.list();
    }

    @Override
    public List<SysMenu> listSimpleMenuNoButton() {
        return sysMenuManager.listMenu();
    }

    @Override
    public SysMenu getById(Long id) {
        return sysMenuManager.getById(id);
    }

    @Override
    public void save(SysMenu menu) {
        sysMenuManager.save(menu);
    }
}
