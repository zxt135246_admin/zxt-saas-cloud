package com.zxt.user.enums;

/**
 * @Comments: 选项枚举
 */
public enum OptionsValueEnum {
    GET_CONTENT_TYPE((byte) 1, "获取文案类型列表"),
    GET_SHARE_USER((byte) 2, "获取共享用户列表"),
    ;

    private byte value;
    private String desc;

    OptionsValueEnum(byte value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public byte getValue() {
        return value;
    }

    public String getDesc() {
        return desc;
    }

    public static boolean exists(Byte status) {
        if (status == null) {
            return false;
        }
        byte s = status.byteValue();
        return exists(s);
    }

    public static boolean exists(byte s) {
        for (OptionsValueEnum element : OptionsValueEnum.values()) {
            if (element.value == s) {
                return true;
            }
        }
        return false;
    }

    public boolean equal(Byte val) {
        return val == null ? false : val.byteValue() == this.value;
    }

    public static String getDescByValue(Byte value) {
        if (value == null) {
            return "";
        }
        for (OptionsValueEnum element : OptionsValueEnum.values()) {
            if (element.value == value.byteValue()) {
                return element.desc;
            }
        }
        return "";
    }
}
