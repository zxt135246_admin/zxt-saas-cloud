package com.zxt.user.service.impl;

import com.zxt.user.manager.UserIntegralDetailManager;
import com.zxt.user.service.UserIntegralDetailService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author zxt
 */
@Slf4j
@Service
@AllArgsConstructor
public class UserIntegralDetailImpl implements UserIntegralDetailService {

    private final UserIntegralDetailManager userIntegralDetailManager;

}
