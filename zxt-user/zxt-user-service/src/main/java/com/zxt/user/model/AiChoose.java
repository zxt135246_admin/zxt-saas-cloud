package com.zxt.user.model;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString(callSuper = true)
@TableName(value = "sc_ai_choose", autoResultMap = true)
public class AiChoose {
    public static final LambdaQueryWrapper<AiChoose> gw() {
        return new LambdaQueryWrapper<>();
    }

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @TableField(value = "ai_choose")
    private String aiChoose;

    @TableField(value = "ai_choose_key")
    private String aiChooseKey;

    @TableField(value = "parent_id")
    private Long parentId;

    @TableField(value = "level")
    private Integer level;

    @TableField(value = "is_need_reply")
    private Boolean isNeedReply;

    @TableField(value = "options_enum_value")
    private Byte optionsEnumValue;

    @TableField(value = "sort_num")
    private Integer sortNum;

    /**
     * 创建时间
     */
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    /**
     * 是否删除
     */
    @TableLogic
    @TableField(value = "is_deleted")
    private Boolean isDeleted;

}
