package com.zxt.user.service.impl;

import com.zxt.bean.utils.BeanUtils;
import com.zxt.user.manager.UserConcernManager;
import com.zxt.user.model.UserConcern;
import com.zxt.user.service.UserConcernService;
import com.zxt.user.vo.UserConcernVO;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author zxt
 */
@Slf4j
@Service
@AllArgsConstructor
public class UserConcernImpl implements UserConcernService {

    private final UserConcernManager userConcernManager;

    @Override
    public List<UserConcernVO> getByConcernUserId(Long concernUserId) {
        List<UserConcern> userConcernList = userConcernManager.getByConcernUserId(concernUserId);
        return BeanUtils.beanCopy(userConcernList, UserConcernVO.class);
    }

    @Override
    public UserConcernVO getByUserIdAndConcernUserId(Long userId, Long concernUserId) {
        return BeanUtils.beanCopy(userConcernManager.getByUserIdAndConcernUserId(userId, concernUserId), UserConcernVO.class);
    }
}
