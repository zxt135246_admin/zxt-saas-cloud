package com.zxt.user.service.impl;

import com.zxt.bean.exeption.ServiceException;
import com.zxt.bean.result.ResultCode;
import com.zxt.bean.utils.BeanUtils;
import com.zxt.user.dto.UpdateStatusDTO;
import com.zxt.user.dto.UserFourQuadrantsTaskDTO;
import com.zxt.user.enums.FourTaskStatusEnum;
import com.zxt.user.manager.UserFourQuadrantsTaskManager;
import com.zxt.user.model.UserFourQuadrantsTask;
import com.zxt.user.service.UserFourQuadrantsTaskService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;

/**
 * @author zxt
 */
@Slf4j
@Service
@AllArgsConstructor
public class UserFourQuadrantsTaskImpl implements UserFourQuadrantsTaskService {
    private final UserFourQuadrantsTaskManager userFourQuadrantsTaskManager;

    @Override
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT, rollbackFor = Exception.class)
    public void addTask(UserFourQuadrantsTaskDTO dto) {
        dto.setId(null);
        dto.setTaskStatus(FourTaskStatusEnum.TODO.getValue());
        userFourQuadrantsTaskManager.save(BeanUtils.beanCopy(dto, UserFourQuadrantsTask.class));
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT, rollbackFor = Exception.class)
    public void updateStatus(UpdateStatusDTO dto) {
        if (Objects.isNull(dto.getId())) {
            throw new ServiceException(ResultCode.COMMON_MESSAGE, "请选择修改状态的任务");
        }
        if (Objects.isNull(dto.getCommonStatus())) {
            throw new ServiceException(ResultCode.COMMON_MESSAGE, "请选择修改的状态");
        }
        userFourQuadrantsTaskManager.updateStatus(dto);
    }
}
