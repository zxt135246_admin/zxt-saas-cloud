package com.zxt.user.dto;

import com.zxt.bean.req.AddInterface;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString(callSuper = true)
@ApiModel("用户四象限任务入参")
public class UserFourQuadrantsTaskDTO {

    private Long id;

    private Long userId;

    /**
     * 四象限任务类型 1.紧急且重要 2.不紧急且重要 3.紧急且不重要 4,不紧急且不重要
     */
    @NotNull(message = "请选择任务类型", groups = AddInterface.class)
    private Byte taskType;

    /**
     * 任务状态 1.待办 2.已完成
     */
    private Byte taskStatus;

    /**
     * 任务内容
     */
    @NotBlank(message = "任务内容不能为空", groups = AddInterface.class)
    private String taskContent;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

}
