package com.zxt.user.vo;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.zxt.bean.convertor.LongJsonDeserializer;
import com.zxt.bean.convertor.LongJsonSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString(callSuper = true)
@ApiModel("用户地址信息出参")
public class UserAddressVO implements Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 1L;


    /**
     * id
     */
    @ApiModelProperty("id")
    @JsonSerialize(using = LongJsonSerializer.class)
    @JsonDeserialize(using = LongJsonDeserializer.class)
    private Long id;

    /**
     * 地址类型 1.用户 2.企业
     */
    @ApiModelProperty("地址类型 1.用户 2.商城 3.渠道 4.系统 5.经纬度 6.ip")
    private Byte addressType;

    /**
     * 用户id/企业id
     */
    @ApiModelProperty("用户id/企业id")
    @JsonSerialize(using = LongJsonSerializer.class)
    @JsonDeserialize(using = LongJsonDeserializer.class)
    private Long entityId;

    @ApiModelProperty("人群限制")
    @JsonSerialize(using = LongJsonSerializer.class)
    @JsonDeserialize(using = LongJsonDeserializer.class)
    private Long crowdId;

    /**
     * 收货人
     */
    @ApiModelProperty("收货人")
    private String consignee;

    /**
     * 手机号码
     */
    @ApiModelProperty("手机号码")
    private String mobile;

    /**
     * 省份编码
     */
    @ApiModelProperty("省份编码")
    private String provinceCode;

    /**
     * 省份名称
     */
    @ApiModelProperty("省份名称")
    private String provinceName;

    /**
     * 城市编码
     */
    @ApiModelProperty("城市编码")
    private String cityCode;

    /**
     * 城市名称
     */
    @ApiModelProperty("城市名称")
    private String cityName;

    /**
     * 区域编码
     */
    @ApiModelProperty("区域编码")
    private String regionCode;

    /**
     * 区域名称
     */
    @ApiModelProperty("区域名称")
    private String regionName;

    /**
     * 县级编码
     */
    @ApiModelProperty("县级编码")
    private String countyCode;

    /**
     * 县级名称
     */
    @ApiModelProperty("县级名称")
    private String countyName;

    /**
     * 详细地址
     */
    @ApiModelProperty("详细地址")
    private String detailedAddress;

    /**
     * 是否默认
     */
    @ApiModelProperty("是否默认")
    private Boolean isDefault;

    /**
     * 创建时间
     */
    @ApiModelProperty("创建时间")
    private LocalDateTime createTime;

    /**
     * 创建时间
     */
    @ApiModelProperty("修改时间")
    private LocalDateTime updateTime;


    /**
     * -------------------------------------------------
     * 上面字段由工具自动生成，请在下面添加扩充字段
     * -------------------------------------------------
     */


}