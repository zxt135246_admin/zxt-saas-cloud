package com.zxt.user.dao;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zxt.user.model.Grade;

/**
 * @author zxt
 */
public interface GradeMapper extends BaseMapper<Grade> {
}
