package com.zxt.user.service;

import com.zxt.user.vo.UserConcernVO;

import java.util.List;

public interface UserConcernService {

    /**
     * 获取用户的粉丝列表
     *
     * @param concernUserId
     * @return
     */
    List<UserConcernVO> getByConcernUserId(Long concernUserId);

    /**
     * 根据用户id和关注用户id获取
     *
     * @param userId
     * @param concernUserId
     * @return
     */
    UserConcernVO getByUserIdAndConcernUserId(Long userId, Long concernUserId);
}
