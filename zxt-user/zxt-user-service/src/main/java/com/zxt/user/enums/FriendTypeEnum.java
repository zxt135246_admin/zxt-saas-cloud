package com.zxt.user.enums;

/**
 * @Comments: 朋友类型枚举
 */
public enum FriendTypeEnum {
    CONCERN((byte) 1, "关注"),
    FENSI((byte) 2, "粉丝"),
    MUTUAL_CONCERN((byte) 3, "互关"),
    ;

    private byte value;
    private String desc;

    FriendTypeEnum(byte value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public byte getValue() {
        return value;
    }

    public String getDesc() {
        return desc;
    }

    public static boolean exists(Byte status) {
        if (status == null) {
            return false;
        }
        byte s = status.byteValue();
        return exists(s);
    }

    public static boolean exists(byte s) {
        for (FriendTypeEnum element : FriendTypeEnum.values()) {
            if (element.value == s) {
                return true;
            }
        }
        return false;
    }

    public boolean equal(Byte val) {
        return val == null ? false : val.byteValue() == this.value;
    }

    public static String getDescByValue(Byte value) {
        if (value == null) {
            return "";
        }
        for (FriendTypeEnum element : FriendTypeEnum.values()) {
            if (element.value == value.byteValue()) {
                return element.desc;
            }
        }
        return "";
    }
}
