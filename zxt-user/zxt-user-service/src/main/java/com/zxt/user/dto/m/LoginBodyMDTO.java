package com.zxt.user.dto.m;

import com.alibaba.fastjson.annotation.JSONField;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author zxt
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString(callSuper = true)
@ApiModel("商户端用户登录对象入参")
public class LoginBodyMDTO {

    /**
     * 用户名
     */
    @JSONField(name = "userName")
    private String username;

    /**
     * 用户密码
     */
    @JSONField(name = "passWord")
    private String password;

}
