package com.zxt.user.vo;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.zxt.bean.convertor.LongJsonDeserializer;
import com.zxt.bean.convertor.LongJsonSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString(callSuper = true)
@ApiModel(description = "用户资产出参")
public class UserMallAssetsVO implements Serializable {

    @ApiModelProperty("资产名")
    private String assetItemName;

    @ApiModelProperty("资产id")
    @JsonSerialize(using = LongJsonSerializer.class)
    @JsonDeserialize(using = LongJsonDeserializer.class)
    private Long assetItemValueId;

    @ApiModelProperty("资产类型")
    private Byte assetItemType;

    @ApiModelProperty("资产数量")
    private BigDecimal assetsNumber;
}
