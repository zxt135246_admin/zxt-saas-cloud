package com.zxt.user.service.impl;

import com.zxt.bean.exeption.ServiceException;
import com.zxt.bean.result.ResultCode;
import com.zxt.bean.utils.BeanUtils;
import com.zxt.user.dto.UserAddressDTO;
import com.zxt.user.manager.UserAddressManager;
import com.zxt.user.model.UserAddress;
import com.zxt.user.service.UserAddressService;
import com.zxt.user.vo.UserAddressVO;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;

/**
 * @author zxt
 */
@Slf4j
@Service
@AllArgsConstructor
public class UserAddressImpl implements UserAddressService {

    private final UserAddressManager userAddressManager;

    @Override
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT, rollbackFor = Exception.class)
    public void createUserAddress(UserAddressDTO dto) {
        userAddressManager.createUserAddress(BeanUtils.beanCopy(dto, UserAddress.class));
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT, rollbackFor = Exception.class)
    public void updateUserAddress(UserAddressDTO dto) {
        userAddressManager.updateUserAddress(BeanUtils.beanCopy(dto, UserAddress.class));
    }

    @Override
    public void deleteUserAddress(Long id) {
        userAddressManager.removeById(id);
    }

    @Override
    public UserAddressVO getUserAddress(Long id, Long userId) {
        UserAddress userAddress = userAddressManager.getById(id);
        if(Objects.isNull(userAddress)){
            throw new ServiceException(ResultCode.DATA_NOT_EXIST, "地址");
        }
        if (!Objects.equals(userAddress.getUserId(), userId)) {
            throw new ServiceException(ResultCode.DATA_NOT_EXIST, "地址");
        }
        return BeanUtils.beanCopy(userAddress, UserAddressVO.class);
    }

    @Override
    public List<UserAddressVO> getUserAddress(Long userId) {
        List<UserAddress> userAddressList = userAddressManager.getByUserId(userId);
        return BeanUtils.beanCopy(userAddressList, UserAddressVO.class);
    }
}
