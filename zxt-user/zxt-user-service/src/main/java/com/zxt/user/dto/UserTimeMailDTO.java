package com.zxt.user.dto;

import com.zxt.bean.query.QueryDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author zxt
 */
@Data
@ApiModel("时间邮件入参")
public class UserTimeMailDTO {

    @ApiModelProperty(value = "id", hidden = true)
    private Long id;

    @ApiModelProperty(value = "用户id", hidden = true)
    private Long userId;

    @ApiModelProperty(value = "发送时间")
    private LocalDateTime sendTime;

    @ApiModelProperty(value = "收件邮箱")
    private String sendMail;

    @ApiModelProperty(value = "内容标题")
    private String contentTitle;

    @ApiModelProperty(value = "内容")
    private String content;

    @ApiModelProperty(value = "加密盐值", hidden = true)
    private String contentSalt;
}
