package com.zxt.user.manager;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zxt.user.dao.UserFourQuadrantsTaskMapper;
import com.zxt.user.dto.UpdateStatusDTO;
import com.zxt.user.dto.UserFourQuadrantsTaskDTO;
import com.zxt.user.enums.FourTaskStatusEnum;
import com.zxt.user.model.UserFourQuadrantsTask;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 用户四象限管理
 *
 * @author zxt
 */
@Slf4j
@Service
public class UserFourQuadrantsTaskManager extends ServiceImpl<UserFourQuadrantsTaskMapper, UserFourQuadrantsTask> {

    /**
     * 查询用户四象限任务列表
     *
     * @param userId
     * @return
     */
    public List<UserFourQuadrantsTask> getByUserId(Long userId) {
        LambdaQueryWrapper<UserFourQuadrantsTask> wrapper = UserFourQuadrantsTask.gw();
        wrapper.eq(UserFourQuadrantsTask::getUserId, userId);
        return list(wrapper);
    }

    /**
     * 修改任务状态
     *
     * @param dto
     */
    public void updateStatus(UpdateStatusDTO dto) {
        LambdaUpdateWrapper<UserFourQuadrantsTask> wrapper = new LambdaUpdateWrapper();
        wrapper.eq(UserFourQuadrantsTask::getId, dto.getId());
        wrapper.set(UserFourQuadrantsTask::getTaskStatus, dto.getCommonStatus());
        update(wrapper);
    }
}
