package com.zxt.user.service;


import com.zxt.user.dto.UserAddressDTO;
import com.zxt.user.vo.UserAddressVO;

import java.util.List;

/**
 * 业务逻辑层-地址信息接口类
 */
public interface UserAddressService {

    /**
     * 创建用户地址
     *
     * @param dto
     */
    void createUserAddress(UserAddressDTO dto);

    /**
     * 修改用户地址
     *
     * @param dto
     */
    void updateUserAddress(UserAddressDTO dto);


    /**
     * 删除用户地址
     *
     * @param id
     */
    void deleteUserAddress(Long id);

    /**
     * 获取用户地址信息
     *
     * @param id
     * @param userId
     * @return
     */
    UserAddressVO getUserAddress(Long id, Long userId);

    /**
     * 获取用户地址列表
     *
     * @param userId
     * @return
     */
    List<UserAddressVO> getUserAddress(Long userId);
}