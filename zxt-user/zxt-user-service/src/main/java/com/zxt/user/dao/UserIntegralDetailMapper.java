package com.zxt.user.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zxt.user.model.UserIntegralDetail;

/**
 * @author zxt
 */
public interface UserIntegralDetailMapper extends BaseMapper<UserIntegralDetail> {
}
