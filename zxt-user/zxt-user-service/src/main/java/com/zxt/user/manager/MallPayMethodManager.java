package com.zxt.user.manager;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zxt.user.dao.MallPayMethodMapper;
import com.zxt.user.model.MallPayMethod;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class MallPayMethodManager extends ServiceImpl<MallPayMethodMapper, MallPayMethod> {

    /**
     * 根据商城id删除
     *
     * @param mallId
     */
    public void removeByMallId(Long mallId) {
        LambdaQueryWrapper<MallPayMethod> wrapper = MallPayMethod.gw();
        wrapper.eq(MallPayMethod::getMallId, mallId);
        remove(wrapper);
    }
}
