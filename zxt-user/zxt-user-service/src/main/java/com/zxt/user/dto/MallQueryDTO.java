package com.zxt.user.dto;

import com.zxt.bean.query.QueryDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author zxt
 */
@Data
@ApiModel("商城搜索入参")
public class MallQueryDTO extends QueryDTO {
    @ApiModelProperty("商城名称模糊")
    private String mallNameLike;

    @ApiModelProperty("商城状态")
    private Byte commonStatus;
}
