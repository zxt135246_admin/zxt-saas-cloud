package com.zxt.user.model;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString(callSuper = true)
@TableName("sc_user_address")
public class UserAddress {

    public static final LambdaQueryWrapper<UserAddress> gw() {
        return new LambdaQueryWrapper<>();
    }


    /**
     * id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 用户id
     */
    @TableField(value = "user_id")
    private Long userId;

    /**
     * 收货人
     */
    @TableField(value = "consignee")
    private String consignee;

    /**
     * 手机号码
     */
    @TableField(value = "mobile")
    private String mobile;

    /**
     * 省份编码
     */
    @TableField(value = "province_code")
    private String provinceCode;

    /**
     * 省份名称
     */
    @TableField(value = "province_name")
    private String provinceName;

    /**
     * 城市编码
     */
    @TableField(value = "city_code")
    private String cityCode;

    /**
     * 城市名称
     */
    @TableField(value = "city_name")
    private String cityName;

    /**
     * 区域编码
     */
    @TableField(value = "region_code")
    private String regionCode;

    /**
     * 区域名称
     */
    @TableField(value = "region_name")
    private String regionName;

    /**
     * 县级编码
     */
    @TableField(value = "county_code")
    private String countyCode;

    /**
     * 县级名称
     */
    @TableField(value = "county_name")
    private String countyName;

    /**
     * 详细地址
     */
    @TableField(value = "detailed_address")
    private String detailedAddress;

    /**
     * 是否默认
     */
    @TableField(value = "is_default")
    private Boolean isDefault;

    /**
     * 创建时间
     */
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    /**
     * 是否删除
     */
    @TableLogic
    @TableField(value = "is_deleted")
    private Boolean isDeleted;


    /**
     * -------------------------------------------------
     * 上面字段由工具自动生成，请在下面添加扩充字段
     * -------------------------------------------------
     */


}