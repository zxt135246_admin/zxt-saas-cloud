package com.zxt.user.service.impl;

import com.alibaba.fastjson.JSON;
import com.zxt.bean.utils.CollectionsUtil;
import com.zxt.bean.utils.PasswordEncoderUtil;
import com.zxt.bean.utils.StringUtils;
import com.zxt.content.api.dto.ContentApiDTO;
import com.zxt.content.api.dto.ImagesApiDTO;
import com.zxt.content.api.vo.ContentApiVO;
import com.zxt.content.api.vo.ContentTypeApiVO;
import com.zxt.user.clients.ContentClient;
import com.zxt.user.enums.AiOperateEnum;
import com.zxt.user.enums.OptionsValueEnum;
import com.zxt.user.manager.*;
import com.zxt.user.model.*;
import com.zxt.user.service.AiService;
import com.zxt.user.utils.AesUtils;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Slf4j
@Service
@AllArgsConstructor
public class AiServiceImpl implements AiService {

    private final UserManager userManager;
    private final AiChooseManager aiChooseManager;
    private final UserChooseManager userChooseManager;
    private final PasswordEncoderUtil passwordEncoderUtil;
    private final ContentClient contentClient;
    private final UserAccountRecordManager userAccountRecordManager;
    private final UserFourQuadrantsTaskManager userFourQuadrantsTaskManager;
    private final UserTimeMailManager userTimeMailManager;

    @Override
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT, rollbackFor = Exception.class)
    public String autoReply(String wxOpenId, String content) {
        log.info("开始识别自动回复处理:{}", content);

        if (StringUtils.isEmpty(content) || content.equals("0")) {
            clearUserChoose(wxOpenId);
            return getMenu(null);
        }

        User user = userManager.getByWxOpenId(wxOpenId);

        UserChoose userChoose = userChooseManager.getByOpenId(wxOpenId);


        /**
         * 绑定校验
         */
        if (Objects.isNull(userChoose)) {
            if (!content.equals("1") && Objects.isNull(user)) {
                return getMenu("你得先绑定账号，不然我都不知道你是谁啊！");
            }
        } else {
            if (!userChoose.getChooseId().equals(1L) && Objects.isNull(user)) {
                return getMenu("你得先绑定账号，不然我都不知道你是谁啊！");
            }
        }

        if (Objects.isNull(userChoose)) {
            /**
             * 用户需要选择选项
             */
            return selectOptions(wxOpenId, content, user);
        } else {
            /**
             * 用户需要回答答案
             */
            return answerTheAnswer(wxOpenId, content, userChoose, user);
        }
    }

    /**
     * 选择选项
     *
     * @param wxOpenId 用户标识
     * @param content  做出的选项
     * @param user
     * @return
     */
    private String selectOptions(String wxOpenId, String content, User user) {
        Long chooseId = null;
        try {
            //回答的不是数字
            chooseId = Long.parseLong(content);
        } catch (Exception e) {
            return getMenu("眼不好使吗，选序号 三个字看不到吗？\n能不能行啊？？\n回复序号的数字，ok不？？？");
        }
        AiChoose aiChoose = aiChooseManager.getById(chooseId);
        if (Objects.isNull(aiChoose)) {
            return getMenu("就是说，我给你这个选项了吗？\n你考试A、B、C、D四个答案选个E？？");
        }
        //选项是否需要回答
        if (aiChoose.getIsNeedReply()) {
            //如果用户选的是绑定账号，判断是否绑定过
            if (chooseId.equals(1L) && Objects.nonNull(user)) {
                return getMenu("你是不是忘了你已经绑定过了？你是鱼吗？记忆就七秒？");
            }
            UserChoose newUserChoose = new UserChoose();
            newUserChoose.setOpenId(wxOpenId);
            newUserChoose.setReplyValue(JSON.toJSONString(new HashMap<>()));
            newUserChoose.setChooseId(aiChoose.getId());
            //当前选项正在回答的问题序号
            newUserChoose.setSortNum(1);
            //记录用户需要回答的选项
            userChooseManager.save(newUserChoose);

            //获取第一个问题让用户回答
            AiChoose nextAiChoose = aiChooseManager.getNextChoose(chooseId, 1);
            if (Objects.isNull(nextAiChoose)) {
                //没有配置问题
                return dealResult(chooseId, wxOpenId, user, null);
            }
            return nextAiChoose.getAiChoose();
        } else {
            //继续让用户选择下一级选项
            List<AiChoose> aiChooseList = aiChooseManager.getNextChooseList(chooseId);
            if (CollectionsUtil.isEmpty(aiChooseList)) {
                return getMenu("emmmm，这个功能我忘了\n你去试试别的吧，输入“0”重新选择");
            }
            StringBuilder builder = new StringBuilder();
            builder.append("请输入序号选择：");
            builder.append("\n");
            builder.append("序号\t\t\t选项");
            for (AiChoose nextAiChoose : aiChooseList) {
                builder.append("\n");
                builder.append(nextAiChoose.getId() + "\t\t\t" + nextAiChoose.getAiChoose());
            }
            return builder.toString();
        }
    }

    /**
     * 回答答案
     *
     * @param wxOpenId   用户标识
     * @param content    本地回答内容
     * @param userChoose 用户的选项和历史回答内容
     * @param user       用户信息
     * @return
     */
    private String answerTheAnswer(String wxOpenId, String content, UserChoose userChoose, User user) {
        AiChoose aiChoose = aiChooseManager.getByParentIdAndSortNum(userChoose.getChooseId(), userChoose.getSortNum());
        //问题下级还未拓展新的问题
        if (Objects.isNull(aiChoose)) {
            return getMenu("抱歉，我的记忆错乱了\t你去试试别的吧");
        }

        //获取用户之前的回答记录
        Map<String, String> map = JSON.parseObject(userChoose.getReplyValue(), Map.class);
        //记录用户最新的回答
        map.put(aiChoose.getAiChooseKey(), content);

        //将回答记录落库
        userChoose.setReplyValue(JSON.toJSONString(map));
        userChoose.setSortNum(userChoose.getSortNum() + 1);
        userChooseManager.updateById(userChoose);

        //获取下一个问题
        AiChoose nextAiChoose = aiChooseManager.getNextChoose(userChoose.getChooseId(), userChoose.getSortNum());

        //没有下一个问题，回答结束，开始分析处理结果
        if (Objects.isNull(nextAiChoose)) {
            return dealResult(userChoose.getChooseId(), wxOpenId, user, map);
        }

        if (OptionsValueEnum.GET_CONTENT_TYPE.equal(nextAiChoose.getOptionsEnumValue())) {
            //文案类型
            List<ContentTypeApiVO> contentTypeApiVOList = contentClient.getContentTypeList();
            Map<Long, String> optionMaps = Optional.ofNullable(contentTypeApiVOList).orElse(new ArrayList<>()).stream().collect(Collectors.toMap(ContentTypeApiVO::getId, ContentTypeApiVO::getTypeName));

            return returnOptions("请选择文案类型序号", optionMaps);
        } else if (OptionsValueEnum.GET_SHARE_USER.equal(nextAiChoose.getOptionsEnumValue())) {
            //共享用户
            List<User> list = userManager.list();
            List<User> userList = list.stream().filter((inUser) -> {
                return !inUser.getId().equals(user.getId());
            }).collect(Collectors.toList());
            Map<Long, String> optionMaps = userList.stream().collect(Collectors.toMap(User::getId, User::getNickName));
            return returnOptions("请选择共享的用户序号（输入”00“为不共享）", optionMaps);
        }

        //继续回答
        return nextAiChoose.getAiChoose();
    }

    /**
     * 处理用户的回答
     *
     * @param chooseId 当前回答的选项
     * @param wxOpenId 微信openId
     * @param user     用户信息
     * @param map      历史回答记录
     * @return
     */
    private String dealResult(Long chooseId, String wxOpenId, User user, Map<String, String> map) {

        //清除用户做的选择
        clearUserChoose(wxOpenId);

        AiChoose aiChoose = aiChooseManager.getById(chooseId);
        Byte chooseEnum = Byte.parseByte(aiChoose.getAiChooseKey());
        if (!AiOperateEnum.exists(chooseEnum)) {
            return getMenu("emmmm，这个功能我忘了\n你去试试别的吧");
        }

        if (AiOperateEnum.BIND_COUNT.equal(chooseEnum)) {
            String userName = map.get("userName");
            String passWord = map.get("passWord");
            User existUser = userManager.getByUserName(userName);
            if (Objects.isNull(existUser)) {
                return getMenu("我不记得我有创建这个账号啊，你再想想吧");
            }
            boolean matches = passwordEncoderUtil.matches(passWord, existUser.getUserPwd());
            if (!matches) {
                return getMenu("密码错了，我真服了你这个老六");
            }
            userManager.bindWxOpenIdByUserName(userName, wxOpenId);
            return getMenu("恭喜你绑定失败(骗你的，绑定成功了！)");
        } else if (AiOperateEnum.PUBLISH_CONTENT.equal(chooseEnum)) {
            ContentApiDTO dto = new ContentApiDTO();
            dto.setUserId(user.getId());
            //生成系统消息用到
            dto.setNickName(user.getNickName());
            dto.setContent(map.get("content"));
            dto.setContentSource(map.get("contentSource"));
            try {
                Long contentTypeId = Long.parseLong(map.get("contentTypeId"));
                dto.setContentTypeId(contentTypeId);
            } catch (Exception e) {
                return getMenu("选择的文案类型有问题呢，要输入文案类型的序号");
            }
            dto.setIsPublish(Boolean.TRUE);
            dto.setContentHeat(0);
            Boolean isTrue = contentClient.addContent(dto);
            if (Objects.isNull(isTrue) || !isTrue) {
                return getMenu("上传失败了呢，请确认您输入的是否都正确");
            }
            return getMenu("文案上传成功，真是个不错的文案呢~");
        } else if (AiOperateEnum.GET_PRIVATE_ONE_CONTENT.equal(chooseEnum)) {
            ContentApiVO contentApiVO = contentClient.randGetOneContentByUserId(user.getId());
            if (Objects.isNull(contentApiVO)) {
                return getMenu("你有没有文案，自己心里没数吗？");
            }
            return contentApiVO.getContent();
        } else if (AiOperateEnum.GET_PUBLIC_ONE_CONTENT.equal(chooseEnum)) {
            ContentApiVO contentApiVO = contentClient.randGetOneContentByUserId(null);
            if (Objects.isNull(contentApiVO)) {
                return getMenu("没有文案呢，您可以上传第一条作为0的突破哦~");
            }
            return contentApiVO.getContent();
        } else if (AiOperateEnum.PUBLISH_IMAGE.equal(chooseEnum)) {
            try {
                Long shareUserId = null;
                String shareUserIdStr = map.get("shareUserId");
                if (shareUserIdStr.equals("00")) {
                    shareUserId = 0L;
                } else {
                    shareUserId = Long.parseLong(shareUserIdStr);
                }
                ImagesApiDTO apiDTO = new ImagesApiDTO();
                apiDTO.setUserId(user.getId());
                apiDTO.setImageUrl(map.get("imageUrl"));
                apiDTO.setRemark(map.get("remark"));
                apiDTO.setCityName(map.get("cityName"));
                apiDTO.setShareUserId(shareUserId);
                //生成系统消息需要
                apiDTO.setNickName(user.getNickName());

                return getMenu("暂不支持上传呢");
                /*Boolean isTrue = contentClient.wxUploadImages(apiDTO);
                if (Objects.isNull(isTrue) || !isTrue) {
                    return getMenu("上传失败了呢，请确认您输入的是否都正确");
                }
                return getMenu("照片上传成功，真是个不错的照片呢~");*/
            } catch (Exception e) {
                return getMenu("选择的共享用户有问题呢，要输入共享用户的序号");
            }
        } else if (AiOperateEnum.GET_ACCOUNT_RECORD.equal(chooseEnum)) {
            List<UserAccountRecord> userAccountRecordList = userAccountRecordManager.getByUserId(user.getId());
            StringBuilder builder = new StringBuilder();
            builder.append("保管平台名称\t\t保管账号\t\t保管密码");
            if (CollectionsUtil.isEmpty(userAccountRecordList)) {
                builder.append("\n");
                builder.append("暂无记录\t\t暂无记录\t\t暂无记录");
                return getMenu(builder.toString());
            }
            for (UserAccountRecord record : userAccountRecordList) {
                builder.append("\n");
                builder.append(record.getPlatformName() + "\t\t" + record.getUserName() + "\t\t" + record.getUserPwd());
            }
            return getMenu(builder.toString());
        } else if (AiOperateEnum.ADD_ACCOUNT_RECORD.equal(chooseEnum)) {
            String platformName = map.get("platformName");
            String userName = map.get("userName");
            String userPwd = map.get("userPwd");
            UserAccountRecord userAccountRecord = userAccountRecordManager.getByUserIdAndPlatformNameAndUserName(user.getId(), platformName, userName);

            UserAccountRecord newAccountRecord = new UserAccountRecord();
            if (Objects.nonNull(userAccountRecord)) {
                newAccountRecord.setId(userAccountRecord.getId());
            }
            newAccountRecord.setUserId(user.getId());
            newAccountRecord.setPlatformName(platformName);
            newAccountRecord.setUserName(userName);
            newAccountRecord.setUserPwd(userPwd);
            userAccountRecordManager.saveOrUpdate(newAccountRecord);
            return getMenu("保管成功，交给我啦！我一定会保管好的~");
        } else if (AiOperateEnum.ADD_FOUR_QUADRANTS_TASK.equal(chooseEnum)) {
            String taskContent = map.get("taskContent");
            String taskType = map.get("taskType");

            UserFourQuadrantsTask userFourQuadrantsTask = new UserFourQuadrantsTask();
            userFourQuadrantsTask.setUserId(user.getId());
            userFourQuadrantsTask.setTaskType(Byte.parseByte(taskType));
            userFourQuadrantsTask.setTaskContent(taskContent);
            userFourQuadrantsTaskManager.save(userFourQuadrantsTask);
            return getMenu("任务设置成功~");
        } else if (AiOperateEnum.GET_FOUR_QUADRANTS_LIST.equal(chooseEnum)) {
            List<UserFourQuadrantsTask> userFourQuadrantsTaskList = userFourQuadrantsTaskManager.getByUserId(user.getId());
            if (CollectionsUtil.isEmpty(userFourQuadrantsTaskList)) {
                return getMenu("暂时没有四象限任务呢~");
            } else {
                return getMenu("懒得继续开发了，下次再搞~");
            }
        } else if (AiOperateEnum.GET_NOT_SENT_MAIL.equal(chooseEnum)) {
            List<UserTimeMail> userTimeMailList = userTimeMailManager.getUserNotSendMail(user.getId());
            StringBuilder builder = new StringBuilder();
            builder.append("待发送邮件标题\t\t接收邮箱\t\t发送时间");
            if (CollectionsUtil.isEmpty(userTimeMailList)) {
                builder.append("\n");
                builder.append("暂无记录\t\t暂无记录\t\t暂无记录");
                return getMenu(builder.toString());
            }
            for (UserTimeMail record : userTimeMailList) {
                builder.append("\n");
                builder.append(record.getContentTitle() + "\t\t" + record.getSendMail() + "\t\t" + record.getSendTime());
            }
            return getMenu(builder.toString());
        } else if (AiOperateEnum.ADD_USER_TIME_MAIL.equal(chooseEnum)) {
            UserTimeMail userTimeMail = new UserTimeMail();
            userTimeMail.setUserId(user.getId());
            //生成加密盐值
            userTimeMail.setContentSalt(UUID.randomUUID().toString().replaceAll("-", ""));
            //使用Aes进行加密存储
            userTimeMail.setContent(AesUtils.encryption(map.get("content"), userTimeMail.getContentSalt()));
            userTimeMail.setContentTitle(map.get("contentTitle"));
            userTimeMail.setSendMail(map.get("sendMail"));
            LocalDateTime sendTime = null;
            try {
                DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-M-d H:m:s");
                sendTime = LocalDateTime.parse(map.get("sendTime"),df);
            } catch (Exception e) {
                return getMenu("发送时间格式应该为 2024-01-01 01:01:01");
            }
            userTimeMail.setSendTime(sendTime);
            userTimeMailManager.save(userTimeMail);
            return getMenu("任务设置成功~");
        }

        return getMenu("这个功能点我忘了欸");
    }

    /**
     * 清除用户历史的选择
     *
     * @param wxOpenId
     */
    private void clearUserChoose(String wxOpenId) {
        userChooseManager.deleteByOpenId(wxOpenId);
    }

    private String returnOptions(String headStr, Map<Long, String> optionMaps) {
        StringBuilder builder = new StringBuilder();
        builder.append(headStr);
        builder.append("\n");
        builder.append("序号\t\t\t选项");
        for (Map.Entry<Long, String> entry : optionMaps.entrySet()) {
            builder.append("\n");
            builder.append(entry.getKey() + "\t\t\t" + entry.getValue());
        }
        return builder.toString();
    }


    /**
     * 获取一级菜单
     *
     * @param message
     * @return
     */
    private String getMenu(String message) {
        List<AiChoose> aiChooseList = aiChooseManager.getByLevel(0);
        StringBuilder builder = new StringBuilder();
        if (StringUtils.isNotBlank(message)) {
            builder.append(message);
            builder.append("\n");
            builder.append("\n");
        }
        builder.append("请输入序号选择：");
        builder.append("\n");
        builder.append("序号\t\t\t选项");
        for (AiChoose aiChoose : aiChooseList) {
            builder.append("\n");
            builder.append(aiChoose.getId() + "\t\t\t" + aiChoose.getAiChoose());
        }
        return builder.toString();
    }
}
