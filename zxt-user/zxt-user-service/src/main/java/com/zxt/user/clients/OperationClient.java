package com.zxt.user.clients;

import com.alibaba.fastjson.JSON;
import com.zxt.bean.result.Result;
import com.zxt.operation.api.dto.SendEmailApiDTO;
import com.zxt.operation.api.dto.SystemNewsApiDTO;
import com.zxt.operation.api.feignClient.MessageFeignClient;
import com.zxt.operation.api.feignClient.SystemNewsFeignClient;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@AllArgsConstructor
public class OperationClient {

    private final MessageFeignClient messageFeignClient;
    private final SystemNewsFeignClient systemNewsFeignClient;

    public Boolean sendEmail(SendEmailApiDTO dto) {
        log.info("文案中心-运营中心 发送邮件 入参:{}", JSON.toJSONString(dto));
        try {
            Result<Boolean> result = messageFeignClient.sendEmail(dto);
            log.info("文案中心-运营中心 发送邮件，出参:{}", JSON.toJSONString(result));
            return result.getData();
        } catch (Exception e) {
            log.error("文案中心-运营中心 发送邮件,异常信息:{}", e);
        }
        return null;
    }

    /**
     * 保存系统消息
     *
     * @param apiDTO
     */
    public void saveSystemNews(SystemNewsApiDTO apiDTO) {
        log.info("文案中心-运营中心 保存系统消息 入参:{}", JSON.toJSONString(apiDTO));
        Result result = systemNewsFeignClient.saveSystemNews(apiDTO);
        log.info("文案中心-运营中心 保存系统消息，出参:{}", JSON.toJSONString(result));
    }
}
