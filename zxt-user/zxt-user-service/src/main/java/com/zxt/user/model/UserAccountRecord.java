package com.zxt.user.model;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.zxt.database.config.VarcharEncrypTypeHandler;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString(callSuper = true)
@TableName(value = "sc_user_account_record", autoResultMap = true)
public class UserAccountRecord {
    public static final LambdaQueryWrapper<UserAccountRecord> gw() {
        return new LambdaQueryWrapper<>();
    }

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @TableField(value = "user_id")
    private Long userId;

    /**
     * 平台名称
     */
    @TableField(value = "platform_name")
    private String platformName;

    @TableField(value = "user_name", typeHandler = VarcharEncrypTypeHandler.class)
    private String userName;

    @TableField(value = "user_pwd", typeHandler = VarcharEncrypTypeHandler.class)
    private String userPwd;

    /**
     * 创建时间
     */
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    /**
     * 是否删除
     */
    @TableLogic
    @TableField(value = "is_deleted")
    private Boolean isDeleted;

}
