package com.zxt.user.manager;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zxt.user.dao.UserAccountRecordMapper;
import com.zxt.user.model.UserAccountRecord;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author zxt
 */
@Slf4j
@Service
public class UserAccountRecordManager extends ServiceImpl<UserAccountRecordMapper, UserAccountRecord> {

    /**
     * 根据用户id查询列表
     *
     * @param userId
     * @return
     */
    public List<UserAccountRecord> getByUserId(Long userId) {
        LambdaQueryWrapper<UserAccountRecord> wrapper = UserAccountRecord.gw();
        wrapper.eq(UserAccountRecord::getUserId, userId);
        return list(wrapper);
    }

    /**
     * 根据用户id 平台名称 用户账户获取记录
     *
     * @param userId
     * @param platformName
     * @param userName
     * @return
     */
    public UserAccountRecord getByUserIdAndPlatformNameAndUserName(Long userId, String platformName, String userName) {
        LambdaQueryWrapper<UserAccountRecord> wrapper = UserAccountRecord.gw();
        wrapper.eq(UserAccountRecord::getUserId, userId);
        wrapper.eq(UserAccountRecord::getPlatformName, platformName);
        wrapper.eq(UserAccountRecord::getUserName, userName);
        return getOne(wrapper);
    }
}
