package com.zxt.user.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;


@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString(callSuper = true)
@ApiModel("地址信息入参")
public class UserAddressDTO implements Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 1L;


    /**
     * id
     */
    @ApiModelProperty(value = "id")
    private Long id;
    /**
     * 用户id/企业id
     */
    @ApiModelProperty("用户id/企业id")
    private Long userId;


    /**
     * 收货人
     */
    @ApiModelProperty("收货人")
    @NotNull(message = "收货人不能为空")
    @Size(max = 64, message = "收货人不能超过64位")
    private String consignee;

    /**
     * 手机号码
     */
    @ApiModelProperty("手机号码")
    @Size(max = 32, message = "手机号码不能超过32位")
    private String mobile;

    /**
     * 省份编码
     */
    @ApiModelProperty("省份编码")
    @Size(max = 32, message = "省份编码不能超过32位")
    private String provinceCode;

    /**
     * 省份名称
     */
    @ApiModelProperty("省份名称")
    @Size(max = 100, message = "省份名称不能超过100位")
    private String provinceName;

    /**
     * 城市编码
     */
    @ApiModelProperty("城市编码")
    @Size(max = 32, message = "城市编码不能超过32位")
    private String cityCode;

    /**
     * 城市名称
     */
    @ApiModelProperty("城市名称")
    @Size(max = 100, message = "城市名称不能超过100位")
    private String cityName;

    /**
     * 区域编码
     */
    @ApiModelProperty("区域编码")
    @Size(max = 32, message = "区域编码不能超过32位")
    private String regionCode;

    /**
     * 区域名称
     */
    @ApiModelProperty("区域名称")
    @Size(max = 100, message = "区域名称不能超过100位")
    private String regionName;

    /**
     * 县级编码
     */
    @ApiModelProperty("县级编码")
    @Size(max = 32, message = "县级编码不能超过32位")
    private String countyCode;

    /**
     * 县级名称
     */
    @ApiModelProperty("县级名称")
    @Size(max = 100, message = "县级名称不能超过100位")
    private String countyName;


    /**
     * 详细地址
     */
    @ApiModelProperty("详细地址")
    @Size(max = 255, message = "详细地址不能超过255位")
    private String detailedAddress;

    /**
     * 是否默认
     */
    @ApiModelProperty("是否默认")
    private Boolean isDefault;


    /**
     * -------------------------------------------------
     * 上面字段由工具自动生成，请在下面添加扩充字段
     * -------------------------------------------------
     */


}