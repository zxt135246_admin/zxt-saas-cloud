package com.zxt.user.manager;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zxt.user.dao.AiChooseMapper;
import com.zxt.user.model.AiChoose;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class AiChooseManager extends ServiceImpl<AiChooseMapper, AiChoose> {

    public List<AiChoose> getByLevel(Integer level) {
        LambdaQueryWrapper<AiChoose> wrapper = AiChoose.gw();
        wrapper.eq(AiChoose::getLevel, level);
        return list(wrapper);
    }

    public AiChoose getNextChoose(Long parentChooseId, Integer sortNum) {
        LambdaQueryWrapper<AiChoose> wrapper = AiChoose.gw();
        wrapper.eq(AiChoose::getParentId, parentChooseId);
        wrapper.eq(AiChoose::getSortNum, sortNum);
        return getOne(wrapper);
    }

    public List<AiChoose> getNextChooseList(Long parentChooseId) {
        LambdaQueryWrapper<AiChoose> wrapper = AiChoose.gw();
        wrapper.eq(AiChoose::getParentId, parentChooseId);
        return list(wrapper);
    }

    public AiChoose getByParentIdAndSortNum(Long chooseId, Integer sortNum) {
        LambdaQueryWrapper<AiChoose> wrapper = AiChoose.gw();
        wrapper.eq(AiChoose::getParentId, chooseId);
        wrapper.eq(AiChoose::getSortNum, sortNum);
        return getOne(wrapper);
    }
}
