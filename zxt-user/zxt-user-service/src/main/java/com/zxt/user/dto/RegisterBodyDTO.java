package com.zxt.user.dto;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.validation.constraints.NotNull;

/**
 * @author zxt
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString(callSuper = true)
@ApiModel("用户注册对象入参")
public class RegisterBodyDTO {

    @NotNull(message = "用户名不能为空")
    private String userName;

    @NotNull(message = "密码不能为空")
    private String userPwd;

    @NotNull(message = "手机号不能为空")
    private String phone;

    @NotNull(message = "邮箱不能为空")
    private String email;

    @NotNull(message = "头像不能为空")
    private String avatarUrl;

    @NotNull(message = "昵称不能为空")
    private String nickName;
}
