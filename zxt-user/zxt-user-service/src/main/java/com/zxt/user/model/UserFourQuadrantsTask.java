package com.zxt.user.model;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.zxt.database.config.VarcharEncrypTypeHandler;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString(callSuper = true)
@TableName(value = "sc_user_four_quadrants_task", autoResultMap = true)
public class UserFourQuadrantsTask {
    public static final LambdaQueryWrapper<UserFourQuadrantsTask> gw() {
        return new LambdaQueryWrapper<>();
    }

    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;

    @TableField(value = "user_id")
    private Long userId;

    /**
     * 四象限任务类型 1.紧急且重要 2.不紧急且重要 3.紧急且不重要 4,不紧急且不重要
     */
    @TableField(value = "task_type")
    private Byte taskType;

    /**
     * 任务状态 1.待办 2.已完成
     */
    @TableField(value = "task_status")
    private Byte taskStatus;

    /**
     * 任务内容
     */
    @TableField(value = "task_content")
    private String taskContent;

    /**
     * 创建时间
     */
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    /**
     * 是否删除
     */
    @TableLogic
    @TableField(value = "is_deleted")
    private Boolean isDeleted;

}
