package com.zxt.user.vo;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.zxt.bean.convertor.LongJsonDeserializer;
import com.zxt.bean.convertor.LongJsonSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString(callSuper = true)
@ApiModel("用户信息出参")
public class UserVO {

    /**
     * id
     */
    @ApiModelProperty("id")
    @JsonSerialize(using = LongJsonSerializer.class)
    @JsonDeserialize(using = LongJsonDeserializer.class)
    private Long id;

    /**
     * 用户昵称
     */
    @ApiModelProperty("用户昵称")
    private String nickName;

    /**
     * 用户名
     */
    @ApiModelProperty("用户名")
    private String userName;

    /**
     * 手机号
     */
    @ApiModelProperty("手机号")
    private String phone;

    @ApiModelProperty("头像")
    private String avatarUrl;

    @ApiModelProperty("会员成长数值")
    private Integer growthValue;

    @ApiModelProperty("会员等级描述")
    private String levelDesc;

    @ApiModelProperty("是否管理员")
    private Boolean isAdmin;

    @ApiModelProperty("下一级需要的会员成长数值")
    private Integer nextGrowthValue;
}
