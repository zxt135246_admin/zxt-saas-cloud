package com.zxt.user.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zxt.user.model.User;

/**
 * @author zxt
 */
public interface UserMapper extends BaseMapper<User> {
}
