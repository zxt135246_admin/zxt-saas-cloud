package com.zxt.user.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.time.LocalDateTime;

/**
 * @author zxt
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString(callSuper = true)
@ApiModel("游客登录入参")
public class VisitorLoginDTO {


    @ApiModelProperty("登录ip")
    private String loginIp;

    @ApiModelProperty("登录时间")
    private LocalDateTime loginDate;

    @ApiModelProperty("登录token")
    private String loginToken;
}
