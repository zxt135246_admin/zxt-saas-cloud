package com.zxt.user.dao;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zxt.user.model.User;
import com.zxt.user.model.UserConcern;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author zxt
 * 关注
 */
public interface UserConcernMapper extends BaseMapper<UserConcern> {


    List<User> getMutualConcernUser(@Param("userId") Long userId);
}
