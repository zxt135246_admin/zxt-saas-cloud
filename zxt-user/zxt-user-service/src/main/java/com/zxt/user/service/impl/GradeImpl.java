package com.zxt.user.service.impl;

import com.zxt.bean.utils.BeanUtils;
import com.zxt.user.manager.GradeManager;
import com.zxt.user.model.Grade;
import com.zxt.user.service.GradeService;
import com.zxt.user.vo.GradeVO;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author zxt
 */
@Slf4j
@Service
@AllArgsConstructor
public class GradeImpl implements GradeService {

    private final GradeManager gradeManager;

    @Override
    @Transactional(readOnly = true)
    public List<GradeVO> list() {
        List<Grade> gradeList = gradeManager.list();
        return BeanUtils.beanCopy(gradeList, GradeVO.class);
    }

    @Override
    public GradeVO getGradeByValueNum(Integer growthValue) {
        return BeanUtils.beanCopy(gradeManager.getByValueNum(growthValue), GradeVO.class);
    }
}
