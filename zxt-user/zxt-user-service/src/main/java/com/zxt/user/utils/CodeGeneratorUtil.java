package com.zxt.user.utils;

import com.zxt.bean.utils.LocalDateUtil;
import com.zxt.bean.utils.StringUtils;

import java.time.LocalDateTime;

/**
 * 编码生成工具类
 */
public class CodeGeneratorUtil {

    /**
     * 生成20位流水号
     * JF+12位时间戳+6位随机数
     *
     * @return 分享编码
     */
    public static String generateFlowNo() {
        String date = LocalDateUtil.format(LocalDateTime.now(), "yyMMddHHmmss");
        String random = StringUtils.getRandomString(6, StringUtils.NUMBERS);
        return "JF" + date + random;
    }

}
