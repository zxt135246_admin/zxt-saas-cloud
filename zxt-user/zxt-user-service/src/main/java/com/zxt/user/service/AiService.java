package com.zxt.user.service;

public interface AiService {

    /**
     * 自动回复
     *
     * @param wxOpenId
     * @param content
     * @return
     */
    String autoReply(String wxOpenId, String content);
}
