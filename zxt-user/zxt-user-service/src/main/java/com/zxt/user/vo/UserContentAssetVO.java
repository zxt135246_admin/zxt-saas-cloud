package com.zxt.user.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString(callSuper = true)
@ApiModel("用户资产出参")
public class UserContentAssetVO {


    /**
     * 作品数量
     */
    @ApiModelProperty("作品数量")
    private Integer opusNum;

    @ApiModelProperty("喜欢数量")
    private Integer likeNum;

    @ApiModelProperty("收获热度")
    private Integer totalHeatNum;

    @ApiModelProperty("关注数量")
    private Integer concernNum;

    @ApiModelProperty("粉丝数量")
    private Integer fenSiNum;
}
