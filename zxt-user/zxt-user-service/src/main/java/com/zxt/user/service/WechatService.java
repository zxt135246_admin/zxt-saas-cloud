package com.zxt.user.service;

public interface WechatService {

    /**
     * 获取信息
     *
     * @param signature
     * @param timestamp
     * @param nonce
     * @param echostr
     * @return
     */
    String getEventInfo(String signature, String timestamp, String nonce, String echostr);

    String getBackInfo(String xml, String msgSignature, String signature, String timestamp, String nonce);
}
