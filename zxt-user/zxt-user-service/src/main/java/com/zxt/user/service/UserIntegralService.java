package com.zxt.user.service;

import com.zxt.user.api.req.OperateIntegralApiDTO;
import com.zxt.user.dto.AddIntegralDTO;
import com.zxt.user.vo.UserIntegralVO;

public interface UserIntegralService {

    /**
     * 添加积分
     *
     * @param dto
     */
    void addIntegral(AddIntegralDTO dto);

    /**
     * 获取用户积分
     *
     * @param userId
     * @return
     */
    UserIntegralVO getByUserId(Long userId);

    /**
     * 操作积分
     *
     * @param dto
     * @return
     */
    Boolean operateIntegral(OperateIntegralApiDTO dto);

    /**
     * 取消订单回滚资源（积分）
     *
     * @param orderCode
     */
    void cancelOrder(String orderCode);
}
