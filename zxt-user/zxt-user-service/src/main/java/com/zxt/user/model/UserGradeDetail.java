package com.zxt.user.model;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString(callSuper = true)
@TableName(value = "sc_user_grade_detail", autoResultMap = true)
public class UserGradeDetail {
    public static final LambdaQueryWrapper<UserGradeDetail> gw() {
        return new LambdaQueryWrapper<>();
    }

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @TableField(value = "user_id")
    private Long userId;

    @TableField(value = "growth_value")
    private Integer growthValue;

    /**
     * 成长值来源 1.签到 2.发布文案
     */
    @TableField(value = "growth_value_source")
    private Byte growthValueSource;

    /**
     * 扣减标识1.增加 2.扣除
     */
    @TableField(value = "deduction_flag")
    private Byte deductionFlag;

    /**
     * 操作原因
     */
    @TableField(value = "operation_reason")
    private String operationReason;

    @TableField(value = "data_id")
    private String dataId;

    /**
     * 创建时间
     */
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    /**
     * 是否删除
     */
    @TableLogic
    @TableField(value = "is_deleted")
    private Boolean isDeleted;
}
