package com.zxt.user.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;


@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString(callSuper = true)
@ApiModel("修改状态入参")
public class UpdateStatusDTO implements Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "修改的id")
    private Long id;

    @ApiModelProperty(value = "批量修改的ids")
    private List<Long> ids;

    @ApiModelProperty(value = "修改状态", required = true)
    private Byte commonStatus;

}
