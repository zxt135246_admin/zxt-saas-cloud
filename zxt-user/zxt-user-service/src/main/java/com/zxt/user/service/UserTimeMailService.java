package com.zxt.user.service;

import com.zxt.user.dto.UserTimeMailDTO;

public interface UserTimeMailService {


    /**
     * 新建时间邮箱
     *
     * @param dto
     */
    void add(UserTimeMailDTO dto);

    /**
     * 处理发送时间邮件
     */
    void dealSendUserTimeMail();
}
