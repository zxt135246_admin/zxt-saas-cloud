package com.zxt.user.manager;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zxt.bean.utils.LocalDateUtil;
import com.zxt.user.dao.UserGradeDetailMapper;
import com.zxt.user.enums.DeductionFlagEnum;
import com.zxt.user.enums.GrowthValueSourceEnum;
import com.zxt.user.model.UserGradeDetail;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class UserGradeDetailManager extends ServiceImpl<UserGradeDetailMapper, UserGradeDetail> {

    /**
     * 获取今日签到日志
     *
     * @param userId
     * @return
     */
    public UserGradeDetail getTodayCheckInsLog(Long userId) {
        LambdaQueryWrapper<UserGradeDetail> wrapper = UserGradeDetail.gw();
        wrapper.eq(UserGradeDetail::getGrowthValueSource, GrowthValueSourceEnum.CHECK_INS.getValue());
        wrapper.eq(UserGradeDetail::getDeductionFlag, DeductionFlagEnum.ADD.getValue());
        wrapper.eq(UserGradeDetail::getUserId, userId);
        wrapper.le(UserGradeDetail::getCreateTime, LocalDateUtil.getLatestOfDay());
        wrapper.ge(UserGradeDetail::getCreateTime, LocalDateUtil.getEarliestOfDay());
        return getOne(wrapper);
    }

    public UserGradeDetail getByContentId(Long contentId) {
        LambdaQueryWrapper<UserGradeDetail> wrapper = UserGradeDetail.gw();
        wrapper.eq(UserGradeDetail::getDeductionFlag, DeductionFlagEnum.ADD.getValue());
        wrapper.eq(UserGradeDetail::getGrowthValueSource, GrowthValueSourceEnum.PUBLISH_CONTENT);
        wrapper.eq(UserGradeDetail::getDataId, contentId);
        return getOne(wrapper);
    }
}
