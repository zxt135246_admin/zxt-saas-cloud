package com.zxt.user.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zxt.bean.enums.CommonStatusEnum;
import com.zxt.bean.exeption.ServiceException;
import com.zxt.bean.query.QueryResultVO;
import com.zxt.bean.result.ResultCode;
import com.zxt.bean.utils.BeanUtils;
import com.zxt.bean.utils.CollectionsUtil;
import com.zxt.user.dto.MallDTO;
import com.zxt.user.dto.MallQueryDTO;
import com.zxt.user.dto.UpdateStatusDTO;
import com.zxt.user.manager.MallManager;
import com.zxt.user.manager.MallPayMethodManager;
import com.zxt.user.model.Mall;
import com.zxt.user.model.MallPayMethod;
import com.zxt.user.service.MallService;
import com.zxt.user.vo.MallVO;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @author zxt
 */
@Slf4j
@Service
@AllArgsConstructor
public class MallImpl implements MallService {

    private final MallManager mallManager;
    private final MallPayMethodManager mallPayMethodManager;

    @Override
    public QueryResultVO<MallVO> queryMall(MallQueryDTO dto) {
        Page<Mall> page = mallManager.queryMall(dto);
        QueryResultVO<MallVO> queryResultVO = BeanUtils.pageToQueryResultVO(page, MallVO.class);
        List<MallVO> records = BeanUtils.beanCopy(page.getRecords(), MallVO.class);

        if (CollectionsUtil.isEmpty(records)) {
            queryResultVO.setRecords(new ArrayList<>());
            return queryResultVO;
        }

        for (MallVO mallVO : records) {
            mallVO.setCommonStatusDesc(CommonStatusEnum.getDescByValue(mallVO.getCommonStatus()));
        }

        queryResultVO.setRecords(records);
        return queryResultVO;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT, rollbackFor = Exception.class)
    public void addMall(MallDTO dto) {
        if (CollectionsUtil.isEmpty(dto.getPayMethodIds())) {
            throw new ServiceException(ResultCode.COMMON_MESSAGE, "商城支付方式不能为空");
        }
        Mall mall = BeanUtils.beanCopy(dto, Mall.class);
        mall.setCommonStatus(CommonStatusEnum.CREATE.getValue());
        mallManager.save(mall);

        List<MallPayMethod> mallPayMethodList = new ArrayList<>();
        for (Long payMethodId : dto.getPayMethodIds()) {
            MallPayMethod mallPayMethod = new MallPayMethod();
            mallPayMethod.setMallId(mall.getId());
            mallPayMethod.setPayMethodId(payMethodId);
            mallPayMethodList.add(mallPayMethod);
        }
        mallPayMethodManager.saveBatch(mallPayMethodList);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT, rollbackFor = Exception.class)
    public void updateMall(MallDTO dto) {

        if (CollectionsUtil.isEmpty(dto.getPayMethodIds())) {
            throw new ServiceException(ResultCode.COMMON_MESSAGE, "商城支付方式不能为空");
        }

        Mall oldMall = mallManager.getById(dto.getId());
        if (Objects.isNull(oldMall)) {
            throw new ServiceException(ResultCode.DATA_NOT_EXIST, "商城");
        }

        if (CommonStatusEnum.ENABLE.equal(oldMall.getCommonStatus())) {
            throw new ServiceException(ResultCode.COMMON_MESSAGE, "启用的商城无法修改");
        }

        Mall mall = BeanUtils.beanCopy(dto, Mall.class);
        mall.setCommonStatus(null);
        mallManager.updateById(mall);

        mallPayMethodManager.removeByMallId(dto.getId());
        List<MallPayMethod> mallPayMethodList = new ArrayList<>();
        for (Long payMethodId : dto.getPayMethodIds()) {
            MallPayMethod mallPayMethod = new MallPayMethod();
            mallPayMethod.setMallId(mall.getId());
            mallPayMethod.setPayMethodId(payMethodId);
            mallPayMethodList.add(mallPayMethod);
        }
        mallPayMethodManager.saveBatch(mallPayMethodList);
    }

    @Override
    public MallVO getMall(Long id) {
        MallVO mallVO = BeanUtils.beanCopy(mallManager.getById(id), MallVO.class);
        if (Objects.isNull(mallVO)) {
            return null;
        }
        mallVO.setCommonStatus(CommonStatusEnum.CREATE.getValue());
        return mallVO;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT, rollbackFor = Exception.class)
    public void updateStatus(UpdateStatusDTO dto) {
        if (Objects.isNull(dto.getId())) {
            throw new ServiceException(ResultCode.COMMON_MESSAGE, "请选择商城");
        }

        Mall mall = mallManager.getById(dto.getId());

        if (Objects.isNull(mall)) {
            throw new ServiceException(ResultCode.DATA_NOT_EXIST, "商城");
        }

        if (mall.getCommonStatus().equals(dto.getCommonStatus())) {
            throw new ServiceException(ResultCode.COMMON_MESSAGE, "商城状态无需修改");
        }

        mallManager.updateStatus(dto.getId(), dto.getCommonStatus());
    }
}
