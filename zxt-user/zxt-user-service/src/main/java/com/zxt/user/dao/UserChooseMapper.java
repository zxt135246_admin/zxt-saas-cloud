package com.zxt.user.dao;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zxt.user.model.UserChoose;

/**
 * @author zxt
 */
public interface UserChooseMapper extends BaseMapper<UserChoose> {
}
