package com.zxt.user.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zxt.user.model.SysMenu;

import java.util.List;

/**
 * @author zxt
 */
public interface SysMenuMapper extends BaseMapper<SysMenu> {
}
