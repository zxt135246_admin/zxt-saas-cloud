package com.zxt.user.service;

import com.zxt.bean.model.UserInfoVO;
import com.zxt.user.dto.RegisterBodyDTO;
import com.zxt.user.dto.LoginBodyDTO;
import com.zxt.user.dto.VisitorLoginDTO;
import com.zxt.user.model.User;
import com.zxt.user.vo.UserContentAssetVO;
import com.zxt.user.vo.UserMallAssetsVO;
import com.zxt.user.vo.UserVO;

import java.util.List;

/**
 * @author zxt
 */
public interface UserService {

    /**
     * 登录验证
     *
     * @param dto
     * @return
     */
    UserInfoVO login(LoginBodyDTO dto);

    /**
     * 通过微信openId静默登录
     *
     * @param openId
     * @return
     */
    UserInfoVO quickLogin(String openId);

    /**
     * 根据id更新
     *
     * @param user
     */
    void updateById(User user);

    /**
     * 获取用户信息
     *
     * @param id
     * @return
     */
    UserVO getById(Long id);

    /**
     * 获取用户资产信息
     *
     * @param id
     * @return
     */
    UserContentAssetVO getUserAsset(Long id);

    /**
     * 获取所有用户信息
     * 排除当前用户
     *
     * @return
     */
    List<UserVO> getAllUser();

    /**
     * 注册
     *
     * @param dto
     * @return
     */
    Boolean register(RegisterBodyDTO dto);

    /**
     * 根据用户ids获取用户列表
     *
     * @param userIds
     * @return
     */
    List<UserVO> getByIds(List<Long> userIds);

    /**
     * 关注用户
     *
     * @param userId
     * @param concernUserId
     */
    void concern(Long userId, Long concernUserId);

    /**
     * 获取和该用户互相关注的用户信息
     *
     * @param userId
     * @return
     */
    List<UserVO> getMutualConcernUser(Long userId);

    /**
     * 修改用户昵称
     *
     * @param userId
     * @param nickName
     */
    void updateNickName(Long userId, String nickName);

    /**
     * 获取朋友列表
     *
     * @param friendType
     * @param userId
     * @return
     */
    List<UserVO> getFriendList(Byte friendType, Long userId);

    /**
     * 游客登录
     */
    void visitorLogin(VisitorLoginDTO visitorLoginDTO);

    /**
     * 获取用户商城资产
     * @param userId
     * @return
     */
    List<UserMallAssetsVO> getMallAssets(Long userId);
}
