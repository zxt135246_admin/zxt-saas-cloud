package com.zxt.user.enums;

/**
 * @Comments: ai操作枚举
 */
public enum AiOperateEnum {
    BIND_COUNT((byte) 1, "绑定账号"),
    PUBLISH_CONTENT((byte) 2, "发布文案"),
    GET_PRIVATE_ONE_CONTENT((byte) 3, "随机获取一条我的文案"),
    GET_PUBLIC_ONE_CONTENT((byte) 4, "随机获取一条公共文案"),
    PUBLISH_IMAGE((byte) 5, "发布照片"),
    GET_ACCOUNT_RECORD((byte) 6, "获取用户保管账号记录"),
    ADD_ACCOUNT_RECORD((byte) 7, "新增用户保管账号记录"),
    ADD_FOUR_QUADRANTS_TASK((byte) 8, "新增时间四象限任务"),
    GET_FOUR_QUADRANTS_LIST((byte) 9, "查询四象限列表"),
    GET_NOT_SENT_MAIL((byte) 10, "查询未发送邮件"),
    ADD_USER_TIME_MAIL((byte) 11, "设置新的时间邮件"),
    ;

    private byte value;
    private String desc;

    AiOperateEnum(byte value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public byte getValue() {
        return value;
    }

    public String getDesc() {
        return desc;
    }

    public static boolean exists(Byte status) {
        if (status == null) {
            return false;
        }
        byte s = status.byteValue();
        return exists(s);
    }

    public static boolean exists(byte s) {
        for (AiOperateEnum element : AiOperateEnum.values()) {
            if (element.value == s) {
                return true;
            }
        }
        return false;
    }

    public boolean equal(Byte val) {
        return val == null ? false : val.byteValue() == this.value;
    }

    public static String getDescByValue(Byte value) {
        if (value == null) {
            return "";
        }
        for (AiOperateEnum element : AiOperateEnum.values()) {
            if (element.value == value.byteValue()) {
                return element.desc;
            }
        }
        return "";
    }
}
