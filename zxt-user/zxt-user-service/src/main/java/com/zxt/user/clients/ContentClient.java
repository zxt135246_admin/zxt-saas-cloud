package com.zxt.user.clients;

import com.alibaba.fastjson.JSON;
import com.zxt.bean.result.Result;
import com.zxt.content.api.dto.ContentApiDTO;
import com.zxt.content.api.dto.ImagesApiDTO;
import com.zxt.content.api.feignClient.ContentFeignClient;
import com.zxt.content.api.feignClient.ImagesFeignClient;
import com.zxt.content.api.vo.ContentApiVO;
import com.zxt.content.api.vo.ContentTypeApiVO;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 文案中心
 *
 * @author zxt
 */
@Component
@Slf4j
@AllArgsConstructor
public class ContentClient {

    private final ContentFeignClient contentFeignClient;
    private final ImagesFeignClient imagesFeignClient;

    public String getContentByName(String name) {
        log.info("用户中心-文案中心 获取文案，入参:{}", JSON.toJSONString(name));
        try {
            Result<String> result = contentFeignClient.getContent(name);
            log.info("用户中心-文案中心 获取文案，出参:{}", JSON.toJSONString(result));
            return result.getData();
        } catch (Exception e) {
            log.error("用户中心-文案中心 获取文案,异常信息:{}", e);
        }
        return null;
    }

    public List<ContentApiVO> getPrivateContentByUserId(Long userId) {
        log.info("用户中心-文案中心 获取用户私有文案，入参:{}", userId);
        try {
            Result<List<ContentApiVO>> result = contentFeignClient.getPrivateContentByUserId(userId);
            log.info("用户中心-文案中心 获取用户私有文案，出参:{}", JSON.toJSONString(result));
            return result.getData();
        } catch (Exception e) {
            log.error("用户中心-文案中心 获取用户私有文案,异常信息:{}", e);
        }
        return null;
    }

    public Boolean addContent(ContentApiDTO dto) {
        log.info("用户中心-文案中心 新增文案，入参:{}", JSON.toJSONString(dto));
        try {
            Result<Boolean> result = contentFeignClient.addContent(dto);
            log.info("用户中心-文案中心 新增文案，出参:{}", JSON.toJSONString(result));
            return result.getData();
        } catch (Exception e) {
            log.error("用户中心-文案中心 新增文案,异常信息:{}", e);
        }
        return null;
    }

    /**
     * 随机获取一条文案
     *
     * @param id
     * @return
     */
    public ContentApiVO randGetOneContentByUserId(Long id) {
        log.info("用户中心-文案中心 随机获取一条文案，入参:{}", id);
        try {
            Result<ContentApiVO> result = contentFeignClient.randGetOneContentByUserId(id);
            log.info("用户中心-文案中心 随机获取一条文案，出参:{}", JSON.toJSONString(result));
            return result.getData();
        } catch (Exception e) {
            log.error("用户中心-文案中心 随机获取一条文案,异常信息:{}", e);
        }
        return null;
    }

    public List<ContentTypeApiVO> getContentTypeList() {
        log.info("用户中心-文案中心 获取文案类型列表");
        try {
            Result<List<ContentTypeApiVO>> result = contentFeignClient.getContentTypeList();
            log.info("用户中心-文案中心 获取文案类型列表，出参:{}", JSON.toJSONString(result));
            return result.getData();
        } catch (Exception e) {
            log.error("用户中心-文案中心 获取文案类型列表,异常信息:{}", e);
        }
        return null;
    }

    public ContentApiVO getContentById(Long contentId) {
        log.info("用户中心-文案中心 获取文案, 入参:{}", JSON.toJSONString(contentId));
        try {
            Result<ContentApiVO> result = contentFeignClient.getById(contentId);
            log.info("用户中心-文案中心 获取文案，出参:{}", JSON.toJSONString(result));
            return result.getData();
        } catch (Exception e) {
            log.error("用户中心-文案中心 获取文案,异常信息:{}", e);
        }
        return null;
    }
}
