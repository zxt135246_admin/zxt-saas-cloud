package com.zxt.user.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author zxt
 */
@Data
@ApiModel("商城入参")
public class MallDTO implements Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 1L;

    @ApiModelProperty("id")
    private Long id;

    @ApiModelProperty("商城名称")
    private String mallName;

    @ApiModelProperty("商城支付方式")
    private List<Long> payMethodIds;
}
