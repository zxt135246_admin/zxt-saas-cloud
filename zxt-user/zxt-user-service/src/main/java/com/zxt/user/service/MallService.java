package com.zxt.user.service;

import com.zxt.bean.query.QueryResultVO;
import com.zxt.user.dto.MallDTO;
import com.zxt.user.dto.MallQueryDTO;
import com.zxt.user.dto.UpdateStatusDTO;
import com.zxt.user.vo.MallVO;

import java.util.List;

public interface MallService {

    /**
     * 查询商城列表
     *
     * @param dto
     * @return
     */
    QueryResultVO<MallVO> queryMall(MallQueryDTO dto);

    /**
     * 新建商城
     *
     * @param dto
     */
    void addMall(MallDTO dto);

    /**
     * 修改商城
     *
     * @param dto
     */
    void updateMall(MallDTO dto);

    /**
     * 查询商城详情
     *
     * @param id
     * @return
     */
    MallVO getMall(Long id);

    /**
     * 修改商城状态
     *
     * @param dto
     */
    void updateStatus(UpdateStatusDTO dto);
}
