package com.zxt.user.enums;

/**
 * @description 四象限任务状态 1.待办 2.已完成
 */
public enum FourTaskStatusEnum {

    TODO         ((byte) 1, "待办"),
    COMPLETED          ((byte) 2, "已完成"),
    ;

    private byte value;
    private String desc;

    FourTaskStatusEnum(byte value, String desc){
        this.value = value;
        this.desc = desc;
    }

    public byte getValue() {
        return value;
    }

    public String getDesc() {
        return desc;
    }

    public static boolean exists(Byte status) {
        if (status == null) {
            return false;
        }
        byte s = status.byteValue();
        return exists(s);
    }

    public static boolean exists(byte s) {
        for (FourTaskStatusEnum element : FourTaskStatusEnum.values()) {
            if (element.value == s) {
                return true;
            }
        }
        return false;
    }

    public boolean equal(Byte val) {
        return val == null ? false : val.byteValue() == this.value;
    }

    public static String getDescByValue(Byte value) {
        if (value == null) {
            return "";
        }
        for (FourTaskStatusEnum element : FourTaskStatusEnum.values()) {
            if (element.value == value.byteValue()) {
                return element.desc;
            }
        }
        return "";
    }
}
