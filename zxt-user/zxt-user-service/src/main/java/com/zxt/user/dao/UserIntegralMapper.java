package com.zxt.user.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zxt.user.model.UserIntegral;

/**
 * @author zxt
 */
public interface UserIntegralMapper extends BaseMapper<UserIntegral> {
}
