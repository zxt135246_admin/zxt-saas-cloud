package com.zxt.user.manager;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zxt.bean.enums.CommonStatusEnum;
import com.zxt.user.dao.PayMethodMapper;
import com.zxt.user.model.PayMethod;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class PayMethodManager extends ServiceImpl<PayMethodMapper, PayMethod> {

    /**
     * 获取启用的支付方式
     *
     * @return
     */
    public List<PayMethod> getEnableList() {
        LambdaQueryWrapper<PayMethod> wrapper = PayMethod.gw();
        wrapper.eq(PayMethod::getCommonStatus, CommonStatusEnum.ENABLE.getValue());
        return list(wrapper);
    }
}
