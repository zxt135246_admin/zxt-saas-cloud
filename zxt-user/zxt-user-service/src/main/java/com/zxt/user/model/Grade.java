package com.zxt.user.model;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString(callSuper = true)
@TableName(value = "sc_grade", autoResultMap = true)
public class Grade {
    public static final LambdaQueryWrapper<Grade> gw() {
        return new LambdaQueryWrapper<>();
    }

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 等级名称
     */
    @TableField(value = "grade_name")
    private String gradeName;

    /**
     * 等级级数
     */
    @TableField(value = "grade_level")
    private Integer gradeLevel;

    /**
     * 开始数值
     */
    @TableField(value = "start_growth_value")
    private Integer startGrowthValue;

    /**
     * 结束数值
     */
    @TableField(value = "end_growth_value")
    private Integer endGrowthValue;

    /**
     * 等级图标
     */
    @TableField(value = "grade_images")
    private String gradeImages;

    /**
     * 折扣比例 0-100
     */
    @TableField(value = "discount_ratio")
    private Integer discountRatio;

    /**
     * 创建时间
     */
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    /**
     * 是否删除
     */
    @TableLogic
    @TableField(value = "is_deleted")
    private Boolean isDeleted;
}
