package com.zxt.user.service;

import com.zxt.user.dto.UpdateStatusDTO;
import com.zxt.user.dto.UserFourQuadrantsTaskDTO;

public interface UserFourQuadrantsTaskService {

    /**
     * 新增四象限任务
     *
     * @param dto
     */
    void addTask(UserFourQuadrantsTaskDTO dto);

    /**
     * 修改任务状态
     *
     * @param dto
     */
    void updateStatus(UpdateStatusDTO dto);
}
