package com.zxt.user.dao;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zxt.user.model.UserGradeDetail;

/**
 * @author zxt
 */
public interface UserGradeDetailMapper extends BaseMapper<UserGradeDetail> {
}
