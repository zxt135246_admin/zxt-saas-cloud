package com.zxt.user.manager;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zxt.bean.exeption.ServiceException;
import com.zxt.bean.result.ResultCode;
import com.zxt.bean.utils.BeanUtils;
import com.zxt.bean.utils.StringUtils;
import com.zxt.user.dao.UserAddressMapper;
import com.zxt.user.model.UserAddress;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

/**
 * 通用逻辑层-地址信息管理类
 */
@Slf4j
@Service
public class UserAddressManager extends ServiceImpl<UserAddressMapper, UserAddress> {

    /**
     * 清空默认地址信息
     *
     * @param userId
     */
    public void clearDafaultAddress(Long userId) {
        LambdaUpdateWrapper<UserAddress> wrapper = new LambdaUpdateWrapper();
        wrapper.set(UserAddress::getIsDefault, Boolean.FALSE);
        wrapper.eq(UserAddress::getUserId, userId);
        update(wrapper);
    }

    public boolean updateUserAddress(UserAddress userAddress) {
        UserAddress oldUserAddress = getById(userAddress.getId());
        if(Objects.isNull(oldUserAddress)){
            throw new ServiceException(ResultCode.DATA_NOT_EXIST, "地址");
        }

        if (Objects.nonNull(userAddress.getIsDefault()) && userAddress.getIsDefault()) {
            clearDafaultAddress(userAddress.getUserId());
        }
        if (StringUtils.isBlank(userAddress.getCountyCode())) {
            LambdaUpdateWrapper<UserAddress> updateWrapper = new LambdaUpdateWrapper<>();
            updateWrapper.set(UserAddress::getCountyCode, null);
            updateWrapper.set(UserAddress::getCountyName, null);
            updateWrapper.eq(UserAddress::getId, userAddress.getId());
            super.update(updateWrapper);
        }
        return super.updateById(BeanUtils.beanCopy(userAddress, UserAddress.class));
    }

    public void createUserAddress(UserAddress userAddress) {
        if (Objects.nonNull(userAddress.getIsDefault()) && userAddress.getIsDefault()) {
            clearDafaultAddress(userAddress.getUserId());
        }
        super.save(userAddress);
    }

    public List<UserAddress> getByUserId(Long userId) {
        LambdaQueryWrapper<UserAddress> wrapper = UserAddress.gw();
        wrapper.eq(UserAddress::getUserId, userId);
        return list(wrapper);
    }
}