package com.zxt.user.dao;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zxt.user.model.VisitorLoginLog;

/**
 * @author zxt
 */
public interface VisitorLoginLogMapper extends BaseMapper<VisitorLoginLog> {
}
