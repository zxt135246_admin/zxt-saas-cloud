package com.zxt.user.service.impl;

import com.zxt.bean.utils.BeanUtils;
import com.zxt.user.manager.PayMethodManager;
import com.zxt.user.service.PayMethodService;
import com.zxt.user.vo.PayMethodVO;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author zxt
 */
@Slf4j
@Service
@AllArgsConstructor
public class PayMethodImpl implements PayMethodService {

    private final PayMethodManager payMethodManager;

    @Override
    public PayMethodVO getById(Long payMethodId) {
        return BeanUtils.beanCopy(payMethodManager.getById(payMethodId), PayMethodVO.class);
    }

    @Override
    public List<PayMethodVO> getEnableList() {
        return BeanUtils.beanCopy(payMethodManager.getEnableList(), PayMethodVO.class);
    }
}
