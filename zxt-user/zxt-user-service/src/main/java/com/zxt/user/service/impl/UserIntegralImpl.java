package com.zxt.user.service.impl;

import com.zxt.bean.exeption.ServiceException;
import com.zxt.bean.result.ResultCode;
import com.zxt.bean.utils.BeanUtils;
import com.zxt.lock.RedissonLock;
import com.zxt.user.api.enums.OperateTypeEnum;
import com.zxt.user.api.req.OperateIntegralApiDTO;
import com.zxt.user.constants.RedisKeyUser;
import com.zxt.user.dto.AddIntegralDTO;
import com.zxt.user.enums.DeductionFlagEnum;
import com.zxt.user.enums.IntegralSourceEnum;
import com.zxt.user.manager.UserIntegralDetailManager;
import com.zxt.user.manager.UserIntegralManager;
import com.zxt.user.model.UserIntegral;
import com.zxt.user.model.UserIntegralDetail;
import com.zxt.user.service.UserIntegralService;
import com.zxt.user.utils.CodeGeneratorUtil;
import com.zxt.user.vo.UserIntegralVO;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Objects;

/**
 * @author zxt
 */
@Slf4j
@Service
@AllArgsConstructor
public class UserIntegralImpl implements UserIntegralService {

    private final UserIntegralManager userIntegralManager;
    private final UserIntegralDetailManager userIntegralDetailManager;
    private final RedissonLock redissonLock;

    @Override
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT, rollbackFor = Exception.class)
    public void addIntegral(AddIntegralDTO dto) {

        if (dto.getIntegral().compareTo(BigDecimal.ZERO) <= 0) {
            throw new ServiceException(ResultCode.COMMON_MESSAGE, "请输入正确的积分");
        }

        OperateIntegralApiDTO integralApiDTO = new OperateIntegralApiDTO();
        integralApiDTO.setOperateType(OperateTypeEnum.ADD.getValue());
        integralApiDTO.setIntegral(dto.getIntegral());
        integralApiDTO.setOperationReason("发放积分");
        integralApiDTO.setUserId(dto.getUserId());
        Boolean isTrue = operateIntegral(integralApiDTO);
        if (!isTrue) {
            throw new ServiceException(ResultCode.SYS_ERROR);
        }

        log.info("积分发放完成");
    }

    @Override
    public UserIntegralVO getByUserId(Long userId) {
        return BeanUtils.beanCopy(userIntegralManager.getByUserId(userId), UserIntegralVO.class);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT, rollbackFor = Exception.class)
    public Boolean operateIntegral(OperateIntegralApiDTO dto) {
        Boolean isTrue = null;
        try {
            if (!redissonLock.lock(RedisKeyUser.getLock("updateUserIntegral", dto.getUserId()), 0, 10)) {
                throw new ServiceException(ResultCode.COMMON_MESSAGE, "用户积分计算中，请稍后再试！");
            }
            UserIntegral userIntegral = userIntegralManager.getByUserId(dto.getUserId());
            if (OperateTypeEnum.ADD.equal(dto.getOperateType())) {

                if (Objects.isNull(userIntegral)) {
                    //初始化用户积分
                    userIntegral = initUserIntegral(dto.getUserId());
                    userIntegral.setTotalIntegral(dto.getIntegral());
                    userIntegral.setAvailableIntegral(dto.getIntegral());
                    userIntegralManager.save(userIntegral);
                } else {
                    isTrue = userIntegralManager.addIntegral(userIntegral.getId(), dto.getIntegral());
                    if (!isTrue) {
                        throw new ServiceException(ResultCode.SYS_ERROR);
                    }
                    UserIntegralDetail userIntegralDetail = new UserIntegralDetail();
                    userIntegralDetail.setUserIntegralId(userIntegral.getId());
                    userIntegralDetail.setUserId(userIntegral.getUserId());
                    userIntegralDetail.setIntegral(dto.getIntegral());
                    userIntegralDetail.setIntegralSource(IntegralSourceEnum.ADD.getValue());
                    userIntegralDetail.setOperationReason(dto.getOperationReason());
                    userIntegralDetail.setOrderCode(dto.getOrderCode());
                    userIntegralDetail.setDeductionFlag(DeductionFlagEnum.ADD.getValue());
                    userIntegralDetail.setFlowNo(CodeGeneratorUtil.generateFlowNo());
                    userIntegralDetailManager.save(userIntegralDetail);
                }
            } else {
                isTrue = userIntegralManager.deductionIntegral(userIntegral.getId(), dto.getIntegral());
                if (!isTrue) {
                    throw new ServiceException(ResultCode.COMMON_MESSAGE, "余额不足！");
                }
                UserIntegralDetail userIntegralDetail = new UserIntegralDetail();
                userIntegralDetail.setUserIntegralId(userIntegral.getId());
                userIntegralDetail.setUserId(userIntegral.getUserId());
                userIntegralDetail.setIntegral(dto.getIntegral());
                userIntegralDetail.setIntegralSource(IntegralSourceEnum.REDUCTION.getValue());
                userIntegralDetail.setOperationReason(dto.getOperationReason());
                userIntegralDetail.setOrderCode(dto.getOrderCode());
                userIntegralDetail.setDeductionFlag(DeductionFlagEnum.REDUCTION.getValue());
                userIntegralDetail.setFlowNo(CodeGeneratorUtil.generateFlowNo());
                userIntegralDetailManager.save(userIntegralDetail);
            }
        } finally {
            redissonLock.release(RedisKeyUser.getLock("updateUserIntegral", dto.getUserId()));
        }
        return isTrue;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT, rollbackFor = Exception.class)
    public void cancelOrder(String orderCode) {
        UserIntegralDetail userIntegralDetail = userIntegralDetailManager.getByOrderCode(orderCode);

        if (Objects.isNull(userIntegralDetail)) {
            log.info("无积分扣减记录");
            return;
        }

        OperateIntegralApiDTO integralApiDTO = new OperateIntegralApiDTO();
        integralApiDTO.setOperateType(OperateTypeEnum.ADD.getValue());
        integralApiDTO.setIntegral(userIntegralDetail.getIntegral());
        integralApiDTO.setOperationReason("订单取消积分返还");
        integralApiDTO.setUserId(userIntegralDetail.getUserId());
        integralApiDTO.setOrderCode(orderCode);
        Boolean isTrue = operateIntegral(integralApiDTO);
        if (!isTrue) {
            throw new ServiceException(ResultCode.SYS_ERROR);
        }

        log.info("积分返还结束");
    }

    private UserIntegral initUserIntegral(Long userId) {
        UserIntegral userIntegral = new UserIntegral();
        userIntegral.setUserId(userId);
        userIntegral.setTotalIntegral(BigDecimal.ZERO);
        userIntegral.setAvailableIntegral(BigDecimal.ZERO);
        userIntegral.setUsedIntegral(BigDecimal.ZERO);
        log.info("用户积分为空，初始化积分，用户id:{}", userId);
        return userIntegral;
    }
}
