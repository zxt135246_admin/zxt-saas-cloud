package com.zxt.user.service.impl;

import com.zxt.bean.exeption.ServiceException;
import com.zxt.bean.result.ResultCode;
import com.zxt.bean.utils.TrueUtils;
import com.zxt.content.api.vo.ContentApiVO;
import com.zxt.lock.RedissonLock;
import com.zxt.user.clients.ContentClient;
import com.zxt.user.constants.RedisKeyUser;
import com.zxt.user.enums.DeductionFlagEnum;
import com.zxt.user.enums.GrowthValueSourceEnum;
import com.zxt.user.manager.UserGradeDetailManager;
import com.zxt.user.manager.UserManager;
import com.zxt.user.model.User;
import com.zxt.user.model.UserGradeDetail;
import com.zxt.user.service.UserGradeDetailService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;

/**
 * @author zxt
 */
@Slf4j
@Service
@RefreshScope
public class UserGradeDetailImpl implements UserGradeDetailService {

    private final UserGradeDetailManager userGradeDetailManager;
    private final UserManager userManager;
    private final RedissonLock redissonLock;
    private final ContentClient contentClient;

    /**
     * 每日签到积分
     */
    @Value("${growth.value:50}")
    private Integer growthValue;

    @Autowired
    public UserGradeDetailImpl(UserGradeDetailManager userGradeDetailManager, UserManager userManager, RedissonLock redissonLock, ContentClient contentClient) {
        this.userGradeDetailManager = userGradeDetailManager;
        this.userManager = userManager;
        this.redissonLock = redissonLock;
        this.contentClient = contentClient;
    }

    @Override
    public Boolean isCheckIns(Long userId) {
        UserGradeDetail userGradeDetail = userGradeDetailManager.getTodayCheckInsLog(userId);
        return Objects.nonNull(userGradeDetail);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT, rollbackFor = Exception.class)
    public void checkIns(Long userId) {
        try {
            if (!redissonLock.lock(RedisKeyUser.getLock("updateGrade", userId), 0, 10)) {
                throw new ServiceException(ResultCode.COMMON_MESSAGE, "积分计算中，请稍后再试！");
            }
            UserGradeDetail userGradeDetail = userGradeDetailManager.getTodayCheckInsLog(userId);

            TrueUtils.isTrue(Objects.nonNull(userGradeDetail)).throwMessage("你已经签到过啦~");

            UserGradeDetail newUserGradeDetail = new UserGradeDetail();
            newUserGradeDetail.setUserId(userId);
            newUserGradeDetail.setGrowthValue(growthValue);
            newUserGradeDetail.setGrowthValueSource(GrowthValueSourceEnum.CHECK_INS.getValue());
            newUserGradeDetail.setDeductionFlag(DeductionFlagEnum.ADD.getValue());
            newUserGradeDetail.setOperationReason("每日签到");
            userGradeDetailManager.save(newUserGradeDetail);

            User user = userManager.getById(userId);
            boolean isTrue = userManager.addGrowthValue(user.getId(), user.getGrowthValue(), growthValue);
            if (!isTrue) {
                throw new ServiceException(ResultCode.COMMON_MESSAGE);
            }
        } finally {
            redissonLock.release(RedisKeyUser.getLock("updateGrade", userId));
        }
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT, rollbackFor = Exception.class)
    public void addContentGrade(Long contentId) {

        ContentApiVO contentApiVO = contentClient.getContentById(contentId);
        if (Objects.isNull(contentApiVO)) {
            log.info("文案为空，不做成长值处理");
        }

        try {
            if (!redissonLock.lock(RedisKeyUser.getLock("updateGrade", contentApiVO.getUserId()), 0, 10)) {
                throw new ServiceException(ResultCode.COMMON_MESSAGE, "积分计算中，请稍后再试！");
            }
            UserGradeDetail userGradeDetail = userGradeDetailManager.getByContentId(contentId);
            if (Objects.nonNull(userGradeDetail)) {
                log.info("文案成长值已经发放过了");
                return;
            }

            UserGradeDetail saveUserGradeDetail = new UserGradeDetail();
            saveUserGradeDetail.setUserId(contentApiVO.getUserId());
            saveUserGradeDetail.setGrowthValue(20);
            saveUserGradeDetail.setGrowthValueSource(GrowthValueSourceEnum.PUBLISH_CONTENT.getValue());
            saveUserGradeDetail.setDeductionFlag(DeductionFlagEnum.ADD.getValue());
            saveUserGradeDetail.setOperationReason("发布文案");
            saveUserGradeDetail.setDataId(contentId.toString());
            userGradeDetailManager.save(saveUserGradeDetail);

            User user = userManager.getById(contentApiVO.getUserId());
            boolean isTrue = userManager.addGrowthValue(contentApiVO.getUserId(), user.getGrowthValue(), 20);
            if (!isTrue) {
                throw new ServiceException(ResultCode.COMMON_MESSAGE);
            }
        } finally {
            redissonLock.release(RedisKeyUser.getLock("updateGrade", contentApiVO.getUserId()));
        }
    }
}
