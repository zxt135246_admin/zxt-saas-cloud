package com.zxt.user.dao;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zxt.user.model.UserFourQuadrantsTask;

/**
 * @author zxt
 */
public interface UserFourQuadrantsTaskMapper extends BaseMapper<UserFourQuadrantsTask> {
}
