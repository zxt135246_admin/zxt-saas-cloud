package com.zxt.user.api.feignClient;

import com.zxt.bean.constants.ServiceNameConstant;
import com.zxt.bean.result.Result;
import com.zxt.user.api.req.OperateIntegralApiDTO;
import com.zxt.user.api.vo.*;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * @author zxt
 */
@FeignClient(value = ServiceNameConstant.ZXT_USER, path = "/feign/user")
public interface UserFeignClient {


    /**
     * 查询用户
     *
     * @param userId
     * @return
     */
    @GetMapping("getUserById")
    Result<UserApiVO> getUserById(@RequestParam Long userId);

    /**
     * 查询用户列表
     *
     * @param userIds
     * @return
     */
    @PostMapping("getByUserIds")
    Result<List<UserApiVO>> getByUserIds(@RequestBody List<Long> userIds);

    /**
     * 获取用户的粉丝列表
     *
     * @param concernUserId
     * @return
     */
    @GetMapping("getUserConcernByConcernUserId")
    Result<List<UserConcernApiVO>> getUserConcernByConcernUserId(@RequestParam("concernUserId") Long concernUserId);

    /**
     * 根据用户id和关注用户id获取
     *
     * @param userId
     * @param concernUserId
     * @return
     */
    @GetMapping(value = "getByUserIdAndConcernUserId")
    Result<UserConcernApiVO> getByUserIdAndConcernUserId(@RequestParam("userId") Long userId, @RequestParam("concernUserId") Long concernUserId);

    /**
     * 获取全用户列表
     *
     * @return
     */
    @GetMapping("getAllUser")
    Result<List<UserApiVO>> getAllUser();

    /**
     * 获取当前成长值所属的等级
     *
     * @param growthValue
     * @return
     */
    @GetMapping("getGradeByValueNum")
    Result<GradeApiVO> getGradeByValueNum(@RequestParam("growthValue") Integer growthValue);

    /**
     * 获取用户积分
     *
     * @param userId
     * @return
     */
    @GetMapping("getUserIntegral")
    Result<UserIntegralApiVO> getUserIntegral(@RequestParam("userId") Long userId);

    /**
     * 操作积分
     *
     * @param dto
     * @return
     */
    @PostMapping("operateIntegral")
    Result<Boolean> operateIntegral(@RequestBody OperateIntegralApiDTO dto);

    /**
     * 获取支付方式信息
     *
     * @param payMethodId
     * @return
     */
    @GetMapping("getPayMethodById")
    Result<PayMethodApiVO> getPayMethodById(@RequestParam("payMethodId") Long payMethodId);
}
