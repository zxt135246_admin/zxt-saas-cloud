package com.zxt.user.api.req;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.zxt.bean.convertor.LongJsonDeserializer;
import com.zxt.bean.convertor.LongJsonSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString(callSuper = true)
@ApiModel("操作积分api入参")
public class OperateIntegralApiDTO {

    @ApiModelProperty("userId")
    @JsonSerialize(using = LongJsonSerializer.class)
    @JsonDeserialize(using = LongJsonDeserializer.class)
    private Long userId;

    /**
     * 用户积分id
     */
    @ApiModelProperty("userIntegralId")
    @JsonSerialize(using = LongJsonSerializer.class)
    @JsonDeserialize(using = LongJsonDeserializer.class)
    private Long userIntegralId;

    /**
     * 操作类型 1.扣减 2新增
     */
    @ApiModelProperty("操作类型 1.新增 2.扣减")
    private Byte operateType;

    @ApiModelProperty("积分")
    private BigDecimal integral;

    @ApiModelProperty("订单号")
    private String orderCode;

    @ApiModelProperty("操作原因")
    private String operationReason;
}
