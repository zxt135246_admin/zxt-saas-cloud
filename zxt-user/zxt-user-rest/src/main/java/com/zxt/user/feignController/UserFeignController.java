package com.zxt.user.feignController;

import com.zxt.bean.result.Result;
import com.zxt.bean.utils.BeanUtils;
import com.zxt.user.api.feignClient.UserFeignClient;
import com.zxt.user.api.req.OperateIntegralApiDTO;
import com.zxt.user.api.vo.*;
import com.zxt.user.service.*;
import com.zxt.user.vo.UserConcernVO;
import com.zxt.user.vo.UserIntegralVO;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author zxt
 */
@RestController
@RequestMapping(value = "/feign/user")
@AllArgsConstructor
@Slf4j
public class UserFeignController implements UserFeignClient {

    private final UserService userService;
    private final UserConcernService userConcernService;
    private final UserGradeDetailService userGradeDetailService;
    private final GradeService gradeService;
    private final UserIntegralService userIntegralService;
    private final PayMethodService payMethodService;

    @Override
    @GetMapping("getUserById")
    public Result<UserApiVO> getUserById(@RequestParam Long userId) {
        return Result.newSuccess(BeanUtils.beanCopy(userService.getById(userId), UserApiVO.class));
    }

    @Override
    @PostMapping("getByUserIds")
    public Result<List<UserApiVO>> getByUserIds(@RequestBody List<Long> userIds) {
        return Result.newSuccess(BeanUtils.beanCopy(userService.getByIds(userIds), UserApiVO.class));
    }

    @Override
    @GetMapping("getUserConcernByConcernUserId")
    public Result<List<UserConcernApiVO>> getUserConcernByConcernUserId(@RequestParam("concernUserId") Long concernUserId) {
        List<UserConcernVO> userConcernVOList = userConcernService.getByConcernUserId(concernUserId);
        return Result.newSuccess(BeanUtils.beanCopy(userConcernVOList, UserConcernApiVO.class));
    }

    @Override
    @GetMapping(value = "getByUserIdAndConcernUserId")
    public Result<UserConcernApiVO> getByUserIdAndConcernUserId(@RequestParam("userId") Long userId, @RequestParam("concernUserId") Long concernUserId) {
        return Result.newSuccess(BeanUtils.beanCopy(userConcernService.getByUserIdAndConcernUserId(userId, concernUserId), UserConcernApiVO.class));
    }

    @Override
    @GetMapping("getAllUser")
    public Result<List<UserApiVO>> getAllUser() {
        return Result.newSuccess(BeanUtils.beanCopy(userService.getAllUser(), UserApiVO.class));
    }

    @Override
    @GetMapping("getGradeByValueNum")
    public Result<GradeApiVO> getGradeByValueNum(@RequestParam("growthValue") Integer growthValue) {
        return Result.newSuccess(BeanUtils.beanCopy(gradeService.getGradeByValueNum(growthValue), GradeApiVO.class));
    }

    @Override
    @GetMapping("getUserIntegral")
    public Result<UserIntegralApiVO> getUserIntegral(@RequestParam("userId") Long userId) {
        UserIntegralVO userIntegralVO = userIntegralService.getByUserId(userId);
        return Result.newSuccess(BeanUtils.beanCopy(userIntegralVO, UserIntegralApiVO.class));
    }

    @Override
    @PostMapping("operateIntegral")
    public Result<Boolean> operateIntegral(@RequestBody OperateIntegralApiDTO dto) {
        return Result.newSuccess(userIntegralService.operateIntegral(dto));
    }

    @Override
    @GetMapping("getPayMethodById")
    public Result<PayMethodApiVO> getPayMethodById(@RequestParam("payMethodId") Long payMethodId) {
        return Result.newSuccess(BeanUtils.beanCopy(payMethodService.getById(payMethodId), PayMethodApiVO.class));
    }
}
