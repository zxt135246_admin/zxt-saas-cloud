package com.zxt.user.controller;

import com.zxt.bean.result.Result;
import com.zxt.user.dto.UserAddressDTO;
import com.zxt.user.service.UserAddressService;
import com.zxt.user.vo.UserAddressVO;
import com.zxt.web.controller.CommonController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 请求处理层-用戶地址管理类
 */
@Slf4j
@RequestMapping("userAddress")
@RestController
@Api(tags = "用戶地址管理接口")
public class UserAddressController extends CommonController {

    private final UserAddressService userAddressService;

    @Autowired
    public UserAddressController(UserAddressService userAddressService) {
        this.userAddressService = userAddressService;
    }

    @ApiOperation(value = "创建/更新用戶地址", notes = "创建/更新用戶地址")
    @PutMapping("addOrUpdate")
    public Result addOrUpdate(@Validated @RequestBody UserAddressDTO dto) {
        dto.setUserId(getUser().getId());
        if (dto.getId() == null) {
            userAddressService.createUserAddress(dto);
        } else {
            userAddressService.updateUserAddress(dto);
        }
        return Result.newSuccess();
    }

    @ApiOperation(value = "删除用戶地址", notes = "删除用戶地址")
    @DeleteMapping("delete/{id}")
    public Result<Integer> delete(@ApiParam("id") @PathVariable Long id) {
        userAddressService.deleteUserAddress(id);
        return Result.newSuccess();
    }

    @ApiOperation(value = "获取用戶地址信息", notes = "根据用戶地址ID获取用戶地址信息")
    @GetMapping("get/{id}")
    public Result<UserAddressVO> get(@ApiParam("id") @PathVariable Long id) {
        return Result.newSuccess(userAddressService.getUserAddress(id, getUser().getId()));
    }

    @ApiOperation(value = "获取用戶地址列表信息", notes = "获取用戶地址列表信息")
    @PostMapping("list")
    public Result<List<UserAddressVO>> list() {
        return Result.newSuccess(userAddressService.getUserAddress(getUser().getId()));
    }


}
