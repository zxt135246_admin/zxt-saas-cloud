package com.zxt.user.controller;

import com.zxt.bean.result.Result;
import com.zxt.user.service.PayMethodService;
import com.zxt.user.vo.PayMethodVO;
import com.zxt.web.controller.CommonController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RequestMapping("payMethod")
@RestController
@AllArgsConstructor
@Api(tags = "支付方式管理接口")
public class PayMethodController extends CommonController {
    private PayMethodService payMethodService;

    @ApiOperation(value = "获取启用的支付方式", notes = "获取启用的支付方式")
    @PostMapping("getEnableList")
    public Result<List<PayMethodVO>> getEnableList() {
        List<PayMethodVO> payMethodVOList = payMethodService.getEnableList();
        return Result.newSuccess(payMethodVOList);
    }

}