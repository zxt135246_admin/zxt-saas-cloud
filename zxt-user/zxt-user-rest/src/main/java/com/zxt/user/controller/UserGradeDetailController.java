package com.zxt.user.controller;

import com.zxt.bean.result.Result;
import com.zxt.user.service.UserGradeDetailService;
import com.zxt.web.controller.CommonController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RequestMapping("userGradeDetail")
@RestController
@AllArgsConstructor
@Api(tags = "用户成长值详情记录管理接口")
public class UserGradeDetailController extends CommonController {

    private final UserGradeDetailService userGradeDetailService;

    @ApiOperation(value = "是否签到过", notes = "是否签到过")
    @PostMapping("isCheckIns")
    public Result<Boolean> isCheckIns() {
        return Result.newSuccess(userGradeDetailService.isCheckIns(getUser().getId()));
    }

    @ApiOperation(value = "每日签到", notes = "每日签到")
    @PostMapping("checkIns")
    public Result checkIns() {
        userGradeDetailService.checkIns(getUser().getId());
        return Result.newSuccess();
    }

}