package com.zxt.user.controller;

import com.zxt.bean.result.Result;
import com.zxt.user.dto.AddIntegralDTO;
import com.zxt.user.service.UserIntegralService;
import com.zxt.web.controller.CommonController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RequestMapping("userIntegral")
@RestController
@AllArgsConstructor
@Api(tags = "用户积分管理接口")
public class UserIntegralController extends CommonController {

    private final UserIntegralService userIntegralService;

    @ApiOperation(value = "发放积分", notes = "发放积分")
    @PostMapping("addIntegral")
    public Result addIntegral(@RequestBody AddIntegralDTO dto) {
        dto.setUserId(getUser().getId());
        userIntegralService.addIntegral(dto);
        return Result.newSuccess();
    }

}