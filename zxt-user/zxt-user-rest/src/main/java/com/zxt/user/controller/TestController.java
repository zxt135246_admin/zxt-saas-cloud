package com.zxt.user.controller;

import com.zxt.bean.exeption.ServiceException;
import com.zxt.bean.result.Result;
import com.zxt.bean.result.ResultCode;
import com.zxt.user.dto.UserFourQuadrantsTaskDTO;
import com.zxt.user.service.GradeService;
import com.zxt.user.service.UserFourQuadrantsTaskService;
import com.zxt.web.controller.CommonController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RequestMapping("test")
@RestController
@AllArgsConstructor
@Api(tags = "测试管理接口")
public class TestController extends CommonController {

    private final GradeService gradeService;
    private final UserFourQuadrantsTaskService userFourQuadrantsTaskService;

    @ApiOperation(value = "success", notes = "success")
    @GetMapping("success")
    public Result<String> success() {
        UserFourQuadrantsTaskDTO dto = new UserFourQuadrantsTaskDTO();
        dto.setUserId(1L);
        dto.setTaskType((byte) 1);
        dto.setTaskStatus((byte) 1);
        dto.setTaskContent("内容");
        userFourQuadrantsTaskService.addTask(dto);
        return Result.newSuccess("success");
    }

    @ApiOperation(value = "fail", notes = "fail")
    @GetMapping("fail")
    public Result<String> fail() {
        throw new ServiceException(ResultCode.COMMON_MESSAGE, "fail");
    }

}