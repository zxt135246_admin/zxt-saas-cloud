package com.zxt.user.controller;

import com.zxt.bean.annotation.PassLogin;
import com.zxt.bean.model.UserInfoVO;
import com.zxt.bean.result.Result;
import com.zxt.bean.utils.BeanUtils;
import com.zxt.redis.utils.UserUtil;
import com.zxt.user.service.UserService;
import com.zxt.user.vo.UserContentAssetVO;
import com.zxt.user.vo.UserMallAssetsVO;
import com.zxt.user.vo.UserVO;
import com.zxt.web.annotation.RepeatSubmit;
import com.zxt.web.container.UserContainer;
import com.zxt.web.controller.CommonController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author zxt
 */
@Slf4j
@RequestMapping("user")
@RestController
@AllArgsConstructor
@Api(tags = "用户管理接口")
public class UserController extends CommonController {

    private UserService userService;

    @ApiOperation(value = "获取用户信息", notes = "获取用户信息")
    @GetMapping("get")
    public Result<UserInfoVO> get() {
        UserInfoVO user = UserUtil.getUser(null);
        UserVO userVO = userService.getById(user.getId());
        UserInfoVO userInfoVO = BeanUtils.beanCopy(userVO, UserInfoVO.class);
        UserContainer.putUserInfo(userInfoVO);
        return Result.newSuccess(userInfoVO);
    }

    @ApiOperation(value = "获取所有用户信息", notes = "获取所有用户信息")
    @PostMapping("getAllUser")
    @PassLogin
    public Result<List<UserVO>> getAllUser() {
        List<UserVO> userVOList = userService.getAllUser();
        return Result.newSuccess(userVOList);
    }

    @ApiOperation(value = "修改用户名称", notes = "修改用户名称")
    @GetMapping("updateName")
    public Result updateName(@RequestParam("nickName") String nickName) {
        userService.updateNickName(getUser().getId(), nickName);
        return Result.newSuccess();
    }

    @ApiOperation(value = "获取和当前和用户互相关注的用户信息", notes = "获取和当前和用户互相关注的用户信息")
    @PostMapping("getMutualConcernUser")
    public Result<List<UserVO>> getMutualConcernUser() {
        List<UserVO> userVOList = userService.getMutualConcernUser(getUser().getId());
        return Result.newSuccess(userVOList);
    }

    @ApiOperation(value = "获取朋友列表", notes = "获取朋友列表")
    @GetMapping("getFriendList")
    public Result<List<UserVO>> getFriendList(@RequestParam("type") Byte type) {
        //type 1查询我的关注 2查询我的粉丝 3查询互关
        List<UserVO> userVOList = userService.getFriendList(type, getUser().getId());
        return Result.newSuccess(userVOList);
    }

    @ApiOperation(value = "获取用户资产信息", notes = "获取用户资产信息")
    @GetMapping("getUserAsset")
    public Result<UserContentAssetVO> getUserAsset() {
        return Result.newSuccess(userService.getUserAsset(getUser().getId()));
    }

    @ApiOperation(value = "关注用户", notes = "关注用户")
    @GetMapping("concern/{concernUserId}")
    @RepeatSubmit(message = "已经关注过了哦")
    public Result concern(@ApiParam("关注的用户id") @PathVariable Long concernUserId) {
        userService.concern(getUser().getId(), concernUserId);
        return Result.newSuccess();
    }

    @ApiOperation(value = "查询用户商城资产", notes = "查询用户商城资产")
    @GetMapping("getMallAssets")
    public Result<List<UserMallAssetsVO>> getMallAssets() {
        return Result.newSuccess(userService.getMallAssets(getUser().getId()));
    }
}