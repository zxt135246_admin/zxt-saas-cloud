package com.zxt.user.mq;

import com.alibaba.fastjson.JSON;
import com.zxt.bean.utils.LogUtils;
import com.zxt.content.api.MqConstant.ContentMqConstant;
import com.zxt.content.api.dto.CreateMessagesApiDTO;
import com.zxt.content.api.enums.CreateMessagesTypeEnum;
import com.zxt.lock.RedissonLock;
import com.zxt.rocketmq.annotation.RocketMqLog;
import com.zxt.user.constants.RedisKeyUser;
import com.zxt.user.service.UserGradeDetailService;
import com.zxt.user.service.UserIntegralService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * mq消息处理
 */
@Component
@Slf4j
@AllArgsConstructor
public class UserMq {

    private UserGradeDetailService userGradeDetailService;
    private UserIntegralService userIntegralService;
    private final RedissonLock redissonLock;

    @StreamListener(ContentMqConstant.CONTENT_CREATE_SUCCESS_CONSUME)
    @RocketMqLog(value = "文案/相册创建成功消息", name = "content->user")
    public void contentCreateMessages(@Payload CreateMessagesApiDTO dto, @Headers Map headers) {
        LogUtils.putAsyncTraceId(dto.getTraceId());
        log.info("消费消息messages={},headers={}", JSON.toJSONString(dto), headers);
        if (CreateMessagesTypeEnum.IMAGES.equal(dto.getMessagesType())) {
            log.info("无需处理");
        } else {
            log.info("增加用户成长值");
            userGradeDetailService.addContentGrade(dto.getDataId());
        }
    }

    @StreamListener(ContentMqConstant.CONTENT_ORDER_CREATE_FAIL_CONSUME)
    @RocketMqLog(value = "订单创建失败回滚积分消息", name = "content->user")
    public void contentOrderCreateFailMessages(String orderCode, @Headers Map headers) {
        log.info("订单创建失败回滚积分 消费消息 订单号={},headers={}", orderCode, headers);
        if (!redissonLock.lock(RedisKeyUser.getLock("orderCreateFail/integral", orderCode), 0, 3)) {
            return;
        }
        try {
            userIntegralService.cancelOrder(orderCode);
        } finally {
            redissonLock.release(RedisKeyUser.getLock("orderCreateFail/integral", orderCode));
        }
    }
}
