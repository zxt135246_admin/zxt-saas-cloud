package com.zxt.user.controller;

import com.zxt.bean.req.AddInterface;
import com.zxt.bean.result.Result;
import com.zxt.bean.utils.ValidateUtil;
import com.zxt.user.dto.UpdateStatusDTO;
import com.zxt.user.dto.UserFourQuadrantsTaskDTO;
import com.zxt.user.service.UserFourQuadrantsTaskService;
import com.zxt.web.controller.CommonController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RequestMapping("userFourQuadrantsTask")
@RestController
@AllArgsConstructor
@Api(tags = "用户四象限任务管理接口")
public class UserFourQuadrantsTaskController extends CommonController {
    private UserFourQuadrantsTaskService userFourQuadrantsTaskService;

    @ApiOperation(value = "新增任务", notes = "新增任务")
    @PostMapping("add")
    public Result add(@RequestBody UserFourQuadrantsTaskDTO dto) {
        Result validate = ValidateUtil.validate(dto, "参数错误", AddInterface.class);
        if (validate != null) {
            return validate;
        }
        dto.setUserId(getUser().getId());
        userFourQuadrantsTaskService.addTask(dto);
        return Result.newSuccess();
    }

    @ApiOperation(value = "修改任务状态", notes = "修改任务状态")
    @PostMapping("updateStatus")
    public Result updateStatus(@RequestBody UpdateStatusDTO dto) {
        userFourQuadrantsTaskService.updateStatus(dto);
        return Result.newSuccess();
    }

}