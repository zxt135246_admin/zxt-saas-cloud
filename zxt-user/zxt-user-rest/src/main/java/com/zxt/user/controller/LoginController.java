package com.zxt.user.controller;

import com.wf.captcha.ArithmeticCaptcha;
import com.zxt.bean.annotation.PassLogin;
import com.zxt.bean.constants.RedisKeyUser;
import com.zxt.bean.enums.LimitTypeEnums;
import com.zxt.bean.exeption.ServiceException;
import com.zxt.bean.model.UserInfoVO;
import com.zxt.bean.result.Result;
import com.zxt.bean.result.ResultCode;
import com.zxt.bean.utils.StringUtils;
import com.zxt.bean.utils.UuIdUtils;
import com.zxt.bean.utils.ip.IpUtil;
import com.zxt.redis.annotation.RateLimiter;
import com.zxt.redis.utils.RedisUtil;
import com.zxt.redis.utils.UserUtil;
import com.zxt.user.dto.LoginBodyDTO;
import com.zxt.user.dto.RegisterBodyDTO;
import com.zxt.user.dto.VisitorLoginDTO;
import com.zxt.user.model.User;
import com.zxt.user.service.UserService;
import com.zxt.web.controller.CommonController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Slf4j
@RequestMapping("/login")
@RestController
@Api(tags = "用户登录管理接口")
public class LoginController extends CommonController {

    @Value("${captcha.enable:false}")
    private Boolean enableCaptcha;

    private final UserService userService;

    @Autowired
    public LoginController(UserService userService) {
        this.userService = userService;
    }

    @ApiOperation(value = "获取验证码", notes = "获取验证码")
    @GetMapping("/captcha/code")
    @PassLogin
    @RateLimiter(count = 5, time = 60, limitType = LimitTypeEnums.IP)
    public Result code() {
        Map<String, String> data = new HashMap<>();
        String uuid = UuIdUtils.randomUUID(Boolean.TRUE);
        ArithmeticCaptcha captcha = new ArithmeticCaptcha(120, 40);
        // 获取运算的公式
        captcha.getArithmeticString();
        // 获取运算的结果
        String text = captcha.text();
        data.put("uuid", uuid);
        data.put("img", captcha.toBase64());
        // 验证码结果放到缓存中，30分钟有效
        RedisUtil.set(RedisKeyUser.getCodeKey(uuid), text, TimeUnit.MINUTES, 30);
        return Result.newSuccess(data);
    }

    @ApiOperation(value = "用户登录", notes = "用户登录")
    @PostMapping("/login")
    @PassLogin
    public Result login(@RequestBody LoginBodyDTO dto) {
        if (enableCaptcha) {
            // 验证码
            if (StringUtils.isBlank(dto.getUuid()) || StringUtils.isBlank(dto.getCode())) {
                return Result.newError(ResultCode.CAPTCHA_ERROR);
            }
            String codeKey = RedisKeyUser.getCodeKey(dto.getUuid());
            Object codeObject = RedisUtil.get(codeKey);
            if (codeObject == null) {
                return Result.newError(ResultCode.CAPTCHA_ERROR);
            }
            if (!dto.getCode().equals(codeObject.toString())) {
                return Result.newError(ResultCode.CAPTCHA_ERROR);
            }
            RedisUtil.delete(codeKey);
        }
        // 生成令牌
        UserInfoVO userInfoVO = userService.login(dto);

        //更新登录ip和登录时间
        User user = new User();
        user.setId(userInfoVO.getId());
        user.setLoginDate(LocalDateTime.now());
        user.setLoginIp(IpUtil.getIp(getRequest()));
        userService.updateById(user);

        UserUtil.setRedisUser(userInfoVO);
        return Result.newSuccess(userInfoVO.getToken());
    }

    @ApiOperation(value = "用户静默登录", notes = "用户静默登录")
    @GetMapping("/quickLogin")
    @PassLogin
    public Result quickLogin(@RequestParam(value = "openId", required = false) String openId) {
        if (StringUtils.isEmpty(openId)) {
            throw new ServiceException(ResultCode.API_INVLID_DATA);
        }
        // 生成令牌
        UserInfoVO userInfoVO = userService.quickLogin(openId);

        //更新登录ip和登录时间
        User user = new User();
        user.setId(userInfoVO.getId());
        user.setLoginDate(LocalDateTime.now());
        user.setLoginIp(IpUtil.getIp(getRequest()));
        userService.updateById(user);

        UserUtil.setRedisUser(userInfoVO);
        return Result.newSuccess(userInfoVO.getToken());
    }


    @ApiOperation(value = "游客登录", notes = "游客登录")
    @GetMapping("/visitorLogin")
    @PassLogin
    public Result visitorLogin() {
        UserInfoVO userInfoVO = new UserInfoVO();
        userInfoVO.setId(0L);
        userInfoVO.setIsVisitor(Boolean.TRUE);
        UserUtil.setRedisUser(userInfoVO);

        VisitorLoginDTO visitorLoginDTO = new VisitorLoginDTO();
        visitorLoginDTO.setLoginIp(IpUtil.getIp(getRequest()));
        visitorLoginDTO.setLoginDate(LocalDateTime.now());
        visitorLoginDTO.setLoginToken(userInfoVO.getToken());
        userService.visitorLogin(visitorLoginDTO);
        return Result.newSuccess(userInfoVO.getToken());
    }

    @ApiOperation(value = "用户注册", notes = "用户注册")
    @PostMapping("/register")
    @PassLogin
    public Result<Boolean> register(@Validated @RequestBody RegisterBodyDTO dto) {
        return Result.newSuccess(userService.register(dto));
    }
}
