package com.zxt.user.controller;

import com.zxt.bean.result.Result;
import com.zxt.user.service.GradeService;
import com.zxt.user.vo.GradeVO;
import com.zxt.web.controller.CommonController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RequestMapping("grade")
@RestController
@AllArgsConstructor
@Api(tags = "等级级数管理接口")
public class GradeController extends CommonController {
    private GradeService gradeService;

    @ApiOperation(value = "获取等级级数列表", notes = "获取等级级数列表")
    @PostMapping("list")
    public Result<List<GradeVO>> list() {
        List<GradeVO> gradeVOList = gradeService.list();
        return Result.newSuccess(gradeVOList);
    }

}