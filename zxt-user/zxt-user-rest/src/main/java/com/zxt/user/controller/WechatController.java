package com.zxt.user.controller;

import com.zxt.bean.annotation.PassLogin;
import com.zxt.user.service.WechatService;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RequestMapping("/wxApi")
@RestController
@AllArgsConstructor
public class WechatController {

    private final WechatService wechatService;

    @ApiOperation(value = "微信推送事件", notes = "微信推送事件")
    @GetMapping
    @PassLogin
    public String getWxEventInfo(@RequestParam String signature, @RequestParam String timestamp, @RequestParam String nonce, @RequestParam String echostr) {
        log.info("接收微信推送事件 入参：signature={}timestamp={}nonce={}echostr={}", signature, timestamp, nonce, echostr);
        String eventInfo = wechatService.getEventInfo(signature, timestamp, nonce, echostr);
        return eventInfo;
    }

    @ApiOperation(value = "微信回调数据", notes = "微信回调数据")
    @PostMapping
    @PassLogin
    public String getWxBackInfo(@RequestBody String xml, @RequestParam("msg_signature") String msgSignature, @RequestParam String signature, @RequestParam String timestamp, @RequestParam String nonce) {
        log.info("接收微信回调数据 入参：xml={}signature={}", xml, signature);
        String backInfo = wechatService.getBackInfo(xml, msgSignature, signature, timestamp, nonce);
        return backInfo;
    }
}
