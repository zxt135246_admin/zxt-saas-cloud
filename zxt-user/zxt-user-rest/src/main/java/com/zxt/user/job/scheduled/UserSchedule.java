package com.zxt.user.job.scheduled;

import com.zxt.bean.utils.LogUtils;
import com.zxt.user.service.UserTimeMailService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;


@Slf4j
@EnableScheduling
@Component
@EnableAsync
@AllArgsConstructor
public class UserSchedule {

    private final UserTimeMailService userTimeMailService;

    /**
     * 处理发送时间邮件
     *
     * @Async+@Scheduled 定时任务不阻塞功能，让定时任务开异步线程执行
     */
    @Async
    @Scheduled(cron = "0 0/1 * * * ?")
    public void dealSendUserTimeMail() {
        LogUtils.putJobTraceId();
        log.info("定时处理发送时间邮件start..........");
        userTimeMailService.dealSendUserTimeMail();
        log.info("定时处理发送时间邮件end..........");
    }

}

