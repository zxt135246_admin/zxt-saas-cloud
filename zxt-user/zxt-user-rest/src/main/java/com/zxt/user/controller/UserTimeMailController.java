package com.zxt.user.controller;

import com.zxt.bean.result.Result;
import com.zxt.user.dto.UserTimeMailDTO;
import com.zxt.user.service.UserGradeDetailService;
import com.zxt.user.service.UserTimeMailService;
import com.zxt.web.controller.CommonController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RequestMapping("userTimeMail")
@RestController
@AllArgsConstructor
@Api(tags = "用户时间邮件管理接口")
public class UserTimeMailController extends CommonController {

    private final UserTimeMailService userTimeMailService;

    @ApiOperation(value = "创建新的时间邮件", notes = "创建新的时间邮件")
    @PostMapping("add")
    public Result add(@RequestBody UserTimeMailDTO dto) {
        dto.setUserId(getUser().getId());
        userTimeMailService.add(dto);
        return Result.newSuccess();
    }


}