package com.zxt.user;

import com.zxt.bean.utils.FileUtils;
import com.zxt.user.utils.QRCodeGeneratorUtil;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.IOException;
import java.net.URL;

@SpringBootTest
@Slf4j
public class FileTest {


    /**
     * 为图片添加水印
     */
    @Test
    public void addWaterSeal() throws IOException {
        //创建一个URL对象,获取网络图片的地址信息
        URL url = new URL("https://copywritings.oss-accelerate.aliyuncs.com/cloud/content/images/1/洛阳/original/d9804e8bd2f14c4e8c6106363a2f71d1.png");
        //将URL对象输入流转化为图片对象 (url.openStream()方法，获得一个输入流)
        Image srcImg = ImageIO.read(url.openStream());
        FileUtils.addWatermark(srcImg);
    }

    /**
     * 生成二维码
     */
    @Test
    public void generateQRCode() throws IOException {
        String data = "http://101.35.246.250/static/page/login.html"; // 要存储在QR码中的数据
        int width = 300; // QR码的宽度
        int height = 300; // QR码的高度
        QRCodeGeneratorUtil.generateQRCode(data, width, height);
    }
}
