package com.zxt.operation.mq.channel;

import com.zxt.content.api.MqConstant.ContentMqConstant;
import com.zxt.operation.api.constant.OperationMqConstant;
import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;

/**
 * 消费者Channel
 */
public interface OperationSink {
    /**
     * 文案创建成功消费者
     *
     * @return
     */
    @Input(ContentMqConstant.CONTENT_CREATE_SUCCESS_CONSUME)
    SubscribableChannel contentCreateSuccessConsume();
}
