package com.zxt.operation.mq;

import com.alibaba.fastjson.JSON;
import com.zxt.bean.utils.LogUtils;
import com.zxt.content.api.MqConstant.ContentMqConstant;
import com.zxt.content.api.dto.CreateMessagesApiDTO;
import com.zxt.operation.service.ISystemNewsService;
import com.zxt.rocketmq.annotation.RocketMqLog;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @zxt
 */
@Component
@Slf4j
@AllArgsConstructor
public class OperationMQ {
    private final ISystemNewsService systemNewsService;

    @StreamListener(ContentMqConstant.CONTENT_CREATE_SUCCESS_CONSUME)
    @RocketMqLog(value = "文案/相册创建成功消息", name = "content->operation")
    public void contentCreateMessages(@Payload CreateMessagesApiDTO dto, @Headers Map headers) {
        LogUtils.putAsyncTraceId(dto.getTraceId());
        log.info("消费消息messages={},headers={}", JSON.toJSONString(dto), headers);
        systemNewsService.dealCreateContentNews(dto);
        log.info("推送消息通知成功");
    }
}
