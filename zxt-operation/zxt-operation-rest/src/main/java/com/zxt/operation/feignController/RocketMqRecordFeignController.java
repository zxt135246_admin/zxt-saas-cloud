package com.zxt.operation.feignController;

import com.alibaba.fastjson.JSON;
import com.zxt.bean.result.Result;
import com.zxt.bean.utils.BeanUtils;
import com.zxt.operation.service.RocketMqRecordService;
import com.zxt.rocketmq.feignClient.RocketMqRecordFeignClient;
import com.zxt.rocketmq.req.RocketMqRecordDTO;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author zxt
 */
@RestController
@RequestMapping(value = "/feign/rocketMqRecord")
@AllArgsConstructor
@Slf4j
public class RocketMqRecordFeignController implements RocketMqRecordFeignClient {

    private final RocketMqRecordService rocketMqRecordService;

    @PostMapping({"/add"})
    public Result<Long> add(@RequestBody RocketMqRecordDTO dto) {
        log.info("保存mq消息:{}", JSON.toJSONString(dto));
        rocketMqRecordService.add(BeanUtils.beanCopy(dto, com.zxt.operation.dto.RocketMqRecordDTO.class));
        return Result.newSuccess();
    }
}
