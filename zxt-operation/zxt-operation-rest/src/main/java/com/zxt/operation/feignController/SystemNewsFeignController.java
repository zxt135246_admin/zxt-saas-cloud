package com.zxt.operation.feignController;

import com.zxt.bean.result.Result;
import com.zxt.bean.utils.BeanUtils;
import com.zxt.operation.api.dto.SystemNewsApiDTO;
import com.zxt.operation.api.feignClient.SystemNewsFeignClient;
import com.zxt.operation.dto.SystemNewsDTO;
import com.zxt.operation.service.ISystemNewsService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RestController
@RequestMapping(value = "/feign/systemNews")
@AllArgsConstructor
public class SystemNewsFeignController implements SystemNewsFeignClient {

    private final ISystemNewsService systemNewsService;

    @Override
    @PostMapping(value = "saveSystemNews")
    public Result saveSystemNews(@RequestBody SystemNewsApiDTO apiDTO) {
        systemNewsService.createSystemNews(BeanUtils.beanCopy(apiDTO, SystemNewsDTO.class));
        return Result.newSuccess();
    }

    @Override
    @PostMapping(value = "saveBatchSystemNews")
    public Result saveBatchSystemNews(@RequestBody List<SystemNewsApiDTO> dtoList) {
        systemNewsService.saveBatchSystemNews(BeanUtils.beanCopy(dtoList, SystemNewsDTO.class));
        return Result.newSuccess();
    }
}
