package com.zxt.operation.controller;

import com.zxt.bean.result.Result;
import com.zxt.operation.dto.SystemQuestionDTO;
import com.zxt.operation.service.ISystemQuestionService;
import com.zxt.web.controller.CommonController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 请求处理层-系统反馈管理类
 *
 * @author zxt
 */
@Slf4j
@RequestMapping("systemQuestion")
@RestController
@Api(tags = "系统反馈管理接口")
public class SystemQuestionController extends CommonController {

    private final ISystemQuestionService systemQuestionService;

    @Autowired
    public SystemQuestionController(ISystemQuestionService systemQuestionService) {
        this.systemQuestionService = systemQuestionService;
    }

    @ApiOperation(value = "新增系统反馈", notes = "新增系统反馈")
    @PostMapping("add")
    public Result add(@Validated @RequestBody SystemQuestionDTO dto) {
        dto.setUserId(getUser().getId());
        systemQuestionService.add(dto);
        return Result.newSuccess();
    }


}
