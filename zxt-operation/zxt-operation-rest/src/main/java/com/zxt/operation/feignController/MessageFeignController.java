package com.zxt.operation.feignController;

import com.zxt.bean.result.Result;
import com.zxt.operation.api.dto.SendEmailApiDTO;
import com.zxt.operation.api.dto.SmsSendApiDTO;
import com.zxt.operation.api.feignClient.MessageFeignClient;
import com.zxt.operation.service.IMessageService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping(value = "/feign/message")
@RefreshScope
public class MessageFeignController implements MessageFeignClient {

    private final IMessageService messageService;

    @Autowired
    public MessageFeignController(IMessageService messageService) {
        this.messageService = messageService;
    }

    /**
     * 发送短信
     *
     * @param dto 入参
     */
    @Override
    @PostMapping("sendSms")
    public Result sendSms(@Validated @RequestBody SmsSendApiDTO dto) {
        messageService.sendMessage(dto);
        return Result.newSuccess();
    }

    /**
     * 发送邮件
     *
     * @param dto
     */
    @Override
    @PostMapping("sendEmail")
    public Result<Boolean> sendEmail(@Validated @RequestBody SendEmailApiDTO dto) {
        messageService.sendEmail(dto);
        return Result.newSuccess(Boolean.TRUE);
    }

}
