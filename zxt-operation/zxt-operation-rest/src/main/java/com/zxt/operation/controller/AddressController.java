package com.zxt.operation.controller;

import com.zxt.bean.result.Result;
import com.zxt.operation.service.AddressService;
import com.zxt.operation.vo.AddressVO;
import com.zxt.web.controller.CommonController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


/**
 * 请求处理层-地址管理类
 */
@Slf4j
@RequestMapping("address")
@RestController
@Api(tags = "地址管理接口")
public class AddressController extends CommonController {

    private final AddressService addressService;

    @Autowired
    public AddressController(AddressService addressService) {
        this.addressService = addressService;
    }


    @ApiOperation(value = "根据父编码获取地址信息", notes = "根据父编码获取地址信息")
    @GetMapping("getByParentCode/{code}")
    public Result<List<AddressVO>> getByParentCode(@ApiParam("编码") @PathVariable String code) {
        return Result.newSuccess(addressService.getByParentCode(code));
    }

}
