package com.zxt.operation.controller;

import com.zxt.bean.query.QueryResultVO;
import com.zxt.bean.result.Result;
import com.zxt.operation.dto.SystemNewsDTO;
import com.zxt.operation.dto.SystemNewsQueryDTO;
import com.zxt.operation.service.ISystemNewsService;
import com.zxt.operation.vo.SystemNewsVO;
import com.zxt.web.controller.CommonController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 请求处理层-系统消息管理类
 */
@Slf4j
@RequestMapping("systemNews")
@RestController
@Api(tags = "系统消息管理接口")
public class SystemNewsController extends CommonController {

    private final ISystemNewsService systemNewsService;

    @Autowired
    public SystemNewsController(ISystemNewsService systemNewsService) {
        this.systemNewsService = systemNewsService;
    }

    @ApiOperation(value = "发起系统通知", notes = "发起系统消息")
    @PostMapping("addSystemNews")
    public Result addSystemNews(@RequestBody SystemNewsDTO dto) {
        systemNewsService.addSystemNews(dto);
        return Result.newSuccess();
    }

    @ApiOperation(value = "获取系统消息信息", notes = "根据系统消息ID获取系统消息信息")
    @GetMapping("get/{id}")
    public Result<SystemNewsVO> get(@ApiParam("id") @PathVariable Long id) {
        return Result.newSuccess(systemNewsService.getSystemNews(id));
    }

    @ApiOperation(value = "获取系统消息列表信息", notes = "分页获取系统消息列表信息")
    @PostMapping("query")
    public Result<QueryResultVO<SystemNewsVO>> querySystemNews(@Validated @RequestBody SystemNewsQueryDTO systemNewsQueryDTO) {
        systemNewsQueryDTO.setUserId(getUser().getId());
        return Result.newSuccess(systemNewsService.querySystemNews(systemNewsQueryDTO));
    }

    @ApiOperation(value = "更新为已读", notes = "更新为已读")
    @PostMapping("updateIsRead")
    public Result updateIsRead(@RequestBody List<Long> newsIds) {
        systemNewsService.updateIsRead(null, newsIds);
        return Result.newSuccess();
    }

    @ApiOperation(value = "更新全部为已读", notes = "更新全部为已读")
    @PostMapping("updateAllIsRead")
    public Result updateAllIsRead() {
        systemNewsService.updateAllIsRead(null);
        return Result.newSuccess();
    }


}
