package com.zxt.operation.feignController;

import com.alibaba.fastjson.JSON;
import com.zxt.bean.result.Result;
import com.zxt.bean.utils.LogUtils;
import com.zxt.operation.api.dto.ImagesApiDTO;
import com.zxt.operation.api.feignClient.OssFeignClient;
import com.zxt.operation.asyncs.ImagesAsync;
import com.zxt.operation.service.OssService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * @author zxt
 */
@RestController
@RequestMapping(value = "/feign/oss")
@AllArgsConstructor
@Slf4j
public class OssFeignController implements OssFeignClient {

    private final OssService ossService;
    private final ImagesAsync imagesAsync;

    @Override
    @PostMapping(value = "uploadImage", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public Result<Boolean> uploadImage(@RequestPart("file") MultipartFile multiFile, @RequestParam("directory") String directory, @RequestParam("imageId") Long imageId) {
        log.info("文件:{},文件目录:{},id:{}", multiFile, directory, imageId);
        return Result.newSuccess(ossService.uploadImage(multiFile, directory, imageId));
    }

    @Override
    @PostMapping(value = "uploadImageByUrl")
    public Result uploadImageByUrl(@RequestParam("url") String url, @RequestParam("directory") String directory, @RequestParam("imageId") Long imageId) {
        log.info("文件url:{},文件目录:{},id:{}", url, directory, imageId);
        ossService.uploadImageByUrl(url, directory, imageId);
        return Result.newSuccess();
    }

    @Override
    @PostMapping(value = "initImagesExif")
    public Result initImagesExif(@RequestBody List<ImagesApiDTO> imagesList) {
        log.info("批量初始化相册exif,入参:{}", JSON.toJSONString(imagesList));
        imagesAsync.initImagesExif(imagesList, LogUtils.getTraceId());
        return Result.newSuccess();
    }
}
