package com.zxt.operation.controller;

import com.zxt.bean.result.Result;
import com.zxt.operation.vo.server.Server;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 服务器监控
 *
 * @author zxt
 */
@RestController
@RequestMapping("/monitor/server")
@Api(tags = "服务器监控")
public class ServerController {

    @GetMapping()
    @ApiOperation(value = "服务器信息", notes = "服务器信息")
    public Result getInfo() throws Exception {
        Server server = new Server();
        server.copyTo();
        return Result.newSuccess(server);
    }
}
