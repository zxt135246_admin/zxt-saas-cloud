package com.zxt.operation.controller;

import com.alibaba.fastjson.JSON;
import com.zxt.bean.exeption.ServiceException;
import com.zxt.bean.result.Result;
import com.zxt.bean.result.ResultCode;
import com.zxt.content.api.dto.ImagesExifApiDTO;
import com.zxt.operation.service.OssService;
import com.zxt.operation.utils.ExifUtils;
import com.zxt.bean.utils.FileUtils;
import com.zxt.web.annotation.RepeatSubmit;
import com.zxt.web.controller.CommonController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;

@Slf4j
@RequestMapping("oss")
@RestController
@AllArgsConstructor
@Api(tags = "oss管理接口")
public class OssController extends CommonController {

    private final OssService ossService;


    @ApiOperation(value = "上传", notes = "上传")
    @PostMapping("/upload")
    @RepeatSubmit(interval = 1000 * 60 * 10, message = "请勿重复上传")
    public Result upload(@ApiParam("文件") @RequestParam("file") MultipartFile file) {
        String url = null;
        try {
            url = ossService.upload(file);
        } catch (Exception e) {
            log.error("上传图片报错,异常信息:{}", e);
            throw new ServiceException(ResultCode.COMMON_MESSAGE, "请上传正确格式的照片");
        }
        return Result.newSuccess(url);
    }

    @ApiOperation(value = "提取相册Exif信息", notes = "提取相册Exif信息")
    @PostMapping("/extractExif")
    public Result test(@ApiParam("文件") @RequestParam("file") MultipartFile file) throws Exception {
        File f = FileUtils.multipartFileToFile(file);
        ImagesExifApiDTO imagesExifApiDTO = ExifUtils.readImageInfo(f);
        //会在项目目录下自动生成一个临时的file文件
        FileUtils.deleteFile(f);
        return Result.newSuccess(JSON.toJSONString(imagesExifApiDTO));
    }

    @ApiOperation(value = "根据Url上传", notes = "根据Url上传")
    @PostMapping("/uploadByUrl")
    @RepeatSubmit(interval = 1000 * 60 * 10, message = "请勿重复上传")
    public Result uploadByUrl(@ApiParam("文件") @RequestParam("file") String url) {
        String respUrl = null;
        try {
            respUrl = ossService.uploadImageByUrl(url);
        } catch (Exception e) {
            log.error("上传图片报错,异常信息:{}", e);
            throw new ServiceException(ResultCode.COMMON_MESSAGE, "请上传正确格式的照片");
        }
        return Result.newSuccess(respUrl);
    }
}
