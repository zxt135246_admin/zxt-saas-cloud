package com.zxt.operation;

import com.alibaba.fastjson.JSON;
import com.zxt.bean.executor.CatchHandleExecutor;
import com.zxt.bean.executor.strategy.AbstractHandleStrategy;
import com.zxt.operation.api.dto.SmsSendApiDTO;
import com.zxt.operation.executor.context.HandleContext;
import com.zxt.operation.executor.factory.HandleStrategyFactory;
import com.zxt.operation.model.SystemQuestion;
import com.zxt.operation.service.IMessageService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.HashMap;

@SpringBootTest
public class SmsTest {

    @Autowired
    private IMessageService messageService;

    @Test
    public void sms() {
        SmsSendApiDTO dto = new SmsSendApiDTO();
        dto.setMobile("18538880965");
        dto.setSignName("啸天服务");
        dto.setTemplateCode("206536932");
        HashMap<String, Object> map = new HashMap<>();
        map.put("code", "123456");
        dto.setTemplateParam(JSON.toJSONString(map));
        messageService.sendMessage(dto);
    }
}
