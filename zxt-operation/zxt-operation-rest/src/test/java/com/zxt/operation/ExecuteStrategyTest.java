package com.zxt.operation;

import com.zxt.bean.executor.CatchHandleExecutor;
import com.zxt.bean.executor.strategy.AbstractHandleStrategy;
import com.zxt.operation.executor.context.HandleContext;
import com.zxt.operation.executor.factory.HandleStrategyFactory;
import com.zxt.operation.model.SystemQuestion;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class ExecuteStrategyTest {

    @Test
    public void testExecuteRun() {
        /**
         * 创建上下文
         */
        HandleContext context = new HandleContext();
        SystemQuestion question = new SystemQuestion();
        question.setId(1L);
        question.setQuestionContent("提议内容");
        context.setParams(question);
        /**
         * 通过策略工厂创建出想要的策略
         */
        AbstractHandleStrategy strategy = HandleStrategyFactory.createCheckUser();
        /**
         * 通过执行器执行策略
         */
        new CatchHandleExecutor(strategy).applyContext(context).execute();
    }
}
