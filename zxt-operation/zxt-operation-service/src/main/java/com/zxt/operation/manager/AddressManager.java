package com.zxt.operation.manager;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zxt.operation.dao.AddressMapper;
import com.zxt.operation.model.Address;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 通用逻辑层-地址管理类
 */
@Slf4j
@Service
public class AddressManager extends ServiceImpl<AddressMapper, Address> {


    /**
     * 根据地址编码查询地址信息
     *
     * @param codes
     * @return
     */
    public List<Address> getByAddressCodes(List<String> codes) {
        LambdaQueryWrapper<Address> queryWrapper = Address.gw().in(Address::getAddressCode, codes);
        return list(queryWrapper);
    }

    public List<Address> getByParentCode(String parentCode) {
        LambdaQueryWrapper<Address> wrapper = Address.gw().eq(Address::getParentCode, parentCode);
        wrapper.orderByAsc(Address::getSortNumber);
        return list(wrapper);
    }
}