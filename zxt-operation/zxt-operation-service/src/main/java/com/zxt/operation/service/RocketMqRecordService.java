package com.zxt.operation.service;


import com.zxt.operation.dto.RocketMqRecordDTO;

/**
 * mq消息记录业务管理
 *
 * @author zxt
 */
public interface RocketMqRecordService {


    /**
     * 保存消息
     *
     * @param dto
     */
    void add(RocketMqRecordDTO dto);
}
