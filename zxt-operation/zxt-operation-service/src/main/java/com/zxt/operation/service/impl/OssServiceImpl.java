package com.zxt.operation.service.impl;

import com.zxt.bean.exeption.ServiceException;
import com.zxt.bean.result.ResultCode;
import com.zxt.bean.utils.UuIdUtils;
import com.zxt.content.api.dto.ImagesApiDTO;
import com.zxt.content.api.dto.ImagesExifApiDTO;
import com.zxt.operation.client.ContentClient;
import com.zxt.operation.client.OssClient;
import com.zxt.operation.service.OssService;
import com.zxt.operation.utils.ExifUtils;
import com.zxt.bean.utils.FileUtils;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.time.StopWatch;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.Objects;

@Slf4j
@Service
@AllArgsConstructor
public class OssServiceImpl implements OssService {

    private final OssClient ossClient;
    private final ContentClient contentClient;


    @Override
    public Boolean uploadImage(MultipartFile multiFile, String directory, Long imageId) {

        File file = FileUtils.multipartFileToFile(multiFile);
        ImagesExifApiDTO imagesExifApiDTO = null;
        try {
            imagesExifApiDTO = ExifUtils.readImageInfo(file);
            imagesExifApiDTO.setImagesId(imageId);
            contentClient.saveOrUpdateImagesExif(imagesExifApiDTO);
        } catch (Exception e) {
            log.error("提取相册Exif异常,异常原因:{}", e);
        } finally {
            //会在项目目录下自动生成一个临时的file文件
            FileUtils.deleteFile(file);
        }

        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        String fileName = UuIdUtils.randomUUID(Boolean.TRUE);
        String unZipUrl = ossClient.uploadImage(multiFile, directory + "original/", fileName);
        stopWatch.stop();
        log.info("上传原图片耗时:{},相册id:{}", stopWatch.getTime(), imageId);

        stopWatch.reset();
        stopWatch.start();
        String zipUrl = ossClient.uploadZipImage(multiFile, directory, fileName);
        stopWatch.stop();
        log.info("上传压缩图片耗时:{},相册id:{}", stopWatch.getTime(), imageId);

        ImagesApiDTO dto = new ImagesApiDTO();
        dto.setId(imageId);
        dto.setImageUrl(zipUrl);
        dto.setOriginalImageUrl(unZipUrl);
        if (Objects.nonNull(imagesExifApiDTO)) {
            dto.setShootingAddress(imagesExifApiDTO.getShootingAddress());
        }

        Boolean isTrue = contentClient.updateImagesById(dto);
        if (!isTrue) {
            throw new ServiceException(ResultCode.COMMON_MESSAGE, "更新照片信息失败");
        }
        return Boolean.TRUE;
    }

    @Override
    public void uploadImageByUrl(String url, String directory, Long imageId) {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        String unZipUrl = ossClient.uploadImageByUrl(url, directory + "original/");
        stopWatch.stop();
        log.info("上传原图片耗时:{}", stopWatch.getTime());

        stopWatch.reset();
        stopWatch.start();
        String zipUrl = ossClient.uploadZipImageByUrl(url, directory);
        stopWatch.stop();
        log.info("上传压缩图片耗时:{}", stopWatch.getTime());

        ImagesApiDTO dto = new ImagesApiDTO();
        dto.setId(imageId);
        dto.setImageUrl(zipUrl);
        dto.setOriginalImageUrl(unZipUrl);

        Boolean isTrue = contentClient.updateImagesById(dto);
        if (!isTrue) {
            throw new ServiceException(ResultCode.COMMON_MESSAGE, "更新照片信息失败");
        }
    }

    @Override
    public String upload(MultipartFile file) {
        return ossClient.upload(file);
    }

    @Override
    public String uploadImageByUrl(String url) {
        return ossClient.uploadByUrl(url);
    }

    @Override
    public String downloadFile(String fileUrl) {
        return ossClient.downloadFile(fileUrl);
    }
}
