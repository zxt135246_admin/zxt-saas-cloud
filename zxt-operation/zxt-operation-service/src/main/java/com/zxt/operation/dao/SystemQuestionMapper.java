package com.zxt.operation.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zxt.operation.model.SystemQuestion;

/**
 * 数据持久层-系统反馈管理类
 *
 * @author zxt
 */
public interface SystemQuestionMapper extends BaseMapper<SystemQuestion> {


}