package com.zxt.operation.sms.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Getter
@Setter
@ConfigurationProperties("sms")
public class SmsConfig {

    /**
     * 是否开启
     */
    private Boolean open = Boolean.FALSE;

    /**
     * 生效服务
     */
    private String active = "aliyun";
    /**
     * 阿里云
     */
    private Aliyun aliyun;

    @Getter
    @Setter
    public static class Aliyun {
        /**
         * 域名
         */
        private String domain = "dysmsapi.aliyuncs.com";
        /**
         * 版本
         */
        private String version = "2017-05-25";
        /**
         * 发送短信
         */
        private String action = "SendSms";
        /**
         * 查询短信发送详情
         */
        private String queryAction = "QuerySendDetails";

        /**
         * API支持的地域ID
         */
        private String regionId = "cn-hangzhou";

        /**
         * aliyun accessKeyId
         */
        private String accessKeyId;

        /**
         * aliyun AccessKey secret
         */
        private String secret;
    }

}
