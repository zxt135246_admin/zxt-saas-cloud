package com.zxt.operation.client;

import com.aliyun.oss.ClientException;
import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.aliyun.oss.OSSException;
import com.aliyun.oss.model.GetObjectRequest;
import com.aliyun.oss.model.PutObjectRequest;
import com.zxt.bean.utils.StringUtils;
import com.zxt.bean.utils.UuIdUtils;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.coobird.thumbnailator.Thumbnails;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.URL;
import java.time.LocalDate;

/**
 * @author zxt
 */
@Component
@AllArgsConstructor
@Slf4j
@NoArgsConstructor
@RefreshScope
public class OssClient {

    @Value("${storage.aliyun.url:}")
    private String ossUrl;

    @Value("${storage.aliyun.endpoint:}")
    private String endpoint;

    @Value("${storage.aliyun.bucketName:}")
    private String bucket;

    @Value("${storage.aliyun.accessKeyId:}")
    private String accessId;

    @Value("${storage.aliyun.accessKeySecret:}")
    private String secretKey;

    /**
     * oss图片存储的目录
     */
    private final String imageDirectory = "cloud/content/images/";

    /**
     * oss图片存储的目录
     */
    private final String operationDirectory = "cloud/content/operation/";


    /**
     * 上传压缩图片 .jpg
     *
     * @param multipartFile
     * @param directory
     * @param fileName
     * @return
     * @throws OSSException
     * @throws ClientException
     */
    public String uploadZipImage(MultipartFile multipartFile, String directory, String fileName) throws OSSException, ClientException {
        // 创建OSSClient实例。
        OSS ossClient = new OSSClientBuilder().build(this.endpoint, this.accessId, this.secretKey);
        try {
            if (multipartFile.getSize() != 0 && !"".equals(multipartFile.getName())) {
                InputStream multipartFileInputStream = compress(multipartFile);
                //拼接上传后的目录
                StringBuilder builder = new StringBuilder(imageDirectory);
                if (StringUtils.isNotBlank(directory)) {
                    builder.append(directory);
                } else {
                    builder.append(LocalDate.now().toString() + "/");
                }
                if (StringUtils.isBlank(fileName)) {
                    builder.append(UuIdUtils.randomUUID(Boolean.TRUE));
                } else {
                    builder.append(fileName);
                }
                builder.append(".jpg");
                String fileUrl = builder.toString();
                PutObjectRequest putObjectRequest = new PutObjectRequest(bucket, fileUrl, multipartFileInputStream);
                ossClient.putObject(putObjectRequest);
                return ossUrl + fileUrl;
            }
        } catch (Exception e) {
            log.error("上传oss导入图片失败:{}", e);
        } finally {
            // 关闭流
            ossClient.shutdown();
        }
        return null;
    }

    /**
     * 上传原图图片
     *
     * @param multipartFile
     * @param directory
     * @param fileName
     * @return
     * @throws OSSException
     * @throws ClientException
     */
    public String uploadImage(MultipartFile multipartFile, String directory, String fileName) throws OSSException, ClientException {
        // 创建OSSClient实例。
        OSS ossClient = new OSSClientBuilder().build(this.endpoint, this.accessId, this.secretKey);
        try {
            if (multipartFile.getSize() != 0 && !"".equals(multipartFile.getName())) {
                InputStream multipartFileInputStream = multipartFile.getInputStream();
                //拼接上传后的目录
                StringBuilder builder = new StringBuilder(imageDirectory);
                if (StringUtils.isNotBlank(directory)) {
                    builder.append(directory);
                } else {
                    builder.append(LocalDate.now().toString() + "/");
                }
                if (StringUtils.isBlank(fileName)) {
                    builder.append(UuIdUtils.randomUUID(Boolean.TRUE));
                } else {
                    builder.append(fileName);
                }
                builder.append(".png");
                String fileUrl = builder.toString();
                PutObjectRequest putObjectRequest = new PutObjectRequest(bucket, fileUrl, multipartFileInputStream);
                ossClient.putObject(putObjectRequest);
                return ossUrl + fileUrl;
            }
        } catch (Exception e) {
            log.error("上传oss导入图片失败:{}", e);
        } finally {
            // 关闭流
            ossClient.shutdown();
        }
        return null;
    }


    public String upload(MultipartFile multipartFile) {
        // 创建OSSClient实例。
        OSS ossClient = new OSSClientBuilder().build(this.endpoint, this.accessId, this.secretKey);
        try {
            if (multipartFile.getSize() != 0 && !"".equals(multipartFile.getName())) {
                InputStream multipartFileInputStream = multipartFile.getInputStream();
                //拼接上传后的目录
                StringBuilder builder = new StringBuilder(operationDirectory);
                builder.append(LocalDate.now().toString() + "/");
                builder.append(UuIdUtils.randomUUID(Boolean.TRUE));
                builder.append(".png");
                String fileUrl = builder.toString();
                PutObjectRequest putObjectRequest = new PutObjectRequest(bucket, fileUrl, multipartFileInputStream);
                ossClient.putObject(putObjectRequest);
                return ossUrl + fileUrl;
            }
        } catch (Exception e) {
            log.error("上传oss导入图片失败:{}", e);
        } finally {
            // 关闭流
            ossClient.shutdown();
        }
        return null;
    }

    private ByteArrayInputStream compress(MultipartFile multipartFile) throws IOException {
        byte[] bytes = multipartFile.getBytes();
        //压缩
        BufferedImage image = Thumbnails.of(new ByteArrayInputStream(bytes)).size(500, 400).asBufferedImage();
        //设置转换规格，颜色空间,传入图片imga中的wh参数
        BufferedImage newBufferedImage = new BufferedImage(image.getWidth(), image.getHeight(), BufferedImage.TYPE_INT_RGB);
        //使用新图片缓冲流生成图片
        newBufferedImage.createGraphics().drawImage(image, 0, 0, Color.WHITE, null);
        //新建一个输出流
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        //将数据写入输出流
        ImageIO.write(newBufferedImage, "jpg", os);
        //将输出流转换为字节码传入上面准备好的输入流，生成输入流，这样可以直接被阿里云读取
        return new ByteArrayInputStream(os.toByteArray());
    }

    public String uploadImageByUrl(String reqUrl, String directory) {
        // 创建OSSClient实例。
        OSS ossClient = new OSSClientBuilder().build(this.endpoint, this.accessId, this.secretKey);
        try {
            //通过图片的url创建输入流
            InputStream multipartFileInputStream = new URL(reqUrl).openStream();
            //拼接上传后的目录
            StringBuilder builder = new StringBuilder(imageDirectory);
            if (StringUtils.isNotBlank(directory)) {
                builder.append(directory);
            } else {
                builder.append(LocalDate.now().toString() + "/");
            }
            builder.append(UuIdUtils.randomUUID(Boolean.TRUE));
            builder.append(".png");
            String fileUrl = builder.toString();
            PutObjectRequest putObjectRequest = new PutObjectRequest(bucket, fileUrl, multipartFileInputStream);
            ossClient.putObject(putObjectRequest);
            return ossUrl + fileUrl;
        } catch (Exception e) {
            log.error("上传oss导入图片失败:{}", e);
        } finally {
            // 关闭流
            ossClient.shutdown();
        }
        return null;
    }

    public String uploadZipImageByUrl(String reqUrl, String directory) {
        // 创建OSSClient实例。
        OSS ossClient = new OSSClientBuilder().build(this.endpoint, this.accessId, this.secretKey);
        try {
            //通过图片的url创建输入流
            InputStream multipartFileInputStream = new URL(reqUrl).openStream();
            //拼接上传后的目录
            StringBuilder builder = new StringBuilder(imageDirectory);
            if (StringUtils.isNotBlank(directory)) {
                builder.append(directory);
            } else {
                builder.append(LocalDate.now().toString() + "/");
            }
            builder.append(UuIdUtils.randomUUID(Boolean.TRUE));
            builder.append(".jpg");
            String fileUrl = builder.toString();
            PutObjectRequest putObjectRequest = new PutObjectRequest(bucket, fileUrl, multipartFileInputStream);
            ossClient.putObject(putObjectRequest);
            return ossUrl + fileUrl;
        } catch (Exception e) {
            log.error("上传oss导入图片失败:{}", e);
        } finally {
            // 关闭流
            ossClient.shutdown();
        }
        return null;
    }

    public String uploadByUrl(String reqUrl) {
        // 创建OSSClient实例。
        OSS ossClient = new OSSClientBuilder().build(this.endpoint, this.accessId, this.secretKey);
        try {
            //通过图片的url创建输入流
            InputStream multipartFileInputStream = new URL(reqUrl).openStream();
            //拼接上传后的目录
            StringBuilder builder = new StringBuilder(operationDirectory);
            builder.append(LocalDate.now().toString() + "/");
            builder.append(UuIdUtils.randomUUID(Boolean.TRUE));
            builder.append(".png");
            String fileUrl = builder.toString();
            PutObjectRequest putObjectRequest = new PutObjectRequest(bucket, fileUrl, multipartFileInputStream);
            ossClient.putObject(putObjectRequest);
            return ossUrl + fileUrl;
        } catch (Exception e) {
            log.error("上传oss导入图片失败:{}", e);
        } finally {
            // 关闭流
            ossClient.shutdown();
        }
        return null;
    }

    public String downloadFile(String fileUrl) {
        fileUrl = fileUrl.replace(ossUrl, "");
        //本地存储目录
        String directoryPath = "./oss/" + (LocalDate.now().toString());
        File directory = new File(directoryPath);
        //如果没有该目录进行创建
        directory.mkdirs();
        String pathName = directoryPath + fileUrl.substring(fileUrl.lastIndexOf("/"));

        // 创建OSSClient实例。
        OSS ossClient = new OSSClientBuilder().build(this.endpoint, this.accessId, this.secretKey);

        try {
            // 下载Object到本地文件，并保存到指定的本地路径中。如果指定的本地文件存在会覆盖，不存在则新建。
            // 如果未指定本地路径，则下载后的文件默认保存到示例程序所属项目对应本地路径中。
            ossClient.getObject(new GetObjectRequest(this.bucket, fileUrl), new File(pathName));
            return pathName;
        } catch (Exception e) {
            log.error("下载oss导入文件失败:{}", e);
        } finally {
            if (ossClient != null) {
                ossClient.shutdown();
            }
        }
        return directoryPath;
    }
}
