package com.zxt.operation.vo;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 高德地图 逆地理编码接口出参
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AmapRegeoVO extends AmapResponse {

    @JSONField(name="formatted_address")
    private String formattedAddress;

    private AddressComponent addressComponent;

    /**
     * 地址元素
     */
    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public class AddressComponent{

        /**
         * 坐标点所在省名称
         */
        private String province;
        /**
         * 坐标点所在城市名称
         */
        private String city;

        /**
         * 城市编码
         */
        @JSONField(name="citycode")
        private String cityCode;

        /**
         * 坐标点所在区
         */
        private String district;

        /**
         * 行政区编码
         */
        @JSONField(name="adcode")
        private String adCode;

        /**
         * 坐标点所在乡镇/街道（此街道为社区街道，不是道路信息）
         */
        private String township;

        /**
         * 乡镇街道编码
         */
        @JSONField(name="towncode")
        private String townCode;

        /**
         * 所属海域信息
         */
        private String seaArea;
    }
}
