package com.zxt.operation.sms.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.aliyuncs.CommonRequest;
import com.aliyuncs.CommonResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.profile.DefaultProfile;
import com.google.gson.Gson;
import com.zxt.bean.result.Result;
import com.zxt.operation.sms.ISmsService;
import com.zxt.operation.sms.SmsSendDetailVO;
import com.zxt.operation.sms.SmsSendVO;
import com.zxt.operation.sms.config.SmsConfig;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.Map;

@Slf4j
public class AliyunDySmsServiceImpl implements ISmsService {

    private SmsConfig.Aliyun aliyun;

    public AliyunDySmsServiceImpl(SmsConfig.Aliyun aliyun) {
        this.aliyun = aliyun;
    }

    private IAcsClient client = null;


    @Override
    public Result<SmsSendVO> sendMessage(String signName, String mobile, String templateCode, String templateParam) {
        try {
            CommonRequest request = new CommonRequest();
            request.setSysDomain(aliyun.getDomain());
            request.setSysVersion(aliyun.getVersion());
            request.setSysAction(aliyun.getAction());
            // 接收短信的手机号码
            request.putQueryParameter("PhoneNumbers", mobile);
            // 短信签名名称。请在控制台签名管理页面签名名称一列查看（必须是已添加、并通过审核的短信签名）。
            request.putQueryParameter("SignName", signName);
            // 短信模板ID
            request.putQueryParameter("TemplateCode", templateCode);
            // 短信模板变量对应的实际值，JSON格式。
            request.putQueryParameter("TemplateParam", templateParam);
            CommonResponse commonResponse = getClient().getCommonResponse(request);
            String data = commonResponse.getData();
            String sData = data.replaceAll("'\'", "");
            log.info("sendSms={}", JSON.toJSONString(sData));
            // {\"RequestId\":\"F040CF0F-2B17-4234-B6A4-91C0B82BD24A\",\"Message\":\"模板不合法(不存在或被拉黑)\",\"Code\":\"isv.SMS_TEMPLATE_ILLEGAL\"}
            // {\"RequestId\":\"66E047DE-BD76-4E14-9301-654331DB6C27\",\"Message\":\"OK\",\"BizId\":\"937523808712478960^0\",\"Code\":\"OK\"}
            Gson gson = new Gson();
            Map map = gson.fromJson(sData, Map.class);
            Object code = map.get("Code");
            if ("OK".equals(code.toString())) {
                SmsSendVO vo = new SmsSendVO();
                vo.setProvider("阿里云");
                vo.setMsgId(map.get("BizId").toString());
                return Result.newSuccess(vo);
            }
            return Result.newError(map.get("Message").toString());
        } catch (ClientException e) {
            log.error("发送阿里云短信失败", e);
            return Result.newError("发送阿里云短信失败");
        }

    }

    /**
     * 查询发送详情
     */
    public Result<List<SmsSendDetailVO>> querySendDetails(String mobiles, String date, String bizId) {
        try {
            CommonRequest request = new CommonRequest();
            request.setSysDomain(aliyun.getDomain());
            request.setSysVersion(aliyun.getVersion());
            request.setSysAction(aliyun.getAction());
            request.setSysAction(aliyun.getQueryAction());
            // 接收短信的手机号码
            request.putQueryParameter("PhoneNumber", mobiles);
            // 短信发送日期，支持查询最近30天的记录。格式为yyyyMMdd，例如20191010。
            request.putQueryParameter("SendDate", date);
            // 分页记录数量
            request.putQueryParameter("PageSize", "10");
            // 分页当前页码
            request.putQueryParameter("CurrentPage", "1");
            // 发送回执ID，即发送流水号。
            request.putQueryParameter("BizId", bizId);
            CommonResponse response = getClient().getCommonResponse(request);
            log.info("querySendDetails response={}", JSON.toJSONString(response));
            // {"TotalCount":1,"Message":"OK","RequestId":"8100D6A4-D264-44CC-AB3B-73B9C59728D1","Code":"OK","SmsSendDetailDTOs":{"SmsSendDetailDTO":[{"TemplateCode":"SMS_204761915","ReceiveDate":"2020-12-23 16:36:13","PhoneNum":"17701744230","Content":"【博之福选】亲爱的华夏银行用户，您的新户好礼积分已发放，请至公众号“华夏银行广州微生活”进行好礼兑换。","SendStatus":3,"SendDate":"2020-12-23 16:34:39","ErrCode":"DELIVERED"}]}}
            // {"TotalCount":0,"Message":"OK","RequestId":"6CEC9A5A-0165-401A-84C7-FB1088B7BB6F","Code":"OK","SmsSendDetailDTOs":{"SmsSendDetailDTO":[]}}
            JSONObject jsonObject = JSONObject.parseObject(response.getData());
            String code = jsonObject.getString("Code");
            if ("OK".equals(code)) {
                String smsSendDetailDTOs = jsonObject.getString("SmsSendDetailDTOs");
                JSONObject object = JSONObject.parseObject(smsSendDetailDTOs);
                String detailDTO = object.getString("SmsSendDetailDTO");
                return Result.newSuccess(JSON.parseArray(detailDTO, SmsSendDetailVO.class));
            }
            return Result.newError(jsonObject.getString("Message"));
        } catch (Exception e) {
            log.error("查询阿里云短信发送详情失败", e);
            return Result.newError("查询阿里云短信发送详情失败");
        }
    }


    private IAcsClient getClient() {
        if (client == null) {
            // 设置公共请求参数，初始化Client
            DefaultProfile profile = DefaultProfile.getProfile(
                    aliyun.getRegionId(),// API支持的地域ID，如短信API的值为：cn-hangzhou。
                    aliyun.getAccessKeyId(),// 您的AccessKey ID。
                    aliyun.getSecret());// 您的AccessKey Secret。
            client = new DefaultAcsClient(profile);
        }
        return client;
    }

}
