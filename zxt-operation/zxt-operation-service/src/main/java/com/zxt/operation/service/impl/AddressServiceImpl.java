package com.zxt.operation.service.impl;

import com.zxt.bean.utils.BeanUtils;
import com.zxt.operation.manager.AddressManager;
import com.zxt.operation.service.AddressService;
import com.zxt.operation.vo.AddressVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


/**
 * 业务逻辑层-地址实现类
 */
@Slf4j
@Service("addressService")
public class AddressServiceImpl implements AddressService {

    private final AddressManager addressManager;

    @Autowired
    public AddressServiceImpl(AddressManager addressManager) {
        this.addressManager = addressManager;
    }

    @Override
    @Transactional(readOnly = true)
    public List<AddressVO> getByParentCode(String parentCode) {
        return BeanUtils.beanCopy(addressManager.getByParentCode(parentCode), AddressVO.class);
    }
}