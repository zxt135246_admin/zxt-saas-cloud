package com.zxt.operation.model;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString(callSuper = true)
@TableName("op_system_news")
public class SystemNews {

    public static final LambdaQueryWrapper<SystemNews> gw() {
        return new LambdaQueryWrapper<>();
    }


    /**
     * 主键id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 用户id
     */
    @TableField(value = "user_id")
    private Long userId;

    /**
     * 消息类型1.关注消息 2.文案消息 3.相册消息
     */
    @TableField(value = "news_type")
    private Byte newsType;

    /**
     * 是否已读
     */
    @TableField(value = "is_read")
    private Boolean isRead;

    @TableField(value = "data_id")
    private String dataId;

    /**
     * 消息内容
     */
    @TableField(value = "content")
    private String content;

    /**
     * 链路日志id
     */
    @TableField(value = "trace_id")
    private String traceId;

    /**
     * 创建时间
     */
    @TableField(value = "create_time")
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    @TableField(value = "update_time")
    private LocalDateTime updateTime;

    /**
     * 是否删除
     */
    @TableLogic
    @TableField(value = "is_deleted")
    private Boolean isDeleted;


    /**
     * -------------------------------------------------
     * 上面字段由工具自动生成，请在下面添加扩充字段
     * -------------------------------------------------
     */


}