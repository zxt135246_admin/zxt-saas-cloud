package com.zxt.operation.asyncs;

import cn.hutool.core.thread.ThreadUtil;
import com.zxt.bean.utils.CollectionsUtil;
import com.zxt.bean.utils.LogUtils;
import com.zxt.content.api.dto.ImagesExifApiDTO;
import com.zxt.operation.api.dto.ImagesApiDTO;
import com.zxt.operation.client.ContentClient;
import com.zxt.operation.service.OssService;
import com.zxt.operation.utils.ExifUtils;
import com.zxt.bean.utils.FileUtils;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

/**
 * 文案异步处理类
 */
@Component
@Slf4j
@AllArgsConstructor
public class ImagesAsync {

    private final OssService ossService;
    private final ContentClient contentClient;

    @Async
    public void initImagesExif(List<ImagesApiDTO> imagesList, String traceId) {
        //设置异步TraceId
        LogUtils.putAsyncTraceId(traceId);

        List<ImagesExifApiDTO> imagesExifApiDTOList = new ArrayList<>();
        for (ImagesApiDTO imagesApiDTO : imagesList) {
            ThreadUtil.sleep(1, TimeUnit.SECONDS);
            //将文件下载到本地
            File directory = null;
            try {
                String localhostUrl = ossService.downloadFile(imagesApiDTO.getOriginalImageUrl());
                directory = new File(localhostUrl);
                directory.mkdirs();
                ImagesExifApiDTO imagesExifApiDTO = ExifUtils.readImageInfo(directory);
                imagesExifApiDTO.setImagesId(imagesApiDTO.getId());
                imagesExifApiDTOList.add(imagesExifApiDTO);
                FileUtils.deleteFile(directory);
            } catch (Exception e) {
                log.error("提取相册Exif异常,相册id:{},异常原因:{}", imagesApiDTO.getId(), e);
            } finally {
                //会在项目目录下自动生成一个临时的file文件
                if (Objects.nonNull(directory)) {
                    FileUtils.deleteFile(directory);
                }
            }
        }
        if (CollectionsUtil.isNotEmpty(imagesExifApiDTOList)) {
            contentClient.saveBatchImagesExif(imagesExifApiDTOList);
        }
    }
}
