package com.zxt.operation.service;

import org.springframework.web.multipart.MultipartFile;

public interface OssService {

    /**
     * 上传相册-定制接口
     *
     * @param multiFile
     * @param directory
     * @param imageId
     * @return
     */
    Boolean uploadImage(MultipartFile multiFile, String directory, Long imageId);

    /**
     * 根据url上传相册-定制接口
     *
     * @param url
     * @param directory
     * @param imageId
     */
    void uploadImageByUrl(String url, String directory, Long imageId);


    /**
     * 上传图片-通用接口
     *
     * @param file
     * @return
     */
    String upload(MultipartFile file);

    /**
     * 根据url上传图片-通用接口
     *
     * @param url
     * @return
     */
    String uploadImageByUrl(String url);

    /**
     * 下载oss文件到本地
     *
     * @param fileUrl
     * @return
     */
    String downloadFile(String fileUrl);
}
