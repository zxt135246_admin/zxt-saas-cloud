package com.zxt.operation.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zxt.bean.query.QueryResultVO;
import com.zxt.bean.utils.*;
import com.zxt.content.api.dto.CreateMessagesApiDTO;
import com.zxt.content.api.enums.CreateMessagesTypeEnum;
import com.zxt.content.api.vo.ContentApiVO;
import com.zxt.content.api.vo.ImagesApiVO;
import com.zxt.content.api.vo.ImagesCollectionsApiVO;
import com.zxt.operation.client.ContentClient;
import com.zxt.operation.client.UserClient;
import com.zxt.operation.converter.SystemNewsConvertor;
import com.zxt.operation.dto.SystemNewsDTO;
import com.zxt.operation.dto.SystemNewsQueryDTO;
import com.zxt.operation.api.enums.NewsTypeEnum;
import com.zxt.operation.manager.SystemNewsManager;
import com.zxt.operation.model.SystemNews;
import com.zxt.operation.service.ISystemNewsService;
import com.zxt.operation.vo.SystemNewsVO;
import com.zxt.user.api.vo.UserApiVO;
import com.zxt.user.api.vo.UserConcernApiVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;


/**
 * 业务逻辑层-系统消息实现类
 */
@Slf4j
@Service("systemNewsService")
public class SystemNewsServiceImpl implements ISystemNewsService {

    private final SystemNewsManager systemNewsManager;
    private final UserClient userClient;
    private final ContentClient contentClient;

    @Autowired
    public SystemNewsServiceImpl(SystemNewsManager systemNewsManager, UserClient userClient, ContentClient contentClient) {
        this.systemNewsManager = systemNewsManager;
        this.userClient = userClient;
        this.contentClient = contentClient;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT, rollbackFor = Exception.class)
    public Boolean deleteSystemNews(Long id) {
        return systemNewsManager.deleteSystemNews(id);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT, rollbackFor = Exception.class)
    public Long createSystemNews(SystemNewsDTO systemNewsDTO) {
        return systemNewsManager.createSystemNews(SystemNewsConvertor.toSystemNews(systemNewsDTO));
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT, rollbackFor = Exception.class)
    public Boolean updateSystemNews(SystemNewsDTO systemNewsDTO) {
        return systemNewsManager.updateSystemNews(SystemNewsConvertor.toSystemNews(systemNewsDTO));
    }

    @Override
    public SystemNewsVO getSystemNews(Long id) {
        return SystemNewsConvertor.toSystemNewsVO(systemNewsManager.getSystemNews(id));
    }

    @Override
    public QueryResultVO<SystemNewsVO> querySystemNews(SystemNewsQueryDTO systemNewsQueryDTO) {
        Page<SystemNews> page = systemNewsManager.querySystemNews(systemNewsQueryDTO);
        QueryResultVO<SystemNewsVO> queryResultVO = BeanUtils.pageToQueryResultVO(page, SystemNewsVO.class);
        List<SystemNewsVO> records = SystemNewsConvertor.toSystemNewsVOList(page.getRecords());
        if (CollectionsUtil.isEmpty(records)) {
            queryResultVO.setRecords(records);
            return queryResultVO;
        }
        for (SystemNewsVO record : records) {
            //设置消息类型描述
            record.setNewsTypeDesc(NewsTypeEnum.getDescByValue(record.getNewsType()));
        }

        //TODO 后续需要更改为，用户点击消息才算已读
        if (Objects.isNull(systemNewsQueryDTO.getIsRead())) {
            //用户读取消息，更改消息为已读
            List<SystemNewsVO> notReadNewsList = records.stream().filter((systemNewsVO) -> {
                return !systemNewsVO.getIsRead();
            }).collect(Collectors.toList());

            List<Long> newsIds = notReadNewsList.stream().map(SystemNewsVO::getId).collect(Collectors.toList());
            if (CollectionsUtil.isNotEmpty(newsIds)) {
                systemNewsManager.updateAllIsRead(systemNewsQueryDTO.getUserId(), newsIds);
            }
        }
        queryResultVO.setRecords(records);
        return queryResultVO;
    }

    @Override
    public void updateAllIsRead(Long userId) {
        systemNewsManager.updateAllIsRead(userId, null);
    }

    @Override
    public void updateIsRead(Long userId, List<Long> newsIds) {
        if (CollectionsUtil.isEmpty(newsIds)) {
            return;
        }
        systemNewsManager.updateAllIsRead(userId, newsIds);
    }

    @Override
    public Boolean saveBatchSystemNews(List<SystemNewsDTO> dtoList) {
        return systemNewsManager.saveBatch(SystemNewsConvertor.toSystemNewsList(dtoList));
    }

    @Override
    public void addSystemNews(SystemNewsDTO dto) {

        TrueUtils.isTrue(StringUtils.isBlank(dto.getContent())).throwMessage("消息内容不能为空");

        List<UserApiVO> userApiVOList = userClient.getAllUser();
        TrueUtils.isTrue(CollectionsUtil.isEmpty(userApiVOList)).throwMessage("未获取到用户列表");

        List<SystemNews> systemNewsList = new ArrayList<>(userApiVOList.size());
        for (UserApiVO userApiVO : userApiVOList) {
            SystemNews systemNews = new SystemNews();
            systemNews.setUserId(userApiVO.getId());
            systemNews.setNewsType(NewsTypeEnum.SYSTEM.getValue());
            systemNews.setIsRead(Boolean.FALSE);
            systemNews.setDataId(userApiVO.getId().toString());
            systemNews.setContent(dto.getContent());
            systemNews.setTraceId(LogUtils.getTraceId());
            systemNewsList.add(systemNews);
        }
        systemNewsManager.saveBatch(systemNewsList);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT, rollbackFor = Exception.class)
    public void dealCreateContentNews(CreateMessagesApiDTO dto) {
        if (CreateMessagesTypeEnum.CONTENT.equal(dto.getMessagesType())) {
            ContentApiVO contentApiVO = contentClient.getContentById(dto.getDataId());
            if (Objects.isNull(contentApiVO)) {
                log.info("文案不存在");
                return;
            }
            log.info("文案发布成功，开始向粉丝列表推送消息通知");

            UserApiVO userApiVO = userClient.getUserById(contentApiVO.getUserId());
            //获取粉丝列表
            List<UserConcernApiVO> userConcernApiVOList = userClient.getUserConcernByConcernUserId(contentApiVO.getUserId());

            if (CollectionsUtil.isNotEmpty(userConcernApiVOList)) {
                //粉丝列表不为空
                List<SystemNews> systemNewsList = new ArrayList<>();
                for (UserConcernApiVO concernApiVO : userConcernApiVOList) {
                    SystemNews systemNews = new SystemNews();
                    systemNews.setUserId(concernApiVO.getUserId());
                    systemNews.setTraceId(LogUtils.getTraceId());
                    systemNews.setIsRead(Boolean.FALSE);
                    systemNews.setDataId(contentApiVO.getId().toString());
                    systemNews.setNewsType(NewsTypeEnum.CONTENT.getValue());
                    systemNews.setContent("您关注的作者: " + userApiVO.getNickName() + " ，发布了新的文案！（可点击查看）");
                    systemNewsList.add(systemNews);
                }
                systemNewsManager.saveBatch(systemNewsList);
            }
        } else if (CreateMessagesTypeEnum.IMAGES.equal(dto.getMessagesType())) {
            ImagesApiVO imagesApiVO = contentClient.getImagesById(dto.getDataId());
            if (Objects.isNull(imagesApiVO)) {
                log.info("相册不存在");
                return;
            }
            log.info("文案发布成功，开始向共享用户推送消息通知");

            UserApiVO userApiVO = userClient.getUserById(imagesApiVO.getUserId());

            ImagesCollectionsApiVO imagesCollectionsApiVO = contentClient.getImagesCollectionsById(imagesApiVO.getCollectionsId());
            ImagesCollectionsApiVO parentImagesCollections = contentClient.getImagesCollectionsById(imagesCollectionsApiVO.getParentId());

            List<Long> shareUserIds = JSON.parseArray(imagesCollectionsApiVO.getShareUserJson(), Long.class);
            shareUserIds.add(imagesCollectionsApiVO.getUserId());
            shareUserIds.removeAll(CollectionsUtil.asList(0L, userApiVO.getId()));

            if (CollectionsUtil.isNotEmpty(shareUserIds)) {
                List<SystemNews> systemNewsList = new ArrayList<>();
                for (Long shareUserId : shareUserIds) {
                    SystemNews systemNews = new SystemNews();
                    systemNews.setUserId(shareUserId);
                    systemNews.setTraceId(LogUtils.getTraceId());
                    systemNews.setIsRead(Boolean.FALSE);
                    systemNews.setDataId(imagesCollectionsApiVO.getId().toString());
                    systemNews.setNewsType(NewsTypeEnum.IMAGES.getValue());
                    systemNews.setContent(userApiVO.getNickName() + " 共享了新的照片到 " + parentImagesCollections.getCollectionsName() + "-" + imagesCollectionsApiVO.getCollectionsName() + " 合集！（可点击查看）");
                    systemNewsList.add(systemNews);
                }
                systemNewsManager.saveBatch(systemNewsList);
            }
        }
    }

}
