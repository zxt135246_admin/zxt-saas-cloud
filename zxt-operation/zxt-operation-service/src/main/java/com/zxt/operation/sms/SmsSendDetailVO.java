package com.zxt.operation.sms;

import com.google.gson.annotations.SerializedName;
import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString(callSuper = true)
public class SmsSendDetailVO implements Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 1L;

    /**
     * 短信内容
     */
    @SerializedName("Content")
    private String content;
    /**
     * 运营商短信状态码
     * 短信发送成功：DELIVERED
     */
    @SerializedName("ErrCode")
    private String errCode;
    /**
     * 外部流水扩展字段
     */
    @SerializedName("OutId")
    private String outId;
    /**
     * 接收短信的手机号码
     */
    @SerializedName("PhoneNum")
    private String mobile;
    /**
     * 短信接收日期和时间
     */
    @SerializedName("ReceiveDate")
    private String receiveDate;
    /**
     * 短信发送日期和时间
     */
    @SerializedName("SendDate")
    private String sendDate;
    /**
     * 短信发送状态
     * 1：等待回执
     * 2：发送失败
     * 3：发送成功
     */
    @SerializedName("SendStatus")
    private Integer sendStatus;
    /**
     * 短信模板ID
     */
    @SerializedName("TemplateCode")
    private String templateCode;

}
