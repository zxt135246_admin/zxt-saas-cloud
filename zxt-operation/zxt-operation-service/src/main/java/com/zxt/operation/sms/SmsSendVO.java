package com.zxt.operation.sms;

import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString(callSuper = true)
public class SmsSendVO implements Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 1L;

    /**
     * 供应商
     */
    private String provider;

    /**
     * 返回短信消息ID
     */
    private String msgId;

}
