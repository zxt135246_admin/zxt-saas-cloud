package com.zxt.operation.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.validation.constraints.Size;
import java.io.Serializable;


@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString(callSuper = true)
@ApiModel("系统消息入参")
public class SystemNewsDTO implements Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 1L;


    /**
     * 主键id
     */
    @ApiModelProperty(value = "主键id", required = true, hidden = true)
    private Long id;


    /**
     * 消息类型1.关注消息 2.文案消息 3.相册消息
     */
    @ApiModelProperty(value = "消息类型1.关注消息 2.文案消息 3.相册消息", required = true, hidden = true)
    private Byte newsType;

    /**
     * 消息内容
     */
    @ApiModelProperty(value = "消息内容")
    @Size(max = 255, message = "消息内容不能超过255位")
    private String content;

    /**
     * 链路日志id
     */
    @ApiModelProperty(value = "链路日志id", hidden = true)
    private String traceId;

    @ApiModelProperty(value = "用户id", hidden = true)
    private Long userId;

    @ApiModelProperty(value = "数据id", hidden = true)
    private String dataId;

    @ApiModelProperty(value = "是否已读")
    private Boolean isRead;


    /**
     * -------------------------------------------------
     * 上面字段由工具自动生成，请在下面添加扩充字段
     * -------------------------------------------------
     */


}
