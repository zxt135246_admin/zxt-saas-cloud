package com.zxt.operation.service;

import com.zxt.operation.dto.SystemQuestionDTO;

/**
 * 业务逻辑层-系统反馈接口类
 *
 * @author zxt
 */
public interface ISystemQuestionService {

    /**
     * 新增系统反馈
     *
     * @param dto
     */
    void add(SystemQuestionDTO dto);
}