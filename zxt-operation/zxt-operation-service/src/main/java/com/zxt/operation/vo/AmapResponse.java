package com.zxt.operation.vo;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AmapResponse {

    /**
     * 返回值为 0 或 1，0 表示请求失败；1 表示请求成功。
     */
    private String status;

    /**
     * 当 status 为 0 时，info 会返回具体错误原因，否则返回“OK”。详情可以参考
     */
    private String info;

    /**
     * 出参
     */
    @JSONField(name="regeocode")
    private AmapRegeoVO regeoCode;
}
