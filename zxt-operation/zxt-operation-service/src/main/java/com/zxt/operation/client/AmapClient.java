package com.zxt.operation.client;

import com.alibaba.fastjson.JSON;
import com.zxt.bean.exeption.ServiceException;
import com.zxt.bean.result.ResultCode;
import com.zxt.operation.vo.AmapRegeoVO;
import com.zxt.operation.vo.AmapResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
@Slf4j
@RefreshScope
public class AmapClient {

    private final RestTemplate httpTemplate;
    private String apiKey = "e2d86176d3b63e3c3613adc595db7a4d";

    @Autowired
    public AmapClient(RestTemplate httpTemplate) {
        this.httpTemplate = httpTemplate;
    }

    public AmapRegeoVO getRegeo(String locations) {
        String requestURl = "https://restapi.amap.com/v3/geocode/regeo?output=JSON&location=" + locations + "&key=" + apiKey + "&radius=1000&extensions=base ";
        log.info("高德地图 查询逆地理编码 url:{}", requestURl);
        ResponseEntity<AmapResponse> result = httpTemplate.getForEntity(requestURl, AmapResponse.class);
        log.info("高德地图 查询逆地理编码 出参:{}", JSON.toJSONString(result));
        AmapResponse body = result.getBody();

        if (!body.getStatus().equals("1")) {
            throw new ServiceException(ResultCode.COMMON_MESSAGE, "查询逆地理编码失败");
        }
        return body.getRegeoCode();
    }
}
