package com.zxt.operation.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zxt.operation.model.RocketMqRecord;

public interface RocketMqRecordMapper extends BaseMapper<RocketMqRecord> {
}
