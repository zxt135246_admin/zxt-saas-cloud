package com.zxt.operation.converter;

import com.zxt.operation.dto.SystemNewsDTO;
import com.zxt.operation.model.SystemNews;
import com.zxt.operation.vo.SystemNewsVO;
import org.springframework.cglib.beans.BeanCopier;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * 系统消息对象相互转换
 */
public abstract class SystemNewsConvertor {

    private static final BeanCopier beanCopierForSystemNewsVO = BeanCopier.create(SystemNews.class, SystemNewsVO.class, false);
    private static final BeanCopier beanCopierForSystemNews = BeanCopier.create(SystemNewsDTO.class, SystemNews.class, false);

    public static SystemNewsVO toSystemNewsVO(SystemNews systemNews) {
        if (Objects.isNull(systemNews)) {
            return null;
        }
        SystemNewsVO systemNewsVO = new SystemNewsVO();
        beanCopierForSystemNewsVO.copy(systemNews, systemNewsVO, null);
        return systemNewsVO;
    }

    public static SystemNews toSystemNews(SystemNewsDTO systemNewsDTO) {
        SystemNews systemNews = new SystemNews();
        beanCopierForSystemNews.copy(systemNewsDTO, systemNews, null);
        return systemNews;
    }

    public static List<SystemNewsVO> toSystemNewsVOList(List<SystemNews> systemNewsList) {
        if (Objects.isNull(systemNewsList) || systemNewsList.isEmpty()) {
            return new ArrayList<>();
        }
        List<SystemNewsVO> systemNewsInfoList = new ArrayList<SystemNewsVO>(systemNewsList.size());
        for (SystemNews systemNews : systemNewsList) {
            systemNewsInfoList.add(toSystemNewsVO(systemNews));
        }
        return systemNewsInfoList;
    }

    public static List<SystemNews> toSystemNewsList(List<SystemNewsDTO> systemNewsDTOList) {
        if (Objects.isNull(systemNewsDTOList) || systemNewsDTOList.isEmpty()) {
            return new ArrayList<>();
        }
        List<SystemNews> systemNewsList = new ArrayList<SystemNews>(systemNewsDTOList.size());
        for (SystemNewsDTO systemNewsDTO : systemNewsDTOList) {
            systemNewsList.add(toSystemNews(systemNewsDTO));
        }
        return systemNewsList;
    }

}
