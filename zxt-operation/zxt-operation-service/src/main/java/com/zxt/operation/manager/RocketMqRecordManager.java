package com.zxt.operation.manager;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zxt.operation.model.RocketMqRecord;
import com.zxt.operation.dao.RocketMqRecordMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author zxt
 */
@Slf4j
@Service
public class RocketMqRecordManager extends ServiceImpl<RocketMqRecordMapper, RocketMqRecord> {


    public RocketMqRecord getByMessageId(String messageId) {
        LambdaQueryWrapper<RocketMqRecord> wrapper = RocketMqRecord.gw();
        wrapper.eq(RocketMqRecord::getMessageId, messageId);
        return getOne(wrapper);
    }

    public RocketMqRecord getByMessageIdAndProjectName(String messageId, String projectName) {
        LambdaQueryWrapper<RocketMqRecord> wrapper = RocketMqRecord.gw();
        wrapper.eq(RocketMqRecord::getMessageId, messageId);
        wrapper.eq(RocketMqRecord::getProjectName, projectName);
        return getOne(wrapper);
    }
}
