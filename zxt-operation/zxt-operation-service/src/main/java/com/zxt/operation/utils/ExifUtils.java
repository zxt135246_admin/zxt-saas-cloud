package com.zxt.operation.utils;

import com.alibaba.fastjson.JSON;
import com.drew.imaging.ImageMetadataReader;
import com.drew.imaging.ImageProcessingException;
import com.drew.metadata.Directory;
import com.drew.metadata.Metadata;
import com.drew.metadata.Tag;
import com.zxt.content.api.dto.ImagesExifApiDTO;
import com.zxt.operation.vo.AmapRegeoVO;
import com.zxt.operation.vo.AmapResponse;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Slf4j
public class ExifUtils {

    /*private final AmapClient amapClient;*/

    /*public static void main(String[] args) throws Exception {
        File file = new File("E:\\3dde2c45df834dfe835dc6e297e5074a.png");
        readImageInfo(file);
    }*/


    /**
     * 提取照片里面的信息
     *
     * @param file 照片文件
     * @return
     * @throws ImageProcessingException
     * @throws Exception
     */
    public static ImagesExifApiDTO readImageInfo(File file) throws ImageProcessingException, Exception {

        Metadata metadata = ImageMetadataReader.readMetadata(file);

        /*log.info("---拉取相册全部详情---");
        for (Directory directory : metadata.getDirectories()) {
            for (Tag tag : directory.getTags()) {
                log.info("[%s] - %s = %s\n", directory.getName(), tag.getTagName(), tag.getDescription());
            }
            if (directory.hasErrors()) {
                for (String error : directory.getErrors()) {
                    log.info("ERROR: %s", error);
                }
            }
        }*/


        ImagesExifApiDTO exifApiDTO = new ImagesExifApiDTO();
        log.info("--拉取相册常用信息---");
        Double lat = null;
        Double lng = null;
        for (Directory directory : metadata.getDirectories()) {
            for (Tag tag : directory.getTags()) {
                String tagName = tag.getTagName();  //标签名
                String desc = tag.getDescription(); //标签信息
                if (tagName.equals("Image Height")) {
                    log.info("图片高度: " + desc);
                    exifApiDTO.setImagesHeight(Integer.parseInt(desc.replaceAll(" pixels", "")));
                } else if (tagName.equals("Image Width")) {
                    log.info("图片宽度: " + desc);
                    exifApiDTO.setImagesWidth(Integer.parseInt(desc.replaceAll(" pixels", "")));
                } else if (tagName.equals("Date/Time Original")) {
                    log.info("拍摄时间: " + desc);
                    DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy:MM:dd HH:mm:ss");
                    exifApiDTO.setShootingTime(LocalDateTime.parse(desc, df));
                } else if (tagName.equals("GPS Latitude")) {
                    log.info("纬度 : " + desc);
                    log.info("纬度(度分秒格式) : " + pointToLatlong(desc));
                    lat = latLng2Decimal(desc);
                    exifApiDTO.setLatitude(lat.toString());
                } else if (tagName.equals("GPS Longitude")) {
                    log.info("经度: " + desc);
                    log.info("经度(度分秒格式): " + pointToLatlong(desc));
                    lng = latLng2Decimal(desc);
                    exifApiDTO.setLongitude(lng.toString());
                }
            }
        }
        log.info("--经纬度转地址--");
        //经纬度转地主使用百度api
        if (Objects.nonNull(lat) && Objects.nonNull(lng)) {
            AmapRegeoVO amapRegeoVO = convertGpsToLoaction(lat, lng);
            if (Objects.nonNull(amapRegeoVO)) {
                //设置拍摄地址
                exifApiDTO.setShootingAddress(amapRegeoVO.getFormattedAddress());
            }
        }


        return exifApiDTO;
    }


    /**
     * 经纬度格式  转换为  度分秒格式 ,如果需要的话可以调用该方法进行转换
     *
     * @param point 坐标点
     * @return
     */
    public static String pointToLatlong(String point) {
        Double du = Double.parseDouble(point.substring(0, point.indexOf("°")).trim());
        Double fen = Double.parseDouble(point.substring(point.indexOf("°") + 1, point.indexOf("'")).trim());
        Double miao = Double.parseDouble(point.substring(point.indexOf("'") + 1, point.indexOf("\"")).trim());
        Double duStr = du + fen / 60 + miao / 60 / 60;
        return duStr.toString();
    }


    /***
     * 经纬度坐标格式转换（* °转十进制格式）
     * @param gps
     */
    public static double latLng2Decimal(String gps) {
        String a = gps.split("°")[0].replace(" ", "");
        String b = gps.split("°")[1].split("'")[0].replace(" ", "");
        String c = gps.split("°")[1].split("'")[1].replace(" ", "").replace("\"", "");
        double gps_dou = Double.parseDouble(a) + Double.parseDouble(b) / 60 + Double.parseDouble(c) / 60 / 60;
        return gps_dou;
    }


    /**
     * 经纬度转地址信息
     *
     * @param gps_latitude  维度
     * @param gps_longitude 精度
     * @return
     */
    private static AmapRegeoVO convertGpsToLoaction(double gps_latitude, double gps_longitude) {
        try {
            String locations = gps_longitude + "," + gps_latitude;
            String apiKey = "e2d86176d3b63e3c3613adc595db7a4d";
            String requestURl = "https://restapi.amap.com/v3/geocode/regeo";
            Map<String, String> params = new HashMap<>();
            params.put("output", "JSON");
            params.put("location", locations);
            params.put("key", apiKey);
            params.put("radius", "500");
            params.put("extensions", "base");
            String result = HttpUtils.doGet(requestURl, params, null);
            AmapResponse amapResponse = JSON.parseObject(result, AmapResponse.class);
            log.info(JSON.toJSONString(amapResponse));
            return amapResponse.getRegeoCode();
        } catch (Exception e) {
            log.error("高德地图，根据经纬度获取地址信息异常，异常原因:{}", e);
        }
        return null;
    }


}
