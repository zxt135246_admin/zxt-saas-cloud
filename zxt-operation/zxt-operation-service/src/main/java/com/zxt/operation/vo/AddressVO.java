package com.zxt.operation.vo;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.zxt.bean.convertor.LongJsonDeserializer;
import com.zxt.bean.convertor.LongJsonSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString(callSuper = true)
@ApiModel("地址出参")
public class AddressVO implements Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 1L;


    /**
     * id
     */
    @ApiModelProperty("id")
    @JsonSerialize(using = LongJsonSerializer.class)
    @JsonDeserialize(using = LongJsonDeserializer.class)
    private Long id;

    /**
     * 地址编码
     */
    @ApiModelProperty("地址编码")
    private String addressCode;

    /**
     * 地址名称
     */
    @ApiModelProperty("地址名称")
    private String addressName;

    /**
     * 地址级别
     */
    @ApiModelProperty("地址级别")
    private Integer addressLevel;

    /**
     * 首字母缩写
     */
    @ApiModelProperty("首字母缩写")
    private String abbreviation;

    /**
     * 父id
     */
    @ApiModelProperty("父id")
    @JsonSerialize(using = LongJsonSerializer.class)
    @JsonDeserialize(using = LongJsonDeserializer.class)
    private Long parentId;

    /**
     * 父编码
     */
    @ApiModelProperty("父编码")
    private String parentCode;

    /**
     * 标签
     */
    @ApiModelProperty("标签")
    private String tag;

    /**
     * 编码全路径
     */
    @ApiModelProperty("编码全路径")
    private String fullCodePath;

    /**
     * 名称全路径
     */
    @ApiModelProperty("名称全路径")
    private String fullNamePath;

    /**
     * 排序
     */
    @ApiModelProperty("排序")
    private Integer sortNumber;

    /**
     * 创建时间
     */
    @ApiModelProperty("创建时间")
    private LocalDateTime createTime;


    /**
     * -------------------------------------------------
     * 上面字段由工具自动生成，请在下面添加扩充字段
     * -------------------------------------------------
     */


}