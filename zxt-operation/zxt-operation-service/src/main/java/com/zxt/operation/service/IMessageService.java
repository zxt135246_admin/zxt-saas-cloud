package com.zxt.operation.service;

import com.zxt.operation.api.dto.SendEmailApiDTO;
import com.zxt.operation.api.dto.SmsSendApiDTO;

public interface IMessageService {

    /**
     * 发送短信
     *
     * @param dto 入参
     */
    void sendMessage(SmsSendApiDTO dto);


    /**
     * 发送邮箱
     */
    void sendEmail(SendEmailApiDTO dto);

}
