package com.zxt.operation.model;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString(callSuper = true)
@TableName(value = "op_rocket_mq_record")
public class RocketMqRecord {

    public static final LambdaQueryWrapper<RocketMqRecord> gw() {
        return new LambdaQueryWrapper<>();
    }

    /**
     * id标识
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 项目名称
     */
    @TableField(value = "project_name")
    private String projectName;

    /**
     * 记录名称
     */
    @TableField(value = "record_name")
    private String recordName;

    /**
     * topic
     */
    @TableField(value = "topic")
    private String topic;

    /**
     * 消息id
     */
    @TableField(value = "message_id")
    private String messageId;

    /**
     * 发送者host
     */
    @TableField(value = "send_host")
    private String sendHost;

    /**
     * 发送时间
     */
    @TableField(value = "send_time")
    private LocalDateTime sendTime;

    /**
     * 重试次数
     */
    @TableField(value = "reconsume_times")
    private Integer reconsumeTimes;

    /**
     * 处理方法
     */
    @TableField(value = "method")
    private String method;

    /**
     * 处理参数
     */
    @TableField(value = "params")
    private String params;

    /**
     * 执行时长(毫秒)
     */
    @TableField(value = "execution_time")
    private Long executionTime;

    /**
     * traceId
     */
    @TableField(value = "trace_id")
    private String traceId;

    /**
     * 创建时间
     */
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    /**
     * 是否删除
     */
    @TableLogic
    @TableField(value = "is_deleted")
    private Boolean isDeleted;
}
