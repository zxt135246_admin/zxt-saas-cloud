package com.zxt.operation.manager;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zxt.operation.dao.SystemQuestionMapper;
import com.zxt.operation.model.SystemQuestion;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * 通用逻辑层-系统反馈管理类
 *
 * @author zxt
 */
@Slf4j
@Service
public class SystemQuestionManager extends ServiceImpl<SystemQuestionMapper, SystemQuestion> {

}
