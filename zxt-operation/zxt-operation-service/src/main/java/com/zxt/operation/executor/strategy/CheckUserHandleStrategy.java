package com.zxt.operation.executor.strategy;

import com.zxt.bean.executor.strategy.AbstractHandleStrategy;

public class CheckUserHandleStrategy extends AbstractHandleStrategy {

    public CheckUserHandleStrategy() {
        super("checkUserHandleStrategy");
    }
}
