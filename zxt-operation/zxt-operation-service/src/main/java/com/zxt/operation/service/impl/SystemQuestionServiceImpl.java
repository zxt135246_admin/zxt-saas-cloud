package com.zxt.operation.service.impl;

import com.zxt.bean.utils.BeanUtils;
import com.zxt.operation.dto.SystemQuestionDTO;
import com.zxt.operation.manager.SystemQuestionManager;
import com.zxt.operation.model.SystemQuestion;
import com.zxt.operation.service.ISystemQuestionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 业务逻辑层-系统消息实现类
 *
 * @author zxt
 */
@Slf4j
@Service("systemQuestionService")
public class SystemQuestionServiceImpl implements ISystemQuestionService {

    private final SystemQuestionManager systemQuestionManager;

    @Autowired
    public SystemQuestionServiceImpl(SystemQuestionManager systemQuestionManager) {
        this.systemQuestionManager = systemQuestionManager;
    }

    @Override
    public void add(SystemQuestionDTO dto) {
        SystemQuestion systemQuestion = BeanUtils.beanCopy(dto, SystemQuestion.class);
        systemQuestionManager.save(systemQuestion);
    }
}
