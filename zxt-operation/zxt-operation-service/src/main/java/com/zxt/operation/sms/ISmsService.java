package com.zxt.operation.sms;


import com.zxt.bean.result.Result;

import java.util.List;

public interface ISmsService {

    /**
     * 发送短信
     *
     * @param signName      签名名称
     * @param mobile        手机号
     * @param templateCode  模板编码
     * @param templateParam 模板参数
     * @return
     */
    Result<SmsSendVO> sendMessage(String signName, String mobile, String templateCode, String templateParam);

    /**
     * 查询发送结果
     *
     * @param mobiles
     * @param date
     * @param bizId
     * @return
     */
    Result<List<SmsSendDetailVO>> querySendDetails(String mobiles, String date, String bizId);

}
