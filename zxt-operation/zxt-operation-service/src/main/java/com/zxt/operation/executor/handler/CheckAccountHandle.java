package com.zxt.operation.executor.handler;

import com.alibaba.fastjson.JSON;
import com.zxt.bean.executor.context.HandleContextBase;
import com.zxt.bean.executor.handler.AbstractHandle;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class CheckAccountHandle extends AbstractHandle {

    public CheckAccountHandle() {
        super("checkAccountHandle");
    }

    @Override
    public boolean run(HandleContextBase handleContext) {
        log.info("校验用户账号 : {}", JSON.toJSONString(handleContext.getParams()));
        return Boolean.TRUE;
    }


    /**
     * 直接放行 返回false代表当前校验直接通过
     *
     * @param handleContext the condition passed from invoker
     * @return
     */
    @Override
    public boolean accept(HandleContextBase handleContext) {
        log.info("是否需要校验用户账号 ->:{}", handleContext.getParams());
        return true;
    }
}
