package com.zxt.operation.client;

import com.alibaba.fastjson.JSON;
import com.zxt.bean.result.Result;
import com.zxt.content.api.dto.ImagesApiDTO;
import com.zxt.content.api.dto.ImagesExifApiDTO;
import com.zxt.content.api.feignClient.ContentFeignClient;
import com.zxt.content.api.feignClient.ImagesCollectionsFeignClient;
import com.zxt.content.api.feignClient.ImagesFeignClient;
import com.zxt.content.api.vo.ContentApiVO;
import com.zxt.content.api.vo.ImagesApiVO;
import com.zxt.content.api.vo.ImagesCollectionsApiVO;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 文案中心
 *
 * @author zxt
 */
@Component
@Slf4j
@AllArgsConstructor
public class ContentClient {

    private final ImagesFeignClient imagesFeignClient;
    private final ContentFeignClient contentFeignClient;
    private final ImagesCollectionsFeignClient imagesCollectionsFeignClient;

    public Boolean updateImagesById(ImagesApiDTO dto) {
        log.info("用户中心-文案中心 更新照片信息，入参:{}", JSON.toJSONString(dto));
        try {
            Result<Boolean> result = imagesFeignClient.updateById(dto);
            log.info("用户中心-文案中心 更新照片信息，出参:{}", JSON.toJSONString(result));
            return result.getData();
        } catch (Exception e) {
            log.error("用户中心-文案中心 更新照片信息,异常信息:{}", e);
        }
        return null;
    }

    public void saveOrUpdateImagesExif(ImagesExifApiDTO dto) {
        log.info("用户中心-文案中心 保存或修改照片Exif，入参:{}", JSON.toJSONString(dto));
        Result<Boolean> result = imagesFeignClient.saveOrUpdateImagesExif(dto);
        log.info("用户中心-文案中心 保存或修改照片Exif，出参:{}", JSON.toJSONString(result));
    }

    public void saveBatchImagesExif(List<ImagesExifApiDTO> imagesExifApiDTOList) {
        log.info("用户中心-文案中心 批量保存Exif，入参:{}", JSON.toJSONString(imagesExifApiDTOList));
        Result<Boolean> result = imagesFeignClient.saveBatchImagesExif(imagesExifApiDTOList);
        log.info("用户中心-文案中心 批量保存Exif，出参:{}", JSON.toJSONString(result));
    }

    public ContentApiVO getContentById(Long contentId) {
        log.info("用户中心-文案中心 查询文案信息，入参:{}", JSON.toJSONString(contentId));
        try {
            Result<ContentApiVO> result = contentFeignClient.getById(contentId);
            log.info("用户中心-文案中心 查询文案信息，出参:{}", JSON.toJSONString(result));
            return result.getData();
        } catch (Exception e) {
            log.error("用户中心-文案中心 查询文案信息,异常信息:{}", e);
        }
        return null;
    }

    public ImagesApiVO getImagesById(Long imagesId) {
        log.info("用户中心-文案中心 查询相册信息，入参:{}", JSON.toJSONString(imagesId));
        try {
            Result<ImagesApiVO> result = imagesFeignClient.getById(imagesId);
            log.info("用户中心-文案中心 查询相册信息，出参:{}", JSON.toJSONString(result));
            return result.getData();
        } catch (Exception e) {
            log.error("用户中心-文案中心 查询相册信息,异常信息:{}", e);
        }
        return null;
    }

    public ImagesCollectionsApiVO getImagesCollectionsById(Long collectionsId) {
        log.info("用户中心-文案中心 查询相册合集信息，入参:{}", JSON.toJSONString(collectionsId));
        try {
            Result<ImagesCollectionsApiVO> result = imagesCollectionsFeignClient.getById(collectionsId);
            log.info("用户中心-文案中心 查询相册合集信息，出参:{}", JSON.toJSONString(result));
            return result.getData();
        } catch (Exception e) {
            log.error("用户中心-文案中心 查询相册合集信息,异常信息:{}", e);
        }
        return null;
    }
}
