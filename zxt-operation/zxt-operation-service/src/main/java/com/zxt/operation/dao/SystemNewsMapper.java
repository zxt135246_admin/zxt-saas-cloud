package com.zxt.operation.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zxt.operation.model.SystemNews;

/**
 * 数据持久层-系统消息管理类
 */
public interface SystemNewsMapper extends BaseMapper<SystemNews> {
    


}