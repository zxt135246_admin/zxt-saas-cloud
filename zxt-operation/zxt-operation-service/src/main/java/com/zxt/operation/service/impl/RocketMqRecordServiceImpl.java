package com.zxt.operation.service.impl;

import com.zxt.operation.dto.RocketMqRecordDTO;
import com.zxt.operation.model.RocketMqRecord;
import com.zxt.bean.utils.BeanUtils;
import com.zxt.operation.manager.RocketMqRecordManager;
import com.zxt.operation.service.RocketMqRecordService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;

@Slf4j
@Service
@AllArgsConstructor
public class RocketMqRecordServiceImpl implements RocketMqRecordService {

    private final RocketMqRecordManager rocketMqRecordManager;

    @Override
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT, rollbackFor = Exception.class)
    public void add(RocketMqRecordDTO dto) {
        RocketMqRecord rocketMqRecord = rocketMqRecordManager.getByMessageIdAndProjectName(dto.getMessageId(), dto.getProjectName());
        if (Objects.nonNull(rocketMqRecord)) {
            dto.setId(rocketMqRecord.getId());
        }
        RocketMqRecord record = BeanUtils.beanCopy(dto, RocketMqRecord.class);
        rocketMqRecordManager.saveOrUpdate(record);
    }
}
