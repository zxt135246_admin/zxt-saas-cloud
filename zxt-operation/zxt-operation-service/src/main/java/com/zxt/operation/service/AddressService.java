package com.zxt.operation.service;


import com.zxt.operation.vo.AddressVO;

import java.util.List;

/**
 * 业务逻辑层-地址接口类
 */
public interface AddressService {


    /**
     * 根据父编码获取地址信息
     *
     * @param parentCode 父编码
     * @return 地址信息
     */
    List<AddressVO> getByParentCode(String parentCode);
}