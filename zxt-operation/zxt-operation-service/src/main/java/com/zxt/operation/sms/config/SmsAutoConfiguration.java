package com.zxt.operation.sms.config;

import com.zxt.bean.utils.StringUtils;
import com.zxt.operation.sms.ISmsService;
import com.zxt.operation.sms.impl.AliyunDySmsServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Slf4j
@Configuration
@EnableConfigurationProperties(SmsConfig.class)
public class SmsAutoConfiguration {

    private final SmsConfig config;

    public SmsAutoConfiguration(SmsConfig config) {
        this.config = config;
    }

    @Bean
    public ISmsService smsService() {
        String active = this.config.getActive();
        if (StringUtils.isNotBlank(active)) {
            if ("aliyun".equals(active)) {
                return new AliyunDySmsServiceImpl(config.getAliyun());
            }
        }
        log.error("短信配置了不支持的运行商,请查明原因,active=", active);
        throw new RuntimeException("当前短信运行商" + active + "不支持");
    }

}
