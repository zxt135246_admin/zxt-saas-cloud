package com.zxt.operation.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zxt.operation.model.Address;

/**
 * 数据持久层-地址管理类
 */
public interface AddressMapper extends BaseMapper<Address> {


}