package com.zxt.operation.manager;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zxt.bean.exeption.ServiceException;
import com.zxt.bean.result.ResultCode;
import com.zxt.bean.utils.CollectionsUtil;
import com.zxt.bean.utils.StringUtils;
import com.zxt.operation.dao.SystemNewsMapper;
import com.zxt.operation.dto.SystemNewsQueryDTO;
import com.zxt.operation.model.SystemNews;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

/**
 * 通用逻辑层-系统消息管理类
 */
@Slf4j
@Service
public class SystemNewsManager extends ServiceImpl<SystemNewsMapper, SystemNews> {


    /**
     * 删除系统消息
     *
     * @param id
     * @return 是否成功
     */
    public boolean deleteSystemNews(Long id) {
        getBaseSystemNews(id);
        return super.removeById(id);
    }

    /**
     * 创建系统消息
     *
     * @param systemNews 系统消息入参
     * @return id
     */
    public Long createSystemNews(SystemNews systemNews) {
        systemNews.setId(null);
        super.save(systemNews);
        return systemNews.getId();
    }

    /**
     * 更新系统消息
     *
     * @param systemNews 系统消息入参
     * @return 是否成功
     */
    public boolean updateSystemNews(SystemNews systemNews) {
        getBaseSystemNews(systemNews.getId());
        return super.updateById(systemNews);
    }

    /**
     * 根据ID获取系统消息信息
     *
     * @param id
     * @return 系统消息信息
     */
    public SystemNews getSystemNews(Long id) {
        return getBaseSystemNews(id);
    }

    public SystemNews getBaseSystemNews(Long id) {
        SystemNews systemNews = super.getById(id);
        if (Objects.isNull(systemNews) || systemNews.getIsDeleted()) {
            throw new ServiceException(ResultCode.DATA_NOT_EXIST, "系统消息");
        }
        return systemNews;
    }

    /**
     * 分页查询系统消息信息
     *
     * @param queryDTO 查询参数
     * @return 系统消息信息
     */
    public Page<SystemNews> querySystemNews(SystemNewsQueryDTO queryDTO) {
        Page<SystemNews> page = new Page<>();
        page.setCurrent(queryDTO.getPageNo());
        page.setSize(queryDTO.getPageSize());
        LambdaQueryWrapper<SystemNews> wrapper = SystemNews.gw();
        if (Objects.nonNull(queryDTO.getNewsType())) {
            // 消息类型1.导入不为空
            wrapper.eq(SystemNews::getNewsType, queryDTO.getNewsType());
        }
        if (Objects.nonNull(queryDTO.getUserId())) {
            // 用户id
            wrapper.eq(SystemNews::getUserId, queryDTO.getUserId());
        }
        if (Objects.nonNull(queryDTO.getIsRead())) {
            // 是否已读
            wrapper.eq(SystemNews::getIsRead, queryDTO.getIsRead());
        }
        if (StringUtils.isNotBlank(queryDTO.getContentLike())) {
            // 消息内容模糊查询
            wrapper.like(SystemNews::getContent, queryDTO.getContentLike());
        }
        wrapper.orderByAsc(SystemNews::getIsRead);
        wrapper.orderByDesc(SystemNews::getCreateTime);
        return super.page(page, wrapper);
    }

    public void updateAllIsRead(Long userId, List<Long> newsIds) {
        LambdaUpdateWrapper<SystemNews> wrapper = new LambdaUpdateWrapper<>();
        wrapper.set(SystemNews::getIsRead, Boolean.TRUE);
        if (Objects.nonNull(userId)) {
            wrapper.eq(SystemNews::getUserId, userId);
        }
        if (CollectionsUtil.isNotEmpty(newsIds)) {
            wrapper.in(SystemNews::getId, newsIds);
        }
        update(wrapper);
    }
}
