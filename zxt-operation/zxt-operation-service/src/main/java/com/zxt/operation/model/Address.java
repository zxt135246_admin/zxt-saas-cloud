package com.zxt.operation.model;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString(callSuper = true)
@TableName("op_address")
public class Address {

    public static final LambdaQueryWrapper<Address> gw() {
        return new LambdaQueryWrapper<>();
    }


    /**
     * id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 地址编码
     */
    @TableField(value = "address_code")
    private String addressCode;

    /**
     * 地址名称
     */
    @TableField(value = "address_name")
    private String addressName;

    /**
     * 地址级别
     */
    @TableField(value = "address_level")
    private Integer addressLevel;

    /**
     * 首字母缩写
     */
    @TableField(value = "abbreviation")
    private String abbreviation;

    /**
     * 父id
     */
    @TableField(value = "parent_id")
    private Long parentId;

    /**
     * 父编码
     */
    @TableField(value = "parent_code")
    private String parentCode;

    /**
     * 标签
     */
    @TableField(value = "tag")
    private String tag;

    /**
     * 编码全路径
     */
    @TableField(value = "full_code_path")
    private String fullCodePath;

    /**
     * 名称全路径
     */
    @TableField(value = "full_name_path")
    private String fullNamePath;

    /**
     * 排序
     */
    @TableField(value = "sort_number")
    private Integer sortNumber;

    /**
     * 创建时间
     */
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    /**
     * 是否删除
     */
    @TableLogic
    @TableField(value = "is_deleted")
    private Boolean isDeleted;


    /**
     * -------------------------------------------------
     * 上面字段由工具自动生成，请在下面添加扩充字段
     * -------------------------------------------------
     */


}