package com.zxt.operation.service;


import com.zxt.bean.query.QueryResultVO;
import com.zxt.content.api.dto.CreateMessagesApiDTO;
import com.zxt.operation.dto.SystemNewsDTO;
import com.zxt.operation.dto.SystemNewsQueryDTO;
import com.zxt.operation.vo.SystemNewsVO;

import java.util.List;

/**
 * 业务逻辑层-系统消息接口类
 */
public interface ISystemNewsService {


    /**
     * 删除系统消息
     *
     * @param id
     * @return 是否成功
     */
    Boolean deleteSystemNews(Long id);

    /**
     * 创建系统消息
     *
     * @param systemNewsDTO 系统消息入参
     * @return id
     */
    Long createSystemNews(SystemNewsDTO systemNewsDTO);


    /**
     * 更新系统消息
     *
     * @param systemNewsDTO 系统消息入参
     * @return 是否成功
     */
    Boolean updateSystemNews(SystemNewsDTO systemNewsDTO);

    /**
     * 根据ID获取系统消息信息
     *
     * @param id
     * @return 系统消息信息
     */
    SystemNewsVO getSystemNews(Long id);

    /**
     * 分页查询系统消息信息
     *
     * @param systemNewsQueryDTO 系统消息查询入参
     * @return 系统消息信息
     */
    QueryResultVO<SystemNewsVO> querySystemNews(SystemNewsQueryDTO systemNewsQueryDTO);

    /**
     * 更新用户平台类型下所有消息为已读
     *
     * @param userId 用户id
     */
    void updateAllIsRead(Long userId);

    /**
     * 更新指定的消息为已读
     *
     * @param userId  用户id
     * @param newsIds 消息id
     */
    void updateIsRead(Long userId, List<Long> newsIds);

    /**
     * 批量保存系统消息
     *
     * @param dtoList
     */
    Boolean saveBatchSystemNews(List<SystemNewsDTO> dtoList);


    /**
     * 新增系统消息
     *
     * @param dto
     */
    void addSystemNews(SystemNewsDTO dto);

    /**
     * 处理创建文案成功系统消息
     *
     * @param dto
     */
    void dealCreateContentNews(CreateMessagesApiDTO dto);
}