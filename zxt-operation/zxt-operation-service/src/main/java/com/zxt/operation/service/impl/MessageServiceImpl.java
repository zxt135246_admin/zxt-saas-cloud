package com.zxt.operation.service.impl;

import com.alibaba.fastjson.JSON;
import com.zxt.bean.result.Result;
import com.zxt.operation.api.dto.SendEmailApiDTO;
import com.zxt.operation.api.dto.SmsSendApiDTO;
import com.zxt.operation.client.MailClient;
import com.zxt.operation.service.IMessageService;
import com.zxt.operation.sms.ISmsService;
import com.zxt.operation.sms.SmsSendVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Service;


/**
 * 业务逻辑层-短信消息记录实现类
 */
@Slf4j
@Service
@RefreshScope
public class MessageServiceImpl implements IMessageService {

    private final ISmsService smsService;
    private final MailClient mailClient;

    @Autowired
    public MessageServiceImpl(ISmsService smsService, MailClient mailClient) {
        this.smsService = smsService;
        this.mailClient = mailClient;
    }

    @Value("${sms.open:false}")
    private boolean open;

    /**
     * 发送短信
     *
     * @param dto 入参
     */
    @Override
    public void sendMessage(SmsSendApiDTO dto) {
        log.info("发送短信入参={}", JSON.toJSONString(dto));
        if (!dto.getTemplateCode().contains("SMS_")) {
            dto.setTemplateCode("SMS_" + dto.getTemplateCode());
        }
        if (open) {
            Result<SmsSendVO> result = smsService.sendMessage(dto.getSignName(), dto.getMobile(), dto.getTemplateCode(), dto.getTemplateParam());
            log.info("短信发送结果={}", JSON.toJSONString(result));
        }
    }


    @Override
    public void sendEmail(SendEmailApiDTO dto) {
        mailClient.sendEmail(dto);
    }
}
