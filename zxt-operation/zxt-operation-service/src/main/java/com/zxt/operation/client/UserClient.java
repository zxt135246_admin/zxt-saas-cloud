package com.zxt.operation.client;

import com.alibaba.fastjson.JSON;
import com.zxt.bean.result.Result;
import com.zxt.user.api.feignClient.UserFeignClient;
import com.zxt.user.api.vo.UserApiVO;
import com.zxt.user.api.vo.UserConcernApiVO;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Slf4j
@AllArgsConstructor
public class UserClient {


    private final UserFeignClient userFeignClient;

    /**
     * 获取全部用户列表
     *
     * @return
     */
    public List<UserApiVO> getAllUser() {
        log.info("运营中心-用户中心 获取全用户列表信息");
        try {
            Result<List<UserApiVO>> result = userFeignClient.getAllUser();
            log.info("运营中心-用户中心 获取全用户列表信息，出参:{}", JSON.toJSONString(result));
            return result.getData();
        } catch (Exception e) {
            log.error("运营中心-用户中心 获取全用户列表信息,异常信息:{}", e);
        }
        return null;
    }

    /**
     * 获取用户的粉丝列表
     *
     * @param concernUserId
     * @return
     */
    public List<UserConcernApiVO> getUserConcernByConcernUserId(Long concernUserId) {
        log.info("文案中心-用户中心 获取用户的粉丝列表，入参:{}", JSON.toJSONString(concernUserId));
        try {
            Result<List<UserConcernApiVO>> result = userFeignClient.getUserConcernByConcernUserId(concernUserId);
            log.info("文案中心-用户中心 获取用户的粉丝列表，出参:{}", JSON.toJSONString(result));
            return result.getData();
        } catch (Exception e) {
            log.error("文案中心-用户中心 获取用户的粉丝列表,异常信息:{}", e);
        }
        return null;
    }

    public UserApiVO getUserById(Long userId) {
        log.info("文案中心-用户中心 获取用户信息，入参:{}", JSON.toJSONString(userId));
        try {
            Result<UserApiVO> result = userFeignClient.getUserById(userId);
            log.info("文案中心-用户中心 获取用户信息，出参:{}", JSON.toJSONString(result));
            return result.getData();
        } catch (Exception e) {
            log.error("文案中心-用户中心 获取用户信息,异常信息:{}", e);
        }
        return null;
    }
}
