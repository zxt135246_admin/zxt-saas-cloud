package com.zxt.operation.dto;

import com.zxt.bean.query.QueryDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("系统消息查询入参")
public class SystemNewsQueryDTO extends QueryDTO {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 1L;

    @ApiModelProperty("消息类型1.关注消息 2.文案消息 3.相册消息")
    private Byte newsType;

    @ApiModelProperty("消息内容模糊字段")
    private String contentLike;

    @ApiModelProperty(value = "用户id", hidden = true)
    private Long userId;

    @ApiModelProperty(value = "是否已读")
    private Boolean isRead;

    /**
     * -------------------------------------------------
     * 上面字段由工具自动生成，请在下面添加扩充字段
     * -------------------------------------------------
     */


}
