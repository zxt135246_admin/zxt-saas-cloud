package com.zxt.operation.api.feignClient;

import com.zxt.bean.constants.ServiceNameConstant;
import com.zxt.bean.result.Result;
import com.zxt.operation.api.dto.SystemNewsApiDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@FeignClient(value = ServiceNameConstant.ZXT_OPERATION, path = "/feign/systemNews")
public interface SystemNewsFeignClient {


    /**
     * 保存系统消息
     *
     * @param apiDTO
     * @return
     */
    @PostMapping(value = "saveSystemNews")
    Result saveSystemNews(@RequestBody SystemNewsApiDTO apiDTO);

    /**
     * 批量保存系统消息
     *
     * @param dtoList
     * @return
     */
    @PostMapping(value = "saveBatchSystemNews")
    Result saveBatchSystemNews(@RequestBody List<SystemNewsApiDTO> dtoList);
}
