package com.zxt.operation.api.feignClient;

import com.zxt.bean.constants.ServiceNameConstant;
import com.zxt.bean.result.Result;
import com.zxt.operation.api.dto.SendEmailApiDTO;
import com.zxt.operation.api.dto.SmsSendApiDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(value = ServiceNameConstant.ZXT_OPERATION, path = "/feign/message")
public interface MessageFeignClient {

    /**
     * 发送短信
     *
     * @param dto 入参
     */
    @PostMapping("sendSms")
    Result sendSms(@Validated @RequestBody SmsSendApiDTO dto);


    /**
     * 发送邮件
     */
    @PostMapping("sendEmail")
    Result<Boolean> sendEmail(@Validated @RequestBody SendEmailApiDTO dto);
}
