package com.zxt.operation.api.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author zxt
 */
@Data
@ApiModel("运营中心相册api入参")
public class ImagesApiDTO {

    @ApiModelProperty(value = "id")
    private Long id;
    @ApiModelProperty(value = "原图url")
    private String originalImageUrl;
}
