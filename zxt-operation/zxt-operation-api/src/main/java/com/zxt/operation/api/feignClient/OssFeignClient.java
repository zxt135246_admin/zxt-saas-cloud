package com.zxt.operation.api.feignClient;

import com.zxt.bean.constants.ServiceNameConstant;
import com.zxt.bean.result.Result;
import com.zxt.operation.api.dto.ImagesApiDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * @author zxt
 */
@FeignClient(value = ServiceNameConstant.ZXT_OPERATION, path = "/feign/oss")
public interface OssFeignClient {

    @PostMapping(value = "uploadImage", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    Result<Boolean> uploadImage(@RequestPart("file") MultipartFile multiFile, @RequestParam("directory") String directory, @RequestParam("imageId") Long imageId);

    /**
     * 根据url上传图片
     *
     * @param url
     * @param directory
     * @param imageId
     * @return
     */
    @PostMapping(value = "uploadImageByUrl")
    Result uploadImageByUrl(@RequestParam("url") String url, @RequestParam("directory") String directory, @RequestParam("imageId") Long imageId);

    /**
     * 初始化相册exif
     *
     * @param imagesList
     * @return
     */
    @PostMapping(value = "initImagesExif")
    Result initImagesExif(@RequestBody List<ImagesApiDTO> imagesList);
}
