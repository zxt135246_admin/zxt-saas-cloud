package com.zxt.operation.api.dto;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;


@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString(callSuper = true)
@ApiModel("短信发送api入参")
public class SmsSendApiDTO implements Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 1L;

    /**
     * 手机号
     */
    @NotNull(message = "手机号不能为空")
    @Size(max = 20, message = "手机号不能超过20位")
    private String mobile;

    /**
     * 签名名称
     */
    private String signName;

    /**
     * 模版编码
     */
    @NotNull(message = "模版编码不能为空")
    @Size(max = 32, message = "模版编码不能超过32位")
    private String templateCode;

    /**
     * 模版参数
     */
    private String templateParam;

    /**
     * 发送来源 无需填
     */
    private String sendSource;

    /**
     * 发送类型 无需填
     */
    private Byte sendType;


}
