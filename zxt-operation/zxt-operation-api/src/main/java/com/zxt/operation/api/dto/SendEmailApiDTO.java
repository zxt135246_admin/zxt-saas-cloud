package com.zxt.operation.api.dto;

import lombok.*;

import javax.validation.constraints.NotNull;
import java.io.Serializable;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString(callSuper = true)
public class SendEmailApiDTO implements Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 1L;

    /**
     * 收件人邮箱
     */
    @NotNull(message = "收件人邮箱不能为空")
    private String toEmail;

    /**
     * 邮箱标题
     */
    @NotNull(message = "邮箱标题不能为空")
    private String subject;

    /**
     * 邮件内容
     */
    @NotNull(message = "邮件内容不能为空")
    private String content;


}
