package com.zxt.operation.api.enums;

/**
 * @Comments: 消息类型1.关注消息 2.文案消息 3.相册消息 4.系统通知
 */
public enum NewsTypeEnum {
	CONCERN                 ((byte) 1, "关注消息"),
	CONTENT                 ((byte) 2, "文案消息"),
	IMAGES                ((byte) 3, "相册消息"),
	SYSTEM                ((byte) 4, "系统通知"),
	;

	private byte value;
	private String desc;

	NewsTypeEnum(byte value, String desc){
		this.value = value;
		this.desc = desc;
	}

    public byte getValue() {
        return value;
    }

    public String getDesc() {
		return desc;
	}

	public static boolean exists(Byte status) {
        if (status == null) {
            return false;
        }
        byte s = status.byteValue();
        return exists(s);
    }

    public static boolean exists(byte s) {
        for (NewsTypeEnum element : NewsTypeEnum.values()) {
            if (element.value == s) {
                return true;
            }
        }
        return false;
    }

    public boolean equal(Byte val) {
        return val == null ? false : val.byteValue() == this.value;
    }

	public static String getDescByValue(Byte value) {
		if (value == null) {
			return "";
		}
		for (NewsTypeEnum element : NewsTypeEnum.values()) {
			if (element.value == value.byteValue()) {
				return element.desc;
			}
		}
		return "";
	}
}
