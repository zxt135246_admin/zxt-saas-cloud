package com.zxt.rocketmq.async;


import com.alibaba.fastjson.JSON;
import com.zxt.bean.result.Result;
import com.zxt.rocketmq.annotation.RocketMqLog;
import com.zxt.rocketmq.feignClient.RocketMqRecordFeignClient;
import com.zxt.rocketmq.req.RocketMqDTO;
import com.zxt.rocketmq.req.RocketMqRecordDTO;
import com.zxt.rocketmq.utils.RocketMqUtil;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.lang.reflect.Method;
import java.util.Map;
import java.util.Objects;

@Service
public class RocketMqLogAsync {
    private static final Logger log = LoggerFactory.getLogger(RocketMqLogAsync.class);
    private final RocketMqRecordFeignClient rocketMqRecordFeignClient;

    @Async
    public void add(ProceedingJoinPoint joinPoint, Long time, String traceId) {
        try {
            MethodSignature signature = (MethodSignature) joinPoint.getSignature();
            Method method = signature.getMethod();
            RocketMqRecordDTO dto = new RocketMqRecordDTO();
            RocketMqLog rocketMqLog = (RocketMqLog) method.getAnnotation(RocketMqLog.class);
            if (rocketMqLog != null) {
                dto.setRecordName(rocketMqLog.value());
                dto.setProjectName(rocketMqLog.name());
            }

            String className = joinPoint.getTarget().getClass().getName();
            String methodName = signature.getName();
            dto.setMethod(className + "." + methodName + "()");
            Object[] args = joinPoint.getArgs();
            String params = JSON.toJSONString(args[0]);
            dto.setParams(params);
            dto.setTraceId(traceId);
            if (!Objects.isNull(args[1])) {
                Map headers = (Map) args[1];
                RocketMqDTO mqDTO = RocketMqUtil.getDto(headers);
                dto.setTopic(mqDTO.getTopic());
                dto.setMessageId(mqDTO.getMessageId());
                dto.setSendHost(mqDTO.getSendHost());
                dto.setSendTime(mqDTO.getSendTime());
                dto.setReconsumeTimes(mqDTO.getReconsumeTimes());
                dto.setExecutionTime(time);
                Result<Long> result = this.rocketMqRecordFeignClient.add(dto);
                log.info("保存mq记录result={}", JSON.toJSONString(result));
            }
        } catch (Exception var15) {
            log.error(var15.getMessage(), var15);
        }

    }

    public RocketMqLogAsync(final RocketMqRecordFeignClient rocketMqRecordFeignClient) {
        this.rocketMqRecordFeignClient = rocketMqRecordFeignClient;
    }
}