package com.zxt.rocketmq.feignClient;

import com.zxt.bean.constants.ServiceNameConstant;
import com.zxt.bean.result.Result;
import com.zxt.rocketmq.req.RocketMqRecordDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(value = ServiceNameConstant.ZXT_OPERATION, path = "/feign/rocketMqRecord")
public interface RocketMqRecordFeignClient {

    /**
     * 保存mq消息
     *
     * @param dto
     * @return
     */
    @PostMapping({"/add"})
    Result<Long> add(@RequestBody RocketMqRecordDTO dto);
}
