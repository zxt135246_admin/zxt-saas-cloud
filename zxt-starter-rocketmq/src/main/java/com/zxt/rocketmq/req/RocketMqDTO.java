package com.zxt.rocketmq.req;


import java.time.LocalDateTime;

public class RocketMqDTO {
    private String topic;
    private int reconsumeTimes;
    private String messageId;
    private String sendHost;
    private LocalDateTime sendTime;

    public String getTopic() {
        return this.topic;
    }

    public int getReconsumeTimes() {
        return this.reconsumeTimes;
    }

    public String getMessageId() {
        return this.messageId;
    }

    public String getSendHost() {
        return this.sendHost;
    }

    public LocalDateTime getSendTime() {
        return this.sendTime;
    }

    public void setTopic(final String topic) {
        this.topic = topic;
    }

    public void setReconsumeTimes(final int reconsumeTimes) {
        this.reconsumeTimes = reconsumeTimes;
    }

    public void setMessageId(final String messageId) {
        this.messageId = messageId;
    }

    public void setSendHost(final String sendHost) {
        this.sendHost = sendHost;
    }

    public void setSendTime(final LocalDateTime sendTime) {
        this.sendTime = sendTime;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof RocketMqDTO)) {
            return false;
        } else {
            RocketMqDTO other = (RocketMqDTO) o;
            if (!other.canEqual(this)) {
                return false;
            } else if (this.getReconsumeTimes() != other.getReconsumeTimes()) {
                return false;
            } else {
                label61:
                {
                    Object this$topic = this.getTopic();
                    Object other$topic = other.getTopic();
                    if (this$topic == null) {
                        if (other$topic == null) {
                            break label61;
                        }
                    } else if (this$topic.equals(other$topic)) {
                        break label61;
                    }

                    return false;
                }

                label54:
                {
                    Object this$messageId = this.getMessageId();
                    Object other$messageId = other.getMessageId();
                    if (this$messageId == null) {
                        if (other$messageId == null) {
                            break label54;
                        }
                    } else if (this$messageId.equals(other$messageId)) {
                        break label54;
                    }

                    return false;
                }

                Object this$sendHost = this.getSendHost();
                Object other$sendHost = other.getSendHost();
                if (this$sendHost == null) {
                    if (other$sendHost != null) {
                        return false;
                    }
                } else if (!this$sendHost.equals(other$sendHost)) {
                    return false;
                }

                Object this$sendTime = this.getSendTime();
                Object other$sendTime = other.getSendTime();
                if (this$sendTime == null) {
                    if (other$sendTime != null) {
                        return false;
                    }
                } else if (!this$sendTime.equals(other$sendTime)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof RocketMqDTO;
    }

    public int hashCode() {
        Boolean PRIME = true;
        int result = 1;
        result = result * 59 + this.getReconsumeTimes();
        Object $topic = this.getTopic();
        result = result * 59 + ($topic == null ? 43 : $topic.hashCode());
        Object $messageId = this.getMessageId();
        result = result * 59 + ($messageId == null ? 43 : $messageId.hashCode());
        Object $sendHost = this.getSendHost();
        result = result * 59 + ($sendHost == null ? 43 : $sendHost.hashCode());
        Object $sendTime = this.getSendTime();
        result = result * 59 + ($sendTime == null ? 43 : $sendTime.hashCode());
        return result;
    }

    public RocketMqDTO() {
    }

    public RocketMqDTO(final String topic, final int reconsumeTimes, final String messageId, final String sendHost, final LocalDateTime sendTime) {
        this.topic = topic;
        this.reconsumeTimes = reconsumeTimes;
        this.messageId = messageId;
        this.sendHost = sendHost;
        this.sendTime = sendTime;
    }

    public String toString() {
        return "RocketMqDTO(super=" + super.toString() + ", topic=" + this.getTopic() + ", reconsumeTimes=" + this.getReconsumeTimes() + ", messageId=" + this.getMessageId() + ", sendHost=" + this.getSendHost() + ", sendTime=" + this.getSendTime() + ")";
    }
}