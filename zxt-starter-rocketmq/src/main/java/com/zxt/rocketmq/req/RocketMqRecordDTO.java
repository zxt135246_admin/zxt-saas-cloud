package com.zxt.rocketmq.req;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDateTime;

@ApiModel("RocketMq记录")
@Data
public class RocketMqRecordDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;

    @ApiModelProperty("项目名称")
    @NotNull(message = "项目名称不能为空")
    @Size(max = 64, message = "项目名称不能超过64位")
    private String projectName;

    @ApiModelProperty("记录名称")
    @NotNull(message = "记录名称不能为空")
    @Size(max = 64, message = "记录名称不能超过64位")
    private String recordName;

    @ApiModelProperty("topic")
    @Size(max = 100, message = "topic不能超过100位")
    private String topic;

    @ApiModelProperty("消息id")
    @Size(max = 100, message = "消息id不能超过100位")
    private String messageId;

    @ApiModelProperty("发送者host")
    @Size(max = 64, message = "发送者host不能超过64位")
    private String sendHost;

    @ApiModelProperty("发送时间")
    private LocalDateTime sendTime;

    @ApiModelProperty("重试次数")
    @NotNull(message = "重试次数不能为空")
    private Integer reconsumeTimes;

    @ApiModelProperty("处理方法")
    @Size(max = 255, message = "处理方法不能超过255位")
    private String method;

    @ApiModelProperty("消息参数")
    @Size(max = 1000, message = "消息参数不能超过1000位")
    private String params;

    @ApiModelProperty("执行时长(毫秒)")
    @NotNull(message = "执行时长(毫秒)不能为空")
    private Long executionTime;

    private String traceId;

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof RocketMqRecordDTO)) {
            return false;
        } else {
            RocketMqRecordDTO other = (RocketMqRecordDTO) o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label143:
                {
                    Object this$reconsumeTimes = this.getReconsumeTimes();
                    Object other$reconsumeTimes = other.getReconsumeTimes();
                    if (this$reconsumeTimes == null) {
                        if (other$reconsumeTimes == null) {
                            break label143;
                        }
                    } else if (this$reconsumeTimes.equals(other$reconsumeTimes)) {
                        break label143;
                    }

                    return false;
                }

                Object this$executionTime = this.getExecutionTime();
                Object other$executionTime = other.getExecutionTime();
                if (this$executionTime == null) {
                    if (other$executionTime != null) {
                        return false;
                    }
                } else if (!this$executionTime.equals(other$executionTime)) {
                    return false;
                }

                Object this$projectName = this.getProjectName();
                Object other$projectName = other.getProjectName();
                if (this$projectName == null) {
                    if (other$projectName != null) {
                        return false;
                    }
                } else if (!this$projectName.equals(other$projectName)) {
                    return false;
                }

                label122:
                {
                    Object this$recordName = this.getRecordName();
                    Object other$recordName = other.getRecordName();
                    if (this$recordName == null) {
                        if (other$recordName == null) {
                            break label122;
                        }
                    } else if (this$recordName.equals(other$recordName)) {
                        break label122;
                    }

                    return false;
                }

                label115:
                {
                    Object this$topic = this.getTopic();
                    Object other$topic = other.getTopic();
                    if (this$topic == null) {
                        if (other$topic == null) {
                            break label115;
                        }
                    } else if (this$topic.equals(other$topic)) {
                        break label115;
                    }

                    return false;
                }

                Object this$messageId = this.getMessageId();
                Object other$messageId = other.getMessageId();
                if (this$messageId == null) {
                    if (other$messageId != null) {
                        return false;
                    }
                } else if (!this$messageId.equals(other$messageId)) {
                    return false;
                }

                Object this$sendHost = this.getSendHost();
                Object other$sendHost = other.getSendHost();
                if (this$sendHost == null) {
                    if (other$sendHost != null) {
                        return false;
                    }
                } else if (!this$sendHost.equals(other$sendHost)) {
                    return false;
                }

                label94:
                {
                    Object this$sendTime = this.getSendTime();
                    Object other$sendTime = other.getSendTime();
                    if (this$sendTime == null) {
                        if (other$sendTime == null) {
                            break label94;
                        }
                    } else if (this$sendTime.equals(other$sendTime)) {
                        break label94;
                    }

                    return false;
                }

                label87:
                {
                    Object this$method = this.getMethod();
                    Object other$method = other.getMethod();
                    if (this$method == null) {
                        if (other$method == null) {
                            break label87;
                        }
                    } else if (this$method.equals(other$method)) {
                        break label87;
                    }

                    return false;
                }

                Object this$params = this.getParams();
                Object other$params = other.getParams();
                if (this$params == null) {
                    if (other$params != null) {
                        return false;
                    }
                } else if (!this$params.equals(other$params)) {
                    return false;
                }

                Object this$traceId = this.getTraceId();
                Object other$traceId = other.getTraceId();
                if (this$traceId == null) {
                    if (other$traceId != null) {
                        return false;
                    }
                } else if (!this$traceId.equals(other$traceId)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof RocketMqRecordDTO;
    }

    public int hashCode() {
        Boolean PRIME = true;
        int result = 1;
        Object $reconsumeTimes = this.getReconsumeTimes();
        result = result * 59 + ($reconsumeTimes == null ? 43 : $reconsumeTimes.hashCode());
        Object $executionTime = this.getExecutionTime();
        result = result * 59 + ($executionTime == null ? 43 : $executionTime.hashCode());
        Object $projectName = this.getProjectName();
        result = result * 59 + ($projectName == null ? 43 : $projectName.hashCode());
        Object $recordName = this.getRecordName();
        result = result * 59 + ($recordName == null ? 43 : $recordName.hashCode());
        Object $topic = this.getTopic();
        result = result * 59 + ($topic == null ? 43 : $topic.hashCode());
        Object $messageId = this.getMessageId();
        result = result * 59 + ($messageId == null ? 43 : $messageId.hashCode());
        Object $sendHost = this.getSendHost();
        result = result * 59 + ($sendHost == null ? 43 : $sendHost.hashCode());
        Object $sendTime = this.getSendTime();
        result = result * 59 + ($sendTime == null ? 43 : $sendTime.hashCode());
        Object $method = this.getMethod();
        result = result * 59 + ($method == null ? 43 : $method.hashCode());
        Object $params = this.getParams();
        result = result * 59 + ($params == null ? 43 : $params.hashCode());
        Object $traceId = this.getTraceId();
        result = result * 59 + ($traceId == null ? 43 : $traceId.hashCode());
        return result;
    }

    public RocketMqRecordDTO() {
    }

    public RocketMqRecordDTO(final String projectName, final String recordName, final String topic, final String messageId, final String sendHost, final LocalDateTime sendTime, final Integer reconsumeTimes, final String method, final String params, final Long executionTime, final String traceId) {
        this.projectName = projectName;
        this.recordName = recordName;
        this.topic = topic;
        this.messageId = messageId;
        this.sendHost = sendHost;
        this.sendTime = sendTime;
        this.reconsumeTimes = reconsumeTimes;
        this.method = method;
        this.params = params;
        this.executionTime = executionTime;
        this.traceId = traceId;
    }

    public String toString() {
        return "RocketMqRecordDTO(super=" + super.toString() + ", projectName=" + this.getProjectName() + ", recordName=" + this.getRecordName() + ", topic=" + this.getTopic() + ", messageId=" + this.getMessageId() + ", sendHost=" + this.getSendHost() + ", sendTime=" + this.getSendTime() + ", reconsumeTimes=" + this.getReconsumeTimes() + ", method=" + this.getMethod() + ", params=" + this.getParams() + ", executionTime=" + this.getExecutionTime() + ", traceId=" + this.getTraceId() + ")";
    }
}
