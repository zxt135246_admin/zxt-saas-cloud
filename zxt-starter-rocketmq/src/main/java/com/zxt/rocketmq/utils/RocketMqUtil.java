package com.zxt.rocketmq.utils;


import com.zxt.bean.utils.LocalDateUtil;
import com.zxt.rocketmq.req.RocketMqDTO;

import java.time.LocalDateTime;
import java.util.Map;
import java.util.Objects;

public class RocketMqUtil {
    public RocketMqUtil() {
    }

    public static RocketMqDTO getDto(Map headerMap) {
        RocketMqDTO dto = new RocketMqDTO();
        if (!Objects.isNull(headerMap) && headerMap.size() != 0) {
            dto.setTopic(headerMap.get("rocketmq_TOPIC").toString());
            int reconsumeTimes = 0;

            try {
                Object timesObject = headerMap.get("rocketmq_RECONSUME_TIMES");
                if (!Objects.isNull(timesObject)) {
                    reconsumeTimes = new Integer(timesObject.toString());
                }
            } catch (NumberFormatException var7) {
            }

            dto.setReconsumeTimes(reconsumeTimes);
            dto.setMessageId(headerMap.get("rocketmq_MESSAGE_ID").toString());
            LocalDateTime sendTime = LocalDateTime.now();

            try {
                Object object = headerMap.get("rocketmq_BORN_TIMESTAMP");
                if (!Objects.isNull(object)) {
                    Long timestamp = new Long(object.toString());
                    sendTime = LocalDateUtil.toLocalTime(timestamp);
                }
            } catch (Exception var6) {
            }

            dto.setSendTime(sendTime);
            dto.setSendHost(headerMap.get("rocketmq_BORN_HOST").toString());
            return dto;
        } else {
            return dto;
        }
    }
}