package com.zxt.rocketmq.annotation;

import java.lang.annotation.*;

/**
 * @author zxt
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RocketMqLog {
    /**
     * 记录
     *
     * @return
     */
    String value() default "";

    /**
     * 项目名称
     *
     * @return
     */
    String name() default "";
}