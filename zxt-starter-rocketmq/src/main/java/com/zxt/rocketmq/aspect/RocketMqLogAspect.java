package com.zxt.rocketmq.aspect;

import com.zxt.bean.utils.StringUtils;
import com.zxt.rocketmq.async.RocketMqLogAsync;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class RocketMqLogAspect {
    private final RocketMqLogAsync rocketMqLogAsync;

    @Pointcut("@annotation(com.zxt.rocketmq.annotation.RocketMqLog)")
    public void logPointCut() {
    }

    @Around("logPointCut()")
    public Object around(ProceedingJoinPoint joinPoint) throws Throwable {
        long beginTime = System.currentTimeMillis();
        Object result = joinPoint.proceed();
        long time = System.currentTimeMillis() - beginTime;
        this.rocketMqLogAsync.add(joinPoint, time, StringUtils.getTraceId());
        return result;
    }

    public RocketMqLogAspect(final RocketMqLogAsync rocketMqLogAsync) {
        this.rocketMqLogAsync = rocketMqLogAsync;
    }
}
