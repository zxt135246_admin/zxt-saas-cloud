package com.zxt.rocketmq.aspect;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.MDC;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Aspect
@Component
public class RocketMqTraceIdAspect {
    @Pointcut("@annotation(org.springframework.cloud.stream.annotation.StreamListener)")
    public void logPointCut() {
    }

    @Before("logPointCut()")
    public void before() throws Throwable {
        try {
            MDC.put("traceId", UUID.randomUUID().toString().replace("-", "").toUpperCase());
        } catch (Exception var2) {
        }

    }

    public RocketMqTraceIdAspect() {
    }
}