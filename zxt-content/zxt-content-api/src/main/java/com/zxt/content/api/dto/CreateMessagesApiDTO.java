package com.zxt.content.api.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@ApiModel("创建mq消息api入参")
@AllArgsConstructor
@NoArgsConstructor
public class CreateMessagesApiDTO {

    @ApiModelProperty(value = "1.文案 2.相册")
    private Byte messagesType;

    @ApiModelProperty(value = "数据id")
    private Long DataId;

    /**
     * 日志id
     */
    String traceId;
}
