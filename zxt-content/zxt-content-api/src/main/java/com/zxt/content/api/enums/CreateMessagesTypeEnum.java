package com.zxt.content.api.enums;

/**
 * @Comments: 创建mq消息类型
 */
public enum CreateMessagesTypeEnum {
	CONTENT                 ((byte) 1, "文案"),
	IMAGES                 ((byte) 2, "相册"),
	;

	private byte value;
	private String desc;

	CreateMessagesTypeEnum(byte value, String desc){
		this.value = value;
		this.desc = desc;
	}

    public byte getValue() {
        return value;
    }

    public String getDesc() {
		return desc;
	}

	public static boolean exists(Byte status) {
        if (status == null) {
            return false;
        }
        byte s = status.byteValue();
        return exists(s);
    }

    public static boolean exists(byte s) {
        for (CreateMessagesTypeEnum element : CreateMessagesTypeEnum.values()) {
            if (element.value == s) {
                return true;
            }
        }
        return false;
    }

    public boolean equal(Byte val) {
        return val == null ? false : val.byteValue() == this.value;
    }

	public static String getDescByValue(Byte value) {
		if (value == null) {
			return "";
		}
		for (CreateMessagesTypeEnum element : CreateMessagesTypeEnum.values()) {
			if (element.value == value.byteValue()) {
				return element.desc;
			}
		}
		return "";
	}
}
