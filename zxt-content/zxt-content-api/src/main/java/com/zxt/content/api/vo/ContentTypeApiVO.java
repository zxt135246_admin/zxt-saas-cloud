package com.zxt.content.api.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author zxt
 */
@Data
@ApiModel("文案类型api出参")
public class ContentTypeApiVO {
    @ApiModelProperty("id")
    private Long id;

    @ApiModelProperty("类型名称")
    private String typeName;
}
