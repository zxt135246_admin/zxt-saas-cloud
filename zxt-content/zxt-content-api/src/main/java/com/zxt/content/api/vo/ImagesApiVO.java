package com.zxt.content.api.vo;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.zxt.bean.convertor.LongJsonDeserializer;
import com.zxt.bean.convertor.LongJsonSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author zxt
 */
@Data
@ApiModel("相册api出参")
public class ImagesApiVO {

    @ApiModelProperty("id")
    @JsonSerialize(using = LongJsonSerializer.class)
    @JsonDeserialize(using = LongJsonDeserializer.class)
    private Long id;

    @ApiModelProperty("用户id")
    @JsonSerialize(using = LongJsonSerializer.class)
    @JsonDeserialize(using = LongJsonDeserializer.class)
    private Long userId;

    @ApiModelProperty("相册集合id")
    @JsonSerialize(using = LongJsonSerializer.class)
    @JsonDeserialize(using = LongJsonDeserializer.class)
    private Long collectionsId;

    @ApiModelProperty("城市名称")
    private String cityName;

    @ApiModelProperty("相册地址")
    private String imageUrl;

    @ApiModelProperty("原图地址")
    private String originalImageUrl;

    @ApiModelProperty("相册备注")
    private String remark;

    @ApiModelProperty("创建时间")
    private LocalDateTime createTime;
}
