package com.zxt.content.api.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author zxt
 */
@Data
@ApiModel("文案中心相册api入参")
public class ImagesApiDTO {

    @ApiModelProperty(value = "id")
    private Long id;

    @ApiModelProperty(value = "用户id")
    private Long userId;

    @ApiModelProperty(value = "城市名称")
    private String cityName;

    @ApiModelProperty(value = "共享用户id")
    private Long shareUserId;

    @ApiModelProperty(value = "图片url")
    private String imageUrl;

    @ApiModelProperty(value = "原图url")
    private String originalImageUrl;

    @ApiModelProperty(value = "说明")
    private String remark;

    /**
     * 其他需要
     */
    @ApiModelProperty("用户昵称")
    private String nickName;

    @ApiModelProperty("拍摄地址")
    private String shootingAddress;
}
