package com.zxt.content.api.feignClient;

import com.zxt.bean.constants.ServiceNameConstant;
import com.zxt.bean.result.Result;
import com.zxt.content.api.dto.ContentApiDTO;
import com.zxt.content.api.vo.ContentApiVO;
import com.zxt.content.api.vo.ContentTypeApiVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * @author zxt
 */
@FeignClient(value = ServiceNameConstant.ZXT_CONTENT, path = "/feign/content")
public interface ContentFeignClient {

    /**
     * 查询文案
     *
     * @param name
     * @return
     */
    @GetMapping("getContent")
    Result<String> getContent(@RequestParam String name);

    /**
     * 获取用户发布的文案
     *
     * @param userId
     * @return
     */
    @GetMapping(value = "getPrivateContentByUserId")
    Result<List<ContentApiVO>> getPrivateContentByUserId(@RequestParam Long userId);

    /**
     * 新增文案
     *
     * @param dto
     * @return
     */
    @PostMapping(value = "addContent")
    Result<Boolean> addContent(@RequestBody ContentApiDTO dto);

    /**
     * 随机获取一条文案
     *
     * @param userId
     * @return
     */
    @GetMapping(value = "randGetOneContentByUserId")
    Result<ContentApiVO> randGetOneContentByUserId(@RequestParam(value = "userId", required = false) Long userId);

    /**
     * 获取文案类型列表
     *
     * @return
     */
    @GetMapping(value = "getContentTypeList")
    Result<List<ContentTypeApiVO>> getContentTypeList();

    /**
     * 根据文案id查询
     *
     * @param contentId
     * @return
     */
    @GetMapping(value = "getById")
    Result<ContentApiVO> getById(@RequestParam("contentId") Long contentId);
}
