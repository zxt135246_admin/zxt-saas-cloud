package com.zxt.content.api.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("文案api出参")
public class ContentApiVO {

    @ApiModelProperty("id")
    private Long id;

    @ApiModelProperty("用户id")
    private Long userId;

    @ApiModelProperty("内容")
    private String content;

    @ApiModelProperty("来源")
    private String contentSource;

    @ApiModelProperty("文案类型id")
    private Long contentTypeId;

    @ApiModelProperty("是否已经发布")
    private Boolean isPublish;

    @ApiModelProperty("文案热度")
    private Integer contentHeat;
}
