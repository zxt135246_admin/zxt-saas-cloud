package com.zxt.content.api.feignClient;

import com.zxt.bean.constants.ServiceNameConstant;
import com.zxt.bean.result.Result;
import com.zxt.content.api.dto.ImagesApiDTO;
import com.zxt.content.api.dto.ImagesExifApiDTO;
import com.zxt.content.api.vo.ImagesApiVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * @author zxt
 */
@FeignClient(value = ServiceNameConstant.ZXT_CONTENT, path = "/feign/images")
public interface ImagesFeignClient {

    /**
     * 更新
     *
     * @param dto
     * @return
     */
    @PostMapping("updateById")
    Result<Boolean> updateById(@RequestBody ImagesApiDTO dto);

    /**
     * 保存相册Exif
     *
     * @param dto
     * @return
     */
    @PostMapping("saveOrUpdateImagesExif")
    Result<Boolean> saveOrUpdateImagesExif(@RequestBody ImagesExifApiDTO dto);

    @PostMapping("saveBatchImagesExif")
    Result<Boolean> saveBatchImagesExif(@RequestBody List<ImagesExifApiDTO> imagesExifApiDTOList);

    /**
     * 查询相册
     *
     * @param imagesId
     * @return
     */
    @GetMapping("getById")
    Result<ImagesApiVO> getById(@RequestParam("imagesId") Long imagesId);
}
