package com.zxt.content.api.feignClient;

import com.zxt.bean.constants.ServiceNameConstant;
import com.zxt.bean.result.Result;
import com.zxt.content.api.vo.ImagesCollectionsApiVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;


/**
 * @author zxt
 */
@FeignClient(value = ServiceNameConstant.ZXT_CONTENT, path = "/feign/imagesCollections")
public interface ImagesCollectionsFeignClient {


    /**
     * 查询相册合集
     *
     * @param id
     * @return
     */
    @GetMapping("getById")
    Result<ImagesCollectionsApiVO> getById(@RequestParam("id") Long id);
}
