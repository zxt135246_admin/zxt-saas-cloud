package com.zxt.content.api.MqConstant;


import com.zxt.bean.constants.MqConstant;

/**
 * 文案中心RocketMQ定义
 */
public interface ContentMqConstant {

    /**
     * 文案创建成功
     */
    String CONTENT_CREATE_SUCCESS = "content-create-success";

    /**
     * 文案创建成功生成者
     */
    String CONTENT_CREATE_SUCCESS_SUBJECT = CONTENT_CREATE_SUCCESS + MqConstant.DASH + MqConstant.SUBJECT;

    /**
     * 文案创建成功消费者
     */
    String CONTENT_CREATE_SUCCESS_CONSUME = CONTENT_CREATE_SUCCESS + MqConstant.DASH + MqConstant.CONSUME;

    /**
     * 订单创建失败
     */
    String CONTENT_ORDER_CREATE_FAIL = "content-order-create-fail";

    /**
     * 订单创建失败生成者
     */
    String CONTENT_ORDER_CREATE_FAIL_SUBJECT = CONTENT_ORDER_CREATE_FAIL + MqConstant.DASH + MqConstant.SUBJECT;

    /**
     * 订单创建失败消费者
     */
    String CONTENT_ORDER_CREATE_FAIL_CONSUME = CONTENT_ORDER_CREATE_FAIL + MqConstant.DASH + MqConstant.CONSUME;

}
