package com.zxt.content.api.dto;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@ApiModel("相册exifApi入参")
public class ImagesExifApiDTO {

    @ApiModelProperty("id")
    private Long id;

    @ApiModelProperty("图片id")
    private Long imagesId;

    @ApiModelProperty("高度")
    private Integer imagesHeight;
    @ApiModelProperty("宽度")
    private Integer imagesWidth;
    /**
     * 相册exif参数
     */
    @ApiModelProperty("参数")
    private String exifJson;

    @ApiModelProperty("拍摄时间")
    private LocalDateTime shootingTime;
    /**
     * 经度
     */
    @ApiModelProperty("经度")
    private String longitude;
    /**
     * 纬度
     */
    @ApiModelProperty("纬度")
    private String latitude;

    /**
     * 拍摄地址
     */
    @ApiModelProperty("拍摄地址")
    private String shootingAddress;

    /**
     * 创建时间
     */
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    /**
     * 是否删除
     */
    @TableLogic
    @TableField(value = "is_deleted")
    private Boolean isDeleted;
}
