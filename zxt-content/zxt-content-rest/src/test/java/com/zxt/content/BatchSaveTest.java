package com.zxt.content;

import com.zxt.bean.utils.CollectionsUtil;
import com.zxt.content.asyncs.ContentAsync;
import com.zxt.content.model.ContentType;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.CountDownLatch;

@SpringBootTest
@Slf4j
public class BatchSaveTest {

    @Autowired
    private ContentAsync contentAsync;

    @Test
    public void testBatchSave() {
        log.info("开始批量操作.........");
        Random rand = new Random();
        List<ContentType> list = new ArrayList<>();
        //造100万条数据
        for (int i = 0; i < 1000000; i++) {
            ContentType contentType = new ContentType();
            contentType.setTypeName(String.valueOf(i));
            list.add(contentType);
        }
        log.info("当前集合总共有{}条数据", list.size());
        //2、开始多线程异步批量导入
        long startTime = System.currentTimeMillis(); // 开始时间

        List<List<ContentType>> splitList = CollectionsUtil.split(list, 1000);//拆分集合
        CountDownLatch countDownLatch = new CountDownLatch(splitList.size());
        for (List<ContentType> contentTypeList : splitList) {
            contentAsync.executeAsync(contentTypeList, countDownLatch);
        }
        try {
            countDownLatch.await(); //保证之前的所有的线程都执行完成，才会走下面的；
            long endTime = System.currentTimeMillis(); //结束时间
            log.info("一共耗时time: " + (endTime - startTime) / 1000 + " s");
            // 这样就可以在下面拿到所有线程执行完的集合结果
        } catch (Exception e) {
            log.error("阻塞异常:" + e.getMessage());
        }
    }
}
