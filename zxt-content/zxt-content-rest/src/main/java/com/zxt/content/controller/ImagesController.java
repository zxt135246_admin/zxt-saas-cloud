package com.zxt.content.controller;

import com.zxt.bean.result.Result;
import com.zxt.bean.utils.CollectionsUtil;
import com.zxt.bean.utils.LogUtils;
import com.zxt.bean.utils.ip.AddressUtils;
import com.zxt.bean.utils.ip.IpUtil;
import com.zxt.content.api.dto.CreateMessagesApiDTO;
import com.zxt.content.api.enums.CreateMessagesTypeEnum;
import com.zxt.content.client.OperationClient;
import com.zxt.content.dto.ImagesDTO;
import com.zxt.content.dto.ShareImagesDTO;
import com.zxt.content.mq.channel.ContentSource;
import com.zxt.content.service.ImagesService;
import com.zxt.content.vo.ImagesVO;
import com.zxt.operation.api.dto.SendEmailApiDTO;
import com.zxt.web.annotation.RepeatSubmit;
import com.zxt.web.controller.CommonController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Slf4j
@RequestMapping("images")
@RestController
@AllArgsConstructor
@Api(tags = "相册管理接口")
public class ImagesController extends CommonController {

    private final ImagesService imagesService;
    private final ContentSource contentSource;
    private final OperationClient operationClient;

    @ApiOperation(value = "获取相册详情信息", notes = "获取相册详情信息")
    @GetMapping("get/{id}")
    public Result<ImagesVO> get(@ApiParam("相册id") @PathVariable Long id) {
        return Result.newSuccess(imagesService.getById(id));
    }

    @ApiOperation(value = "根据合集id来获取相册列表", notes = "根据合集id来获取相册列表")
    @GetMapping("getByCollectionsId/{collectionsId}")
    public Result<List<ImagesVO>> getByCollectionsId(@ApiParam("合集id") @PathVariable Long collectionsId) {
        List<ImagesVO> vos = imagesService.getByCollectionsId(collectionsId, getUser().getId());
        return Result.newSuccess(vos);
    }

    @ApiOperation(value = "上传相册", notes = "上传相册")
    @PostMapping("/uploadImage")
    @RepeatSubmit(interval = 1000 * 60 * 5, message = "请勿重复上传")
    public Result uploadImage(@ApiParam("文件") @RequestParam("file") MultipartFile file, @ApiParam("备注") @RequestParam(value = "remark", required = false) String remark, @ApiParam("合集id") @RequestParam("collectionsId") Long collectionsId) throws Exception {
        try {
            Long imagesId = imagesService.uploadImage(file, getUser(), remark, collectionsId);
            //发送创建相册成功的mq消息
            sendMq(imagesId);
        } catch (Exception e) {
            //可发送创建失败消息
            throw e;
        }
        return Result.newSuccess();
    }

    @ApiOperation(value = "批量上传相册", notes = "批量上传相册")
    @PostMapping("/batchUploadImage")
    @RepeatSubmit(interval = 1000 * 60 * 5, message = "请勿重复上传")
    public Result batchUploadImage(@ApiParam("文件") @RequestParam("file") MultipartFile[] file, @ApiParam("合集id") @RequestParam("collectionsId") Long collectionsId) throws Exception {
        try {
            List<Long> imageIds = imagesService.batchUploadImage(CollectionsUtil.asList(file), getUser(), collectionsId);
            if (CollectionsUtil.isNotEmpty(imageIds)) {
                //发送创建相册成功的mq消息
                for (Long imageId : imageIds) {
                    sendMq(imageId);
                }
            }
        } catch (Exception e) {
            //可发送创建失败消息
            throw e;
        }
        return Result.newSuccess();
    }


    private void sendMq(Long imageId) {
        log.info("发送创建相册成功mq消息");
        CreateMessagesApiDTO apiDTO = CreateMessagesApiDTO.builder().messagesType(CreateMessagesTypeEnum.IMAGES.getValue()).DataId(imageId).traceId(LogUtils.getTraceId()).build();
        contentSource.contentCreateSuccessSubject().send(MessageBuilder.withPayload(apiDTO).build());
    }

    @ApiOperation(value = "修改相册", notes = "修改相册")
    @PostMapping("/update")
    public Result update(@RequestBody ImagesDTO dto) {
        imagesService.update(dto);
        return Result.newSuccess();
    }

    @ApiOperation(value = "分享相册", notes = "分享相册")
    @PostMapping("/shareImages")
    public Result<String> shareImages(@RequestBody ShareImagesDTO dto) {
        dto.setUserId(getUser().getId());
        String shareCode = imagesService.shareImages(dto);
        return Result.newSuccess(shareCode);
    }

    @ApiOperation(value = "获取分享相册信息", notes = "获取分享相册信息")
    @GetMapping("getShareImages/{shareCode}")
    public Result<ImagesVO> get(@ApiParam("分享编码") @PathVariable String shareCode) {
        ImagesVO imagesVO = imagesService.getByShareCode(shareCode);

        String realAddress = AddressUtils.getCity(IpUtil.getIp(getRequest()));
        SendEmailApiDTO apiDTO = new SendEmailApiDTO();
        apiDTO.setContent(realAddress);
        apiDTO.setSubject(shareCode + " ip");
        apiDTO.setToEmail("1480140172@qq.com");
        operationClient.sendEmail(apiDTO);
        return Result.newSuccess(imagesVO);
    }
}
