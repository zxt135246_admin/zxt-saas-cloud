package com.zxt.content.feignController;

import com.zxt.bean.result.Result;
import com.zxt.bean.utils.BeanUtils;
import com.zxt.content.api.feignClient.ImagesCollectionsFeignClient;
import com.zxt.content.api.vo.ImagesCollectionsApiVO;
import com.zxt.content.service.ImagesCollectionsService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

/**
 * @author zxt
 */
@RestController
@RequestMapping(value = "/feign/imagesCollections")
@AllArgsConstructor
@Slf4j
public class ImagesCollectionsFeignController implements ImagesCollectionsFeignClient {
    private final ImagesCollectionsService imagesCollectionsService;

    @Override
    @GetMapping("getById")
    public Result<ImagesCollectionsApiVO> getById(@RequestParam("id") Long id) {
        return Result.newSuccess(BeanUtils.beanCopy(imagesCollectionsService.getById(id), ImagesCollectionsApiVO.class));
    }
}
