package com.zxt.content.mq;

import com.alibaba.fastjson.JSON;
import com.zxt.bean.utils.LogUtils;
import com.zxt.content.api.MqConstant.ContentMqConstant;
import com.zxt.content.api.dto.CreateMessagesApiDTO;
import com.zxt.content.api.enums.CreateMessagesTypeEnum;
import com.zxt.content.service.ImagesService;
import com.zxt.rocketmq.annotation.RocketMqLog;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * mq消息处理
 */
@Component
@Slf4j
@AllArgsConstructor
public class ContentMq {

    private final ImagesService imagesService;

    @StreamListener(ContentMqConstant.CONTENT_CREATE_SUCCESS_CONSUME)
    @RocketMqLog(value = "文案/相册创建成功消息", name = "content->content")
    public void contentCreateMessages(@Payload CreateMessagesApiDTO dto, @Headers Map headers) {
        LogUtils.putAsyncTraceId(dto.getTraceId());
        log.info("消费消息messages={},headers={}", JSON.toJSONString(dto), headers);
        if (CreateMessagesTypeEnum.IMAGES.equal(dto.getMessagesType())) {
            log.info("开始将本地相册推送到oss保存");
            imagesService.pushLocalToOssSave(dto.getDataId());
        } else {
            log.info("无需处理");
        }
    }
}
