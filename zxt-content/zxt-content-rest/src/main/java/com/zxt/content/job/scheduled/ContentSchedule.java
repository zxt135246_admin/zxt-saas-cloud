package com.zxt.content.job.scheduled;

import com.zxt.bean.utils.LogUtils;
import com.zxt.content.service.PublicContentService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;


@Slf4j
@EnableScheduling
@Component
@EnableAsync
@AllArgsConstructor
public class ContentSchedule {

    private final PublicContentService publicContentService;

    /**
     * 处理推送文案
     *
     * @Async+@Scheduled 定时任务不阻塞功能，让定时任务开异步线程执行
     */
    @Async
    @Scheduled(cron = "0 0 8 * * ?")
    public void dealPushContentByEmail() {
        LogUtils.putJobTraceId();
        log.info("定时处理邮箱推送文案start..........");
        publicContentService.dealPushEmail();
        log.info("定时处理邮箱推送文案end..........");
    }

    /**
     * 定时拉取网易云热评生成文案
     */
    @Async
    //@Scheduled(cron = "0 * * * * ?")
    public void dealGenerationContent() {
        LogUtils.putJobTraceId();
        log.info("定时拉取网易云生成文案start..........");
        publicContentService.dealGenerationContent();
        log.info("定时拉取网易云生成文案end..........");
    }

}

