package com.zxt.content.controller;

import com.alibaba.fastjson.JSON;
import com.zxt.bean.result.Result;
import com.zxt.content.dto.ImagesCollectionsDTO;
import com.zxt.content.dto.ImagesDTO;
import com.zxt.content.service.ImagesCollectionsService;
import com.zxt.content.vo.ImagesCollectionsVO;
import com.zxt.web.controller.CommonController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RequestMapping("imagesCollections")
@RestController
@AllArgsConstructor
@Api(tags = "相册合集管理接口")
public class ImagesCollectionsController extends CommonController {

    private final ImagesCollectionsService imagesCollectionsService;

    @ApiOperation(value = "查询相册合集列表", notes = "查询相册合集列表")
    @PostMapping("list")
    public Result<List<ImagesCollectionsVO>> list(@RequestBody ImagesCollectionsDTO dto) {
        dto.setUserId(getUser().getId());
        List<ImagesCollectionsVO> vos = imagesCollectionsService.list(dto);
        return Result.newSuccess(vos);
    }

    @ApiOperation(value = "新增相册合集", notes = "新增相册合集")
    @PostMapping("add")
    public Result<List<ImagesCollectionsVO>> add(@RequestBody ImagesCollectionsDTO dto) {
        dto.setUserId(getUser().getId());
        dto.setShareUserJson(JSON.toJSONString(dto.getShareUserList()));
        imagesCollectionsService.add(dto);
        return Result.newSuccess();
    }
}
