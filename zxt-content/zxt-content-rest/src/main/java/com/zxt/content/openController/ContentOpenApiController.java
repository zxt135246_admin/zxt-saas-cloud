package com.zxt.content.openController;

import com.zxt.bean.annotation.PassLogin;
import com.zxt.bean.result.Result;
import com.zxt.content.service.PrivateContentService;
import com.zxt.content.vo.PrivateContentVO;
import com.zxt.web.controller.CommonController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * 文案对外接口
 */
@Slf4j
@RequestMapping("/content/openApi")
@RestController
@Api(tags = "文案对外接口")
public class ContentOpenApiController extends CommonController {

    private final PrivateContentService privateContentService;

    @Autowired
    public ContentOpenApiController(PrivateContentService privateContentService) {
        this.privateContentService = privateContentService;
    }

    @ApiOperation(value = "获取一条文案", notes = "获取一条文案")
    @GetMapping("get")
    @PassLogin
    public Result<Map<String, Object>> get() {
        log.info("对外接口获取一条文案");
        PrivateContentVO privateContentVO = privateContentService.randGetOneContentByUserId(null);
        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("id", privateContentVO.getId());
        resultMap.put("source", privateContentVO.getContentSource());
        resultMap.put("content", privateContentVO.getContent());
        resultMap.put("typeName", privateContentVO.getTypeName());
        resultMap.put("heat", privateContentVO.getContentHeat());
        resultMap.put("createTime", privateContentVO.getCreateTime());
        return Result.newSuccess(resultMap);
    }

}
