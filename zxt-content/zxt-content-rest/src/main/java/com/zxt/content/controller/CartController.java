package com.zxt.content.controller;

import com.zxt.bean.result.Result;
import com.zxt.content.service.CartService;
import com.zxt.content.vo.CartVO;
import com.zxt.content.vo.ImagesVO;
import com.zxt.web.controller.CommonController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RequestMapping("cart")
@RestController
@AllArgsConstructor
@Api(tags = "购物车管理接口")
public class CartController extends CommonController {

    private final CartService cartService;

    @ApiOperation(value = "加车", notes = "加车")
    @GetMapping("addCart/{mallGoodsSkuId}")
    public Result<ImagesVO> addCart(@ApiParam("商城商品id") @PathVariable Long mallGoodsSkuId) {
        cartService.addCart(mallGoodsSkuId, getUser().getId());
        return Result.newSuccess();
    }

    @ApiOperation(value = "删除商品", notes = "删除商品")
    @GetMapping("remove/{id}")
    public Result<ImagesVO> remove(@ApiParam("购物车id") @PathVariable Long id) {
        cartService.remove(id);
        return Result.newSuccess();
    }

    @ApiOperation(value = "清空购物车", notes = "清空购物车")
    @GetMapping("clear")
    public Result<ImagesVO> clear() {
        cartService.clear(getUser().getId());
        return Result.newSuccess();
    }

    @ApiOperation(value = "查询购物车", notes = "查询购物车")
    @GetMapping("list")
    public Result<List<CartVO>> list() {
        return Result.newSuccess(cartService.getByUserId(getUser().getId()));
    }
}
