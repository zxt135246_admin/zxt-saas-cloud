package com.zxt.content.controller;

import com.zxt.bean.result.Result;
import com.zxt.bean.utils.LogUtils;
import com.zxt.content.api.dto.CreateMessagesApiDTO;
import com.zxt.content.api.enums.CreateMessagesTypeEnum;
import com.zxt.content.dto.ContentDTO;
import com.zxt.content.dto.PublishContentDTO;
import com.zxt.content.mq.channel.ContentSource;
import com.zxt.content.service.PrivateContentService;
import com.zxt.web.controller.CommonController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RequestMapping("privateContent")
@RestController
@AllArgsConstructor
@Api(tags = "私有文案管理接口")
public class PrivateContentController extends CommonController {

    private final PrivateContentService privateContentService;

    private final ContentSource contentSource;

    @ApiOperation(value = "新增私有文案", notes = "新增私有文案")
    @PostMapping("add")
    public Result add(@RequestBody ContentDTO dto) {
        try {
            dto.setUserId(getUser().getId());
            Long contentId = privateContentService.add(dto, getUser());
            //发送创建文案成功的mq消息
            sendMq(contentId);
        } catch (Exception e) {
            //可发送创建失败消息
            throw e;
        }
        return Result.newSuccess();
    }

    private void sendMq(Long contentId) {
        log.info("发送创建文案成功mq消息");
        CreateMessagesApiDTO apiDTO = CreateMessagesApiDTO.builder().messagesType(CreateMessagesTypeEnum.CONTENT.getValue()).DataId(contentId).traceId(LogUtils.getTraceId()).build();
        contentSource.contentCreateSuccessSubject().send(MessageBuilder.withPayload(apiDTO).build());
    }

    @ApiOperation(value = "公开/私有文案", notes = "公开/私有文案")
    @PostMapping("publishContent")
    public Result publishContent(@RequestBody PublishContentDTO dto) {
        privateContentService.publishContent(dto);
        return Result.newSuccess();
    }
}
