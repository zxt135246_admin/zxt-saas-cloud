package com.zxt.content.mq.channel;

import com.zxt.content.api.MqConstant.ContentMqConstant;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.SubscribableChannel;

/**
 * 生产者channel
 */
public interface ContentSource {

    /**
     * 文案创建成功生产者
     *
     * @return SubscribableChannel
     */
    @Output(ContentMqConstant.CONTENT_CREATE_SUCCESS_SUBJECT)
    SubscribableChannel contentCreateSuccessSubject();

    /**
     * 订单创建失败生产者
     *
     * @return SubscribableChannel
     */
    @Output(ContentMqConstant.CONTENT_ORDER_CREATE_FAIL_SUBJECT)
    SubscribableChannel contentOrderCreateFailSubject();
}
