package com.zxt.content.feignController;

import com.zxt.bean.result.Result;
import com.zxt.bean.utils.BeanUtils;
import com.zxt.content.api.dto.ImagesApiDTO;
import com.zxt.content.api.dto.ImagesExifApiDTO;
import com.zxt.content.api.feignClient.ImagesFeignClient;
import com.zxt.content.api.vo.ImagesApiVO;
import com.zxt.content.dto.ImagesDTO;
import com.zxt.content.service.ImagesService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author zxt
 */
@RestController
@RequestMapping(value = "/feign/images")
@AllArgsConstructor
@Slf4j
public class ImagesFeignController implements ImagesFeignClient {
    private final ImagesService imagesService;

    @Override
    @PostMapping("updateById")
    public Result<Boolean> updateById(@RequestBody ImagesApiDTO dto) {
        return Result.newSuccess(imagesService.updateById(BeanUtils.beanCopy(dto, ImagesDTO.class)));
    }

    @Override
    @PostMapping("saveOrUpdateImagesExif")
    public Result<Boolean> saveOrUpdateImagesExif(@RequestBody ImagesExifApiDTO dto) {
        return Result.newSuccess(imagesService.saveOrUpdateImagesExif(dto));
    }

    @Override
    @PostMapping("saveBatchImagesExif")
    public Result<Boolean> saveBatchImagesExif(@RequestBody List<ImagesExifApiDTO> imagesExifApiDTOList) {
        imagesService.saveBatchImagesExif(imagesExifApiDTOList);
        return Result.newSuccess(Boolean.TRUE);
    }

    @Override
    @GetMapping("getById")
    public Result<ImagesApiVO> getById(@RequestParam("imagesId") Long imagesId) {
        return Result.newSuccess(BeanUtils.beanCopy(imagesService.getById(imagesId), ImagesApiVO.class));
    }
}
