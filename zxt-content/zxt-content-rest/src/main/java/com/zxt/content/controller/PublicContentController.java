package com.zxt.content.controller;

import com.zxt.bean.query.QueryResultVO;
import com.zxt.bean.result.Result;
import com.zxt.content.dto.ContentQueryDTO;
import com.zxt.content.service.PublicContentService;
import com.zxt.content.vo.PublicContentDetailVO;
import com.zxt.content.vo.PublicContentVO;
import com.zxt.redis.annotation.CustomCache;
import com.zxt.web.controller.CommonController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;

@Slf4j
@RequestMapping("publicContent")
@RestController
@AllArgsConstructor
@Api(tags = "公共文案管理接口")
public class PublicContentController extends CommonController {

    private final PublicContentService publicContentService;

    @ApiOperation(value = "分页查询公共文案", notes = "分页查询公共文案")
    @PostMapping("query")
    public Result<QueryResultVO<PublicContentVO>> query(@RequestBody ContentQueryDTO dto) {
        if (Objects.nonNull(dto.getIsSelf()) && dto.getIsSelf()) {
            dto.setUserId(getUser().getId());
        }
        return Result.newSuccess(publicContentService.query(dto));
    }

    @ApiOperation(value = "获取文案详情信息", notes = "获取文案详情信息")
    @GetMapping("get/{id}")
    public Result<PublicContentDetailVO> get(@ApiParam("文案id") @PathVariable Long id) {
        PublicContentDetailVO vo = publicContentService.get(id, getUser());
        return Result.newSuccess(vo);
    }

    @ApiOperation(value = "手动推进推送文案", notes = "手动推进推送文案")
    @GetMapping("pushContent")
    public Result<PublicContentDetailVO> pushContent() {
        publicContentService.dealPushEmail();
        return Result.newSuccess();
    }
}
