package com.zxt.content.controller;

import com.zxt.bean.query.QueryResultVO;
import com.zxt.bean.result.Result;
import com.zxt.content.dto.ContentCommentDTO;
import com.zxt.content.dto.ContentCommentQueryDTO;
import com.zxt.content.service.ContentCommentService;
import com.zxt.content.vo.ContentCommentVO;
import com.zxt.web.controller.CommonController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RequestMapping("contentComment")
@RestController
@AllArgsConstructor
@Api(tags = "文案评论管理接口")
public class ContentCommentController extends CommonController {

    private final ContentCommentService contentCommentService;

    @ApiOperation(value = "新增文案评论", notes = "新增文案评论")
    @PostMapping("add")
    public Result add(@RequestBody ContentCommentDTO dto) {
        dto.setUserId(getUser().getId());
        dto.setNickName(getUser().getNickName());
        contentCommentService.add(dto);
        return Result.newSuccess();
    }

    @ApiOperation(value = "分页查询文案评论", notes = "分页查询文案评论")
    @PostMapping("query")
    public Result<QueryResultVO<ContentCommentVO>> query(@RequestBody ContentCommentQueryDTO dto) {
        return Result.newSuccess(contentCommentService.queryContentComment(dto));
    }
}
