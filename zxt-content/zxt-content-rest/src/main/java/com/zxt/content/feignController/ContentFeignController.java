package com.zxt.content.feignController;

import com.zxt.bean.model.UserInfoVO;
import com.zxt.bean.query.QueryResultVO;
import com.zxt.bean.result.Result;
import com.zxt.bean.utils.BeanUtils;
import com.zxt.bean.utils.CollectionsUtil;
import com.zxt.bean.utils.LogUtils;
import com.zxt.content.api.dto.ContentApiDTO;
import com.zxt.content.api.dto.CreateMessagesApiDTO;
import com.zxt.content.api.enums.CreateMessagesTypeEnum;
import com.zxt.content.api.feignClient.ContentFeignClient;
import com.zxt.content.api.vo.ContentApiVO;
import com.zxt.content.api.vo.ContentTypeApiVO;
import com.zxt.content.dto.ContentDTO;
import com.zxt.content.dto.ContentTypeQueryDTO;
import com.zxt.content.mq.channel.ContentSource;
import com.zxt.content.service.ContentTypeService;
import com.zxt.content.service.PrivateContentService;
import com.zxt.content.vo.ContentTypeVO;
import com.zxt.content.vo.PrivateContentVO;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * @author zxt
 */
@RestController
@RequestMapping(value = "/feign/content")
@AllArgsConstructor
@Slf4j
public class ContentFeignController implements ContentFeignClient {
    private PrivateContentService privateContentService;
    private ContentTypeService contentTypeService;
    private ContentSource contentSource;

    @Override
    @GetMapping("getContent")
    public Result<String> getContent(@RequestParam String name) {
        log.info(name);
        return Result.newSuccess(name);
    }

    @Override
    @GetMapping("getPrivateContentByUserId")
    public Result<List<ContentApiVO>> getPrivateContentByUserId(@RequestParam Long userId) {
        List<PrivateContentVO> vos = privateContentService.getByUserId(userId);
        return Result.newSuccess(BeanUtils.beanCopy(vos, ContentApiVO.class));
    }

    @Override
    @PostMapping(value = "addContent")
    public Result<Boolean> addContent(@RequestBody ContentApiDTO dto) {
        try {
            UserInfoVO userInfoVO = new UserInfoVO();
            userInfoVO.setId(dto.getUserId());
            userInfoVO.setNickName(dto.getNickName());
            Long contentId = privateContentService.add(BeanUtils.beanCopy(dto, ContentDTO.class), userInfoVO);
            //发送创建文案成功的mq消息
            sendMq(contentId);
        } catch (Exception e) {
            //可发送创建失败消息
            throw e;
        }
        return Result.newSuccess(Boolean.TRUE);
    }

    private void sendMq(Long contentId) {
        CreateMessagesApiDTO apiDTO = CreateMessagesApiDTO.builder().messagesType(CreateMessagesTypeEnum.CONTENT.getValue()).DataId(contentId).traceId(LogUtils.getTraceId()).build();
        contentSource.contentCreateSuccessSubject().send(MessageBuilder.withPayload(contentId).build());
    }

    @Override
    @GetMapping(value = "randGetOneContentByUserId")
    public Result<ContentApiVO> randGetOneContentByUserId(@RequestParam(value = "userId", required = false) Long userId) {
        PrivateContentVO vo = privateContentService.randGetOneContentByUserId(userId);
        return Result.newSuccess(BeanUtils.beanCopy(vo, ContentApiVO.class));
    }

    @Override
    @GetMapping(value = "getContentTypeList")
    public Result<List<ContentTypeApiVO>> getContentTypeList() {
        ContentTypeQueryDTO dto = new ContentTypeQueryDTO();
        dto.setPageNo(1);
        dto.setPageSize(50000);
        QueryResultVO<ContentTypeVO> resultVO = contentTypeService.queryContentType(dto);
        List<ContentTypeVO> records = resultVO.getRecords();
        if (CollectionsUtil.isEmpty(records)) {
            return Result.newSuccess(new ArrayList<>());
        }

        return Result.newSuccess(BeanUtils.beanCopy(records, ContentTypeApiVO.class));
    }

    /**
     * 根据文案id查询
     *
     * @param contentId
     * @return
     */
    @Override
    @GetMapping(value = "getById")
    public Result<ContentApiVO> getById(@RequestParam("contentId") Long contentId) {
        PrivateContentVO privateContentVO = privateContentService.getById(contentId);
        return Result.newSuccess(BeanUtils.beanCopy(privateContentVO, ContentApiVO.class));
    }
}
