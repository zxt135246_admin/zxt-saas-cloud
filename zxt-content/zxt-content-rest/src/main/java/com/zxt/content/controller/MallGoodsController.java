package com.zxt.content.controller;

import com.zxt.bean.query.QueryResultVO;
import com.zxt.bean.result.Result;
import com.zxt.content.dto.GoodsSearchDTO;
import com.zxt.content.service.MallGoodsSkuService;
import com.zxt.content.vo.MallGoodsVO;
import com.zxt.web.controller.CommonController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RequestMapping("mallGoods")
@RestController
@AllArgsConstructor
@Api(tags = "商城商品spu管理接口")
public class MallGoodsController extends CommonController {
    private MallGoodsSkuService mallGoodsSkuService;

    @ApiOperation(value = "商品搜索", notes = "商品搜索")
    @PostMapping("search")
    public Result<QueryResultVO<MallGoodsVO>> search(@RequestBody GoodsSearchDTO dto) {
        return Result.newSuccess(mallGoodsSkuService.search(dto));
    }
}