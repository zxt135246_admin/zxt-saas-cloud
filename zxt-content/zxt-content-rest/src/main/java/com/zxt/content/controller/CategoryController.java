package com.zxt.content.controller;

import com.zxt.bean.query.QueryResultVO;
import com.zxt.bean.result.Result;
import com.zxt.bean.utils.ObjectUtils;
import com.zxt.content.dto.CategoryDTO;
import com.zxt.content.dto.CategoryQueryDTO;
import com.zxt.content.model.CategoryView;
import com.zxt.content.service.CategoryService;
import com.zxt.web.controller.CommonController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;

@Slf4j
@RequestMapping("category")
@RestController
@AllArgsConstructor
@Api(tags = "类目管理接口")
public class CategoryController extends CommonController {
    private final CategoryService categoryService;

    @ApiOperation(value = "创建/更新类目", notes = "创建/更新类目")
    @PutMapping("addOrUpdate")
    public Result addOrUpdate(@Validated @RequestBody CategoryDTO dto) {
        ObjectUtils.removeStringSpaces(dto);
        if (Objects.isNull(dto.getId())) {
            categoryService.createCategory(dto);
        } else {
            categoryService.updateCategory(dto);
        }
        return Result.newSuccess();
    }

    @ApiOperation(value = "分页查询类目", notes = "分页查询类目")
    @PostMapping("query")
    public Result<QueryResultVO<CategoryView>> query(@RequestBody CategoryQueryDTO dto) {
        return Result.newSuccess(categoryService.queryCategory(dto));
    }

}