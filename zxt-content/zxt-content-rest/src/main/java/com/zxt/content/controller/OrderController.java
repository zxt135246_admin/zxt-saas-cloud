package com.zxt.content.controller;

import com.alibaba.fastjson.JSON;
import com.zxt.bean.result.Result;
import com.zxt.bean.utils.StringUtils;
import com.zxt.content.constants.RedisKeyContent;
import com.zxt.content.dto.OrderAddDTO;
import com.zxt.content.dto.OrderDTO;
import com.zxt.content.dto.OrderPayDTO;
import com.zxt.content.enums.OrderStatusEnum;
import com.zxt.content.mq.channel.ContentSource;
import com.zxt.content.service.OrderService;
import com.zxt.content.vo.OrderVO;
import com.zxt.lock.RedissonLock;
import com.zxt.redis.utils.RedisUtil;
import com.zxt.redis.utils.UserUtil;
import com.zxt.web.controller.CommonController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.Objects;

@Slf4j
@RequestMapping("order")
@RestController
@AllArgsConstructor
@Api(tags = "订单管理接口")
public class OrderController extends CommonController {

    private final OrderService orderService;
    private final RedissonLock redissonLock;
    private final ContentSource contentSource;


    @ApiOperation(value = "初始化订单", notes = "初始化订单")
    @GetMapping("init")
    public Result<OrderAddDTO> init() {
        return Result.newSuccess(orderService.orderInit(getUser()));
    }

    @ApiOperation(value = "获取初始化订单", notes = "获取初始化订单")
    @GetMapping("get")
    public Result<OrderAddDTO> get() {
        OrderAddDTO orderAddDTO = null;
        String orderKey = RedisKeyContent.getInitOrderDTOKey(UserUtil.getToken());
        Object o = RedisUtil.get(orderKey);
        if (Objects.nonNull(o)) {
            orderAddDTO = JSON.parseObject(o.toString(), OrderAddDTO.class);
        }
        return Result.newSuccess(orderAddDTO);
    }

    @ApiOperation(value = "创建订单", notes = "创建订单")
    @PutMapping("add")
    public Result<String> add() {
        if (!redissonLock.lock(RedisKeyContent.getLock("addOrder", getUser().getId()), 0, 10)) {
            return Result.newError("请勿重复提交");
        }

        String orderCode = null;
        try {
            Object object = RedisUtil.get(RedisKeyContent.getInitOrderDTOKey(UserUtil.getToken()));
            if (Objects.isNull(object)) {
                return Result.newError("缓存失效，请刷新页面");
            }
            OrderAddDTO orderAddDTO = JSON.parseObject(object.toString(), OrderAddDTO.class);
            if (Objects.isNull(orderAddDTO)) {
                return Result.newError("缓存失效，请刷新页面");
            }
            orderCode = orderService.createOrder(orderAddDTO, getUser());
            //TODO 发送订单创建成功消息，延时30分钟以上用来关闭未支付的订单
        } catch (Exception e) {
            //可以处理异常措施
            log.error("创建订单出现异常，异常信息:{}", e);
            if (StringUtils.isNotBlank(orderCode)) {
                //发送回滚订单消息
                log.info("发送订单回滚消息 订单号:{}", orderCode);
                contentSource.contentOrderCreateFailSubject().send(MessageBuilder.withPayload(orderCode).build());
            }
            throw e;
        } finally {
            RedisUtil.delete(RedisKeyContent.getInitOrderDTOKey(UserUtil.getToken()));
            redissonLock.release(RedisKeyContent.getLock("addOrder", getUser().getId()));
        }
        return Result.newSuccess(orderCode);
    }


    @ApiOperation(value = "订单支付", notes = "订单支付")
    @PutMapping("orderPay")
    public Result orderPay(@Validated @RequestBody OrderPayDTO dto) {
        orderService.orderPayCheck(dto, getUser());
        try {
            orderService.orderPay(dto, getUser());
            //TODO 支付成功，发送支付成功，确认预占，获取下单成功奖励等
        } catch (Exception e) {
            //支付失败，关闭订单，发送回滚订单消息
            orderService.updateStatus(dto.getOrderCode(), OrderStatusEnum.CLOSED.getValue(), OrderStatusEnum.PENDING_PAYMENT.getValue());
            log.info("发送订单回滚消息 订单号:{}", dto.getOrderCode());
            contentSource.contentOrderCreateFailSubject().send(MessageBuilder.withPayload(dto.getOrderCode()).build());
        }
        return Result.newSuccess();
    }

    @ApiOperation(value = "订单状态数量查询", notes = "订单状态数量查询")
    @GetMapping("quantityQuery")
    public Result<Map<String, Integer>> quantityQuery() {
        //订单状态数量查询逻辑
        return Result.newSuccess(orderService.quantityQuery(getUser().getId()));
    }

    @ApiOperation(value = "获取订单详情", notes = "获取订单详情")
    @GetMapping("getOrderDetail/{orderCode}")
    public Result<OrderVO> getOrderDetail(@ApiParam("订单号") @PathVariable String orderCode) {
        return Result.newSuccess(orderService.getOrderDetail(orderCode));
    }


    @ApiOperation(value = "获取用户订单列表", notes = "获取用户订单列表")
    @PostMapping("getUserOrder")
    public Result<List<OrderVO>> getUserOrder(@RequestBody OrderDTO dto) {
        dto.setUserId(getUser().getId());
        List<OrderVO> orderVOList = orderService.getUserOrder(dto);
        return Result.newSuccess(orderVOList);
    }

}
