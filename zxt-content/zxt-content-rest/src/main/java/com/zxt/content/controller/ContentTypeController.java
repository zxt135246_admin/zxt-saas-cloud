package com.zxt.content.controller;

import com.zxt.bean.query.QueryResultVO;
import com.zxt.bean.result.Result;
import com.zxt.content.dto.ContentTypeDTO;
import com.zxt.content.dto.ContentTypeQueryDTO;
import com.zxt.content.dto.excel.ContentTypeDataDTO;
import com.zxt.content.service.ContentTypeService;
import com.zxt.web.excel.ExcelUtil;
import com.zxt.content.vo.ContentTypeVO;
import com.zxt.redis.annotation.CustomCache;
import com.zxt.web.controller.CommonController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@Slf4j
@RequestMapping("contentType")
@RestController
@AllArgsConstructor
@Api(tags = "文案类型管理接口")
public class ContentTypeController extends CommonController {

    private final ContentTypeService contentTypeService;

    @ApiOperation(value = "新增文案类型", notes = "新增文案类型")
    @PostMapping("add")
    public Result add(@RequestBody ContentTypeDTO dto) {
        contentTypeService.add(dto);
        return Result.newSuccess();
    }

    @ApiModelProperty(value = "分页查询文案类型", notes = "分页查询文案类型")
    @PostMapping("query")
    @CustomCache(expirationTime = 60 * 5)
    public Result<QueryResultVO<ContentTypeVO>> query(@RequestBody ContentTypeQueryDTO dto) {
        return Result.newSuccess(contentTypeService.queryContentType(dto));
    }

    @ApiOperation(value = "导入模板下载", notes = "导入模板下载")
    @PostMapping(value = "/downloadExcel")
    public void downloadExcel() {
        ExcelUtil.simpleWrite("导入模板下载", "业务商品", ContentTypeDataDTO.class, new ArrayList<>(), getResponse());
    }
}
