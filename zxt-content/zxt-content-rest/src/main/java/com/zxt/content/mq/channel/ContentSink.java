package com.zxt.content.mq.channel;

import com.zxt.content.api.MqConstant.ContentMqConstant;
import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;

/**
 * 消费者channel
 */
public interface ContentSink {
    /**
     * 文案创建成功消费者
     *
     * @return
     */
    @Input(ContentMqConstant.CONTENT_CREATE_SUCCESS_CONSUME)
    SubscribableChannel contentCreateSuccessConsume();
}
