package com.zxt.content.controller;

import com.zxt.bean.result.Result;
import com.zxt.content.service.ImagesService;
import com.zxt.web.controller.CommonController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RequestMapping("init")
@RestController
@AllArgsConstructor
@Api(tags = "初始化管理接口")
public class InitController extends CommonController {

    private final ImagesService imagesService;


    @ApiModelProperty(value = "初始化所有相册的Exif", notes = "初始化所有相册的Exif")
    @GetMapping("initImagesExif")
    public Result initImagesExif(@RequestParam("是否初始化全部") Boolean isInitAll) {
        imagesService.initImagesExif(isInitAll);
        return Result.newSuccess();
    }
}
