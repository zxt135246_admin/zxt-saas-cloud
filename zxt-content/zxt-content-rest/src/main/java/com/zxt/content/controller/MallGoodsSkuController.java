package com.zxt.content.controller;

import com.zxt.bean.query.QueryResultVO;
import com.zxt.bean.result.Result;
import com.zxt.bean.utils.ObjectUtils;
import com.zxt.content.dto.GoodsSearchDTO;
import com.zxt.content.dto.MallGoodsSkuDTO;
import com.zxt.content.dto.MallGoodsSkuQueryDTO;
import com.zxt.content.dto.UpdateStatusDTO;
import com.zxt.content.service.MallGoodsSkuService;
import com.zxt.content.vo.GoodsSearchVO;
import com.zxt.content.vo.MallGoodsSkuVO;
import com.zxt.web.controller.CommonController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;

@Slf4j
@RequestMapping("mallGoodsSku")
@RestController
@AllArgsConstructor
@Api(tags = "商城商品sku管理接口")
public class MallGoodsSkuController extends CommonController {
    private MallGoodsSkuService mallGoodsSkuService;

    @ApiOperation(value = "分页查询商城商品sku列表", notes = "分页查询商城商品sku列表")
    @PostMapping("query")
    public Result<QueryResultVO<MallGoodsSkuVO>> query(@RequestBody MallGoodsSkuQueryDTO dto) {
        return Result.newSuccess(mallGoodsSkuService.queryMallGoodsSku(dto));
    }

    @ApiOperation(value = "新增商城商品/修改商城商品", notes = "新增商城商品/修改商城商品")
    @PostMapping("addOrUpdate")
    public Result addOrUpdate(@Validated @RequestBody MallGoodsSkuDTO dto) {
        ObjectUtils.removeStringSpaces(dto);
        if (Objects.isNull(dto.getId())) {
            mallGoodsSkuService.addMallGoodsSku(dto);
        } else {
            mallGoodsSkuService.updateMallGoodsSku(dto);
        }
        return Result.newSuccess();
    }

    @ApiModelProperty(value = "修改商城商品状态", notes = "修改商城商品状态")
    @PostMapping("updateStatus")
    public Result updateStatus(@RequestBody UpdateStatusDTO dto) {
        mallGoodsSkuService.updateStatus(dto);
        return Result.newSuccess();
    }


    @ApiOperation(value = "分页查询商城商品sku列表", notes = "分页查询商城商品sku列表")
    @PostMapping("getSpu")
    public Result<GoodsSearchVO> getSpu(@RequestBody GoodsSearchDTO dto) {
        return Result.newSuccess(mallGoodsSkuService.getSpu(dto));
    }
}