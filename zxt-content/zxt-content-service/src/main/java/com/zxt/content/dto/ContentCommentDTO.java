package com.zxt.content.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author zxt
 */
@Data
@ApiModel("文案评论入参")
public class ContentCommentDTO {

    @ApiModelProperty(value = "用户id", hidden = true)
    private Long userId;

    @ApiModelProperty(value = "文案id")
    private Long contentId;


    @ApiModelProperty("评论内容")
    private String commentContent;

    @ApiModelProperty("评分")
    private Integer score;


    @ApiModelProperty("昵称")
    private String nickName;
}
