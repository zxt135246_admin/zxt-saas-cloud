package com.zxt.content.manager;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zxt.bean.utils.StringUtils;
import com.zxt.content.dao.ContentTypeMapper;
import com.zxt.content.dto.ContentTypeQueryDTO;
import com.zxt.content.model.ContentType;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author zxt
 */
@Slf4j
@Service
public class ContentTypeManager extends ServiceImpl<ContentTypeMapper, ContentType> {


    /**
     * 分页查询
     *
     * @param dto
     * @return
     */
    public Page<ContentType> queryContentType(ContentTypeQueryDTO dto) {
        Page<ContentType> page = new Page<>();
        page.setCurrent(dto.getPageNo());
        page.setSize(dto.getPageSize());
        LambdaQueryWrapper<ContentType> queryWrapper = ContentType.gw();
        if (StringUtils.isNotBlank(dto.getTypeNameLike())) {
            queryWrapper.like(ContentType::getTypeName, dto.getTypeNameLike());
        }
        return page(page, queryWrapper);
    }

    /**
     * 根据类型名称查询
     *
     * @param name
     * @return
     */
    public ContentType getByName(String name) {
        LambdaQueryWrapper<ContentType> wrapper = ContentType.gw();
        wrapper.eq(ContentType::getTypeName, name);
        return getOne(wrapper);
    }
}
