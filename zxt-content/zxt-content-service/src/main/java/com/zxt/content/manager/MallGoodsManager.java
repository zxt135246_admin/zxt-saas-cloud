package com.zxt.content.manager;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zxt.content.dao.MallGoodsMapper;
import com.zxt.content.model.MallGoods;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class MallGoodsManager extends ServiceImpl<MallGoodsMapper, MallGoods> {

}
