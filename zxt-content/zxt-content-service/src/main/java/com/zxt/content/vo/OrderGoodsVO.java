package com.zxt.content.vo;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.zxt.bean.convertor.LongJsonDeserializer;
import com.zxt.bean.convertor.LongJsonSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author zxt
 */
@Data
@ApiModel("订单商品出参")
public class OrderGoodsVO {

    @ApiModelProperty("订单号")
    private String orderCode;

    @ApiModelProperty("商城商品skuId")
    @JsonSerialize(using = LongJsonSerializer.class)
    @JsonDeserialize(using = LongJsonDeserializer.class)
    private Long mallGoodsSkuId;

    @ApiModelProperty("商品数量")
    private Integer goodsNumber;

    @ApiModelProperty("总价格")
    private BigDecimal totalPrice;

    @ApiModelProperty("商品名称")
    private String goodsName;

    @ApiModelProperty("商品图片")
    private String goodsPicture;

    @ApiModelProperty("商城商品spuId")
    @JsonSerialize(using = LongJsonSerializer.class)
    @JsonDeserialize(using = LongJsonDeserializer.class)
    private Long mallGoodsId;

}
