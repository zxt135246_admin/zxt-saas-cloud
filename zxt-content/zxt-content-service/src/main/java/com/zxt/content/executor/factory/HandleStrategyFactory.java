package com.zxt.content.executor.factory;

import com.zxt.bean.executor.strategy.AbstractHandleStrategy;
import com.zxt.content.executor.handler.CheckExistHandle;
import com.zxt.content.executor.strategy.PublishContentHandleStrategy;
import com.zxt.content.manager.PrivateContentManager;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.Objects;

/**
 * @author zxt
 * 策略模式工厂
 */
@Slf4j
public class HandleStrategyFactory {

    private static HashMap<String, AbstractHandleStrategy> map = new HashMap<>();

    /**
     * 创建校验用户信息策略模式
     *
     * @return
     */
    public static AbstractHandleStrategy publishContentStrategy(PrivateContentManager privateContentManager) {
        String mapKey = genKey(PublishContentHandleStrategy.class);
        AbstractHandleStrategy abstractHandleStrategy = getCacheMap(mapKey);
        if (abstractHandleStrategy != null) {
            return abstractHandleStrategy;
        }

        //不存在新建
        AbstractHandleStrategy handleStrategy = new PublishContentHandleStrategy();
        handleStrategy.withHandle(new CheckExistHandle(privateContentManager, "文案"));
        map.put(mapKey, handleStrategy);
        return handleStrategy;
    }

    private static AbstractHandleStrategy getCacheMap(String mapKey) {
        AbstractHandleStrategy abstractHandleStrategy = map.get(mapKey);
        if (Objects.nonNull(abstractHandleStrategy)) {
            log.info("map返回");
            return abstractHandleStrategy;
        }
        return null;
    }

    private static String genKey(Object target) {
        //通过原类型的类名和转换类型的类名生成key
        return target.getClass().getName();
    }
}
