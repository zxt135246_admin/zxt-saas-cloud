package com.zxt.content.utils;

import com.zxt.bean.utils.LocalDateUtil;
import com.zxt.bean.utils.StringUtils;

import java.time.LocalDateTime;

/**
 * 编码生成工具类
 */
public class CodeGeneratorUtil {

    /**
     * 生成15位分享编码
     *
     * @return 分享编码
     */
    public static String generateShareCode() {
        String date = LocalDateUtil.format(LocalDateTime.now(), "yyMMddHHmmss");
        String random = StringUtils.getRandomString(3, StringUtils.NUMBERS);
        return date + random;
    }

    /**
     * 生成订单编码
     * 时间yyMMddSSS + 随机数9位
     * 9位 + 9位
     *
     * @return 订单编码
     */
    public static String generateOrderCode() {
        String date = LocalDateUtil.format(LocalDateTime.now(), "yyMMddSSS");
        String random = StringUtils.getRandomString(9, StringUtils.NUMBERS);
        return date + random;
    }

}
