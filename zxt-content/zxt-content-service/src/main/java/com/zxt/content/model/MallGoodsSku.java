package com.zxt.content.model;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString(callSuper = true)
@TableName(value = "sc_mall_goods_sku")
public class MallGoodsSku {
    public static final LambdaQueryWrapper<MallGoodsSku> gw() {
        return new LambdaQueryWrapper<>();
    }

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @TableField(value = "mall_id")
    private Long mallId;

    @TableField(value = "mall_goods_id")
    private Long mallGoodsId;

    @TableField(value = "category_id")
    private Long categoryId;

    @TableField(value = "goods_name")
    private String goodsName;

    @TableField(value = "goods_type")
    private Byte goodsType;

    @TableField(value = "sku_code")
    private String skuCode;

    @TableField(value = "out_goods_code")
    private String outGoodsCode;

    @TableField(value = "goods_picture")
    private String goodsPicture;

    @TableField(value = "common_status")
    private Byte commonStatus;

    @TableField(value = "spec_value")
    private String specValue;

    @TableField(value = "sale_price")
    private BigDecimal salePrice;

    @TableField(value = "cost_price")
    private BigDecimal costPrice;

    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    @TableLogic
    @TableField(value = "is_deleted")
    private Boolean isDeleted;
}
