package com.zxt.content.event.addOrder;

import com.alibaba.fastjson.JSON;
import com.zxt.bean.enums.CommonStatusEnum;
import com.zxt.bean.exeption.ServiceException;
import com.zxt.bean.model.UserInfoVO;
import com.zxt.bean.result.ResultCode;
import com.zxt.bean.utils.BeanUtils;
import com.zxt.bean.utils.CollectionsUtil;
import com.zxt.content.client.UserClient;
import com.zxt.content.dto.OrderAddDTO;
import com.zxt.content.dto.OrderGoodsAddDTO;
import com.zxt.content.enums.OrderSourceEnum;
import com.zxt.content.enums.OrderStatusEnum;
import com.zxt.content.enums.PayStatusEnum;
import com.zxt.content.manager.MallGoodsSkuManager;
import com.zxt.content.manager.OrderGoodsManager;
import com.zxt.content.manager.OrderManager;
import com.zxt.content.manager.PrivateContentManager;
import com.zxt.content.model.MallGoodsSku;
import com.zxt.content.model.OrderGoods;
import com.zxt.content.rules.bean.OrderGoodsInputBean;
import com.zxt.content.rules.model.OrderDiscount;
import com.zxt.content.rules.model.OrderRequest;
import com.zxt.content.rules.service.OrderDiscountService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author zxt 下单监听
 */
@Slf4j
@Component
public class CreateOrderEventListener {

    private final OrderManager orderManager;
    private final OrderGoodsManager orderGoodsManager;
    private final PrivateContentManager privateContentManager;
    private final MallGoodsSkuManager mallGoodsSkuManager;
    private final UserClient userClient;
    private final OrderDiscountService orderDiscountService;

    @Autowired
    public CreateOrderEventListener(OrderManager orderManager, OrderGoodsManager orderGoodsManager, PrivateContentManager privateContentManager, MallGoodsSkuManager mallGoodsSkuManager, UserClient userClient, OrderDiscountService orderDiscountService) {
        this.orderManager = orderManager;
        this.orderGoodsManager = orderGoodsManager;
        this.privateContentManager = privateContentManager;
        this.mallGoodsSkuManager = mallGoodsSkuManager;
        this.userClient = userClient;
        this.orderDiscountService = orderDiscountService;
    }

    @Order(1)
    @EventListener(CreateOrderEvent.class)
    public void checkOrder(CreateOrderEvent event) {
        CreateOrderEventContent content = event.getContent();
        log.info("开始校验订单合法性,入参:{}", JSON.toJSONString(content.getOrderAddDTO()));
        UserInfoVO userInfoVO = content.getUserInfoVO();
        OrderAddDTO orderAddDTO = content.getOrderAddDTO();

        List<OrderGoodsAddDTO> orderGoodsList = orderAddDTO.getOrderGoodsList();
        List<Long> goodsIds = orderGoodsList.stream().map(OrderGoodsAddDTO::getMallGoodsSkuId).collect(Collectors.toList());

        List<MallGoodsSku> mallGoodsSkuList = mallGoodsSkuManager.listByIds(goodsIds);
        content.setMallGoodsSkuList(mallGoodsSkuList);
        checkMallGoods(content, orderGoodsList);

        content.setTotalPrice(orderAddDTO.getOrderTotalPrice());
        content.setDiscountPrice(orderAddDTO.getDiscountPrice());
        content.setPayPrice(orderAddDTO.getOrderTotalPrice().subtract(orderAddDTO.getDiscountPrice()));

        log.info("校验订单合法性完成");
    }

    @Order(2)
    @EventListener(CreateOrderEvent.class)
    public void createOrder(CreateOrderEvent event) {
        CreateOrderEventContent content = event.getContent();
        log.info("开始保存订单信息");
        UserInfoVO userInfoVO = content.getUserInfoVO();
        OrderAddDTO orderAddDTO = content.getOrderAddDTO();

        dealCreateOrder(content, userInfoVO, orderAddDTO);

        com.zxt.content.model.Order order = content.getOrder();
        orderManager.addOrder(order);
        List<OrderGoods> orderGoodsList = content.getOrderGoodsList();
        for (OrderGoods orderGoods : orderGoodsList) {
            orderGoods.setOrderCode(order.getOrderCode());
        }
        orderGoodsManager.saveBatch(orderGoodsList);

        content.setOrderCode(order.getOrderCode());
        log.info("保存订单信息完成");
    }

    /*@Order(3)
    @EventListener(CreateOrderEvent.class)
    public void payOrder(CreateOrderEvent event) {
        CreateOrderEventContent content = event.getContent();
        log.info("开始支付订单积分");
        UserInfoVO userInfoVO = content.getUserInfoVO();
        OrderAddDTO orderAddDTO = content.getOrderAddDTO();
        UserIntegralApiVO userIntegralApiVO = content.getUserIntegralApiVO();

        OperateIntegralApiDTO integralApiDTO = new OperateIntegralApiDTO();
        integralApiDTO.setOperateType(OperateTypeEnum.DEDUCTION.getValue());
        integralApiDTO.setIntegral(content.getPayPrice());
        integralApiDTO.setOrderCode(content.getOrderCode());
        integralApiDTO.setUserId(userInfoVO.getId());
        integralApiDTO.setOperationReason("下单扣减积分");
        userClient.operateIntegral(integralApiDTO);


        orderManager.updatePaySuccess(content.getOrderCode());

        if (OrderSourceEnum.BUSINESS.equal(orderAddDTO.getOrderSource())) {
            //文案单修改订单状态为待收货
            orderManager.updateStatus(content.getOrderCode(), OrderStatusEnum.SHIPPED.getValue(), OrderStatusEnum.PENDING_PAYMENT.getValue());
            orderGoodsManager.updateStatus(content.getOrderCode(), OrderStatusEnum.SHIPPED.getValue(), OrderStatusEnum.PENDING_PAYMENT.getValue());
        } else {
            //TODO 其他订单
            throw new ServiceException(ResultCode.SYS_ERROR);
        }

        log.info("订单积分支付完成");
    }*/

    /**
     * 校验商城商品
     *
     * @param content
     * @param orderGoodsList
     */
    private void checkMallGoods(CreateOrderEventContent content, List<OrderGoodsAddDTO> orderGoodsList) {

        List<MallGoodsSku> mallGoodsSkuList = content.getMallGoodsSkuList();
        if (CollectionsUtil.isEmpty(mallGoodsSkuList)) {
            throw new ServiceException("商品不存在，请重新选择商品");
        }
        Map<Long, MallGoodsSku> mallGoodsSkuMap = mallGoodsSkuList.stream().collect(Collectors.toMap(MallGoodsSku::getId, Function.identity()));

        Set<Long> existSet = new HashSet<>();
        for (OrderGoodsAddDTO orderGoodsAddDTO : orderGoodsList) {

            Long mallGoodsSkuId = orderGoodsAddDTO.getMallGoodsSkuId();

            MallGoodsSku mallGoodsSku = mallGoodsSkuMap.get(mallGoodsSkuId);
            if (Objects.isNull(mallGoodsSku)) {
                throw new ServiceException("商品：" + mallGoodsSku.getGoodsName() + " 不存在");
            }

            if (!CommonStatusEnum.ENABLE.equal(mallGoodsSku.getCommonStatus())) {
                throw new ServiceException(ResultCode.COMMON_MESSAGE, "商品:" + mallGoodsSku.getGoodsName() + " 已下架，请重新选择商品");
            }

            if (Objects.isNull(orderGoodsAddDTO.getGoodsNumber())) {
                throw new ServiceException("商品：" + mallGoodsSku.getGoodsName() + " 购买数量为空");
            }

            if (orderGoodsAddDTO.getGoodsNumber() < 1) {
                throw new ServiceException("请正确填写购买数量");
            }

            boolean isTrue = existSet.add(mallGoodsSkuId);
            if (!isTrue) {
                throw new ServiceException("商品：" + mallGoodsSku.getGoodsName() + " 重复");
            }

            if (Objects.isNull(mallGoodsSku.getSalePrice()) || mallGoodsSku.getSalePrice().compareTo(BigDecimal.ZERO) <= 0) {
                throw new ServiceException("商品：" + mallGoodsSku.getGoodsName() + " 未设置价格，无法购买");
            }

            if (mallGoodsSku.getSalePrice().compareTo(orderGoodsAddDTO.getSalePrice()) != 0) {
                throw new ServiceException("商品价格发生变化，请重新下单");
            }
        }

    }

    /**
     * 创建文案订单
     *
     * @param content
     * @param userInfoVO
     * @param orderAddDTO
     */
    private void dealCreateOrder(CreateOrderEventContent content, UserInfoVO userInfoVO, OrderAddDTO orderAddDTO) {

        List<OrderGoodsAddDTO> dtoOrderGoodsList = orderAddDTO.getOrderGoodsList();

        List<MallGoodsSku> mallGoodsSkuList = content.getMallGoodsSkuList();
        Map<Long, MallGoodsSku> mallGoodsSkuMap = mallGoodsSkuList.stream().collect(Collectors.toMap(MallGoodsSku::getId, Function.identity()));


        /**
         * 通过规则引擎计算抵扣
         */
        OrderRequest orderRequest = new OrderRequest();
        orderRequest.setOrderGoodsList(BeanUtils.beanCopy(dtoOrderGoodsList, OrderGoodsInputBean.class));
        orderRequest.setIsUseGradeDiscount(content.getOrderAddDTO().getIsUseGradeDiscount());
        orderRequest.setGradeDiscountRatio(content.getOrderAddDTO().getGradeApiVO().getDiscountRatio());
        OrderDiscount orderDiscount = orderDiscountService.calcDiscount(orderRequest);

        com.zxt.content.model.Order order = new com.zxt.content.model.Order();
        order.setUserId(userInfoVO.getId());
        order.setSupplierDataId(null);
        order.setDistributionRatio(orderDiscount.getDiscount());
        order.setOrderSource(OrderSourceEnum.BUSINESS.getValue());
        order.setOrderStatus(OrderStatusEnum.PENDING_PAYMENT.getValue());
        order.setPayCloseTime(LocalDateTime.now().plusMinutes(30));
        order.setPayStatus(PayStatusEnum.WAIT.getValue());
        order.setOrderTotalPrice(content.getTotalPrice());
        order.setGradeDiscountPrice(content.getDiscountPrice());
        order.setPayPrice(content.getPayPrice());

        Map<Long, OrderGoodsInputBean> goodsInputMap = orderDiscount.getOrderGoodsList().stream().collect(Collectors.toMap(OrderGoodsInputBean::getMallGoodsSkuId, Function.identity()));

        List<OrderGoods> orderGoodsList = new ArrayList<>();
        for (OrderGoodsAddDTO orderGoodsAddDTO : dtoOrderGoodsList) {
            MallGoodsSku mallGoodsSku = mallGoodsSkuMap.get(orderGoodsAddDTO.getMallGoodsSkuId());
            OrderGoods orderGoods = new OrderGoods();
            orderGoods.setUserId(userInfoVO.getId());
            orderGoods.setMallGoodsSkuId(mallGoodsSku.getId());
            orderGoods.setGoodsName(mallGoodsSku.getGoodsName());
            orderGoods.setGoodsPrice(mallGoodsSku.getSalePrice());
            orderGoods.setGoodsPicture(mallGoodsSku.getGoodsPicture());
            orderGoods.setGoodsNumber(orderGoodsAddDTO.getGoodsNumber());
            BigDecimal totalIntegral = mallGoodsSku.getSalePrice().multiply(BigDecimal.valueOf(orderGoodsAddDTO.getGoodsNumber()));
            orderGoods.setTotalPrice(totalIntegral);

            OrderGoodsInputBean orderGoodsInput = goodsInputMap.get(orderGoodsAddDTO.getMallGoodsSkuId());
            orderGoods.setGradeDiscountPrice(orderGoodsInput.getGradeDiscount());
            orderGoods.setPayPrice(totalIntegral.subtract(orderGoodsInput.getGradeDiscount()));
            orderGoods.setCommonStatus(OrderStatusEnum.PENDING_PAYMENT.getValue());
            orderGoods.setSkuCode(mallGoodsSku.getSkuCode());
            orderGoods.setGoodsType(mallGoodsSku.getGoodsType());
            orderGoodsList.add(orderGoods);
        }

        content.setOrderGoodsList(orderGoodsList);
        content.setOrder(order);
    }
}
