package com.zxt.content.asyncs;

import com.zxt.bean.constants.ThreadNameConstant;
import com.zxt.content.manager.ContentTypeManager;
import com.zxt.content.manager.PrivateContentManager;
import com.zxt.content.model.ContentType;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.time.StopWatch;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.concurrent.CountDownLatch;

/**
 * 文案异步处理类
 */
@Component
@Slf4j
@AllArgsConstructor
public class ContentAsync {

    private final PrivateContentManager privateContentManager;
    private final ContentTypeManager contentTypeManager;

    @Async(ThreadNameConstant.ASYNC_SERVICE_EXECUTOR)
    public void addHeat(List<Long> ids, Integer heatNum) {
        privateContentManager.addHeat(ids, heatNum);
    }

    @Async(ThreadNameConstant.ASYNC_SERVICE_EXECUTOR)
    public void executeAsync(List<ContentType> list2, CountDownLatch countDownLatch) {
        try {
            log.info("start executeAsync");
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();
            //异步线程要做的事情
            contentTypeManager.saveBatch(list2);
            stopWatch.stop();
            log.info("end executeAsync 耗时:{}", stopWatch.getTime());
        } finally {
            countDownLatch.countDown();// 很关键, 无论上面程序是否异常必须执行countDown,否则await无法释放
        }
    }
}
