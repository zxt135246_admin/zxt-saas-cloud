package com.zxt.content.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author zxt
 */
@Data
@ApiModel("商城商品sku入参")
public class MallGoodsSkuDTO implements Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 1L;

    @ApiModelProperty("id")
    private Long id;

    @NotNull(message = "商城id不能为空")
    @ApiModelProperty("mallId")
    private Long mallId;

    @ApiModelProperty("商城商品spuId")
    private Long mallGoodsId;

    @ApiModelProperty("类目id")
    private Long categoryId;

    @NotBlank(message = "商品名称不能为空")
    @ApiModelProperty(value = "商品名称", required = true)
    private String goodsName;

    @NotNull(message = "商品类型不能为空")
    @ApiModelProperty(value = "商品类型", required = true)
    private Byte goodsType;

    @ApiModelProperty("外部商品编码")
    private String outGoodsCode;

    @NotBlank(message = "规格不能为空")
    @ApiModelProperty(value = "规格", required = true)
    private String specValue;

    @NotNull(message = "销售价不能为空")
    @ApiModelProperty(value = "销售价", required = true)
    private BigDecimal salePrice;

    @NotNull(message = "成本价不能为空")
    @ApiModelProperty(value = "成本价", required = true)
    private BigDecimal costPrice;
}
