package com.zxt.content.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString(callSuper = true)
@ApiModel("商品搜索出参")
public class GoodsSearchVO {

    @ApiModelProperty("商城商品sku列表")
    private List<MallGoodsSkuVO> mallGoodsSkuList;

    @ApiModelProperty("规格值列表")
    private List<String> specValues;
}
