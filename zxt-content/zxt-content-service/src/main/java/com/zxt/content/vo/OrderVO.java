package com.zxt.content.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author zxt
 */
@Data
@ApiModel("订单出参")
public class OrderVO {

    @ApiModelProperty("订单号")
    private String orderCode;

    @ApiModelProperty("订单总金额")
    private BigDecimal orderTotalPrice;

    @ApiModelProperty("会员优惠金额")
    private BigDecimal gradeDiscountPrice;

    @ApiModelProperty("订单支付金额")
    private BigDecimal payPrice;

    @ApiModelProperty("订单状态")
    private Byte orderStatus;

    @ApiModelProperty("订单状态描述")
    private String orderStatusDesc;

    @ApiModelProperty("订单创建时间")
    private LocalDateTime createTime;

    @ApiModelProperty("订单商品列表")
    private List<OrderGoodsVO> orderGoodsList;
}
