package com.zxt.content.manager;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zxt.content.dao.ImagesExifMapper;
import com.zxt.content.model.ImagesExif;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author zxt
 */
@Slf4j
@Service
public class ImagesExifManager extends ServiceImpl<ImagesExifMapper, ImagesExif> {


    public void removeAll() {
        LambdaQueryWrapper<ImagesExif> wrapper = ImagesExif.gw();
        remove(wrapper);
    }

    /**
     * 获取相册的exif信息
     *
     * @param imagesId
     * @return
     */
    public ImagesExif getByImagesId(Long imagesId) {
        LambdaQueryWrapper<ImagesExif> wrapper = ImagesExif.gw();
        wrapper.eq(ImagesExif::getImagesId, imagesId);
        return getOne(wrapper);
    }
}
