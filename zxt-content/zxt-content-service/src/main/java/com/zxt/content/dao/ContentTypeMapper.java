package com.zxt.content.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zxt.content.model.ContentType;

public interface ContentTypeMapper extends BaseMapper<ContentType> {
}
