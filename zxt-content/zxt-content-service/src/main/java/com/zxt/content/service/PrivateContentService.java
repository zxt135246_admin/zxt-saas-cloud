package com.zxt.content.service;

import com.zxt.bean.model.UserInfoVO;
import com.zxt.content.dto.ContentDTO;
import com.zxt.content.dto.PublishContentDTO;
import com.zxt.content.vo.PrivateContentVO;

import java.util.List;

/**
 * @author zxt
 */
public interface PrivateContentService {

    /**
     * 新增私有文案
     *
     * @param dto
     * @param user
     * @return
     */
    Long add(ContentDTO dto, UserInfoVO user);

    /**
     * 获取用户的文案
     *
     * @param userId
     * @return
     */
    List<PrivateContentVO> getByUserId(Long userId);

    /**
     * 发布文案
     *
     * @param dto
     */
    void publishContent(PublishContentDTO dto);

    /**
     * 随机返回一条文案
     *
     * @param userId
     * @return
     */
    PrivateContentVO randGetOneContentByUserId(Long userId);

    /**
     * 根据id查询
     *
     * @param contentId
     * @return
     */
    PrivateContentVO getById(Long contentId);
}
