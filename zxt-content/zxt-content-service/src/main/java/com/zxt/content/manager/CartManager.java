package com.zxt.content.manager;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zxt.content.dao.CartMapper;
import com.zxt.content.model.Cart;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author zxt
 */
@Slf4j
@Service
public class CartManager extends ServiceImpl<CartMapper, Cart> {


    /**
     * 根据商品获取购物车
     *
     * @param mallGoodsSkuId
     * @param userId
     * @return
     */
    public Cart getByMallGoodsSkuIdAndUserId(Long mallGoodsSkuId, Long userId) {
        LambdaQueryWrapper<Cart> wrapper = Cart.gw().eq(Cart::getMallGoodsSkuId, mallGoodsSkuId).eq(Cart::getUserId, userId);
        return getOne(wrapper);
    }


    /**
     * 根据用户id查询
     *
     * @param userId
     * @return
     */
    public List<Cart> getByUserId(Long userId) {
        LambdaQueryWrapper<Cart> wrapper = Cart.gw().eq(Cart::getUserId, userId);
        return list(wrapper);
    }

    /**
     * 清空用户购物车
     *
     * @param userId
     */
    public void removeByUserId(Long userId) {
        LambdaQueryWrapper<Cart> wrapper = Cart.gw().eq(Cart::getUserId, userId);
        remove(wrapper);
    }
}
