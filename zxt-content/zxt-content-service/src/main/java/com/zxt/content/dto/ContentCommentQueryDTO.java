package com.zxt.content.dto;

import com.zxt.bean.query.QueryDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author zxt
 */
@Data
@ApiModel("文案评论搜索入参")
public class ContentCommentQueryDTO extends QueryDTO {

    @ApiModelProperty("文案id")
    private Long contentId;
}
