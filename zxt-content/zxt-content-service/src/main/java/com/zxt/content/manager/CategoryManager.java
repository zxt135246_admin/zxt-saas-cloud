package com.zxt.content.manager;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zxt.content.dao.CategoryMapper;
import com.zxt.content.model.Category;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class CategoryManager extends ServiceImpl<CategoryMapper, Category> {

    /**
     * 根据全类目匹配
     *
     * @param fullNamePath
     * @return
     */
    public Category getByFullName(String fullNamePath) {
        LambdaQueryWrapper<Category> wrapper = Category.gw();
        wrapper.eq(Category::getFullNamePath, fullNamePath);
        return getOne(wrapper);
    }
}
