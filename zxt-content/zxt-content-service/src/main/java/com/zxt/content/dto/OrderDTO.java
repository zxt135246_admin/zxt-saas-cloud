package com.zxt.content.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;

/**
 * @author zxt
 */
@Data
@ApiModel("订单入参")
public class OrderDTO {

    private String orderCode;
    private Long userId;

    /**
     * c端状态 1.待付款 2.待收货 3.已完成
     */
    private Byte customerOrderStatus;
}
