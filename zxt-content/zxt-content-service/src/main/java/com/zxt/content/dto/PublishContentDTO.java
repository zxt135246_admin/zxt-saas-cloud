package com.zxt.content.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("发布文案入参")
public class PublishContentDTO {

    @ApiModelProperty("文案id")
    private Long id;

    @ApiModelProperty("是否发布")
    private Boolean isPublish;

}
