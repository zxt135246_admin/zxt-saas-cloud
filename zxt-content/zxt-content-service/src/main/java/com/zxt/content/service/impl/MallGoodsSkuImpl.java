package com.zxt.content.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zxt.bean.enums.CommonStatusEnum;
import com.zxt.bean.exeption.ServiceException;
import com.zxt.bean.query.QueryResultVO;
import com.zxt.bean.result.ResultCode;
import com.zxt.bean.utils.BeanUtils;
import com.zxt.bean.utils.CollectionsUtil;
import com.zxt.content.dto.GoodsSearchDTO;
import com.zxt.content.dto.MallGoodsSkuDTO;
import com.zxt.content.dto.MallGoodsSkuQueryDTO;
import com.zxt.content.dto.UpdateStatusDTO;
import com.zxt.content.manager.CategoryManager;
import com.zxt.content.manager.MallGoodsManager;
import com.zxt.content.manager.MallGoodsSkuManager;
import com.zxt.content.model.Category;
import com.zxt.content.model.MallGoods;
import com.zxt.content.model.MallGoodsSku;
import com.zxt.content.service.MallGoodsSkuService;
import com.zxt.content.vo.GoodsSearchVO;
import com.zxt.content.vo.MallGoodsSkuVO;
import com.zxt.content.vo.MallGoodsVO;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author zxt
 */
@Slf4j
@Service
@AllArgsConstructor
public class MallGoodsSkuImpl implements MallGoodsSkuService {

    private final MallGoodsManager mallGoodsManager;
    private final MallGoodsSkuManager mallGoodsSkuManager;
    private final CategoryManager categoryManager;

    @Override
    public QueryResultVO<MallGoodsSkuVO> queryMallGoodsSku(MallGoodsSkuQueryDTO dto) {
        Page<MallGoodsSku> page = mallGoodsSkuManager.queryMallGoodsSku(dto);
        QueryResultVO<MallGoodsSkuVO> queryResultVO = BeanUtils.pageToQueryResultVO(page, MallGoodsSkuVO.class);
        List<MallGoodsSkuVO> records = BeanUtils.beanCopy(page.getRecords(), MallGoodsSkuVO.class);

        if (CollectionsUtil.isEmpty(records)) {
            queryResultVO.setRecords(new ArrayList<>());
            return queryResultVO;
        }

        for (MallGoodsSkuVO mallGoodsSkuVO : records) {
            //设置商品状态描述
            mallGoodsSkuVO.setCommonStatusDesc(CommonStatusEnum.getDescByValue(mallGoodsSkuVO.getCommonStatus()));
        }

        queryResultVO.setRecords(records);
        return queryResultVO;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT, rollbackFor = Exception.class)
    public void addMallGoodsSku(MallGoodsSkuDTO dto) {

        Category category = categoryManager.getById(dto.getCategoryId());
        if (Objects.isNull(category) || !category.getCategoryLevel().equals(3)) {
            throw new ServiceException(ResultCode.COMMON_MESSAGE, "请选择商品的三级类目");
        }

        if (Objects.isNull(dto.getMallGoodsId())) {
            //生成spu
            MallGoods mallGoods = new MallGoods();
            mallGoods.setMallId(dto.getMallId());
            mallGoods.setSpuName(dto.getGoodsName());
            mallGoodsManager.save(mallGoods);
            dto.setMallGoodsId(mallGoods.getId());
        }


        MallGoodsSku mallGoodsSku = BeanUtils.beanCopy(dto, MallGoodsSku.class);
        if (!dto.getGoodsName().contains(dto.getSpecValue())) {
            mallGoodsSku.setGoodsName(dto.getGoodsName() + " " + dto.getSpecValue());
        }
        mallGoodsSku.setCommonStatus(CommonStatusEnum.CREATE.getValue());
        mallGoodsSkuManager.addMallGoodsSku(mallGoodsSku);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT, rollbackFor = Exception.class)
    public void updateMallGoodsSku(MallGoodsSkuDTO dto) {

        MallGoodsSku oldMallGoodsSku = mallGoodsSkuManager.getById(dto.getId());
        if (Objects.isNull(oldMallGoodsSku)) {
            throw new ServiceException(ResultCode.DATA_NOT_EXIST, "商城商品");
        }

        Category category = categoryManager.getById(dto.getCategoryId());
        if (Objects.isNull(category) || !category.getCategoryLevel().equals(3)) {
            throw new ServiceException(ResultCode.COMMON_MESSAGE, "请选择商品的三级类目");
        }

        if (Objects.isNull(dto.getMallGoodsId())) {
            //生成spu
            MallGoods mallGoods = new MallGoods();
            mallGoods.setMallId(dto.getMallId());
            mallGoods.setSpuName(dto.getGoodsName());
            mallGoodsManager.save(mallGoods);
            dto.setMallGoodsId(mallGoods.getId());
        }

        MallGoodsSku mallGoodsSku = BeanUtils.beanCopy(dto, MallGoodsSku.class);
        if (!dto.getGoodsName().contains(dto.getSpecValue())) {
            mallGoodsSku.setGoodsName(dto.getGoodsName() + " " + dto.getSpecValue());
        }

        mallGoodsSku.setCommonStatus(null);
        mallGoodsSkuManager.updateById(mallGoodsSku);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT, rollbackFor = Exception.class)
    public void updateStatus(UpdateStatusDTO dto) {
        if (Objects.isNull(dto.getId())) {
            throw new ServiceException(ResultCode.COMMON_MESSAGE, "请选择商城商品");
        }

        MallGoodsSku mallGoodsSku = mallGoodsSkuManager.getById(dto.getId());

        if (Objects.isNull(mallGoodsSku)) {
            throw new ServiceException(ResultCode.DATA_NOT_EXIST, "商城商品");
        }

        if (mallGoodsSku.getCommonStatus().equals(dto.getCommonStatus())) {
            throw new ServiceException(ResultCode.COMMON_MESSAGE, "商品状态无需修改");
        }

        mallGoodsSkuManager.updateStatus(dto.getId(), dto.getCommonStatus());
    }

    @Override
    public GoodsSearchVO getSpu(GoodsSearchDTO dto) {
        List<MallGoodsSku> mallGoodsSkuList = mallGoodsSkuManager.getEnableByMallGoodsId(dto.getMallGoodsId());
        GoodsSearchVO goodsSearchVO = new GoodsSearchVO();
        goodsSearchVO.setSpecValues(mallGoodsSkuList.stream().map(MallGoodsSku::getSpecValue).distinct().collect(Collectors.toList()));
        goodsSearchVO.setMallGoodsSkuList(BeanUtils.beanCopy(mallGoodsSkuList, MallGoodsSkuVO.class));

        for (MallGoodsSkuVO mallGoodsSkuVO : goodsSearchVO.getMallGoodsSkuList()) {
            mallGoodsSkuVO.setGoodsPictures(CollectionsUtil.asList(mallGoodsSkuVO.getGoodsPicture().split(",")));
        }
        return goodsSearchVO;
    }

    @Override
    public QueryResultVO<MallGoodsVO> search(GoodsSearchDTO dto) {
        Page<MallGoods> page =mallGoodsSkuManager.search(dto);
        QueryResultVO<MallGoodsVO> queryResultVO = BeanUtils.pageToQueryResultVO(page, MallGoodsVO.class);
        List<MallGoodsVO> records = BeanUtils.beanCopy(page.getRecords(), MallGoodsVO.class);

        if (CollectionsUtil.isEmpty(records)) {
            queryResultVO.setRecords(new ArrayList<>());
            return queryResultVO;
        }

        List<Long> spuIds = records.stream().map(MallGoodsVO::getId).collect(Collectors.toList());
        List<MallGoodsSku> allMallGoodsSkuList = mallGoodsSkuManager.getEnableByMallGoodsIds(spuIds);
        Map<Long, List<MallGoodsSku>> mallGoodsSkuGroup = allMallGoodsSkuList.stream().collect(Collectors.groupingBy(MallGoodsSku::getMallGoodsId));

        for (MallGoodsVO mallGoodsVO : records) {
            List<MallGoodsSku> mallGoodsSkuList = mallGoodsSkuGroup.get(mallGoodsVO.getId());
            if(CollectionsUtil.isEmpty(mallGoodsSkuList)){
                continue;
            }
            MallGoodsSku mallGoodsSku = mallGoodsSkuList.stream().sorted(Comparator.comparing(MallGoodsSku::getSalePrice, Comparator.nullsFirst(BigDecimal::compareTo))).findFirst().orElse(new MallGoodsSku());
            String[] split = mallGoodsSku.getGoodsPicture().split(",");
            mallGoodsVO.setGoodsPicture(split[0]);
            mallGoodsVO.setSalePrice(mallGoodsSku.getSalePrice());
        }

        queryResultVO.setRecords(records);
        return queryResultVO;
    }
}
