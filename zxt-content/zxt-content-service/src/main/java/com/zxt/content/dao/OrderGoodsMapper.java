package com.zxt.content.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zxt.content.model.OrderGoods;

public interface OrderGoodsMapper extends BaseMapper<OrderGoods> {
}
