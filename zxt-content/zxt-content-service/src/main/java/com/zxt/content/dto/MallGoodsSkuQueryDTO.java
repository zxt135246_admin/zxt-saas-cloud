package com.zxt.content.dto;

import com.zxt.bean.query.QueryDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author zxt
 */
@Data
@ApiModel("商城商品sku搜索入参")
public class MallGoodsSkuQueryDTO extends QueryDTO {

    @ApiModelProperty("状态")
    private Byte commonStatus;
}
