package com.zxt.content.manager;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zxt.bean.enums.CommonStatusEnum;
import com.zxt.bean.utils.CollectionsUtil;
import com.zxt.content.dao.MallGoodsSkuMapper;
import com.zxt.content.dto.GoodsSearchDTO;
import com.zxt.content.dto.MallGoodsSkuQueryDTO;
import com.zxt.content.model.MallGoods;
import com.zxt.content.model.MallGoodsSku;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Slf4j
@Service
public class MallGoodsSkuManager extends ServiceImpl<MallGoodsSkuMapper, MallGoodsSku> {

    public Page<MallGoodsSku> queryMallGoodsSku(MallGoodsSkuQueryDTO dto) {
        Page<MallGoodsSku> page = new Page<>();
        page.setCurrent(dto.getPageNo());
        page.setSize(dto.getPageSize());
        LambdaQueryWrapper<MallGoodsSku> queryWrapper = MallGoodsSku.gw();
        if (Objects.nonNull(dto.getCommonStatus())) {
            queryWrapper.eq(MallGoodsSku::getCommonStatus, dto.getCommonStatus());
        }
        queryWrapper.orderByDesc(MallGoodsSku::getCreateTime);
        return page(page, queryWrapper);
    }

    public void addMallGoodsSku(MallGoodsSku mallGoodsSku) {
        Long skuCode = getGlobalSkuCode();
        setSkuCode(mallGoodsSku, skuCode);
    }

    private Long getGlobalSkuCode() {
        MallGoodsSku mallGoodsSku = baseMapper.queryMaxSkuCode();
        if (Objects.isNull(mallGoodsSku)) {
            return 100000000000L;
        } else {
            return Long.parseLong(mallGoodsSku.getSkuCode()) + 1;
        }
    }

    private void setSkuCode(MallGoodsSku mallGoodsSku, Long skuCode) {
        try {
            mallGoodsSku.setSkuCode(String.valueOf(skuCode));
            save(mallGoodsSku);
        } catch (Exception e) {
            if (e instanceof DuplicateKeyException) {
                log.warn("出现了生成商品编码重复现象,skuCode={}", skuCode);
                skuCode = skuCode + 1;
                setSkuCode(mallGoodsSku, skuCode);
            }
            throw e;
        }
    }

    /**
     * 修改状态
     *
     * @param id
     * @param commonStatus
     */
    public void updateStatus(Long id, Byte commonStatus) {
        LambdaUpdateWrapper<MallGoodsSku> updateWrapper = new LambdaUpdateWrapper();
        updateWrapper.set(MallGoodsSku::getCommonStatus, commonStatus);
        updateWrapper.eq(MallGoodsSku::getId, id);
        update(updateWrapper);
    }

    public List<MallGoodsSku> getEnableByMallGoodsId(Long spuId) {
        LambdaQueryWrapper<MallGoodsSku> wrapper = MallGoodsSku.gw();
        wrapper.eq(MallGoodsSku::getMallGoodsId, spuId);
        wrapper.eq(MallGoodsSku::getCommonStatus, CommonStatusEnum.ENABLE.getValue());
        return list(wrapper);
    }

    public List<MallGoodsSku> getEnableByMallGoodsIds(List<Long> spuIds) {
        LambdaQueryWrapper<MallGoodsSku> wrapper = MallGoodsSku.gw();
        wrapper.in(MallGoodsSku::getMallGoodsId, spuIds);
        wrapper.eq(MallGoodsSku::getCommonStatus, CommonStatusEnum.ENABLE.getValue());
        return list(wrapper);
    }

    public Page<MallGoods> search(GoodsSearchDTO dto) {
        Page<MallGoodsSku> page = new Page<>();
        page.setCurrent(dto.getPageNo());
        page.setSize(dto.getPageSize());
        return baseMapper.search(dto, page);
    }

    public List<MallGoodsSku> getByIds(List<Long> mallGoodsSkuIds) {
        if (CollectionsUtil.isEmpty(mallGoodsSkuIds)) {
            return new ArrayList<>();
        }
        LambdaQueryWrapper<MallGoodsSku> wrapper = MallGoodsSku.gw();
        wrapper.in(MallGoodsSku::getId, mallGoodsSkuIds);
        return list(wrapper);
    }
}
