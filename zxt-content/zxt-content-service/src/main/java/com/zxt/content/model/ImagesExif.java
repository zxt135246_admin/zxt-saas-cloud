package com.zxt.content.model;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString(callSuper = true)
@TableName(value = "sc_images_exif")
public class ImagesExif {
    public static final LambdaQueryWrapper<ImagesExif> gw() {
        return new LambdaQueryWrapper<>();
    }

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @TableField(value = "images_id")
    private Long imagesId;

    @TableField(value = "images_height")
    private Integer imagesHeight;
    @TableField(value = "images_width")
    private Integer imagesWidth;
    /**
     * 相册exif参数
     */
    @TableField(value = "exif_json")
    private String exifJson;
    /**
     * 拍摄时间
     */
    @TableField(value = "shooting_time")
    private LocalDateTime shootingTime;
    /**
     * 经度
     */
    @TableField(value = "longitude")
    private String longitude;
    /**
     * 纬度
     */
    @TableField(value = "latitude")
    private String latitude;

    /**
     * 拍摄地址
     */
    @TableField(value = "shooting_address")
    private String shootingAddress;

    /**
     * 创建时间
     */
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    /**
     * 是否删除
     */
    @TableLogic
    @TableField(value = "is_deleted")
    private Boolean isDeleted;
}
