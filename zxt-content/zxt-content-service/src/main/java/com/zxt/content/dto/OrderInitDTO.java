package com.zxt.content.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author zxt
 */
@Data
@ApiModel("订单初始化入参")
public class OrderInitDTO {

    @ApiModelProperty(value = "1.业务订单")
    private Byte orderSource;

    @ApiModelProperty(value = "数据id集合")
    private List<OrderGoodsAddDTO> orderGoodsList;

    @ApiModelProperty(value = "是否使用成长会员折扣")
    private Boolean isUseGradeDiscount;
}
