package com.zxt.content.event.addContent;

import com.alibaba.fastjson.JSON;
import com.zxt.bean.exeption.ServiceException;
import com.zxt.bean.model.UserInfoVO;
import com.zxt.bean.result.ResultCode;
import com.zxt.bean.utils.BeanUtils;
import com.zxt.content.dto.ContentDTO;
import com.zxt.content.manager.ContentTypeManager;
import com.zxt.content.manager.PrivateContentManager;
import com.zxt.content.model.ContentType;
import com.zxt.content.model.PrivateContent;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.Objects;

/**
 * @author zxt 异步监听日志事件
 */
@Slf4j
@Component
@AllArgsConstructor
public class AddContentEventListener {

    private final ContentTypeManager contentTypeManager;
    private final PrivateContentManager privateContentManager;

    @Order(1)
    @EventListener(AddContentEvent.class)
    public void checkContent(AddContentEvent event) {
        AddContentEventContent content = event.getContent();
        log.info("开始校验文案合法性,文案入参:{}", JSON.toJSONString(content.getContentDTO()));
        ContentDTO contentDTO = content.getContentDTO();
        ContentType contentType = contentTypeManager.getById(contentDTO.getContentTypeId());
        if (Objects.isNull(contentType)) {
            log.error("校验失败，文案类型不存在");
            throw new ServiceException(ResultCode.DATA_NOT_EXIST, "文案类型");
        }
        //将文案类型放到上下文中
        content.setContentType(contentType);
        log.info("校验文案合法性完成");
    }

    @Order(2)
    @EventListener(AddContentEvent.class)
    public void saveContent(AddContentEvent event) {
        AddContentEventContent content = event.getContent();
        log.info("开始保存文案信息");
        ContentDTO contentDTO = content.getContentDTO();
        UserInfoVO userInfoVO = content.getUserInfoVO();
        contentDTO.setContentHeat(0);
        contentDTO.setUserId(userInfoVO.getId());
        //默认为公开文案
        contentDTO.setIsPublish(Boolean.TRUE);
        PrivateContent newContent = BeanUtils.beanCopy(contentDTO, PrivateContent.class);
        boolean saveResult = privateContentManager.save(newContent);
        if (!saveResult) {
            throw new ServiceException(ResultCode.SYS_ERROR);
        }
        contentDTO.setId(newContent.getId());
        log.info("保存文案信息完成");
    }
}
