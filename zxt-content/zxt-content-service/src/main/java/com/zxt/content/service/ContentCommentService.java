package com.zxt.content.service;


import com.zxt.bean.query.QueryResultVO;
import com.zxt.content.dto.ContentCommentDTO;
import com.zxt.content.dto.ContentCommentQueryDTO;
import com.zxt.content.vo.ContentCommentVO;

/**
 * 文案评论业务管理
 *
 * @author zxt
 */
public interface ContentCommentService {


    /**
     * 新增文案评论
     *
     * @param dto
     */
    void add(ContentCommentDTO dto);

    /**
     * 分页查询文案评论
     *
     * @param dto
     * @return
     */
    QueryResultVO<ContentCommentVO> queryContentComment(ContentCommentQueryDTO dto);
}
