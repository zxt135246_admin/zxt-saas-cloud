package com.zxt.content.dto;

import com.zxt.bean.query.QueryDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author zxt
 */
@Data
@ApiModel("文案搜索入参")
public class ContentQueryDTO extends QueryDTO {

    @ApiModelProperty("数据id")
    private String contentLike;

    @ApiModelProperty("数据id")
    private Long contentTypeId;

    @ApiModelProperty("是否只查看自己")
    private Boolean isSelf;

    @ApiModelProperty(value = "用户id", hidden = true)
    private Long userId;
}
