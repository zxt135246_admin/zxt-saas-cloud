package com.zxt.content.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString(callSuper = true)
@ApiModel("订单创建")
public class OrderGoodsAddDTO implements Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "商品id")
    private Long mallGoodsSkuId;

    @ApiModelProperty(value = "购买数量")
    private Integer goodsNumber;

    @ApiModelProperty(value = "销售价", hidden = true)
    private BigDecimal salePrice;

    @ApiModelProperty(value = "商品名称",hidden = true)
    private String goodsName;

    @ApiModelProperty(value = "商品图片",hidden = true)
    private String goodsPicture;
}
