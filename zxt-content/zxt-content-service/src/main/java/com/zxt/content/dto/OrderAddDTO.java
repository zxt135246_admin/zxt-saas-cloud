package com.zxt.content.dto;

import com.zxt.user.api.vo.GradeApiVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString(callSuper = true)
@ApiModel("订单创建")
public class OrderAddDTO implements Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "1.文案订单")
    private Byte orderSource;

    @ApiModelProperty(value = "商品集合")
    private List<OrderGoodsAddDTO> orderGoodsList;

    @ApiModelProperty(value = "订单总金额")
    private BigDecimal orderTotalPrice;

    @ApiModelProperty(value = "成长会员折扣金额")
    private BigDecimal discountPrice;

    @ApiModelProperty(value = "成长会员")
    private GradeApiVO gradeApiVO;

    @ApiModelProperty(value = "是否使用成长会员折扣")
    private Boolean isUseGradeDiscount;

}
