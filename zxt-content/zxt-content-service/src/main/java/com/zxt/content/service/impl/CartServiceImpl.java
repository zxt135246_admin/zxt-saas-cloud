package com.zxt.content.service.impl;

import com.zxt.bean.exeption.ServiceException;
import com.zxt.bean.result.ResultCode;
import com.zxt.bean.utils.BeanUtils;
import com.zxt.content.manager.CartManager;
import com.zxt.content.manager.MallGoodsSkuManager;
import com.zxt.content.model.Cart;
import com.zxt.content.model.MallGoodsSku;
import com.zxt.content.service.CartService;
import com.zxt.content.vo.CartVO;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;

@Slf4j
@Service
@AllArgsConstructor
public class CartServiceImpl implements CartService {

    private final CartManager cartManager;
    private final MallGoodsSkuManager mallGoodsSkuManager;

    @Override
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT, rollbackFor = Exception.class)
    public void addCart(Long mallGoodsSkuId, Long userId) {
        MallGoodsSku mallGoodsSku = mallGoodsSkuManager.getById(mallGoodsSkuId);
        if (Objects.isNull(mallGoodsSku)) {
            throw new ServiceException(ResultCode.COMMON_MESSAGE, "商品已下架");
        }

        Cart cart = cartManager.getByMallGoodsSkuIdAndUserId(mallGoodsSkuId, userId);
        if (Objects.isNull(cart)) {
            cart = new Cart();
            cart.setMallId(mallGoodsSku.getMallId());
            cart.setUserId(userId);
            cart.setMallGoodsSkuId(mallGoodsSkuId);
            cart.setSkuCode(mallGoodsSku.getSkuCode());
            cart.setGoodsName(mallGoodsSku.getGoodsName());
            String[] split = mallGoodsSku.getGoodsPicture().split(",");
            cart.setGoodsPicture(split[0]);
            cart.setGoodsType(mallGoodsSku.getGoodsType());
            cart.setSpecValue(mallGoodsSku.getSpecValue());
            cart.setAddGoodsPrice(mallGoodsSku.getSalePrice());
            cart.setGoodsNumber(1);
            cart.setIsSelected(Boolean.TRUE);
        } else {
            cart.setUpdateTime(null);
            cart.setGoodsNumber(cart.getGoodsNumber() + 1);
        }
        cartManager.saveOrUpdate(cart);
    }

    @Override
    public List<CartVO> getByUserId(Long userId) {
        List<CartVO> cartVOList = BeanUtils.beanCopy(cartManager.getByUserId(userId), CartVO.class);
        for (CartVO cartVO : cartVOList) {
            String[] split = cartVO.getGoodsPicture().split(",");
            cartVO.setGoodsPicture(split[0]);
        }
        return cartVOList;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT, rollbackFor = Exception.class)
    public void remove(Long id) {
        cartManager.removeById(id);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT, rollbackFor = Exception.class)
    public void clear(Long userId) {
        cartManager.removeByUserId(userId);
    }
}
