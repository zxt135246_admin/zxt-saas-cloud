package com.zxt.content.vo;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.zxt.bean.convertor.LongJsonDeserializer;
import com.zxt.bean.convertor.LongJsonSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author zxt
 */
@Data
@ApiModel("购物车出参")
public class CartVO {

    @ApiModelProperty("购物车id")
    @JsonSerialize(using = LongJsonSerializer.class)
    @JsonDeserialize(using = LongJsonDeserializer.class)
    private Long id;

    @ApiModelProperty("商品名称")
    private String goodsName;

    @ApiModelProperty("商品图片")
    private String goodsPicture;

    @ApiModelProperty("添加时商品价格")
    private BigDecimal addGoodsPrice;

    @ApiModelProperty("商品数量")
    private Integer goodsNumber;

    @ApiModelProperty("是否选中")
    private Boolean isSelected;

}
