package com.zxt.content.asyncs;

import com.zxt.content.client.OperationClient;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

/**
 * 相册异步处理类
 */
@Component
@Slf4j
@AllArgsConstructor
public class ImagesAsync {

    private final OperationClient operationClient;

    /*@Async*/
    public Boolean asyncUploadImage(MultipartFile file, String directory, Long imageId) {
        return operationClient.uploadImage(file, directory, imageId);
    }

    @Async
    public void asyncUploadImageByUrl(String url, String directory, Long imageId) {
        operationClient.uploadImageByUrl(url, directory, imageId);
    }
}
