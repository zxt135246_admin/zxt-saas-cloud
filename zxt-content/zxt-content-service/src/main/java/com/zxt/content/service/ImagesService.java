package com.zxt.content.service;


import com.zxt.bean.model.UserInfoVO;
import com.zxt.content.api.dto.ImagesExifApiDTO;
import com.zxt.content.dto.ImagesDTO;
import com.zxt.content.dto.ShareImagesDTO;
import com.zxt.content.vo.ImagesVO;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

/**
 * 相册业务管理
 *
 * @author zxt
 */
public interface ImagesService {

    /**
     * 查询照片详情
     *
     * @param id
     * @return
     */
    ImagesVO getById(Long id);

    /**
     * 上传相册
     *
     * @param file
     * @param userInfoVO
     * @param remark
     * @param collectionsId
     * @return
     */
    Long uploadImage(MultipartFile file, UserInfoVO userInfoVO, String remark, Long collectionsId) throws IOException;

    /**
     * 批量上传相册
     *
     * @param files
     * @param user
     * @param collectionsId
     * @return
     */
    List<Long> batchUploadImage(List<MultipartFile> files, UserInfoVO user, Long collectionsId);

    /**
     * 根据id更新
     *
     * @param dto
     * @return
     */
    Boolean updateById(ImagesDTO dto);

    /**
     * 根据相册合集id来获取相册列表
     *
     * @param collectionsId
     * @param userId
     * @return
     */
    List<ImagesVO> getByCollectionsId(Long collectionsId, Long userId);

    /**
     * 修改图片信息
     *
     * @param dto
     */
    void update(ImagesDTO dto);

    /**
     * 保存相册Exif
     *
     * @param dto
     * @return
     */
    Boolean saveOrUpdateImagesExif(ImagesExifApiDTO dto);

    /**
     * 初始化相册Exif
     *
     * @param isInitAll
     */
    void initImagesExif(Boolean isInitAll);

    /**
     * 批量保存相册Exif
     *
     * @param imagesExifApiDTOList
     */
    void saveBatchImagesExif(List<ImagesExifApiDTO> imagesExifApiDTOList);

    /**
     * 分享图片
     *
     * @param dto
     * @return
     */
    String shareImages(ShareImagesDTO dto);

    /**
     * 根据分享编码获取
     *
     * @param shareCode
     * @return
     */
    ImagesVO getByShareCode(String shareCode);

    /**
     * 将本地图片推送到oss进行保存
     *
     * @param imagesId
     */
    void pushLocalToOssSave(Long imagesId);
}
