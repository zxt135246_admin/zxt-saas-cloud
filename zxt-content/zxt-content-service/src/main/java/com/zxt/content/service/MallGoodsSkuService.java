package com.zxt.content.service;

import com.zxt.bean.query.QueryResultVO;
import com.zxt.content.dto.GoodsSearchDTO;
import com.zxt.content.dto.MallGoodsSkuDTO;
import com.zxt.content.dto.MallGoodsSkuQueryDTO;
import com.zxt.content.dto.UpdateStatusDTO;
import com.zxt.content.vo.GoodsSearchVO;
import com.zxt.content.vo.MallGoodsSkuVO;
import com.zxt.content.vo.MallGoodsVO;

public interface MallGoodsSkuService {

    /**
     * 查询商城商品sku列表
     *
     * @param dto
     * @return
     */
    QueryResultVO<MallGoodsSkuVO> queryMallGoodsSku(MallGoodsSkuQueryDTO dto);

    /**
     * 新增商品商品
     *
     * @param dto
     */
    void addMallGoodsSku(MallGoodsSkuDTO dto);

    /**
     * 修改商城商品
     *
     * @param dto
     */
    void updateMallGoodsSku(MallGoodsSkuDTO dto);

    /**
     * 修改商品状态
     *
     * @param dto
     */
    void updateStatus(UpdateStatusDTO dto);

    /**
     * 搜索
     *
     * @param dto
     * @return
     */
    GoodsSearchVO getSpu(GoodsSearchDTO dto);

    /**
     * 搜索
     *
     * @param dto
     * @return
     */
    QueryResultVO<MallGoodsVO> search(GoodsSearchDTO dto);
}
