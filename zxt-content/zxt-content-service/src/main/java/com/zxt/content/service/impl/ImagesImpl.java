package com.zxt.content.service.impl;

import com.alibaba.fastjson.JSON;
import com.zxt.bean.constants.ThreadNameConstant;
import com.zxt.bean.exeption.ServiceException;
import com.zxt.bean.model.UserInfoVO;
import com.zxt.bean.result.ResultCode;
import com.zxt.bean.utils.BeanUtils;
import com.zxt.bean.utils.CollectionsUtil;
import com.zxt.bean.utils.FileUtils;
import com.zxt.bean.utils.StringUtils;
import com.zxt.content.api.dto.ImagesExifApiDTO;
import com.zxt.content.asyncs.ImagesAsync;
import com.zxt.content.client.OperationClient;
import com.zxt.content.client.UserClient;
import com.zxt.content.constants.RedisKeyContent;
import com.zxt.content.dto.ImagesDTO;
import com.zxt.content.dto.ShareImagesDTO;
import com.zxt.content.enums.ShareTimeTypeEnum;
import com.zxt.content.manager.ImagesCollectionsManager;
import com.zxt.content.manager.ImagesExifManager;
import com.zxt.content.manager.ImagesManager;
import com.zxt.content.model.Images;
import com.zxt.content.model.ImagesCollections;
import com.zxt.content.model.ImagesExif;
import com.zxt.content.service.ImagesService;
import com.zxt.content.utils.CodeGeneratorUtil;
import com.zxt.content.vo.ImagesVO;
import com.zxt.operation.api.dto.ImagesApiDTO;
import com.zxt.redis.utils.RedisUtil;
import com.zxt.user.api.vo.UserApiVO;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

@Slf4j
@Service
@AllArgsConstructor
public class ImagesImpl implements ImagesService {

    private final ImagesManager imagesManager;
    private final ImagesCollectionsManager imagesCollectionsManager;
    private final ImagesExifManager imagesExifManager;
    private final ImagesAsync imagesAsync;
    private final UserClient userClient;
    private final OperationClient operationClient;

    @Override
    public ImagesVO getById(Long id) {
        Images images = imagesManager.getById(id);
        if (Objects.isNull(images)) {
            throw new ServiceException(ResultCode.COMMON_MESSAGE, "相册不存在");
        }
        ImagesVO imagesVO = BeanUtils.beanCopy(images, ImagesVO.class);

        ImagesExif imagesExif = imagesExifManager.getByImagesId(images.getId());
        if (Objects.nonNull(imagesExif)) {
            //设置相册拍摄信息
            imagesVO.setShootingAddress(imagesExif.getShootingAddress());
            imagesVO.setShootingTime(imagesExif.getShootingTime());
        }
        return imagesVO;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT, rollbackFor = Exception.class)
    public Long uploadImage(MultipartFile file, UserInfoVO userInfoVO, String remark, Long collectionsId) throws IOException {

        ImagesCollections imagesCollections = imagesCollectionsManager.getById(collectionsId);
        if (Objects.isNull(imagesCollections)) {
            throw new ServiceException(ResultCode.COMMON_MESSAGE, "合集不存在");
        }

        Images images = generationImageFile(file, userInfoVO, remark, collectionsId);
        return images.getId();
    }


    /**
     * 生成本地图片文件
     *
     * @param file
     * @param userInfoVO
     * @param remark
     * @param collectionsId
     * @return
     */
    private Images generationImageFile(MultipartFile file, UserInfoVO userInfoVO, String remark, Long collectionsId) {
        File localFile;
        try {
            localFile = FileUtils.multipartFileToFile(file);
        } catch (Exception e) {
            log.error("上传相册报错,异常信息:{}", e);
            throw new ServiceException(ResultCode.COMMON_MESSAGE, "请上传正确格式的相册");
        }

        Images images = new Images();
        images.setCollectionsId(collectionsId);
        images.setUserId(userInfoVO.getId());
        images.setImageUrl("https://copywritings.oss-cn-beijing.aliyuncs.com/cloud/content/images/jiazaiing.png");
        images.setOriginalImageUrl(localFile.getPath());
        images.setRemark(remark);
        imagesManager.save(images);
        return images;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT, rollbackFor = Exception.class)
    public List<Long> batchUploadImage(List<MultipartFile> files, UserInfoVO userInfoVO, Long collectionsId) {
        ImagesCollections imagesCollections = imagesCollectionsManager.getById(collectionsId);
        if (Objects.isNull(imagesCollections)) {
            throw new ServiceException(ResultCode.COMMON_MESSAGE, "合集不存在");
        }

        List<Long> imagesIds = new ArrayList<>();
        for (MultipartFile file : files) {
            Images images = generationImageFile(file, userInfoVO, null, collectionsId);
            imagesIds.add(images.getId());
        }
        return imagesIds;
    }

    /*@Override
    public void uploadImage(MultipartFile file, UserInfoVO userInfoVO, String remark, Long collectionsId) throws IOException {

        ImagesCollections imagesCollections = imagesCollectionsManager.getById(collectionsId);
        ImagesCollections parentImagesCollections = imagesCollectionsManager.getById(imagesCollections.getParentId());
        if (Objects.isNull(imagesCollections)) {
            throw new ServiceException(ResultCode.COMMON_MESSAGE, "合集不存在");
        }

        Images images = new Images();
        images.setCollectionsId(collectionsId);
        images.setUserId(userInfoVO.getId());
        images.setImageUrl("https://copywritings.oss-cn-beijing.aliyuncs.com/cloud/content/images/jiazaiing.png");
        images.setOriginalImageUrl("https://copywritings.oss-cn-beijing.aliyuncs.com/cloud/content/images/jiazaiing.png");
        images.setRemark(remark);

        imagesManager.save(images);

        List<Long> shareUserIds = JSON.parseArray(imagesCollections.getShareUserJson(), Long.class);
        shareUserIds.add(imagesCollections.getUserId());
        shareUserIds.removeAll(CollectionsUtil.asList(0L, userInfoVO.getId()));

        if (CollectionsUtil.isNotEmpty(shareUserIds)) {
            for (Long shareUserId : shareUserIds) {
                SystemNewsApiDTO apiDTO = new SystemNewsApiDTO();
                apiDTO.setUserId(shareUserId);
                apiDTO.setTraceId(LogUtils.getTraceId());
                apiDTO.setIsRead(Boolean.FALSE);
                apiDTO.setDataId(imagesCollections.getId().toString());
                apiDTO.setNewsType(NewsTypeEnum.IMAGES.getValue());
                apiDTO.setContent(userInfoVO.getNickName() + " 共享了新的照片到 " + parentImagesCollections.getCollectionsName() + "-" + imagesCollections.getCollectionsName() + " 合集！（可点击查看）");
                operationClient.saveSystemNews(apiDTO);
            }
        }


        imagesAsync.asyncUploadImage(file, userInfoVO.getId() + "/" + imagesCollections.getCollectionsName() + "/", images.getId());
    }*/

    @Override
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT, rollbackFor = Exception.class)
    public Boolean updateById(ImagesDTO dto) {
        Images images = imagesManager.getById(dto.getId());
        //如果未添加说明，则采用拍摄地址
        if (StringUtils.isBlank(images.getRemark())) {
            dto.setRemark(dto.getShootingAddress());
        }
        return imagesManager.updateById(BeanUtils.beanCopy(dto, Images.class));
    }

    @Override
    public List<ImagesVO> getByCollectionsId(Long collectionsId, Long userId) {
        ImagesDTO imagesDTO = new ImagesDTO();
        imagesDTO.setCollectionsId(collectionsId);
        imagesDTO.setUserId(userId);
        List<Images> imagesList = imagesManager.query(imagesDTO);
        return BeanUtils.beanCopy(imagesList, ImagesVO.class);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT, rollbackFor = Exception.class)
    public void update(ImagesDTO dto) {
        if (Objects.isNull(dto.getId())) {
            throw new ServiceException(ResultCode.COMMON_MESSAGE, "请选择要修改的照片");
        }
        imagesManager.updateImages(BeanUtils.beanCopy(dto, Images.class));
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT, rollbackFor = Exception.class)
    public Boolean saveOrUpdateImagesExif(ImagesExifApiDTO dto) {
        if (Objects.isNull(dto.getId())) {
            return imagesExifManager.save(BeanUtils.beanCopy(dto, ImagesExif.class));
        } else {
            return imagesExifManager.updateById(BeanUtils.beanCopy(dto, ImagesExif.class));
        }
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT, rollbackFor = Exception.class)
    public void initImagesExif(Boolean isInitAll) {
        if (isInitAll) {
            //清除所有exif
            imagesExifManager.removeAll();
            List<Images> imagesList = imagesManager.list();
            if (CollectionsUtil.isEmpty(imagesList)) {
                return;
            }
            operationClient.initImagesExif(BeanUtils.beanCopy(imagesList, ImagesApiDTO.class));
        }
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT, rollbackFor = Exception.class)
    public void saveBatchImagesExif(List<ImagesExifApiDTO> imagesExifApiDTOList) {
        imagesExifManager.saveBatch(BeanUtils.beanCopy(imagesExifApiDTOList, ImagesExif.class));
    }

    @Override
    public String shareImages(ShareImagesDTO dto) {
        if (Objects.isNull(dto.getImagesId())) {
            throw new ServiceException(ResultCode.COMMON_MESSAGE, "请选择需要分享的相册");
        }
        Byte shareTimeType = dto.getShareTimeType();
        if (Objects.isNull(shareTimeType)) {
            throw new ServiceException(ResultCode.COMMON_MESSAGE, "请选择分享的时间限制");
        }
        if (Objects.isNull(dto.getIsHiddenRemark())) {
            //默认隐藏说明
            dto.setIsHiddenRemark(Boolean.TRUE);
        }
        //设置分享编码
        setShareCode(dto);

        if (ShareTimeTypeEnum.TEN_SECONDS.equal(shareTimeType)) {
            RedisUtil.set(RedisKeyContent.getShareKey(dto.getShareCode()), JSON.toJSONString(dto), TimeUnit.SECONDS, 10);
        } else if (ShareTimeTypeEnum.ONE_MINUTE.equal(shareTimeType)) {
            RedisUtil.set(RedisKeyContent.getShareKey(dto.getShareCode()), JSON.toJSONString(dto), TimeUnit.MINUTES, 1);
        } else if (ShareTimeTypeEnum.FIVE_MINUTE.equal(shareTimeType)) {
            RedisUtil.set(RedisKeyContent.getShareKey(dto.getShareCode()), JSON.toJSONString(dto), TimeUnit.MINUTES, 5);
        } else if (ShareTimeTypeEnum.TEN_MINUTE.equal(shareTimeType)) {
            RedisUtil.set(RedisKeyContent.getShareKey(dto.getShareCode()), JSON.toJSONString(dto), TimeUnit.MINUTES, 10);
        } else if (ShareTimeTypeEnum.ONE_HOURS.equal(shareTimeType)) {
            RedisUtil.set(RedisKeyContent.getShareKey(dto.getShareCode()), JSON.toJSONString(dto), TimeUnit.HOURS, 1);
        } else if (ShareTimeTypeEnum.FIVE_HOURS.equal(shareTimeType)) {
            RedisUtil.set(RedisKeyContent.getShareKey(dto.getShareCode()), JSON.toJSONString(dto), TimeUnit.HOURS, 5);
        } else if (ShareTimeTypeEnum.ONE_DAY.equal(shareTimeType)) {
            RedisUtil.set(RedisKeyContent.getShareKey(dto.getShareCode()), JSON.toJSONString(dto), TimeUnit.DAYS, 1);
        } else if (ShareTimeTypeEnum.THREE_DAY.equal(shareTimeType)) {
            RedisUtil.set(RedisKeyContent.getShareKey(dto.getShareCode()), JSON.toJSONString(dto), TimeUnit.DAYS, 3);
        } else if (ShareTimeTypeEnum.SEVEN_DAY.equal(shareTimeType)) {
            RedisUtil.set(RedisKeyContent.getShareKey(dto.getShareCode()), JSON.toJSONString(dto), TimeUnit.DAYS, 7);
        } else if (ShareTimeTypeEnum.ONE_MONTH.equal(shareTimeType)) {
            RedisUtil.set(RedisKeyContent.getShareKey(dto.getShareCode()), JSON.toJSONString(dto), TimeUnit.DAYS, 30);
        }

        return dto.getShareCode();
    }

    @Override
    public ImagesVO getByShareCode(String shareCode) {
        Object o = RedisUtil.get(RedisKeyContent.getShareKey(shareCode));
        if (Objects.isNull(o)) {
            throw new ServiceException(ResultCode.COMMON_MESSAGE, "分享相册已过期");
        }

        ShareImagesDTO shareImagesDTO = JSON.parseObject(o.toString(), ShareImagesDTO.class);
        if (Objects.isNull(shareImagesDTO)) {
            throw new ServiceException(ResultCode.COMMON_MESSAGE, "分享相册已过期");
        }

        UserApiVO userApiVO = userClient.getUserById(shareImagesDTO.getUserId());

        Images images = imagesManager.getById(shareImagesDTO.getImagesId());

        if (shareImagesDTO.getIsHiddenRemark()) {
            images.setUserId(null);
            images.setCollectionsId(null);
            images.setRemark(null);
        }

        ImagesVO imagesVO = BeanUtils.beanCopy(images, ImagesVO.class);

        ImagesExif imagesExif = imagesExifManager.getByImagesId(images.getId());
        if (Objects.nonNull(imagesExif)) {
            //设置相册拍摄信息
            imagesVO.setShootingAddress(imagesExif.getShootingAddress());
            imagesVO.setShootingTime(imagesExif.getShootingTime());
        }

        if (Objects.nonNull(userApiVO)) {
            //设置用户信息
            imagesVO.setNickName(userApiVO.getNickName());
            imagesVO.setAvatarUrl(userApiVO.getAvatarUrl());
            imagesVO.setLevelDesc(userApiVO.getLevelDesc());
        }

        return imagesVO;
    }

    @Override
    @Async(ThreadNameConstant.ASYNC_SERVICE_EXECUTOR)
    public void pushLocalToOssSave(Long imagesId) {
        Images images = imagesManager.getById(imagesId);
        if (Objects.isNull(images)) {
            log.info("图片不存在，结束推送");
            return;
        }
        File localFile = new File(images.getOriginalImageUrl());
        MultipartFile multipartFile = FileUtils.fileToMultipartFile(localFile);
        if(Objects.isNull(multipartFile)){
            log.info("已经推送过，结束推送");
            return;
        }
        ImagesCollections imagesCollections = imagesCollectionsManager.getById(images.getCollectionsId());
        Boolean isTrue = imagesAsync.asyncUploadImage(multipartFile, images.getUserId() + "/" + imagesCollections.getCollectionsName() + "/", images.getId());
        if (Objects.nonNull(isTrue) && isTrue) {
            //删除本地文件
            FileUtils.deleteFile(localFile);
        }
        log.info("推送相册完成");
    }

    public void setShareCode(ShareImagesDTO dto) {
        try {
            // 分享编码
            dto.setShareCode(CodeGeneratorUtil.generateShareCode());
            Object o = RedisUtil.get(RedisKeyContent.getShareKey(dto.getShareCode()));
            if (Objects.nonNull(o)) {
                throw new DuplicateKeyException("分享编码重复");
            }
        } catch (Exception e) {
            if (e instanceof DuplicateKeyException) {
                log.warn("出现了生成分享编码重复现象,ShareCode={}", dto.getShareCode());
                setShareCode(dto);
            }
            throw e;
        }
    }
}
