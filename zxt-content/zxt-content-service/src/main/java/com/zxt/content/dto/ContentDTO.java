package com.zxt.content.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("文案入参")
public class ContentDTO {

    @ApiModelProperty("id")
    private Long id;

    @ApiModelProperty("用户id")
    private Long userId;

    /**
     * 文案类型id
     */
    @ApiModelProperty("文案类型id")
    private Long contentTypeId;

    /**
     * 文案内容
     */
    @ApiModelProperty("内容")
    private String content;

    /**
     * 文案来源
     */
    @ApiModelProperty("来源")
    private String contentSource;

    /**
     * 是否已发布
     */
    @ApiModelProperty("是否已发布")
    private Boolean isPublish;

    /**
     * 文案热度
     */
    @ApiModelProperty("文案热度")
    private Integer contentHeat;
}
