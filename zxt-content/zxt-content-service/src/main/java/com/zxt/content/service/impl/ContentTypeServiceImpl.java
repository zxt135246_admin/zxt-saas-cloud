package com.zxt.content.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zxt.bean.exeption.ServiceException;
import com.zxt.bean.query.QueryResultVO;
import com.zxt.bean.result.ResultCode;
import com.zxt.bean.utils.BeanUtils;
import com.zxt.bean.utils.StringUtils;
import com.zxt.content.dto.ContentTypeDTO;
import com.zxt.content.dto.ContentTypeQueryDTO;
import com.zxt.content.manager.ContentTypeManager;
import com.zxt.content.model.ContentType;
import com.zxt.content.service.ContentTypeService;
import com.zxt.content.vo.ContentTypeVO;
import com.zxt.redis.annotation.CustomCache;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
@AllArgsConstructor
public class ContentTypeServiceImpl implements ContentTypeService {

    private final ContentTypeManager contentTypeManager;

    @Override
    public void add(ContentTypeDTO dto) {
        if (StringUtils.isBlank(dto.getTypeName())) {
            throw new ServiceException(ResultCode.COMMON_PARAM_NULL, "文案类型名称");
        }
        contentTypeManager.save(BeanUtils.beanCopy(dto, ContentType.class));
    }

    @Override
    @CustomCache
    public QueryResultVO<ContentTypeVO> queryContentType(ContentTypeQueryDTO dto) {
        Page<ContentType> page = contentTypeManager.queryContentType(dto);
        QueryResultVO<ContentTypeVO> queryResultVO = BeanUtils.pageToQueryResultVO(page, ContentTypeVO.class);
        queryResultVO.setRecords(BeanUtils.beanCopy(page.getRecords(), ContentTypeVO.class));
        return queryResultVO;
    }
}
