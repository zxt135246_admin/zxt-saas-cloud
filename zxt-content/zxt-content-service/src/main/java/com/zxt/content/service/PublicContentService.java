package com.zxt.content.service;

import com.zxt.bean.model.UserInfoVO;
import com.zxt.bean.query.QueryResultVO;
import com.zxt.content.dto.ContentQueryDTO;
import com.zxt.content.vo.PublicContentDetailVO;
import com.zxt.content.vo.PublicContentVO;

/**
 * @author zxt
 */
public interface PublicContentService {
    /**
     * 分页查询
     *
     * @param dto
     * @return
     */
    QueryResultVO<PublicContentVO> query(ContentQueryDTO dto);

    /**
     * 查询文案详情信息
     *
     * @param id
     * @param user
     * @return
     */
    PublicContentDetailVO get(Long id, UserInfoVO user);

    /**
     * 推送文案到邮箱
     */
    void dealPushEmail();

    /**
     * 网易云生成文案
     */
    void dealGenerationContent();
}
