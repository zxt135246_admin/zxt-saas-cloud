package com.zxt.content.enums;

/**
 * @author Lym
 * @version 1.0
 * @date 2021/8/11 3:41 下午
 * @description
 */
public enum PayStatusEnum {
    WAIT((byte) 1, "等待支付"),
    ABANDON((byte) 99, "放弃支付"),
    SUCCESS((byte) 100, "成功支付");


    private byte value;
    private String desc;

    PayStatusEnum(byte value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public byte getValue() {
        return value;
    }

    public String getDesc() {
        return desc;
    }

    public boolean equal(Byte val) {
        return val == null ? false : val.byteValue() == this.value;
    }

    public static String getDescByValue(Byte value) {
        if (value == null) {
            return "";
        }
        for (PayStatusEnum element : PayStatusEnum.values()) {
            if (element.value == value.byteValue()) {
                return element.desc;
            }
        }
        return "";
    }
}
