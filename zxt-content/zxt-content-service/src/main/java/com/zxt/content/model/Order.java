package com.zxt.content.model;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString(callSuper = true)
@TableName(value = "oc_order")
public class Order {
    public static final LambdaQueryWrapper<Order> gw() {
        return new LambdaQueryWrapper<>();
    }

    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;

    @TableField(value = "user_id")
    private Long userId;

    @TableField(value = "supplier_data_id")
    private String supplierDataId;

    @TableField(value = "distribution_ratio")
    private Integer distributionRatio;

    @TableField(value = "order_code")
    private String orderCode;

    @TableField(value = "order_source")
    private Byte orderSource;

    @TableField(value = "order_status")
    private Byte orderStatus;

    @TableField(value = "order_total_price")
    private BigDecimal orderTotalPrice;

    @TableField(value = "grade_discount_price")
    private BigDecimal gradeDiscountPrice;

    @TableField(value = "pay_price")
    private BigDecimal payPrice;

    @TableField(value = "remark")
    private String remark;


    @TableField(value = "pay_close_time")
    private LocalDateTime payCloseTime;

    /**
     * 支付状态
     */
    @TableField(value = "pay_status")
    private Byte payStatus;

    /**
     * 支付时间
     */
    @TableField(value = "pay_time")
    private LocalDateTime payTime;

    /**
     * 确认收货时间
     */
    @TableField(value = "confirm_time")
    private LocalDateTime confirmTime;


    /**
     * 创建时间
     */
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    /**
     * 是否删除
     */
    @TableLogic
    @TableField(value = "is_deleted")
    private Boolean isDeleted;
}
