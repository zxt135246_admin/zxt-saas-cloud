package com.zxt.content.service.impl;

import com.zxt.content.manager.OrderGoodsManager;
import com.zxt.content.service.OrderGoodsService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@AllArgsConstructor
public class OrderGoodsServiceImpl implements OrderGoodsService {

    private final OrderGoodsManager orderGoodsManager;
}
