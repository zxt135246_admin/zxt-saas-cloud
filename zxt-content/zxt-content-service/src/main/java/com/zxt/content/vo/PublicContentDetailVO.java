package com.zxt.content.vo;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.zxt.bean.convertor.LongJsonDeserializer;
import com.zxt.bean.convertor.LongJsonSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author zxt
 */
@Data
@ApiModel("文案出参")
public class PublicContentDetailVO {

    @ApiModelProperty("id")
    @JsonSerialize(using = LongJsonSerializer.class)
    @JsonDeserialize(using = LongJsonDeserializer.class)
    private Long id;

    @ApiModelProperty("用户id")
    @JsonSerialize(using = LongJsonSerializer.class)
    @JsonDeserialize(using = LongJsonDeserializer.class)
    private Long userId;

    @ApiModelProperty("内容")
    private String content;

    @ApiModelProperty("来源")
    private String contentSource;

    @ApiModelProperty("文案类型id")
    @JsonSerialize(using = LongJsonSerializer.class)
    @JsonDeserialize(using = LongJsonDeserializer.class)
    private Long contentTypeId;

    @ApiModelProperty("文案类型描述")
    private String contentTypeDesc;

    @ApiModelProperty("文案热度")
    private Integer contentHeat;

    private LocalDateTime createTime;

    @ApiModelProperty("昵称")
    private String nickName;

    @ApiModelProperty("头像url")
    private String avatarUrl;

    @ApiModelProperty("粉丝数")
    private Integer fenSiNum;

    @ApiModelProperty("是否已关注")
    private Boolean isConcern;
}
