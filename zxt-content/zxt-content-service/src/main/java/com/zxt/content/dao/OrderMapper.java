package com.zxt.content.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zxt.content.model.Order;

public interface OrderMapper extends BaseMapper<Order> {
}
