package com.zxt.content.constants;

import com.zxt.bean.utils.StringUtils;

import java.util.Objects;

/**
 * @author zxt
 */
public class RedisKeyContent {

    private static final String CONTENT = "content:";

    public static String getUploadKey(Long userId) {
        StringBuilder sbBuilder = new StringBuilder(80);
        sbBuilder.append(CONTENT);
        sbBuilder.append("images:userId:");
        sbBuilder.append(userId);
        return sbBuilder.toString();
    }


    public static String getShareKey(String shareCode) {
        StringBuilder sbBuilder = new StringBuilder(80);
        sbBuilder.append(CONTENT);
        sbBuilder.append("images:shareCode:");
        sbBuilder.append(shareCode);
        return sbBuilder.toString();
    }

    public static String getInitOrderDTOKey(String key) {
        if (StringUtils.isEmpty(key)) {
            return "";
        }
        StringBuilder sbBuilder = new StringBuilder(80);
        sbBuilder.append(CONTENT);
        sbBuilder.append("order:init:");
        sbBuilder.append(key);
        return sbBuilder.toString();
    }

    public static String getLock(String key, Object dataId) {
        if (StringUtils.isEmpty(key)) {
            return "";
        }
        StringBuilder builder = new StringBuilder(80);
        builder.append(CONTENT);
        builder.append("lock:key:");
        builder.append(key);
        if (Objects.nonNull(dataId)) {
            builder.append("dataId:");
            builder.append(dataId);
        }
        return builder.toString();
    }
}
