package com.zxt.content.executor.handler;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zxt.bean.executor.context.HandleContextBase;
import com.zxt.bean.executor.handler.AbstractHandle;
import com.zxt.bean.exeption.ServiceException;
import com.zxt.bean.result.ResultCode;
import com.zxt.bean.utils.StringUtils;
import lombok.extern.slf4j.Slf4j;

import java.util.Objects;

/**
 * 通用校验数据是否存在
 */
@Slf4j
public class CheckExistHandle extends AbstractHandle {

    private IService service;
    private String dataName;

    public CheckExistHandle(IService service, String dataName) {
        super("checkExistHandle");
        this.service = service;
        if (StringUtils.isBlank(dataName)) {
            this.dataName = "数据";
        } else {
            this.dataName = dataName;
        }
    }

    @Override
    public boolean run(HandleContextBase handleContext) {
        log.info("开始校验数据是否存在 ->:{}", handleContext.getCheckId());
        Object o = service.getById((Long) handleContext.getParams());
        if (Objects.isNull(o)) {
            throw new ServiceException(ResultCode.DATA_NOT_EXIST, dataName);
        }
        return Boolean.TRUE;
    }


    /**
     * 直接放行 返回false代表当前校验直接通过
     *
     * @param handleContext the condition passed from invoker
     * @return
     */
    @Override
    public boolean accept(HandleContextBase handleContext) {
        //log.info("是否需要校验是否存在 ->:{}", handleContext.getParams());
        return true;
    }
}
