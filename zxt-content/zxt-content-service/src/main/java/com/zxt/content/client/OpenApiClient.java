package com.zxt.content.client;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

/**
 * @author zxt
 */
@Component
@Slf4j
public class OpenApiClient {

    private final RestTemplate httpTemplate;

    @Autowired
    public OpenApiClient(RestTemplate httpTemplate) {
        this.httpTemplate = httpTemplate;
    }


    /**
     * 获取一条网易云热门评论
     *
     * @return
     */
    public String getHotComments() {
        String url = "https://zj.v.api.aa1.cn/api/wenan-wy/?type=json";
        log.info("随机获取一条网易云热门评论 url:{}", url);
        try {
            String resp = httpTemplate.getForObject(url, String.class);
            log.info("随机获取一条网易云热门评论 返回数据：{}", resp);
            JSONObject jsonObject = JSON.parseObject(resp);
            return jsonObject.get("msg").toString();
        } catch (Exception e) {
            log.error("随机获取一条网易云热门评论 出错，异常信息：{}", e);
        }
        return null;
    }

}
