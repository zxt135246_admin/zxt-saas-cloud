package com.zxt.content.dao;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zxt.content.model.ImagesCollections;

/**
 * @author zxt
 */
public interface ImagesCollectionsMapper extends BaseMapper<ImagesCollections> {
}
