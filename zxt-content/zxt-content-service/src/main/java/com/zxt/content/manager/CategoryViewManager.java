package com.zxt.content.manager;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zxt.content.dao.CategoryViewMapper;
import com.zxt.content.dto.CategoryQueryDTO;
import com.zxt.content.model.CategoryView;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Slf4j
@Service
public class CategoryViewManager extends ServiceImpl<CategoryViewMapper, CategoryView> {

    public Page<CategoryView> queryCategory(CategoryQueryDTO dto) {
        Page<CategoryView> page = new Page<>();
        page.setCurrent(dto.getPageNo());
        page.setSize(dto.getPageSize());
        LambdaQueryWrapper<CategoryView> wrapper = CategoryView.gw();
        if (Objects.nonNull(dto.getCategoryId())) {
            wrapper.and((w) -> {
                w.eq(CategoryView::getFirstId, dto.getCategoryId())
                        .or().eq(CategoryView::getSecondId, dto.getCategoryId())
                        .or().eq(CategoryView::getThirdId, dto.getCategoryId());
            });
        }
        return page(page, wrapper);
    }
}
