package com.zxt.content.model;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 文案表
 *
 * @author zxt
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString(callSuper = true)
@TableName(value = "sc_private_content")
public class PrivateContent {
    public static final LambdaQueryWrapper<PrivateContent> gw() {
        return new LambdaQueryWrapper<>();
    }

    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;

    @TableField(value = "user_id")
    private Long userId;

    @TableField(value = "content")
    private String content;

    @TableField(value = "content_source")
    private String contentSource;

    @TableField(value = "content_type_id")
    private Long contentTypeId;

    @TableField(value = "is_publish")
    private Boolean isPublish;

    @TableField(value = "content_heat")
    private Integer contentHeat;

    @TableField(value = "can_push")
    private Boolean canPush;

    @TableField(value = "sale_integral")
    private BigDecimal saleIntegral;


    /**
     * 创建时间
     */
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    /**
     * 是否删除
     */
    @TableLogic
    @TableField(value = "is_deleted")
    private Boolean isDeleted;
}
