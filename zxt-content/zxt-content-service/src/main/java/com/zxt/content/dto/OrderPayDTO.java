package com.zxt.content.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author zxt
 */
@Data
@ApiModel("订单支付入参")
public class OrderPayDTO {

    @NotNull(message = "支付方式不能为空")
    @ApiModelProperty(value = "支付方式id", required = true)
    private Long payMethodId;

    @NotBlank(message = "订单号不能为空")
    @ApiModelProperty(value = "订单号", required = true)
    private String orderCode;
}
