package com.zxt.content.executor.context;

import com.zxt.bean.executor.context.HandleContextBase;
import lombok.Data;

@Data
public class HandleContext extends HandleContextBase {

    private Long id;

}
