package com.zxt.content.event.addContent;

import lombok.Getter;
import org.springframework.context.ApplicationEvent;

@Getter
public class AddContentEvent extends ApplicationEvent {

    private AddContentEventContent content;

    public AddContentEvent(Object o, AddContentEventContent content) {
        super(o);
        this.content = content;
    }
}
