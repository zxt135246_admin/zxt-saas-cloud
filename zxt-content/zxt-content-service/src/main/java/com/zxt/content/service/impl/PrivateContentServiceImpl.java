package com.zxt.content.service.impl;

import com.zxt.bean.executor.ThrowHandleExecutor;
import com.zxt.bean.executor.strategy.AbstractHandleStrategy;
import com.zxt.bean.exeption.ServiceException;
import com.zxt.bean.model.UserInfoVO;
import com.zxt.bean.utils.BeanUtils;
import com.zxt.bean.utils.event.SpringContextHolder;
import com.zxt.content.dto.ContentDTO;
import com.zxt.content.dto.PublishContentDTO;
import com.zxt.content.event.addContent.AddContentEvent;
import com.zxt.content.event.addContent.AddContentEventContent;
import com.zxt.content.executor.context.HandleContext;
import com.zxt.content.executor.factory.HandleStrategyFactory;
import com.zxt.content.manager.ContentTypeManager;
import com.zxt.content.manager.PrivateContentManager;
import com.zxt.content.model.ContentType;
import com.zxt.content.model.PrivateContent;
import com.zxt.content.service.PrivateContentService;
import com.zxt.content.vo.PrivateContentVO;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Slf4j
@Service
@AllArgsConstructor
public class PrivateContentServiceImpl implements PrivateContentService {

    private final PrivateContentManager privateContentManager;
    private final ContentTypeManager contentTypeManager;

    @Override
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT, rollbackFor = Exception.class)
    public Long add(ContentDTO dto, UserInfoVO user) {
        try {
            AddContentEventContent content = AddContentEventContent.builder().contentDTO(dto).userInfoVO(user).build();
            SpringContextHolder.publishEvent(new AddContentEvent(this, content));
            return content.getContentDTO().getId();
        } catch (Exception e) {
            //业务异常
            if (e instanceof ServiceException) {
                log.error("创建文案出现业务异常 异常信息:{}", e);
            }
            throw e;
        }
    }

    @Override
    public List<PrivateContentVO> getByUserId(Long userId) {
        return BeanUtils.beanCopy(privateContentManager.getByUserId(userId), PrivateContentVO.class);
    }

    @Override
    public void publishContent(PublishContentDTO dto) {

        if (dto.getIsPublish()) {
            HandleContext context = new HandleContext();
            context.setParams(dto.getId());
            AbstractHandleStrategy strategy = HandleStrategyFactory.publishContentStrategy(privateContentManager);
            new ThrowHandleExecutor(strategy).applyContext(context).execute();
        }

        PrivateContent newPrivateContent = new PrivateContent();
        newPrivateContent.setId(dto.getId());
        newPrivateContent.setIsPublish(dto.getIsPublish());
        privateContentManager.updateById(newPrivateContent);
    }

    @Override
    public PrivateContentVO randGetOneContentByUserId(Long userId) {
        PrivateContent privateContent = privateContentManager.randGetOneContentByUserId(userId);
        ContentType contentType = contentTypeManager.getById(privateContent.getContentTypeId());
        PrivateContentVO vo = BeanUtils.beanCopy(privateContent, PrivateContentVO.class);
        vo.setTypeName(contentType.getTypeName());
        return vo;
    }

    @Override
    public PrivateContentVO getById(Long contentId) {
        PrivateContent privateContent = privateContentManager.getById(contentId);
        return BeanUtils.beanCopy(privateContent, PrivateContentVO.class);
    }
}
