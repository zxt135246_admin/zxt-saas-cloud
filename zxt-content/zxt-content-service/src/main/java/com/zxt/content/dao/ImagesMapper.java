package com.zxt.content.dao;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zxt.content.model.Images;

public interface ImagesMapper extends BaseMapper<Images> {
}
