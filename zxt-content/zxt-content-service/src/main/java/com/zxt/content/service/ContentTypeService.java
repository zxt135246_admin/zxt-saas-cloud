package com.zxt.content.service;


import com.zxt.bean.query.QueryResultVO;
import com.zxt.content.dto.ContentTypeDTO;
import com.zxt.content.dto.ContentTypeQueryDTO;
import com.zxt.content.vo.ContentTypeVO;

import java.util.List;

/**
 * 文案类型业务管理
 *
 * @author zxt
 */
public interface ContentTypeService {


    /**
     * 新增文案类型
     *
     * @param dto
     */
    void add(ContentTypeDTO dto);

    /**
     * 分页查询
     *
     * @param dto
     * @return
     */
    QueryResultVO<ContentTypeVO> queryContentType(ContentTypeQueryDTO dto);
}
