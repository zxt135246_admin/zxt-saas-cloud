package com.zxt.content.event.addContent;

import com.zxt.bean.model.UserInfoVO;
import com.zxt.content.dto.ContentDTO;
import com.zxt.content.model.ContentType;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Builder
@Data
@EqualsAndHashCode(callSuper = false)
public class AddContentEventContent {

    /**
     * 文案入参
     */
    ContentDTO contentDTO;

    /**
     * 用户信息
     */
    UserInfoVO userInfoVO;

    /**
     * 文案类型
     */
    ContentType contentType;
}
