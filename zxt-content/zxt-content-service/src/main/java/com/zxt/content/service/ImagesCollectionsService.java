package com.zxt.content.service;

import com.zxt.content.dto.ImagesCollectionsDTO;
import com.zxt.content.vo.ImagesCollectionsVO;

import java.util.List;

/**
 * 相册合集业务管理
 *
 * @author zxt
 */
public interface ImagesCollectionsService {

    /**
     * 列表
     *
     * @return
     */
    List<ImagesCollectionsVO> list(ImagesCollectionsDTO dto);

    /**
     * 新增合集
     *
     * @param dto
     */
    void add(ImagesCollectionsDTO dto);

    /**
     * 查询合集
     *
     * @param id
     * @return
     */
    ImagesCollectionsVO getById(Long id);
}
