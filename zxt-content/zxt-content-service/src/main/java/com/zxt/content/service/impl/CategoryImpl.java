package com.zxt.content.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zxt.bean.enums.CommonStatusEnum;
import com.zxt.bean.exeption.ServiceException;
import com.zxt.bean.query.QueryResultVO;
import com.zxt.bean.result.ResultCode;
import com.zxt.bean.utils.BeanUtils;
import com.zxt.bean.utils.CollectionsUtil;
import com.zxt.bean.utils.StringUtils;
import com.zxt.content.dto.CategoryDTO;
import com.zxt.content.dto.CategoryQueryDTO;
import com.zxt.content.manager.CategoryManager;
import com.zxt.content.manager.CategoryViewManager;
import com.zxt.content.model.Category;
import com.zxt.content.model.CategoryView;
import com.zxt.content.service.CategoryService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @author zxt
 */
@Slf4j
@Service
@AllArgsConstructor
public class CategoryImpl implements CategoryService {

    // id路径分隔符
    public static final String FULL_ID_PATH_SEPARATOR = "-";

    private final CategoryManager categoryManager;
    private final CategoryViewManager categoryViewManager;

    @Override
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT, rollbackFor = Exception.class)
    public void createCategory(CategoryDTO dto) {
        Category parentCategory = categoryManager.getById(dto.getParentId());
        if (Objects.isNull(parentCategory)) {
            throw new ServiceException(ResultCode.DATA_NOT_EXIST, "上级类目");
        }

        Category existCategory = categoryManager.getByFullName(parentCategory.getFullNamePath() + FULL_ID_PATH_SEPARATOR + dto.getCategoryName());
        if (Objects.nonNull(existCategory)) {
            throw new ServiceException(ResultCode.DATA_EXIST, "类目名称");
        }

        Category newCategory = BeanUtils.beanCopy(dto, Category.class);
        newCategory.setCategoryStatus(CommonStatusEnum.ENABLE.getValue());
        newCategory.setCategoryLevel(parentCategory.getCategoryLevel() + 1);
        if (Objects.isNull(dto.getSortNumber())) {
            newCategory.setSortNumber(1);
        }

        if (newCategory.getCategoryLevel().equals(3) && newCategory.getFullNamePath().contains("根目录-文案")) {
            //文案的三级类目 默认有类目图片
            if (StringUtils.isBlank(dto.getCategoryPicture())) {
                newCategory.setCategoryPicture("https://copywritings.oss-accelerate.aliyuncs.com/cloud/content/operation/2023-12-29/891e0bcc850243aaa29dc5b9165a5ff7.png");
            }
        }

        if (newCategory.getCategoryLevel() > 3) {
            throw new ServiceException(ResultCode.COMMON_MESSAGE, "超过层级限制");
        }

        if (newCategory.getCategoryLevel().equals(3) && StringUtils.isEmpty(newCategory.getCategoryPicture())) {
            throw new ServiceException(ResultCode.COMMON_MESSAGE, "三级类目的图片不能为空");
        }

        newCategory.setFullNamePath(parentCategory.getFullNamePath() + FULL_ID_PATH_SEPARATOR + newCategory.getCategoryName());
        categoryManager.save(newCategory);
        newCategory.setFullIdPath(parentCategory.getFullIdPath() + FULL_ID_PATH_SEPARATOR + newCategory.getId());
        categoryManager.updateById(newCategory);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT, rollbackFor = Exception.class)
    public void updateCategory(CategoryDTO dto) {
        /*ReceptionCategory receptionCategory = ReceptionCategoryConvertor.toReceptionCategory(receptionCategoryDTO);

        ReceptionCategory parentReceptionCategory = receptionCategoryManager.getById(receptionCategory.getParentId());
        if (Objects.isNull(parentReceptionCategory)) {
            throw new ServiceException(ResultCode.DATA_NOT_EXIST, "上级类目");
        }

        ReceptionCategory existReceptionCategory = receptionCategoryManager.getByChannelIdAndFullNamePath(receptionCategoryDTO.getChannelId(), parentReceptionCategory.getFullNamePath() + FULL_ID_PATH_SEPARATOR + receptionCategoryDTO.getCategoryName());
        if (Objects.nonNull(existReceptionCategory) && !receptionCategoryDTO.getId().equals(existReceptionCategory.getId())) {
            throw new ServiceException(ResultCode.COMMON_MESSAGE, "类目名称已经存在");
        }

        ReceptionCategory oldReceptionCategory = receptionCategoryManager.getById(receptionCategoryDTO.getId());
        if (Objects.isNull(parentReceptionCategory)) {
            throw new ServiceException(ResultCode.DATA_NOT_EXIST, "类目");
        }

        receptionCategoryDTO.setCategoryLevel(parentReceptionCategory.getCategoryLevel() + 1);
        if (receptionCategoryDTO.getCategoryLevel() > 3) {
            throw new ServiceException(ResultCode.COMMON_MESSAGE, "超过层级限制");
        }

        //拿到子类目
        List<ReceptionCategory> childrens = receptionCategoryManager.getChildren(receptionCategoryDTO.getId(), receptionCategoryDTO.getChannelId());

        //校验层级是否改变
        if (oldReceptionCategory.getCategoryLevel().equals(receptionCategoryDTO.getCategoryLevel())) {
            //没有修改层级
        } else {
            //层级改变，校验改变后是否合理
            List<ReceptionCategory> sorted = null;
            if (CollectionsUtil.isNotEmpty(childrens)) {
                sorted = childrens.stream().sorted(Comparator.comparing(ReceptionCategory::getCategoryLevel).reversed()).collect(Collectors.toList());

                //拿到最深层的子级类目
                ReceptionCategory lastChildren = sorted.get(0);

                //移动后的最深子级如果大于三级，超过层级限制
                if ((lastChildren.getCategoryLevel() + receptionCategoryDTO.getCategoryLevel() - oldReceptionCategory.getCategoryLevel()) > 3) {
                    throw new ServiceException(ResultCode.COMMON_MESSAGE, "超过层级限制");
                }
            }
        }

        //修改当前修改的类目信息
        childrens.stream().filter((nowReceptionCategory) -> {
            return nowReceptionCategory.getId().equals(receptionCategoryDTO.getId());
        }).map((nowReceptionCategory) -> {
            nowReceptionCategory.setFullIdPath(parentReceptionCategory.getFullIdPath() + FULL_ID_PATH_SEPARATOR + receptionCategoryDTO.getId());
            nowReceptionCategory.setFullNamePath(parentReceptionCategory.getFullNamePath() + FULL_ID_PATH_SEPARATOR + receptionCategoryDTO.getCategoryName());
            nowReceptionCategory.setCategoryName(receptionCategoryDTO.getCategoryName());
            nowReceptionCategory.setParentId(receptionCategoryDTO.getParentId());
            nowReceptionCategory.setCategoryPicture(receptionCategoryDTO.getCategoryPicture());
            return nowReceptionCategory;
        }).collect(Collectors.toList());

        //递归设置子级类目的全路径id和全路径名称
        setFullIdAndFullName(parentReceptionCategory, childrens);

        receptionCategoryManager.updateBatchById(childrens);*/
    }

    @Override
    public QueryResultVO<CategoryView> queryCategory(CategoryQueryDTO dto) {
        Page<CategoryView> page = categoryViewManager.queryCategory(dto);
        QueryResultVO<CategoryView> queryResultVO = BeanUtils.pageToQueryResultVO(page, CategoryView.class);
        List<CategoryView> records = BeanUtils.beanCopy(page.getRecords(), CategoryView.class);

        if (CollectionsUtil.isEmpty(records)) {
            queryResultVO.setRecords(new ArrayList<>());
            return queryResultVO;
        }

        queryResultVO.setRecords(records);
        return queryResultVO;
    }
}
