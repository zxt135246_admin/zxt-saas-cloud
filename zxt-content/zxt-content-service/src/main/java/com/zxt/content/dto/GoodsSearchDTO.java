package com.zxt.content.dto;

import com.zxt.bean.query.QueryDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author zxt
 */
@Data
@ApiModel("商城商品搜索入参")
public class GoodsSearchDTO extends QueryDTO {

    @ApiModelProperty("spuId")
    private Long mallGoodsId;

    @ApiModelProperty("状态")
    private Byte commonStatus;
}
