package com.zxt.content.vo;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.zxt.bean.convertor.LongJsonDeserializer;
import com.zxt.bean.convertor.LongJsonSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@ApiModel("私有文案出参")
public class PrivateContentVO {

    @ApiModelProperty("id")
    @JsonSerialize(using = LongJsonSerializer.class)
    @JsonDeserialize(using = LongJsonDeserializer.class)
    private Long id;

    @ApiModelProperty("用户id")
    @JsonSerialize(using = LongJsonSerializer.class)
    @JsonDeserialize(using = LongJsonDeserializer.class)
    private Long userId;

    @ApiModelProperty("内容")
    private String content;

    @ApiModelProperty("来源")
    private String contentSource;

    @ApiModelProperty("文案类型id")
    @JsonSerialize(using = LongJsonSerializer.class)
    @JsonDeserialize(using = LongJsonDeserializer.class)
    private Long contentTypeId;

    @ApiModelProperty("是否已经发布")
    private Boolean isPublish;

    @ApiModelProperty("文案热度")
    private Integer contentHeat;

    @ApiModelProperty("创建时间")
    private LocalDateTime createTime;

    @ApiModelProperty("文案类型名称")
    private String typeName;
}
