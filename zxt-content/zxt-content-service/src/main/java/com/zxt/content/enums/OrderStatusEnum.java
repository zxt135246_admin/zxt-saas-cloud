package com.zxt.content.enums;

import java.util.Objects;

/**
 * 订单状态枚举
 */
public enum OrderStatusEnum {

    PENDING_PAYMENT((byte) 10, "待支付"),
    PAYING ((byte) 13, "支付中"),
    GENERATED_APPROVAL((byte) 15, "待审核"),
    PENDING_APPROVAL((byte) 20, "待审核"),
    PENDING_SYNC((byte) 30, "待同步"),
    PENDING_SHIPPMENT((byte) 40, "待发货"),
    LOCKED((byte) 45, "备货中"), // 锁定状态
    PARTIAL_SHIPMENT((byte) 48, "部分发货"),
    SHIPPED((byte) 50, "待收货"),
    SIGNED_FOR_RECEIPT((byte) 60, "已签收"),
    COMPLETED((byte) 80, "已完成"),
    REFUND((byte) 85, "退款中"),
    CLOSED((byte) 90, "已关闭"),
    ;
    private byte value;
    private String desc;

    OrderStatusEnum(byte value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public byte getValue() {
        return value;
    }

    public String getDesc() {
        return desc;
    }

    public static boolean exists(Byte status) {
        if (status == null) {
            return false;
        }
        byte s = status.byteValue();
        return exists(s);
    }

    public static boolean exists(byte s) {
        for (OrderStatusEnum element : OrderStatusEnum.values()) {
            if (element.value == s) {
                return true;
            }
        }
        return false;
    }

    public boolean equal(Byte val) {
        return val == null ? false : val.byteValue() == this.value;
    }

    public static String getDescByValue(Byte value) {
        if (value == null) {
            return "";
        }
        for (OrderStatusEnum element : OrderStatusEnum.values()) {
            if (element.value == value.byteValue()) {
                return element.desc;
            }
        }
        return "";
    }

    public static boolean canRefundStatus(Byte status) {
        if (Objects.isNull(status)) {
            return false;
        }
        return status.byteValue() != PENDING_PAYMENT.getValue() && status.byteValue() != CLOSED.getValue() && status.byteValue() != REFUND.getValue();
    }
}
