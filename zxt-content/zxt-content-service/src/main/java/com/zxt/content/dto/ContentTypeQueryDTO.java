package com.zxt.content.dto;

import com.zxt.bean.query.QueryDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author zxt
 */
@Data
@ApiModel("文案类型搜索入参")
public class ContentTypeQueryDTO extends QueryDTO {
    @ApiModelProperty("类型名称模糊")
    private String typeNameLike;
}
