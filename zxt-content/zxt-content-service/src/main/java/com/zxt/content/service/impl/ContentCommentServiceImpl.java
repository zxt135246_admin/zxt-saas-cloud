package com.zxt.content.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zxt.bean.exeption.ServiceException;
import com.zxt.bean.query.QueryResultVO;
import com.zxt.bean.result.ResultCode;
import com.zxt.bean.utils.BeanUtils;
import com.zxt.bean.utils.CollectionsUtil;
import com.zxt.bean.utils.LogUtils;
import com.zxt.bean.utils.TrueUtils;
import com.zxt.content.client.OperationClient;
import com.zxt.content.client.UserClient;
import com.zxt.content.dto.ContentCommentDTO;
import com.zxt.content.dto.ContentCommentQueryDTO;
import com.zxt.content.manager.ContentCommentManager;
import com.zxt.content.manager.PrivateContentManager;
import com.zxt.content.model.ContentComment;
import com.zxt.content.model.PrivateContent;
import com.zxt.content.service.ContentCommentService;
import com.zxt.content.vo.ContentCommentVO;
import com.zxt.operation.api.dto.SystemNewsApiDTO;
import com.zxt.operation.api.enums.NewsTypeEnum;
import com.zxt.user.api.vo.GradeApiVO;
import com.zxt.user.api.vo.UserApiVO;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

@Slf4j
@Service
@AllArgsConstructor
public class ContentCommentServiceImpl implements ContentCommentService {

    private final ContentCommentManager contentCommentManager;
    private final PrivateContentManager privateContentManager;
    private final UserClient userClient;
    private final OperationClient operationClient;

    @Override
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT, rollbackFor = Exception.class)
    public void add(ContentCommentDTO dto) {
        PrivateContent content = privateContentManager.getById(dto.getContentId());
        TrueUtils.isTrue(Objects.isNull(content)).throwMessage("文案不存在");

        //TODO 张啸天的文案最低评分三星起
        if (content.getUserId().equals(979390678624944128L) && dto.getScore() < 3) {
            throw new ServiceException(ResultCode.COMMON_MESSAGE, "张啸天写的文案没有差的哦，请重新选择星级呢~");
        }
        contentCommentManager.save(BeanUtils.beanCopy(dto, ContentComment.class));

        SystemNewsApiDTO apiDTO = new SystemNewsApiDTO();
        apiDTO.setUserId(content.getUserId());
        apiDTO.setTraceId(LogUtils.getTraceId());
        apiDTO.setIsRead(Boolean.FALSE);
        apiDTO.setDataId(dto.getContentId().toString());
        apiDTO.setNewsType(NewsTypeEnum.CONTENT.getValue());
        apiDTO.setContent(dto.getNickName() + " 评论了您的文案哦~（可点击查看）");
        operationClient.saveSystemNews(apiDTO);
    }
    /*高山落羽皆可藏起，不敢；
    愿山是山，愿海是海，原我。*/

    @Override
    public QueryResultVO<ContentCommentVO> queryContentComment(ContentCommentQueryDTO dto) {
        Page<ContentComment> page = contentCommentManager.queryContentComment(dto);
        QueryResultVO<ContentCommentVO> queryResultVO = BeanUtils.pageToQueryResultVO(page, ContentCommentVO.class);
        List<ContentCommentVO> records = BeanUtils.beanCopy(page.getRecords(), ContentCommentVO.class);

        if (CollectionsUtil.isEmpty(records)) {
            queryResultVO.setRecords(records);
            return queryResultVO;
        }

        List<Long> userIds = records.stream().map(ContentCommentVO::getUserId).distinct().collect(Collectors.toList());
        List<UserApiVO> userApiVOList = userClient.getByUserIds(userIds);
        Map<Long, UserApiVO> userApiVOMap = userApiVOList.stream().collect(Collectors.toMap(UserApiVO::getId, Function.identity()));

        for (ContentCommentVO vo : records) {

            //设置用户信息
            UserApiVO userApiVO = userApiVOMap.get(vo.getUserId());
            if (Objects.nonNull(userApiVO)) {
                //设置用户昵称
                vo.setNickName(userApiVO.getNickName());
                //设置头像地址
                vo.setAvatarUrl(userApiVO.getAvatarUrl());
            }
            GradeApiVO gradeApiVO = userClient.getGradeByValueNum(userApiVO.getGrowthValue());
            if (Objects.nonNull(gradeApiVO)) {
                //设置会员等级描述
                vo.setLevelDesc(gradeApiVO.getGradeName());
            } else {
                vo.setLevelDesc("未知");
            }

            if (vo.getScore().equals(1)) {
                vo.setScoreDesc("烂泥文案");
            } else if (vo.getScore().equals(2)) {
                vo.setScoreDesc("能看文案");
            } else if (vo.getScore().equals(3)) {
                vo.setScoreDesc("尚可文案");
            } else if (vo.getScore().equals(4)) {
                vo.setScoreDesc("奈斯文案");
            } else if (vo.getScore().equals(5)) {
                vo.setScoreDesc("绝代文案");
            }
        }

        queryResultVO.setRecords(records);
        return queryResultVO;
    }
}
