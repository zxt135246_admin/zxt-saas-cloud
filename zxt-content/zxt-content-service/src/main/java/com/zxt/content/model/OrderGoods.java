package com.zxt.content.model;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString(callSuper = true)
@TableName(value = "oc_order_goods")
public class OrderGoods {
    public static final LambdaQueryWrapper<OrderGoods> gw() {
        return new LambdaQueryWrapper<>();
    }

    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;

    @TableField(value = "user_id")
    private Long userId;

    @TableField(value = "mall_goods_sku_id")
    private Long mallGoodsSkuId;

    @Deprecated
    @TableField(value = "data_id")
    private String dataId;

    @TableField(value = "order_code")
    private String orderCode;

    @TableField(value = "goods_name")
    private String goodsName;

    @TableField(value = "goods_price")
    private BigDecimal goodsPrice;

    @TableField(value = "goods_picture")
    private String goodsPicture;

    @TableField(value = "goods_number")
    private Integer goodsNumber;

    @TableField(value = "total_price")
    private BigDecimal totalPrice;

    @TableField(value = "grade_discount_price")
    private BigDecimal gradeDiscountPrice;

    @TableField(value = "pay_price")
    private BigDecimal payPrice;

    @TableField(value = "common_status")
    private Byte commonStatus;

    @TableField(value = "sku_code")
    private String skuCode;

    @TableField(value = "goods_type")
    private Byte goodsType;

    /**
     * 创建时间
     */
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    /**
     * 是否删除
     */
    @TableLogic
    @TableField(value = "is_deleted")
    private Boolean isDeleted;
}
