package com.zxt.content.model;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString(callSuper = true)
@TableName(value = "sc_category_view")
public class CategoryView {
    public static final LambdaQueryWrapper<CategoryView> gw() {
        return new LambdaQueryWrapper<>();
    }

    private Long firstId;

    private String firstName;

    private Byte firstStatus;

    private Long secondId;

    private String secondName;

    private Byte secondStatus;

    private Long thirdId;

    private String thirdName;

    private Byte thirdStatus;

    private String thirdPicture;

}
