package com.zxt.content.vo;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.zxt.bean.convertor.LongJsonDeserializer;
import com.zxt.bean.convertor.LongJsonSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author zxt
 */
@Data
@ApiModel("文案评论出参")
public class ContentCommentVO {

    @ApiModelProperty("id")
    @JsonSerialize(using = LongJsonSerializer.class)
    @JsonDeserialize(using = LongJsonDeserializer.class)
    private Long id;

    @ApiModelProperty("用户id")
    @JsonSerialize(using = LongJsonSerializer.class)
    @JsonDeserialize(using = LongJsonDeserializer.class)
    private Long userId;

    @ApiModelProperty("评论内容")
    private String commentContent;

    @ApiModelProperty("评分")
    private Integer score;

    @ApiModelProperty("评分描述")
    private String scoreDesc;

    @ApiModelProperty("创建时间")
    private LocalDateTime createTime;


    //用户信息

    @ApiModelProperty("头像")
    private String avatarUrl;

    @ApiModelProperty("用户昵称")
    private String nickName;

    @ApiModelProperty("会员等级描述")
    private String levelDesc;

}
