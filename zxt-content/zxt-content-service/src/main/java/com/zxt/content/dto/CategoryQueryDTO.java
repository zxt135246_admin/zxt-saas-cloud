package com.zxt.content.dto;

import com.zxt.bean.query.QueryDTO;
import io.swagger.annotations.ApiModel;
import lombok.Data;

/**
 * @author zxt
 */
@Data
@ApiModel("类目搜索入参")
public class CategoryQueryDTO extends QueryDTO {

    private Long categoryId;

}
