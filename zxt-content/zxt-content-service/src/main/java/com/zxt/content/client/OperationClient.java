package com.zxt.content.client;

import com.alibaba.fastjson.JSON;
import com.zxt.bean.result.Result;
import com.zxt.operation.api.dto.ImagesApiDTO;
import com.zxt.operation.api.dto.SendEmailApiDTO;
import com.zxt.operation.api.dto.SystemNewsApiDTO;
import com.zxt.operation.api.feignClient.MessageFeignClient;
import com.zxt.operation.api.feignClient.OssFeignClient;
import com.zxt.operation.api.feignClient.SystemNewsFeignClient;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Component
@Slf4j
@AllArgsConstructor
public class OperationClient {


    private final OssFeignClient ossFeignClient;
    private final SystemNewsFeignClient systemNewsFeignClient;
    private final MessageFeignClient messageFeignClient;

    public Boolean uploadImage(MultipartFile multiFile, String directory, Long imageId) {
        log.info("文案中心-运营中心 上传图片");
        try {
            Result<Boolean> result = ossFeignClient.uploadImage(multiFile, directory, imageId);
            log.info("文案中心-运营中心 上传图片，出参:{}", JSON.toJSONString(result));
            return result.getData();
        } catch (Exception e) {
            log.error("文案中心-运营中心 上传图片,异常信息:{}", e);
        }
        return null;
    }

    public void uploadImageByUrl(String url, String directory, Long imageId) {
        log.info("文案中心-运营中心 根据url上传图片，入参:{}", url);
        try {
            Result result = ossFeignClient.uploadImageByUrl(url, directory, imageId);
            log.info("文案中心-运营中心 根据url上传图片，出参:{}", JSON.toJSONString(result));
        } catch (Exception e) {
            log.error("文案中心-运营中心 根据url上传图片,异常信息:{}", e);
        }
    }


    /**
     * 保存系统消息
     *
     * @param apiDTO
     */
    public void saveSystemNews(SystemNewsApiDTO apiDTO) {
        log.info("文案中心-运营中心 保存系统消息 入参:{}", JSON.toJSONString(apiDTO));
        Result result = systemNewsFeignClient.saveSystemNews(apiDTO);
        log.info("文案中心-运营中心 保存系统消息，出参:{}", JSON.toJSONString(result));
    }

    /**
     * 批量保存系统消息
     *
     * @param dtoList
     */
    public void saveBatchSystemNews(List<SystemNewsApiDTO> dtoList) {
        log.info("文案中心-运营中心 批量保存系统消息，入参:{}", JSON.toJSONString(dtoList));
        Result result = systemNewsFeignClient.saveBatchSystemNews(dtoList);
        log.info("文案中心-运营中心 批量保存系统消息，出参:{}", JSON.toJSONString(result));
    }

    public void initImagesExif(List<ImagesApiDTO> imagesList) {
        log.info("文案中心-运营中心 初始化相册exif，入参:{}", imagesList);
        try {
            Result result = ossFeignClient.initImagesExif(imagesList);
            log.info("文案中心-运营中心 初始化相册exif，出参:{}", JSON.toJSONString(result));
        } catch (Exception e) {
            log.error("文案中心-运营中心 初始化相册exif,异常信息:{}", e);
        }
    }

    public void sendEmail(SendEmailApiDTO dto) {
        log.info("文案中心-运营中心 发送邮件通知，入参:{}", dto);
        Result result = messageFeignClient.sendEmail(dto);
        log.info("文案中心-运营中心 发送邮件通知，出参:{}", JSON.toJSONString(result));
    }

}
