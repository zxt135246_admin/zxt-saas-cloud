package com.zxt.content.event.addOrder;

import lombok.Getter;
import org.springframework.context.ApplicationEvent;

@Getter
public class CreateOrderEvent extends ApplicationEvent {

    private CreateOrderEventContent content;

    public CreateOrderEvent(Object o, CreateOrderEventContent content) {
        super(o);
        this.content = content;
    }
}
