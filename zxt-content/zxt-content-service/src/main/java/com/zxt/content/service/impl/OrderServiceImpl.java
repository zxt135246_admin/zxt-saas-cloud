package com.zxt.content.service.impl;

import com.alibaba.fastjson.JSON;
import com.zxt.bean.enums.CommonStatusEnum;
import com.zxt.bean.exeption.ServiceException;
import com.zxt.bean.model.UserInfoVO;
import com.zxt.bean.result.ResultCode;
import com.zxt.bean.utils.BeanUtils;
import com.zxt.bean.utils.CollectionsUtil;
import com.zxt.bean.utils.event.SpringContextHolder;
import com.zxt.content.client.UserClient;
import com.zxt.content.constants.RedisKeyContent;
import com.zxt.content.dto.OrderAddDTO;
import com.zxt.content.dto.OrderDTO;
import com.zxt.content.dto.OrderGoodsAddDTO;
import com.zxt.content.dto.OrderPayDTO;
import com.zxt.content.enums.OrderSourceEnum;
import com.zxt.content.enums.OrderStatusEnum;
import com.zxt.content.event.addOrder.CreateOrderEvent;
import com.zxt.content.event.addOrder.CreateOrderEventContent;
import com.zxt.content.manager.*;
import com.zxt.content.model.Cart;
import com.zxt.content.model.MallGoodsSku;
import com.zxt.content.model.Order;
import com.zxt.content.model.OrderGoods;
import com.zxt.content.rules.bean.OrderGoodsInputBean;
import com.zxt.content.rules.model.OrderDiscount;
import com.zxt.content.rules.model.OrderRequest;
import com.zxt.content.rules.service.OrderDiscountService;
import com.zxt.content.service.OrderService;
import com.zxt.content.vo.OrderGoodsVO;
import com.zxt.content.vo.OrderVO;
import com.zxt.redis.utils.RedisUtil;
import com.zxt.redis.utils.UserUtil;
import com.zxt.user.api.enums.OperateTypeEnum;
import com.zxt.user.api.req.OperateIntegralApiDTO;
import com.zxt.user.api.vo.GradeApiVO;
import com.zxt.user.api.vo.PayMethodApiVO;
import com.zxt.user.api.vo.UserIntegralApiVO;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.stream.Collectors;

@Slf4j
@Service
@AllArgsConstructor
public class OrderServiceImpl implements OrderService {

    private final OrderManager orderManager;
    private final OrderGoodsManager orderGoodsManager;
    private final PrivateContentManager privateContentManager;
    private final MallGoodsSkuManager mallGoodsSkuManager;
    private final OrderDiscountService orderDiscountService;
    private final CartManager cartManager;
    private final UserClient userClient;


    @Override
    public OrderAddDTO orderInit(UserInfoVO user) {
        List<Cart> cartList = cartManager.getByUserId(user.getId());
        List<OrderGoodsAddDTO> orderGoodsAddDTOList = new ArrayList<>();
        for (Cart cart : cartList) {
            OrderGoodsAddDTO orderGoodsAddDTO = new OrderGoodsAddDTO();
            orderGoodsAddDTO.setMallGoodsSkuId(cart.getMallGoodsSkuId());
            orderGoodsAddDTO.setGoodsNumber(cart.getGoodsNumber());
            orderGoodsAddDTOList.add(orderGoodsAddDTO);
        }
        //清空购物车
        cartManager.removeByUserId(user.getId());

        OrderAddDTO orderAddDTO = initOrder(orderGoodsAddDTOList, user, Boolean.TRUE);
        String orderKey = RedisKeyContent.getInitOrderDTOKey(UserUtil.getToken());
        RedisUtil.set(orderKey, JSON.toJSONString(orderAddDTO), TimeUnit.HOURS, 4);
        return orderAddDTO;
    }


    @Override
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT, rollbackFor = Exception.class)
    public String createOrder(OrderAddDTO orderAddDTO, UserInfoVO user) {
        CreateOrderEventContent orderEventContent = CreateOrderEventContent.builder().orderAddDTO(orderAddDTO).userInfoVO(user).build();
        SpringContextHolder.publishEvent(new CreateOrderEvent(this, orderEventContent));
        return orderEventContent.getOrderCode();
    }

    @Override
    public void orderPayCheck(OrderPayDTO dto, UserInfoVO user) {
        PayMethodApiVO payMethodApiVO = userClient.getPayMethodById(dto.getPayMethodId());
        if (Objects.isNull(payMethodApiVO) || !CommonStatusEnum.ENABLE.equal(payMethodApiVO.getCommonStatus())) {
            throw new ServiceException(ResultCode.COMMON_MESSAGE, "支付方式未启用");
        }

        if (!payMethodApiVO.getId().equals(1L)) {
            throw new ServiceException(ResultCode.COMMON_MESSAGE, "只允许积分支付");
        }

        Order order = orderManager.getByOrderCode(dto.getOrderCode());
        UserIntegralApiVO userIntegralApiVO = userClient.getUserIntegral(user.getId());
        if (userIntegralApiVO.getAvailableIntegral().compareTo(order.getPayPrice()) < 0) {
            throw new ServiceException(ResultCode.COMMON_MESSAGE, "余额不足");
        }
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT, rollbackFor = Exception.class)
    public void orderPay(OrderPayDTO dto, UserInfoVO user) {
        PayMethodApiVO payMethodApiVO = userClient.getPayMethodById(dto.getPayMethodId());
        if (Objects.isNull(payMethodApiVO) || !CommonStatusEnum.ENABLE.equal(payMethodApiVO.getCommonStatus())) {
            throw new ServiceException(ResultCode.COMMON_MESSAGE, "支付方式未启用");
        }

        if (!payMethodApiVO.getId().equals(1L)) {
            throw new ServiceException(ResultCode.COMMON_MESSAGE, "只允许积分支付");
        }

        Order order = orderManager.getByOrderCode(dto.getOrderCode());
        UserIntegralApiVO userIntegralApiVO = userClient.getUserIntegral(user.getId());
        if (userIntegralApiVO.getAvailableIntegral().compareTo(order.getPayPrice()) < 0) {
            throw new ServiceException(ResultCode.COMMON_MESSAGE, "余额不足");
        }

        orderManager.updatePaySuccess(dto.getOrderCode());
        orderManager.updateStatus(dto.getOrderCode(), OrderStatusEnum.PENDING_SHIPPMENT.getValue(), OrderStatusEnum.PENDING_PAYMENT.getValue());
        orderGoodsManager.updateStatus(dto.getOrderCode(), OrderStatusEnum.PENDING_SHIPPMENT.getValue(), OrderStatusEnum.PENDING_PAYMENT.getValue());

        OperateIntegralApiDTO integralApiDTO = new OperateIntegralApiDTO();
        integralApiDTO.setOperateType(OperateTypeEnum.DEDUCTION.getValue());
        integralApiDTO.setIntegral(order.getPayPrice());
        integralApiDTO.setOrderCode(dto.getOrderCode());
        integralApiDTO.setUserId(user.getId());
        integralApiDTO.setOperationReason("下单扣减积分");
        userClient.operateIntegral(integralApiDTO);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT, rollbackFor = Exception.class)
    public boolean updateStatus(String orderCode, Byte orderStatus, Byte whereOrderStatus) {
        return orderManager.updateStatus(orderCode, orderStatus, whereOrderStatus);
    }

    @Override
    public OrderVO getOrderDetail(String orderCode) {
        Order order = orderManager.getByOrderCode(orderCode);
        OrderVO orderVO = BeanUtils.beanCopy(order, OrderVO.class);
        orderVO.setOrderStatusDesc(OrderStatusEnum.getDescByValue(orderVO.getOrderStatus()));
        List<OrderGoods> orderGoodsList = orderGoodsManager.getByOrderCode(orderCode);
        List<Long> mallGoodsSkuIds = orderGoodsList.stream().map(OrderGoods::getMallGoodsSkuId).collect(Collectors.toList());
        List<MallGoodsSku> mallGoodsSkuList = mallGoodsSkuManager.getByIds(mallGoodsSkuIds);
        Map<Long, MallGoodsSku> mallGoodsSkuMap = mallGoodsSkuList.stream().collect(Collectors.toMap(MallGoodsSku::getId, Function.identity()));

        List<OrderGoodsVO> orderGoodsVOS = BeanUtils.beanCopy(orderGoodsList, OrderGoodsVO.class);

        for (OrderGoodsVO orderGoodsVO : orderGoodsVOS) {
            MallGoodsSku mallGoodsSku = mallGoodsSkuMap.get(orderGoodsVO.getMallGoodsSkuId());
            if (Objects.nonNull(mallGoodsSku)) {
                orderGoodsVO.setMallGoodsId(mallGoodsSku.getMallGoodsId());
            }
        }
        orderVO.setOrderGoodsList(orderGoodsVOS);
        return orderVO;
    }

    @Override
    public Map<String, Integer> quantityQuery(Long userId) {
        Map<String, Integer> map = new HashMap<>();

        //查询所有未删除的订单数量
        List<Order> allOrderList = orderManager.getByUserId(userId);
        map.put("allNum", allOrderList.size());

        //待支付
        List<Order> waitPayOrderList = allOrderList.stream().filter(order -> OrderStatusEnum.PENDING_PAYMENT.equal(order.getOrderStatus())).collect(Collectors.toList());
        map.put("waitPayNum", waitPayOrderList.size());

        //待发货
        List<Byte> waitSendOrderStatus = CollectionsUtil.asList(OrderStatusEnum.PENDING_APPROVAL.getValue(), OrderStatusEnum.PENDING_SYNC.getValue(), OrderStatusEnum.PENDING_SHIPPMENT.getValue());
        List<Order> waitSendOrderList = allOrderList.stream().filter(order -> waitSendOrderStatus.contains(order.getOrderStatus())).collect(Collectors.toList());
        map.put("waitSendNum", waitSendOrderList.size());

        // 待收货
        List<Byte> waitReceiviOrderStatus = CollectionsUtil.asList(OrderStatusEnum.LOCKED.getValue(), OrderStatusEnum.SHIPPED.getValue());
        List<Order> waitReceiviOrderList = allOrderList.stream().filter(order -> waitReceiviOrderStatus.contains(order.getOrderStatus())).collect(Collectors.toList());
        map.put("waitReceiviNum", waitReceiviOrderList.size());

        // 已完成
        List<Order> completedOrderList = allOrderList.stream().filter(order -> OrderStatusEnum.COMPLETED.equal(order.getOrderStatus())).collect(Collectors.toList());
        map.put("completedNum", completedOrderList.size());

        //Integer returningCount = returnOrderManager.getReturningCount(memberId);
        // 退款/售后
        map.put("returnNum", 0);
        return map;
    }

    @Override
    public List<OrderVO> getUserOrder(OrderDTO dto) {
        List<Order> orderList = orderManager.getOrderList(dto);

        List<String> orderCodes = orderList.stream().map(Order::getOrderCode).collect(Collectors.toList());
        List<OrderGoods> allOrderGoodsList = orderGoodsManager.getByOrderCodes(orderCodes);
        Map<String, List<OrderGoods>> orderGoodsGroup = allOrderGoodsList.stream().collect(Collectors.groupingBy(OrderGoods::getOrderCode));

        List<OrderVO> orderVOList = BeanUtils.beanCopy(orderList, OrderVO.class);
        for (OrderVO orderVO : orderVOList) {
            List<OrderGoods> orderGoodsList = orderGoodsGroup.get(orderVO.getOrderCode());
            orderVO.setOrderStatusDesc(OrderStatusEnum.getDescByValue(orderVO.getOrderStatus()));
            orderVO.setOrderGoodsList(BeanUtils.beanCopy(orderGoodsList, OrderGoodsVO.class));
        }
        return orderVOList;
    }

    private OrderAddDTO initOrder(List<OrderGoodsAddDTO> orderGoodsList, UserInfoVO user, Boolean isUseGradeDiscount) {

        List<Long> mallGoodsSkuIds = orderGoodsList.stream().map(OrderGoodsAddDTO::getMallGoodsSkuId).collect(Collectors.toList());

        List<MallGoodsSku> mallGoodsSkuList = mallGoodsSkuManager.listByIds(mallGoodsSkuIds);
        Map<Long, MallGoodsSku> mallGoodsSkuMap = mallGoodsSkuList.stream().collect(Collectors.toMap(MallGoodsSku::getId, Function.identity()));
        List<Long> existMallGoodsSkuIds = mallGoodsSkuList.stream().map(MallGoodsSku::getId).collect(Collectors.toList());

        if (!existMallGoodsSkuIds.containsAll(mallGoodsSkuIds)) {
            throw new ServiceException(ResultCode.COMMON_MESSAGE, "商品已下架，请重新选择商品");
        }

        BigDecimal orderTotalPrice = BigDecimal.ZERO;

        Set<Long> existSet = new HashSet<>();
        for (OrderGoodsAddDTO orderGoodsAddDTO : orderGoodsList) {
            MallGoodsSku mallGoodsSku = mallGoodsSkuMap.get(orderGoodsAddDTO.getMallGoodsSkuId());
            if (Objects.isNull(mallGoodsSku)) {
                throw new ServiceException(ResultCode.COMMON_MESSAGE, "商品已下架，请重新选择商品");
            }

            if (!CommonStatusEnum.ENABLE.equal(mallGoodsSku.getCommonStatus())) {
                throw new ServiceException(ResultCode.COMMON_MESSAGE, "商品:" + mallGoodsSku.getGoodsName() + " 已下架，请重新选择商品");
            }

            boolean isTrue = existSet.add(orderGoodsAddDTO.getMallGoodsSkuId());
            if (!isTrue) {
                throw new ServiceException("商品：" + mallGoodsSku.getGoodsName() + " 重复");
            }

            if (Objects.isNull(mallGoodsSku.getSalePrice()) || mallGoodsSku.getSalePrice().compareTo(BigDecimal.ZERO) <= 0) {
                throw new ServiceException("商品：" + mallGoodsSku.getGoodsName() + " 未设置价格，无法购买");
            }

            if (Objects.isNull(orderGoodsAddDTO.getGoodsNumber())) {
                throw new ServiceException("商品：" + mallGoodsSku.getGoodsName() + " 购买数量为空");
            }

            if (orderGoodsAddDTO.getGoodsNumber() < 1) {
                throw new ServiceException("请正确填写购买数量");
            }

            //反显名称和图片
            orderGoodsAddDTO.setGoodsName(mallGoodsSku.getGoodsName());
            String[] split = mallGoodsSku.getGoodsPicture().split(",");
            orderGoodsAddDTO.setGoodsPicture(split[0]);
            //创建订单时，校验价格使用
            orderGoodsAddDTO.setSalePrice(mallGoodsSku.getSalePrice());

            BigDecimal totalGoodsIntegral = mallGoodsSku.getSalePrice().multiply(BigDecimal.valueOf(orderGoodsAddDTO.getGoodsNumber()));

            orderTotalPrice = orderTotalPrice.add(totalGoodsIntegral);
        }

        GradeApiVO gradeApiVO = userClient.getGradeByValueNum(user.getGrowthValue());

        OrderRequest orderRequest = new OrderRequest();
        orderRequest.setOrderGoodsList(BeanUtils.beanCopy(orderGoodsList, OrderGoodsInputBean.class));
        orderRequest.setIsUseGradeDiscount(isUseGradeDiscount);
        orderRequest.setGradeDiscountRatio(gradeApiVO.getDiscountRatio());
        OrderDiscount orderDiscount = orderDiscountService.calcDiscount(orderRequest);

        OrderAddDTO orderAddDTO = new OrderAddDTO();
        orderAddDTO.setOrderSource(OrderSourceEnum.BUSINESS.getValue());
        orderAddDTO.setOrderGoodsList(orderGoodsList);
        orderAddDTO.setOrderTotalPrice(orderTotalPrice);
        orderAddDTO.setDiscountPrice(orderDiscount.getOrderGradeDiscount());
        orderAddDTO.setGradeApiVO(gradeApiVO);
        orderAddDTO.setIsUseGradeDiscount(isUseGradeDiscount);
        return orderAddDTO;
    }
}
