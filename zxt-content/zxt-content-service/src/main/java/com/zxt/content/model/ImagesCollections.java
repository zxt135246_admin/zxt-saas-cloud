package com.zxt.content.model;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.time.LocalDateTime;

/**
 * @author zxt
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString(callSuper = true)
@TableName(value = "sc_images_collections")
public class ImagesCollections {
    public static final LambdaQueryWrapper<ImagesCollections> gw() {
        return new LambdaQueryWrapper<>();
    }

    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;

    @TableField(value = "user_id")
    private Long userId;

    @TableField(value = "parent_id")
    private Long parentId;

    /**
     * 合集名称
     */
    @TableField(value = "collections_name")
    private String collectionsName;

    /**
     * 图标地址
     */
    @TableField(value = "icon_url")
    private String icon_url;

    /**
     * 共享用户id json
     */
    @TableField(value = "share_user_json")
    private String shareUserJson;

    /**
     * 创建时间
     */
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    /**
     * 是否删除
     */
    @TableLogic
    @TableField(value = "is_deleted")
    private Boolean isDeleted;
}
