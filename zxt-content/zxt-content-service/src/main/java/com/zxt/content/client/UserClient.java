package com.zxt.content.client;

import com.alibaba.fastjson.JSON;
import com.zxt.bean.exeption.ServiceException;
import com.zxt.bean.result.Result;
import com.zxt.bean.result.ResultCode;
import com.zxt.user.api.feignClient.UserFeignClient;
import com.zxt.user.api.req.OperateIntegralApiDTO;
import com.zxt.user.api.vo.*;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;

@Component
@Slf4j
@AllArgsConstructor
public class UserClient {


    private final UserFeignClient userFeignClient;

    public UserApiVO getUserById(Long userId) {
        log.info("文案中心-用户中心 获取用户信息，入参:{}", JSON.toJSONString(userId));
        try {
            Result<UserApiVO> result = userFeignClient.getUserById(userId);
            log.info("文案中心-用户中心 获取用户信息，出参:{}", JSON.toJSONString(result));
            return result.getData();
        } catch (Exception e) {
            log.error("文案中心-用户中心 获取用户信息,异常信息:{}", e);
        }
        return null;
    }

    /**
     * 查询用户列表
     *
     * @param userIds
     * @return
     */
    public List<UserApiVO> getByUserIds(List<Long> userIds) {
        log.info("文案中心-用户中心 获取用户列表，入参:{}", JSON.toJSONString(userIds));
        try {
            Result<List<UserApiVO>> result = userFeignClient.getByUserIds(userIds);
            log.info("文案中心-用户中心 获取用户列表，出参:{}", JSON.toJSONString(result));
            return result.getData();
        } catch (Exception e) {
            log.error("文案中心-用户中心 获取用户列表,异常信息:{}", e);
        }
        return null;
    }

    /**
     * 获取用户的粉丝列表
     *
     * @param concernUserId
     * @return
     */
    public List<UserConcernApiVO> getUserConcernByConcernUserId(Long concernUserId) {
        log.info("文案中心-用户中心 获取用户的粉丝列表，入参:{}", JSON.toJSONString(concernUserId));
        try {
            Result<List<UserConcernApiVO>> result = userFeignClient.getUserConcernByConcernUserId(concernUserId);
            log.info("文案中心-用户中心 获取用户的粉丝列表，出参:{}", JSON.toJSONString(result));
            return result.getData();
        } catch (Exception e) {
            log.error("文案中心-用户中心 获取用户的粉丝列表,异常信息:{}", e);
        }
        return null;
    }

    /**
     * 根据用户id和关注用户id获取
     *
     * @param userId
     * @param concernUserId
     * @return
     */
    public UserConcernApiVO getByUserIdAndConcernUserId(Long userId, Long concernUserId) {
        log.info("文案中心-用户中心 根据用户id和关注用户id获取，入参:{}", JSON.toJSONString(concernUserId));
        try {
            Result<UserConcernApiVO> result = userFeignClient.getByUserIdAndConcernUserId(userId, concernUserId);
            log.info("文案中心-用户中心 根据用户id和关注用户id获取，出参:{}", JSON.toJSONString(result));
            return result.getData();
        } catch (Exception e) {
            log.error("文案中心-用户中心 根据用户id和关注用户id获取,异常信息:{}", e);
        }
        return null;
    }

    /**
     * 获取当前成长值所属的等级
     *
     * @param growthValue
     * @return
     */
    public GradeApiVO getGradeByValueNum(Integer growthValue) {
        log.info("文案中心-用户中心 获取当前成长值所属的等级，入参:{}", JSON.toJSONString(growthValue));
        try {
            Result<GradeApiVO> result = userFeignClient.getGradeByValueNum(growthValue);
            log.info("文案中心-用户中心 获取当前成长值所属的等级，出参:{}", JSON.toJSONString(result));
            return result.getData();
        } catch (Exception e) {
            log.error("文案中心-用户中心 获取当前成长值所属的等级,异常信息:{}", e);
        }
        return null;
    }


    public UserIntegralApiVO getUserIntegral(Long userId) {
        log.info("文案中心-用户中心 获取用户积分，入参:{}", userId);
        try {
            Result<UserIntegralApiVO> result = userFeignClient.getUserIntegral(userId);
            log.info("文案中心-用户中心 获取用户积分，出参:{}", JSON.toJSONString(result));
            return result.getData();
        } catch (Exception e) {
            log.error("文案中心-用户中心 获取用户积分,异常信息:{}", e);
        }
        return null;
    }

    public void operateIntegral(OperateIntegralApiDTO dto) {
        log.info("订单中心-用户中心 操作积分 入参:{}", JSON.toJSONString(dto));
        Result<Boolean> result = userFeignClient.operateIntegral(dto);
        if (Objects.isNull(result) || !result.getSuccess() || !result.getData()) {
            log.error("订单中心-用户中心 操作积分失败，出参：{}", JSON.toJSONString(result));
            throw new ServiceException(ResultCode.COMMON_MESSAGE, "操作积分失败！");
        }
        log.info("订单中心-用户中心 操作积分 结束");
    }


    public PayMethodApiVO getPayMethodById(Long payMethodId) {
        log.info("文案中心-用户中心 获取支付方式信息，入参:{}", payMethodId);
        try {
            Result<PayMethodApiVO> result = userFeignClient.getPayMethodById(payMethodId);
            log.info("文案中心-用户中心 获取支付方式信息，出参:{}", JSON.toJSONString(result));
            return result.getData();
        } catch (Exception e) {
            log.error("文案中心-用户中心 获取支付方式信息,异常信息:{}", e);
        }
        return null;
    }
}
