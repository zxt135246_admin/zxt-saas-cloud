package com.zxt.content.service;

import com.zxt.content.vo.CartVO;

import java.util.List;

/**
 * 购物车类业务管理
 *
 * @author zxt
 */
public interface CartService {


    /**
     * 加入购物车
     *
     * @param mallGoodsSkuId
     * @param id
     */
    void addCart(Long mallGoodsSkuId, Long id);

    /**
     * 查询购物车
     *
     * @param id
     */
    List<CartVO> getByUserId(Long id);

    /**
     * 删除购物车
     *
     * @param id
     */
    void remove(Long id);

    /**
     * 清空购物车
     *
     * @param id
     */
    void clear(Long id);
}
