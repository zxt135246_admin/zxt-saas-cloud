package com.zxt.content.enums;

/**
 * 订单来源枚举
 */
public enum GoodsTypeEnum {

    REAL((byte) 1, "实物"),
    CONTENT((byte) 2, "文案"),
    ;

    private byte value;
    private String desc;

    GoodsTypeEnum(byte value, String desc){
        this.value = value;
        this.desc = desc;
    }

    public byte getValue() {
        return value;
    }

    public String getDesc() {
        return desc;
    }

    public static boolean exists(Byte status) {
        if (status == null) {
            return false;
        }
        byte s = status.byteValue();
        return exists(s);
    }

    public static boolean exists(byte s) {
        for (GoodsTypeEnum element : GoodsTypeEnum.values()) {
            if (element.value == s) {
                return true;
            }
        }
        return false;
    }

    public boolean equal(Byte val) {
        return val == null ? false : val.byteValue() == this.value;
    }

    public static String getDescByValue(Byte value) {
        if (value == null) {
            return "";
        }
        for (GoodsTypeEnum element : GoodsTypeEnum.values()) {
            if (element.value == value.byteValue()) {
                return element.desc;
            }
        }
        return "";
    }
}
