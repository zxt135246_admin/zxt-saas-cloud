package com.zxt.content.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zxt.content.dto.GoodsSearchDTO;
import com.zxt.content.model.MallGoods;
import com.zxt.content.model.MallGoodsSku;
import org.apache.ibatis.annotations.Param;

public interface MallGoodsSkuMapper extends BaseMapper<MallGoodsSku> {

    /**
     * 查询skuCode最大值
     *
     * @return
     */
    MallGoodsSku queryMaxSkuCode();

    /**
     * 搜索商品
     *
     * @param dto
     * @param page
     * @return
     */
    Page<MallGoods> search(@Param("dto") GoodsSearchDTO dto, Page<MallGoodsSku> page);
}
