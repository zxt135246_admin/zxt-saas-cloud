package com.zxt.content.event.addOrder;

import com.zxt.bean.model.UserInfoVO;
import com.zxt.content.dto.OrderAddDTO;
import com.zxt.content.model.MallGoodsSku;
import com.zxt.content.model.Order;
import com.zxt.content.model.OrderGoods;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.List;

@Builder
@Data
@EqualsAndHashCode(callSuper = false)
public class CreateOrderEventContent {

    /**
     * 文案入参
     */
    OrderAddDTO orderAddDTO;

    /**
     * 用户信息
     */
    UserInfoVO userInfoVO;

    String orderCode;

    List<MallGoodsSku> mallGoodsSkuList;

    Order order;

    List<OrderGoods> orderGoodsList;

    /**
     * 订单总积分
     */
    BigDecimal totalPrice;

    /**
     * 订单会员成长折扣积分
     */
    BigDecimal discountPrice;

    /**
     * 订单需要支付的积分
     */
    BigDecimal payPrice;
}
