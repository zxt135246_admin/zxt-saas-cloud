package com.zxt.content.manager;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zxt.content.dao.ImagesCollectionsMapper;
import com.zxt.content.dto.ImagesCollectionsDTO;
import com.zxt.content.model.ImagesCollections;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

/**
 * @author zxt
 */
@Slf4j
@Service
public class ImagesCollectionsManager extends ServiceImpl<ImagesCollectionsMapper, ImagesCollections> {

    /**
     * 获取属于该用户的合集
     *
     * @param userId
     * @param parentId
     * @param collectionsName
     * @return
     */
    public ImagesCollections getByUserId(Long userId, Long parentId, String collectionsName) {
        if (Objects.isNull(parentId)) {
            parentId = 0L;
        }
        LambdaQueryWrapper<ImagesCollections> wrapper = ImagesCollections.gw();
        wrapper.eq(ImagesCollections::getParentId, parentId);
        wrapper.eq(ImagesCollections::getCollectionsName, collectionsName);
        wrapper.and((w) -> {
            w.eq(ImagesCollections::getUserId, userId).or().like(ImagesCollections::getShareUserJson, userId);
        });
        return getOne(wrapper);
    }


    public List<ImagesCollections> list(ImagesCollectionsDTO dto) {
        LambdaQueryWrapper<ImagesCollections> wrapper = ImagesCollections.gw();
        wrapper.eq(ImagesCollections::getParentId, dto.getParentId());
        wrapper.and((w) -> {
            w.eq(ImagesCollections::getUserId, dto.getUserId()).or().like(ImagesCollections::getShareUserJson, dto.getUserId());
        });
        return list(wrapper);
    }
}
