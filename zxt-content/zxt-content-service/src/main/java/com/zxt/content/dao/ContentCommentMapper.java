package com.zxt.content.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zxt.content.model.ContentComment;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 文案评论
 */
public interface ContentCommentMapper extends BaseMapper<ContentComment> {

    /**
     * 获取文案评论数量
     *
     * @param contentIds
     * @return
     */
    List<ContentComment> getContentsNum(@Param("contentIds") List<Long> contentIds);
}
