package com.zxt.content.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zxt.content.model.MallGoods;

public interface MallGoodsMapper extends BaseMapper<MallGoods> {
}
