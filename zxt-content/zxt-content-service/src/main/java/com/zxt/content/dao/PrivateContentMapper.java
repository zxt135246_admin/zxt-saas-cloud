package com.zxt.content.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zxt.content.model.PrivateContent;
import org.apache.ibatis.annotations.Param;

public interface PrivateContentMapper extends BaseMapper<PrivateContent> {

    /**
     * 随机获取一条文案
     *
     * @param userId
     * @return
     */
    PrivateContent randGetOneContentByUserId(@Param("userId") Long userId);
}
