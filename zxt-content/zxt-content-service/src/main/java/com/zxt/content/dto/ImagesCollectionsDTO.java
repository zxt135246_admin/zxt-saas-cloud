package com.zxt.content.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.zxt.bean.convertor.LongJsonDeserializer;
import com.zxt.bean.convertor.LongJsonSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author zxt
 */
@Data
@ApiModel("相册合集入参")
public class ImagesCollectionsDTO {
    @ApiModelProperty("id")
    private Long id;

    @ApiModelProperty("用户id")
    private Long userId;

    @ApiModelProperty("上级id")
    private Long parentId;

    @ApiModelProperty("合集名称")
    private String collectionsName;

    @ApiModelProperty("图标地址")
    private String iconUrl;

    @ApiModelProperty("共享用户json")
    private String shareUserJson;

    /**
     * 创建时间
     */
    @ApiModelProperty("创建时间")
    private LocalDateTime createTime;

    @ApiModelProperty("共享用户列表")
    private List<Long> shareUserList;
}
