package com.zxt.content.model;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString(callSuper = true)
@TableName(value = "sc_category")
public class Category {
    public static final LambdaQueryWrapper<Category> gw() {
        return new LambdaQueryWrapper<>();
    }

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @TableField(value = "parent_id")
    private Long parentId;

    @TableField(value = "category_name")
    private String categoryName;

    @TableField(value = "category_level")
    private Integer categoryLevel;

    @TableField(value = "category_picture")
    private String categoryPicture;

    @TableField(value = "full_id_path")
    private String fullIdPath;

    @TableField(value = "full_name_path")
    private String fullNamePath;

    @TableField(value = "sort_number")
    private Integer sortNumber;

    @TableField(value = "category_status")
    private Byte categoryStatus;

    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    @TableLogic
    @TableField(value = "is_deleted")
    private Boolean isDeleted;
}
