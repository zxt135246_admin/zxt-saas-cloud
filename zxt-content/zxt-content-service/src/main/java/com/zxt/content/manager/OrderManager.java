package com.zxt.content.manager;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zxt.bean.utils.CollectionsUtil;
import com.zxt.bean.utils.StringUtils;
import com.zxt.content.dao.OrderMapper;
import com.zxt.content.dto.OrderDTO;
import com.zxt.content.enums.OrderStatusEnum;
import com.zxt.content.enums.PayStatusEnum;
import com.zxt.content.model.Order;
import com.zxt.content.utils.CodeGeneratorUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

/**
 * @author zxt
 */
@Slf4j
@Service
public class OrderManager extends ServiceImpl<OrderMapper, Order> {


    private void setOrderCode(Order order) {
        try {
            // 订单编码
            if (StringUtils.isBlank(order.getOrderCode())) {
                order.setOrderCode(CodeGeneratorUtil.generateOrderCode());
            }
            super.save(order);
        } catch (Exception e) {
            if (e instanceof DuplicateKeyException) {
                log.warn("出现了生成订单编码重复现象,orderCode={}", order.getOrderCode());
                order.setOrderCode(null);
                setOrderCode(order);
            } else {
                throw e;
            }
        }
    }


    public Long addOrder(Order order) {
        order.setId(null);
        setOrderCode(order);
        return order.getId();
    }

    /**
     * 修改订单状态
     *
     * @param orderCode        订单号
     * @param orderStatus      修改订单状态
     * @param whereOrderStatus 前置订单状态条件
     * @return
     */
    public boolean updateStatus(String orderCode, Byte orderStatus, Byte whereOrderStatus) {
        LambdaUpdateWrapper<Order> wrapper = new LambdaUpdateWrapper<>();
        wrapper.set(Order::getOrderStatus, orderStatus);
        wrapper.eq(Order::getOrderCode, orderCode);
        wrapper.eq(Order::getOrderStatus, whereOrderStatus);
        return update(wrapper);
    }


    /**
     * 修改支付成功
     *
     * @param orderCode
     */
    public void updatePaySuccess(String orderCode) {
        LambdaUpdateWrapper<Order> wrapper = new LambdaUpdateWrapper<>();
        wrapper.set(Order::getPayStatus, PayStatusEnum.SUCCESS.getValue());
        wrapper.set(Order::getPayTime, LocalDateTime.now());
        wrapper.eq(Order::getOrderCode, orderCode);
        update(wrapper);
    }

    /**
     * 查询订单
     *
     * @param orderCode
     * @return
     */
    public Order getByOrderCode(String orderCode) {
        LambdaQueryWrapper<Order> wrapper = Order.gw();
        wrapper.eq(Order::getOrderCode, orderCode);
        return getOne(wrapper);
    }

    /**
     * 查询用户订单列表
     *
     * @param userId
     * @return
     */
    public List<Order> getByUserId(Long userId) {
        LambdaQueryWrapper<Order> wrapper = Order.gw();
        wrapper.eq(Order::getUserId, userId);
        return list(wrapper);
    }

    public List<Order> getOrderList(OrderDTO dto) {
        LambdaQueryWrapper<Order> wrapper = Order.gw();
        if (Objects.nonNull(dto.getUserId())) {
            wrapper.eq(Order::getUserId, dto.getUserId());
        }
        if (StringUtils.isNotBlank(dto.getOrderCode())) {
            wrapper.eq(Order::getOrderCode, dto.getOrderCode());
        }

        if (Objects.nonNull(dto.getCustomerOrderStatus())) {
            if (dto.getCustomerOrderStatus().equals((byte) 1)) {
                wrapper.eq(Order::getOrderStatus, OrderStatusEnum.PENDING_PAYMENT.getValue());
            } else if (dto.getCustomerOrderStatus().equals((byte) 2)) {
                wrapper.in(Order::getOrderStatus, CollectionsUtil.asList(OrderStatusEnum.PENDING_SHIPPMENT.getValue(),
                        OrderStatusEnum.LOCKED.getValue(),
                        OrderStatusEnum.PARTIAL_SHIPMENT.getValue(),
                        OrderStatusEnum.SHIPPED.getValue(),
                        OrderStatusEnum.SIGNED_FOR_RECEIPT.getValue()));
            } else if (dto.getCustomerOrderStatus().equals((byte) 3)) {
                wrapper.eq(Order::getOrderStatus, OrderStatusEnum.COMPLETED.getValue());
            }
        }
        return list(wrapper);
    }

}
