package com.zxt.content.vo;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.zxt.bean.convertor.LongJsonDeserializer;
import com.zxt.bean.convertor.LongJsonSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.math.BigDecimal;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString(callSuper = true)
@ApiModel("商城商品sku出参")
public class MallGoodsSkuVO {

    /**
     * id
     */
    @ApiModelProperty("id")
    @JsonSerialize(using = LongJsonSerializer.class)
    @JsonDeserialize(using = LongJsonDeserializer.class)
    private Long id;


    @ApiModelProperty("商品名称")
    private String goodsName;

    @ApiModelProperty("商品编码")
    private String skuCode;

    @ApiModelProperty("状态")
    private Byte commonStatus;

    @ApiModelProperty("状态描述")
    private String commonStatusDesc;

    @ApiModelProperty("销售价")
    private BigDecimal salePrice;

    @ApiModelProperty("规格值")
    private String specValue;

    @ApiModelProperty("商品图片")
    private String goodsPicture;

    @ApiModelProperty("商品图片列表")
    private List<String> goodsPictures;
}
