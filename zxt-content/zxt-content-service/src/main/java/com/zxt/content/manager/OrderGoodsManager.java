package com.zxt.content.manager;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zxt.bean.utils.CollectionsUtil;
import com.zxt.content.dao.OrderGoodsMapper;
import com.zxt.content.model.OrderGoods;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author zxt
 */
@Slf4j
@Service
public class OrderGoodsManager extends ServiceImpl<OrderGoodsMapper, OrderGoods> {


    /**
     * 修改订单状态
     *
     * @param orderCode        订单号
     * @param orderStatus      修改订单状态
     * @param whereOrderStatus 前置订单状态条件
     * @return
     */
    public boolean updateStatus(String orderCode, Byte orderStatus, Byte whereOrderStatus) {
        LambdaUpdateWrapper<OrderGoods> wrapper = new LambdaUpdateWrapper<>();
        wrapper.set(OrderGoods::getCommonStatus, orderStatus);
        wrapper.eq(OrderGoods::getOrderCode, orderCode);
        wrapper.eq(OrderGoods::getCommonStatus, whereOrderStatus);
        return update(wrapper);
    }

    /**
     * 获取订单行
     *
     * @param orderCode
     * @return
     */
    public List<OrderGoods> getByOrderCode(String orderCode) {
        LambdaQueryWrapper<OrderGoods> wrapper = OrderGoods.gw();
        wrapper.eq(OrderGoods::getOrderCode, orderCode);
        return list(wrapper);
    }

    /**
     * 获取订单商品列表
     *
     * @param orderCodes
     * @return
     */
    public List<OrderGoods> getByOrderCodes(List<String> orderCodes) {
        if (CollectionsUtil.isEmpty(orderCodes)) {
            return new ArrayList<>();
        }
        LambdaQueryWrapper<OrderGoods> wrapper = OrderGoods.gw();
        wrapper.in(OrderGoods::getOrderCode, orderCodes);
        return list(wrapper);
    }
}
