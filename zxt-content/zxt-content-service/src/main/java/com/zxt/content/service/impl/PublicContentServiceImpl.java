package com.zxt.content.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zxt.bean.model.UserInfoVO;
import com.zxt.bean.query.QueryResultVO;
import com.zxt.bean.utils.BeanUtils;
import com.zxt.bean.utils.CollectionsUtil;
import com.zxt.bean.utils.StringUtils;
import com.zxt.bean.utils.TrueUtils;
import com.zxt.content.asyncs.ContentAsync;
import com.zxt.content.client.OpenApiClient;
import com.zxt.content.client.OperationClient;
import com.zxt.content.client.UserClient;
import com.zxt.content.dto.ContentQueryDTO;
import com.zxt.content.manager.ContentCommentManager;
import com.zxt.content.manager.ContentTypeManager;
import com.zxt.content.manager.PrivateContentManager;
import com.zxt.content.model.ContentComment;
import com.zxt.content.model.ContentType;
import com.zxt.content.model.PrivateContent;
import com.zxt.content.service.PublicContentService;
import com.zxt.content.vo.PublicContentDetailVO;
import com.zxt.content.vo.PublicContentVO;
import com.zxt.operation.api.dto.SendEmailApiDTO;
import com.zxt.user.api.vo.UserApiVO;
import com.zxt.user.api.vo.UserConcernApiVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Slf4j
@Service
@RefreshScope
public class PublicContentServiceImpl implements PublicContentService {

    private final PrivateContentManager privateContentManager;
    private final ContentTypeManager contentTypeManager;
    private final UserClient userClient;
    private final ContentAsync contentAsync;
    private final ContentCommentManager contentCommentManager;
    private final OperationClient operationClient;
    private final OpenApiClient openApiClient;

    @Value("${content.push.email:}")
    private String contentPushEmails;
    @Value("${content.push.background:}")
    private String contentPushBackground;

    @Autowired
    public PublicContentServiceImpl(PrivateContentManager privateContentManager, ContentTypeManager contentTypeManager, UserClient userClient, ContentAsync contentAsync, ContentCommentManager contentCommentManager, OperationClient operationClient, OpenApiClient openApiClient) {
        this.privateContentManager = privateContentManager;
        this.contentTypeManager = contentTypeManager;
        this.userClient = userClient;
        this.contentAsync = contentAsync;
        this.contentCommentManager = contentCommentManager;
        this.operationClient = operationClient;
        this.openApiClient = openApiClient;
    }

    @Override
    public QueryResultVO<PublicContentVO> query(ContentQueryDTO dto) {
        Page<PrivateContent> page = privateContentManager.queryPrivateContent(dto);
        QueryResultVO<PublicContentVO> queryResultVO = BeanUtils.pageToQueryResultVO(page, PublicContentVO.class);
        List<PublicContentVO> vos = BeanUtils.beanCopy(page.getRecords(), PublicContentVO.class);

        if (CollectionsUtil.isEmpty(vos)) {
            queryResultVO.setRecords(new ArrayList<>());
            return queryResultVO;
        }

        List<Long> userIds = vos.stream().map(PublicContentVO::getUserId).distinct().collect(Collectors.toList());

        List<UserApiVO> userApiVOList = userClient.getByUserIds(userIds);
        Map<Long, UserApiVO> userApiVOMap = Optional.ofNullable(userApiVOList).orElse(new ArrayList<>()).stream().collect(Collectors.toMap(UserApiVO::getId, Function.identity()));

        List<Long> contentIds = vos.stream().map(PublicContentVO::getId).collect(Collectors.toList());
        List<ContentComment> contentCommentList = contentCommentManager.getContentsNum(contentIds);
        Map<Long, ContentComment> contentCommentMap = contentCommentList.stream().collect(Collectors.toMap(ContentComment::getContentId, Function.identity()));

        List<Long> contentTypeIds = vos.stream().map(PublicContentVO::getContentTypeId).collect(Collectors.toList());
        List<ContentType> contentTypeList = contentTypeManager.listByIds(contentTypeIds);
        Map<Long, ContentType> contentTypeMap = contentTypeList.stream().collect(Collectors.toMap(ContentType::getId, Function.identity()));


        for (PublicContentVO vo : vos) {

            ContentComment contentComment = contentCommentMap.get(vo.getId());
            vo.setCommentsNum(0);
            if (Objects.nonNull(contentComment)) {
                //设置评论数量
                vo.setCommentsNum(contentComment.getContentNum());
            }

            UserApiVO userApiVO = userApiVOMap.get(vo.getUserId());
            if (Objects.nonNull(userApiVO)) {
                //设置用户昵称
                vo.setNickName(userApiVO.getNickName());
            }

            ContentType contentType = contentTypeMap.get(vo.getContentTypeId());
            if (Objects.nonNull(contentType)) {
                //设置文案类型名称
                vo.setTypeName(contentType.getTypeName());
            }
        }

        //文案异步热度加1
        contentAsync.addHeat(contentIds, 10);

        //首先按评论数量降序，评论数量一样的话根据最新上传时间降序
        //vos = vos.stream().sorted(Comparator.comparing(PublicContentVO::getCommentsNum, Comparator.reverseOrder()).thenComparing(PublicContentVO::getCreateTime, Comparator.reverseOrder())).collect(Collectors.toList());

        queryResultVO.setRecords(vos);
        return queryResultVO;
    }

    @Override
    public PublicContentDetailVO get(Long id, UserInfoVO user) {
        PrivateContent privateContent = privateContentManager.getById(id);

        //使用函数式接口校验抛出异常
        TrueUtils.isTrue(Objects.isNull(privateContent)).throwMessage("文案不存在");


        PublicContentDetailVO vo = BeanUtils.beanCopy(privateContent, PublicContentDetailVO.class);

        //文案被查看，异步热度加60
        contentAsync.addHeat(CollectionsUtil.asList(privateContent.getId()), 60);

        UserApiVO userApiVO = userClient.getUserById(privateContent.getUserId());

        if (Objects.nonNull(userApiVO)) {
            //设置用户信息
            vo.setNickName(userApiVO.getNickName());
            vo.setAvatarUrl(userApiVO.getAvatarUrl());
        }

        ContentType contentType = contentTypeManager.getById(privateContent.getContentTypeId());
        if (Objects.nonNull(contentType)) {
            //设置文案类型
            vo.setContentTypeDesc(contentType.getTypeName());
        }

        List<UserConcernApiVO> userConcernApiVOList = userClient.getUserConcernByConcernUserId(vo.getUserId());

        //设置粉丝数量
        vo.setFenSiNum(Optional.ofNullable(userConcernApiVOList).orElse(new ArrayList<>()).size());

        //设置是否已关注
        vo.setIsConcern(Boolean.FALSE);
        if (Objects.nonNull(user)) {
            //已登录
            if (user.getId().equals(vo.getUserId())) {
                //文案作者是自己
                vo.setIsConcern(Boolean.TRUE);
            } else {
                //判断是否已关注
                UserConcernApiVO userConcernApiVO = userClient.getByUserIdAndConcernUserId(user.getId(), vo.getUserId());
                if (Objects.nonNull(userConcernApiVO)) {
                    //是否已关注
                    vo.setIsConcern(Boolean.TRUE);
                }
            }
        }
        return vo;
    }

    @Override
    public void dealPushEmail() {
        PrivateContent privateContent = privateContentManager.randGetOneContentByUserId(null);
        ContentType contentType = contentTypeManager.getById(privateContent.getContentTypeId());
        if (StringUtils.isNotBlank(contentPushEmails)) {
            List<String> emailList = CollectionsUtil.asList(contentPushEmails.split(","));
            List<String> backgroundList = CollectionsUtil.asList(contentPushBackground.split(","));
            Collections.shuffle(backgroundList);
            for (String email : emailList) {
                String content = "<div style=\"height:220px;width: 100%;background: rgba(255,255,255,0.79) url('#{background}') no-repeat; background-size: 100%; background-position: 50%;\"><p style=\"width: 100%; padding: 0 4%; height: 1.5rem; line-height: 1.5rem; font-size: .46rem; color: #333333;\"><br/>概率云端<br/></p><div style=\"padding: 5%;\"><ul style='list-style:none;'><li style=\"font-size: .42rem; color: #000000e3; line-height: .6rem; margin-bottom: 3%; text-indent:3em; font-family: '楷体';\">#{content}</li><br/><br/><li style=\"font-size: .42rem; color: #000000e3; line-height: .6rem; margin-bottom: 3%; text-indent:3em; font-family: '楷体';float:right;border-top: 1px dotted rgba(0,0,0,0.08); color:rgba(0,0,0,0.32);\">文案类型：#{typeName}</li></ul></div></div>";
                content = content.replace("#{background}", backgroundList.get(0));
                content = content.replace("#{content}", privateContent.getContent());
                content = content.replace("#{typeName}", contentType.getTypeName());
                SendEmailApiDTO emailApiDTO = SendEmailApiDTO.builder().toEmail(email).subject("今日份文案已到账~").content(content).build();
                operationClient.sendEmail(emailApiDTO);
            }
        }
    }

    @Override
    public void dealGenerationContent() {
        String hotComments = openApiClient.getHotComments();
        String commonStr = "热评";
        int i = hotComments.lastIndexOf(commonStr);
        if (i == -1) {
            return;
        }
        String singName = hotComments.substring(i + commonStr.length());
        hotComments = hotComments.replace(singName, "");

        Long wyyId = 1055525676778569729L;
        PrivateContent existPrivateContent = privateContentManager.getByContentSource(wyyId, singName);
        PrivateContent privateContent = new PrivateContent();
        if (Objects.nonNull(existPrivateContent)) {
            privateContent.setId(existPrivateContent.getId());
        }else{
            privateContent.setIsPublish(Boolean.TRUE);
            privateContent.setContentHeat(0);
            privateContent.setCanPush(Boolean.TRUE);
        }
        privateContent.setUserId(wyyId);
        privateContent.setContent(hotComments);
        privateContent.setContentSource(singName);
        ContentType contentType = contentTypeManager.getByName("网易云文案");
        privateContent.setContentTypeId(contentType.getId());
        privateContentManager.saveOrUpdate(privateContent);
    }
}
