package com.zxt.content.dto.excel;

import com.alibaba.excel.annotation.ExcelProperty;
import com.zxt.bean.annotation.ExcelSelected;
import lombok.Data;

import java.io.Serializable;

@Data
public class ContentTypeDataDTO implements Serializable {

    @ExcelProperty("类型")
    @ExcelSelected(source = {"类型1", "类型2"}, isUnanimous = false)
    private String type;

    @ExcelProperty("名称")
    private String name;

}
