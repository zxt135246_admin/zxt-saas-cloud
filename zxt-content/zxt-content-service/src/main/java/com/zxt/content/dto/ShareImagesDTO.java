package com.zxt.content.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author zxt
 */
@Data
@ApiModel("分享相册入参")
public class ShareImagesDTO {

    @ApiModelProperty("图片id")
    private Long imagesId;

    @ApiModelProperty(value = "用户id", hidden = true)
    private Long userId;
    @ApiModelProperty("是否隐藏说明")
    private Boolean isHiddenRemark;

    @ApiModelProperty("分享时间 1.10秒 2.1分钟 3.5分钟 4.10分钟 5.1小时 6.5小时 7.12小时 8.1天 9.3天 10.7天 11.1个月")
    private Byte shareTimeType;

    @ApiModelProperty("分享编码")
    private String shareCode;
}
