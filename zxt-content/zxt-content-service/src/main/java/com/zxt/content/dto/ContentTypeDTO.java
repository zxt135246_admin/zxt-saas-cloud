package com.zxt.content.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author zxt
 */
@Data
@ApiModel("文案类型入参")
public class ContentTypeDTO {
    @ApiModelProperty("类型名称")
    private String typeName;
}
