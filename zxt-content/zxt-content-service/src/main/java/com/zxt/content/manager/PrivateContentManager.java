package com.zxt.content.manager;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zxt.content.dao.PrivateContentMapper;
import com.zxt.content.dto.ContentQueryDTO;
import com.zxt.content.model.PrivateContent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

/**
 * @author zxt
 */
@Slf4j
@Service
public class PrivateContentManager extends ServiceImpl<PrivateContentMapper, PrivateContent> {

    /**
     * 分页查询
     *
     * @param dto
     * @return
     */
    public Page<PrivateContent> queryPrivateContent(ContentQueryDTO dto) {
        Page<PrivateContent> page = new Page<>();
        page.setCurrent(dto.getPageNo());
        page.setSize(dto.getPageSize());

        LambdaQueryWrapper<PrivateContent> wrapper = new LambdaQueryWrapper();
        if (Objects.nonNull(dto.getContentTypeId())) {
            wrapper.eq(PrivateContent::getContentTypeId, dto.getContentTypeId());
        }
        if (Objects.nonNull(dto.getUserId())) {
            wrapper.eq(PrivateContent::getUserId, dto.getUserId());
        }
        if (Objects.isNull(dto.getIsSelf()) || !dto.getIsSelf()) {
            wrapper.eq(PrivateContent::getIsPublish, Boolean.TRUE);
        }
        //文案热度降序排序
        wrapper.orderByDesc(PrivateContent::getCreateTime);
        return super.page(page, wrapper);
    }

    public List<PrivateContent> getByUserId(Long userId) {
        LambdaQueryWrapper<PrivateContent> wrapper = PrivateContent.gw();
        wrapper.eq(PrivateContent::getUserId, userId);
        return list(wrapper);
    }

    /**
     * 文案增加热度
     *
     * @param ids
     * @param heatNum
     */
    public void addHeat(List<Long> ids, Integer heatNum) {
        LambdaUpdateWrapper<PrivateContent> wrapper = new LambdaUpdateWrapper();
        wrapper.setSql("content_heat = content_heat + " + heatNum);
        wrapper.in(PrivateContent::getId, ids);
        update(wrapper);
    }

    /**
     * 随机获取一条文案
     *
     * @param userId
     * @return
     */
    public PrivateContent randGetOneContentByUserId(Long userId) {
        return this.baseMapper.randGetOneContentByUserId(userId);
    }

    /**
     * 根据来源获取
     *
     * @param userId
     * @param source
     * @return
     */
    public PrivateContent getByContentSource(Long userId, String source) {
        LambdaQueryWrapper<PrivateContent> wrapper = PrivateContent.gw().eq(PrivateContent::getUserId, userId).eq(PrivateContent::getContentSource, source);
        return getOne(wrapper);
    }
}
