package com.zxt.content.manager;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zxt.content.dao.ContentCommentMapper;
import com.zxt.content.dto.ContentCommentQueryDTO;
import com.zxt.content.model.ContentComment;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

/**
 * 文案评论
 *
 * @author zxt
 */
@Slf4j
@Service
public class ContentCommentManager extends ServiceImpl<ContentCommentMapper, ContentComment> {


    /**
     * 分页查询文案评论
     *
     * @param dto
     * @return
     */
    public Page<ContentComment> queryContentComment(ContentCommentQueryDTO dto) {
        Page<ContentComment> page = new Page<>();
        page.setCurrent(dto.getPageNo());
        page.setSize(dto.getPageSize());
        LambdaQueryWrapper<ContentComment> queryWrapper = ContentComment.gw();
        if (Objects.nonNull(dto.getContentId())) {
            queryWrapper.eq(ContentComment::getContentId, dto.getContentId());
        }
        queryWrapper.orderByDesc(ContentComment::getCreateTime);
        return page(page, queryWrapper);
    }

    /**
     * 获取评论数量
     *
     * @param contentIds
     * @return
     */
    public List<ContentComment> getContentsNum(List<Long> contentIds) {
        return baseMapper.getContentsNum(contentIds);
    }
}
