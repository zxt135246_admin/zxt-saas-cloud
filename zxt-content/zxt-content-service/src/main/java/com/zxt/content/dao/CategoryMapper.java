package com.zxt.content.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zxt.content.model.Category;

public interface CategoryMapper extends BaseMapper<Category> {
}
