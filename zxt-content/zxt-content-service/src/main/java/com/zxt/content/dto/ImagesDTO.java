package com.zxt.content.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author zxt
 */
@Data
@ApiModel("相册入参")
public class ImagesDTO {

    @ApiModelProperty("id")
    private Long id;

    @ApiModelProperty(value = "用户id", hidden = true)
    private Long userId;

    private Long collectionsId;
    @ApiModelProperty("说明")
    private String remark;

    private String imageUrl;

    private String originalImageUrl;

    @ApiModelProperty("拍摄地址")
    private String shootingAddress;
}
