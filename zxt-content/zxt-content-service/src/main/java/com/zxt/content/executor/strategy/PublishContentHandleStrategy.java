package com.zxt.content.executor.strategy;

import com.zxt.bean.executor.strategy.AbstractHandleStrategy;

public class PublishContentHandleStrategy extends AbstractHandleStrategy {

    public PublishContentHandleStrategy() {
        super("publishContentHandleStrategy");
    }
}
