package com.zxt.content.model;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString(callSuper = true)
@TableName(value = "sc_cart")
public class Cart {
    public static final LambdaQueryWrapper<Cart> gw() {
        return new LambdaQueryWrapper<>();
    }

    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;

    @TableField(value = "mall_id")
    private Long mallId;

    @TableField(value = "user_id")
    private Long userId;

    @TableField(value = "mall_goods_sku_id")
    private Long mallGoodsSkuId;

    @TableField(value = "sku_code")
    private String skuCode;

    @TableField(value = "goods_name")
    private String goodsName;

    @TableField(value = "goods_picture")
    private String goodsPicture;

    @TableField(value = "goods_type")
    private Byte goodsType;

    @TableField(value = "spec_value")
    private String specValue;

    @TableField(value = "add_goods_price")
    private BigDecimal addGoodsPrice;

    @TableField(value = "goods_number")
    private Integer goodsNumber;

    @TableField(value = "is_selected")
    private Boolean isSelected;

    /**
     * 创建时间
     */
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    /**
     * 是否删除
     */
    @TableLogic
    @TableField(value = "is_deleted")
    private Boolean isDeleted;
}
