package com.zxt.content.service.impl;

import com.zxt.bean.exeption.ServiceException;
import com.zxt.bean.result.ResultCode;
import com.zxt.bean.utils.BeanUtils;
import com.zxt.content.dto.ImagesCollectionsDTO;
import com.zxt.content.manager.ImagesCollectionsManager;
import com.zxt.content.model.ImagesCollections;
import com.zxt.content.service.ImagesCollectionsService;
import com.zxt.content.vo.ImagesCollectionsVO;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Slf4j
@Service
@AllArgsConstructor
public class ImagesCollectionsImpl implements ImagesCollectionsService {

    private final ImagesCollectionsManager imagesCollectionsManager;


    @Override
    public List<ImagesCollectionsVO> list(ImagesCollectionsDTO dto) {
        if (Objects.isNull(dto.getParentId())) {
            dto.setParentId(0L);
        }
        List<ImagesCollections> imagesCollectionsList = imagesCollectionsManager.list(dto);
        return BeanUtils.beanCopy(imagesCollectionsList, ImagesCollectionsVO.class);
    }

    @Override
    public void add(ImagesCollectionsDTO dto) {
        if (Objects.isNull(dto.getParentId())) {
            dto.setParentId(0L);
        }
        ImagesCollections imagesCollections = imagesCollectionsManager.getByUserId(dto.getUserId(), dto.getParentId(), dto.getCollectionsName());
        if (Objects.nonNull(imagesCollections)) {
            throw new ServiceException(ResultCode.COMMON_MESSAGE, "当前合集已经存在");
        }
        imagesCollectionsManager.save(BeanUtils.beanCopy(dto, ImagesCollections.class));
    }

    @Override
    public ImagesCollectionsVO getById(Long id) {
        return BeanUtils.beanCopy(imagesCollectionsManager.getById(id), ImagesCollectionsVO.class);
    }
}
