package com.zxt.content.service;

import com.zxt.bean.query.QueryResultVO;
import com.zxt.content.dto.CategoryDTO;
import com.zxt.content.dto.CategoryQueryDTO;
import com.zxt.content.model.CategoryView;

public interface CategoryService {

    /**
     * 创建类目
     *
     * @param dto
     */
    void createCategory(CategoryDTO dto);

    /**
     * 修改类目
     *
     * @param dto
     */
    void updateCategory(CategoryDTO dto);

    /**
     * 分页查询类目
     *
     * @return
     */
    QueryResultVO<CategoryView> queryCategory(CategoryQueryDTO dto);
}
