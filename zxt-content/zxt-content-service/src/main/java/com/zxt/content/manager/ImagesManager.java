package com.zxt.content.manager;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zxt.bean.utils.StringUtils;
import com.zxt.content.dao.ImagesMapper;
import com.zxt.content.dto.ImagesDTO;
import com.zxt.content.model.Images;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

/**
 * @author zxt
 */
@Slf4j
@Service
public class ImagesManager extends ServiceImpl<ImagesMapper, Images> {


    public List<Images> query(ImagesDTO dto) {

        LambdaQueryWrapper<Images> wrapper = Images.gw();
        if (Objects.nonNull(dto.getCollectionsId())) {
            wrapper.eq(Images::getCollectionsId, dto.getCollectionsId());
        }

        return list(wrapper);
    }

    public void updateImages(Images images) {
        LambdaUpdateWrapper<Images> wrapper = new LambdaUpdateWrapper<>();
        wrapper.set(Images::getRemark, images.getRemark());
        wrapper.eq(Images::getId, images.getId());
        update(wrapper);
    }
}
