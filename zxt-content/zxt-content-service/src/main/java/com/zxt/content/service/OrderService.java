package com.zxt.content.service;


import com.zxt.bean.model.UserInfoVO;
import com.zxt.content.dto.OrderAddDTO;
import com.zxt.content.dto.OrderDTO;
import com.zxt.content.dto.OrderPayDTO;
import com.zxt.content.vo.OrderVO;

import java.util.List;
import java.util.Map;

/**
 * 订单业务管理
 *
 * @author zxt
 */
public interface OrderService {

    /**
     * 订单初始化
     *
     * @param user
     * @return
     */
    OrderAddDTO orderInit(UserInfoVO user);

    /**
     * 创建订单
     *
     * @param orderAddDTO
     * @param user
     * @return
     */
    String createOrder(OrderAddDTO orderAddDTO, UserInfoVO user);

    /**
     * 订单支付检查
     *
     * @param dto
     * @param user
     */
    void orderPayCheck(OrderPayDTO dto, UserInfoVO user);

    /**
     * 订单支付
     *
     * @param dto
     * @param user
     */
    void orderPay(OrderPayDTO dto, UserInfoVO user);

    /**
     * 修改订单状态
     *
     * @param orderCode
     * @param orderStatus
     * @param whereOrderStatus
     * @return
     */
    public boolean updateStatus(String orderCode, Byte orderStatus, Byte whereOrderStatus);

    /**
     * 获取订单详情
     *
     * @param orderCode
     * @return
     */
    OrderVO getOrderDetail(String orderCode);

    /**
     * 订单状态数量查询
     *
     * @param userId
     * @return
     */
    Map<String, Integer> quantityQuery(Long userId);

    /**
     * 获取用户订单列表
     *
     * @param dto
     * @return
     */
    List<OrderVO> getUserOrder(OrderDTO dto);

}
