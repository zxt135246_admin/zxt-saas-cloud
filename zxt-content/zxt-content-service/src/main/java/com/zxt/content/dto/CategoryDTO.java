package com.zxt.content.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;


@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString(callSuper = true)
@ApiModel("类目入参")
public class CategoryDTO implements Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "id")
    private Long id;

    @ApiModelProperty(value = "父类id", required = true)
    @NotNull(message = "父类id不能为空")
    private Long parentId;

    @ApiModelProperty(value = "类目名称", required = true)
    @NotBlank(message = "类目名称不能为空")
    @Size(max = 6, message = "类目名称不能超过6位")
    private String categoryName;

    @ApiModelProperty(value = "类目图片")
    private String categoryPicture;

    @ApiModelProperty(value = "排序")
    private Integer sortNumber;


    /**
     * -------------------------------------------------
     * 上面字段由工具自动生成，请在下面添加扩充字段
     * -------------------------------------------------
     */

}
