package com.zxt.content.vo;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.zxt.bean.convertor.LongJsonDeserializer;
import com.zxt.bean.convertor.LongJsonSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author zxt
 */
@Data
@ApiModel("相册出参")
public class ImagesVO {
    @ApiModelProperty("id")
    @JsonSerialize(using = LongJsonSerializer.class)
    @JsonDeserialize(using = LongJsonDeserializer.class)
    private Long id;

    @ApiModelProperty("用户id")
    @JsonSerialize(using = LongJsonSerializer.class)
    @JsonDeserialize(using = LongJsonDeserializer.class)
    private Long userId;

    @ApiModelProperty("相册集合id")
    @JsonSerialize(using = LongJsonSerializer.class)
    @JsonDeserialize(using = LongJsonDeserializer.class)
    private Long collectionsId;

    @ApiModelProperty("城市名称")
    private String cityName;

    @ApiModelProperty("相册地址")
    private String imageUrl;

    @ApiModelProperty("原图地址")
    private String originalImageUrl;

    @ApiModelProperty("相册备注")
    private String remark;

    @ApiModelProperty("共享的用户id")
    @JsonSerialize(using = LongJsonSerializer.class)
    @JsonDeserialize(using = LongJsonDeserializer.class)
    private Long shareUserId;

    /**
     * 创建时间
     */
    @ApiModelProperty("创建时间")
    private LocalDateTime createTime;


    /**
     * 其他----------------------------------------------
     */

    @ApiModelProperty("拍摄地址")
    private String shootingAddress;

    @ApiModelProperty("拍摄时间")
    private LocalDateTime shootingTime;

    @ApiModelProperty("用户昵称")
    private String nickName;

    @ApiModelProperty("头像")
    private String avatarUrl;

    @ApiModelProperty("会员等级描述")
    private String levelDesc;
}
