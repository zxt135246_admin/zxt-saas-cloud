package com.zxt.content.enums;

/**
 * @Comments: 分享时间 1.10秒 2.1分钟 3.5分钟 4.10分钟 5.1小时 6.5小时 7.1天 8.3天 9.7天 10.1个月
 */
public enum ShareTimeTypeEnum {
	TEN_SECONDS                 ((byte) 1, "10秒"),
	ONE_MINUTE                 ((byte) 2, "1分钟"),
	FIVE_MINUTE                ((byte) 3, "5分钟"),
	TEN_MINUTE                ((byte) 4, "10分钟"),
	ONE_HOURS                ((byte) 5, "1小时"),
	FIVE_HOURS                ((byte) 6, "5小时"),
	ONE_DAY                ((byte) 7, "1天"),
	THREE_DAY                ((byte) 8, "3天"),
	SEVEN_DAY                ((byte) 9, "7天"),
	ONE_MONTH               ((byte) 10, "1个月"),
	;

	private byte value;
	private String desc;

	ShareTimeTypeEnum(byte value, String desc){
		this.value = value;
		this.desc = desc;
	}

    public byte getValue() {
        return value;
    }

    public String getDesc() {
		return desc;
	}

	public static boolean exists(Byte status) {
        if (status == null) {
            return false;
        }
        byte s = status.byteValue();
        return exists(s);
    }

    public static boolean exists(byte s) {
        for (ShareTimeTypeEnum element : ShareTimeTypeEnum.values()) {
            if (element.value == s) {
                return true;
            }
        }
        return false;
    }

    public boolean equal(Byte val) {
        return val == null ? false : val.byteValue() == this.value;
    }

	public static String getDescByValue(Byte value) {
		if (value == null) {
			return "";
		}
		for (ShareTimeTypeEnum element : ShareTimeTypeEnum.values()) {
			if (element.value == value.byteValue()) {
				return element.desc;
			}
		}
		return "";
	}
}
