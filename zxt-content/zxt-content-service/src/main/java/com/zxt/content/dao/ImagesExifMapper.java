package com.zxt.content.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zxt.content.model.ImagesExif;

public interface ImagesExifMapper extends BaseMapper<ImagesExif> {
}
