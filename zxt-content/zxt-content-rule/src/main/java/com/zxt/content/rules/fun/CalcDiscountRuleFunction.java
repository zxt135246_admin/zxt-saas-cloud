package com.zxt.content.rules.fun;

import com.zxt.content.rules.bean.OrderGoodsInputBean;
import com.zxt.content.rules.model.OrderDiscount;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

@Slf4j
public class CalcDiscountRuleFunction {

    public static OrderDiscount execute(List<OrderGoodsInputBean> orderGoodsList, Integer gradeDiscountRatio, OrderDiscount orderDiscount) {
        log.info("开始计算订单会员折扣");
        BigDecimal discount = BigDecimal.valueOf(gradeDiscountRatio).divide(BigDecimal.valueOf(100));

        BigDecimal totalDiscountIntegral = BigDecimal.ZERO;
        for (OrderGoodsInputBean orderGoods : orderGoodsList) {
            BigDecimal totalGoodsIntegral = orderGoods.getSalePrice().multiply(BigDecimal.valueOf(orderGoods.getGoodsNumber()));
            BigDecimal discountIntegral = totalGoodsIntegral.multiply(discount).setScale(2, RoundingMode.DOWN);

            orderGoods.setGradeDiscount(discountIntegral);
            totalDiscountIntegral = totalDiscountIntegral.add(discountIntegral);
        }

        //customer-discount.drl里加过了这里就不加了
        /*orderDiscount.setDiscount(orderDiscount.getDiscount() + gradeDiscountRatio);*/
        orderDiscount.setOrderGradeDiscount(totalDiscountIntegral);
        orderDiscount.setOrderGoodsList(orderGoodsList);
        return orderDiscount;
    }
}
