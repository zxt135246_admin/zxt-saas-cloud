package com.zxt.content.rules.model;

import com.zxt.content.rules.bean.OrderGoodsInputBean;
import lombok.Data;

import java.util.List;

@Data
public class OrderRequest {

    //订单商品
    private List<OrderGoodsInputBean> orderGoodsList;

    private Boolean isUseGradeDiscount;

    //会员成长值抵扣比例
    private Integer gradeDiscountRatio;
}
