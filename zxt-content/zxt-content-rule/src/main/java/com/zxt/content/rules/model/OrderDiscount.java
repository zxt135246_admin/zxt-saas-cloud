package com.zxt.content.rules.model;

import com.zxt.content.rules.bean.OrderGoodsInputBean;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class OrderDiscount {
    /**
     * 折扣
     */
    private Integer discount = 0;

    /**
     * 订单成长值抵扣
     */
    private BigDecimal orderGradeDiscount;

    private List<OrderGoodsInputBean> orderGoodsList;
}
