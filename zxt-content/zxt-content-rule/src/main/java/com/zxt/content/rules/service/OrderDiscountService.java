package com.zxt.content.rules.service;

import com.alibaba.fastjson.JSON;
import com.zxt.content.rules.model.OrderDiscount;
import com.zxt.content.rules.model.OrderRequest;
import lombok.extern.slf4j.Slf4j;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class OrderDiscountService {
    @Autowired
    private KieContainer kieContainer;

    public OrderDiscount calcDiscount(OrderRequest orderRequest) {
        OrderDiscount orderDiscount = new OrderDiscount();
        // 开启会话
        KieSession kieSession = kieContainer.newKieSession();
        // 设置折扣对象
        kieSession.setGlobal("orderDiscount", orderDiscount);
        // 设置订单对象
        kieSession.insert(orderRequest);
        // 触发规则
        kieSession.fireAllRules();
        //或者  通过规则过滤器实现只执行指定规则
        //kieSession.fireAllRules(new RuleNameEqualsAgendaFilter("grade_discount"));
        // 中止会话
        kieSession.dispose();
        log.info("计算抵扣结果：{}", JSON.toJSONString(orderDiscount));
        return orderDiscount;
    }
}
