package com.github.davidfantasy.mybatisplus.generatorui.controller;

import com.github.davidfantasy.mybatisplus.generatorui.dto.MpgGenCodeDto;
import com.github.davidfantasy.mybatisplus.generatorui.service.MbpGeneratorService;
import com.github.davidfantasy.mybatisplus.generatorui.utils.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;

@RestController
@RequestMapping({"/api/mbp-generator"})
public class MbpGeneratorController {
    @Autowired
    private MbpGeneratorService mbpGeneratorService;

    /**
     * 接收/zxt-gen-code路径下的POST请求，生成代码并返回压缩包文件
     *
     * @param dto      MpgGenCodeDto对象，包含生成代码的配置信息
     * @param response 响应对象
     * @throws IOException 抛出IO异常
     */
    @PostMapping("/zxt-gen-code")
    public void genCode(@RequestBody MpgGenCodeDto dto, HttpServletResponse response) throws IOException {
        // 获取根路径
        //不使用入参传的地址了，使用默认地址
        dto.getGenSetting().setRootPath("D:\\pojects\\zxtGenerator\\code");
        String rootPath = dto.getGenSetting().getRootPath();
        // 创建目录对象
        File directory = new File(rootPath);

        // 如果目录不存在，则创建目录
        if (!directory.exists()) {
            directory.mkdirs();
        }

        // 生成代码
        mbpGeneratorService.genCodeBatch(dto.getGenSetting(), dto.getTables());

        // 压缩文件的路径 TODO linux时，需指定一个路径
        String zipDirectory = "D:\\pojects\\zxtGenerator";
        File zipDirectoryFile = new File(zipDirectory);

        // 如果压缩文件路径不存在，则创建压缩文件路径
        if (!zipDirectoryFile.exists()) {
            zipDirectoryFile.mkdirs();
        }

        // 压缩包名称
        String fileName = "code.zip";
        String zipFilePath = zipDirectory + fileName;

        // 将生成的代码打包成压缩包
        FileUtils.createZipFromDir(rootPath, zipFilePath);

        // 压缩包的字节，用来返回字节流
        byte[] zipBytes = FileUtils.getFileBytes(zipFilePath);

        // 设置响应头信息，触发浏览器下载
        response.setContentType("application/zip");
        response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(fileName, "UTF-8"));
        response.setContentLength(zipBytes.length);

        // 将压缩文件写入HTTP响应中
        ServletOutputStream outputStream = response.getOutputStream();
        outputStream.write(zipBytes);
        outputStream.flush();
        outputStream.close();

        // 删除下载的代码和压缩包
        FileUtils.deleteAllFilesInDir(zipDirectory);
        /*File file = new File(zipFilePath);
        file.delete();*/
    }


}
