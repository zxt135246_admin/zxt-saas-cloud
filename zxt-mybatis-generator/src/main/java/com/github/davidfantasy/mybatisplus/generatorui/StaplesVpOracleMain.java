package com.github.davidfantasy.mybatisplus.generatorui;

import com.baomidou.mybatisplus.annotation.IdType;
import com.github.davidfantasy.mybatisplus.generatorui.mbp.NameConverter;

/**
 * 史泰博 VP oracle代码生成
 */

public class StaplesVpOracleMain {
    public static void main(String[] args) {
        GeneratorConfig config = GeneratorConfig.builder()
                //jdbc
                .jdbcUrl("jdbc:oracle:thin:@10.10.5.225:1521:TSCD")
                .userName("staples")
                .password("staples2323W")
                //驱动
                .driverClassName("oracle.jdbc.OracleDriver")
                // 数据库schema，POSTGRE_SQL,ORACLE,DB2类型的数据库需要指定
                // .schemaName("myBusiness")
                // 如果需要修改各类生成文件的默认命名规则，可自定义一个NameConverter实例，覆盖相应的名称转换方法：
                .idType(IdType.ASSIGN_ID)
                .nameConverter(new NameConverter() {
                    /**
                     * 自定义Service类文件的名称规则
                     */
                    public String serviceNameConvert(String tableName) {
                        return this.entityNameConvert(tableName) + "Service";
                    }

                    /**
                     * 自定义Controller类文件的名称规则
                     */
                    public String controllerNameConvert(String tableName) {
                        return this.entityNameConvert(tableName) + "Controller";
                    }
                }).basePackage("cn.stbchina.bmp.basic").port(8068).build();

        MybatisPlusToolsApplication.run(config);

    }
}
