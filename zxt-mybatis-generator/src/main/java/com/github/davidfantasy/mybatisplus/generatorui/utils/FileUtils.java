package com.github.davidfantasy.mybatisplus.generatorui.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class FileUtils {
    /**
     * 将生成的代码打压缩包
     *
     * @param sourceDirPath     源目录路径
     * @param targetZipFilePath 目标压缩文件路径
     * @throws IOException 抛出IOException异常，当文件操作出现错误时
     */
    public static void createZipFromDir(String sourceDirPath, String targetZipFilePath) throws IOException {
        try (FileOutputStream fos = new FileOutputStream(targetZipFilePath);
             ZipOutputStream zos = new ZipOutputStream(fos)) {
            Files.walk(Paths.get(sourceDirPath))
                    .filter(Files::isRegularFile)
                    .forEach(filePath -> {
                        Path relativePath = Paths.get(sourceDirPath).relativize(filePath);
                        ZipEntry zipEntry = new ZipEntry(relativePath.toString());
                        try {
                            zos.putNextEntry(zipEntry);
                            Files.copy(filePath, zos);
                            zos.closeEntry();
                        } catch (IOException e) {
                            System.err.println("Error adding file to ZIP: " + filePath);
                        }
                    });
        }
    }


    /**
     * 删除指定目录下的所有文件
     *
     * @param dirPath 目录的路径
     */
    public static void deleteAllFilesInDir(String dirPath) {
        File directory = new File(dirPath);

        // 确保路径指向一个存在的目录
        if (directory.exists() && directory.isDirectory()) {
            // 获取目录下的所有文件和子目录列表
            File[] files = directory.listFiles();

            if (files != null) {
                // 遍历并删除每个文件或子目录
                for (File file : files) {
                    // 如果是文件，则直接删除
                    if (file.isFile()) {
                        file.delete();
                    }
                    // 如果是目录，则递归删除该目录中的所有文件及子目录
                    else if (file.isDirectory()) {
                        deleteAllFilesInDir(file.getAbsolutePath());
                        // 删除完子目录中的所有内容后，删除子目录
                        file.delete();
                    }
                }
            }
        } else {
            System.out.println("提供的路径不是一个有效的目录。");
        }
    }


    /**
     * 获取指定文件的字节内容
     *
     * @param filePath 文件路径
     * @return 文件的字节内容
     * @throws IOException 如果文件过大或无法完整读取文件
     */
    public static byte[] getFileBytes(String filePath) throws IOException {
        File file = new File(filePath);
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(file);
            long fileLength = file.length();
            if (fileLength > Integer.MAX_VALUE) {
                throw new IOException("文件过大");
            }

            // 创建一个数组来存储数据
            byte[] bytes = new byte[(int) fileLength];

            // 读取字节
            int offset = 0;
            int numRead = 0;
            while (offset < bytes.length && (numRead = fis.read(bytes, offset, bytes.length - offset)) >= 0) {
                offset += numRead;
            }

            // 确保所有字节都已读取
            if (offset < bytes.length) {
                throw new IOException("无法完整读取文件 " + file.getName());
            }

            return bytes;

        } finally {
            // 关闭文件输入流
            if (fis != null) {
                fis.close();
            }
        }
    }


}
