package com.github.davidfantasy.mybatisplus.generatorui;

import com.baomidou.mybatisplus.annotation.IdType;
import com.github.davidfantasy.mybatisplus.generatorui.mbp.NameConverter;

public class GeneratorMain {
    public static void main(String[] args) {
        GeneratorConfig config = GeneratorConfig.builder()
                //jdbc
                .jdbcUrl("jdbc:mysql://47.92.119.67:7000/zxt3")
                .userName("root")
                .password("txz135246.")
                //驱动
                .driverClassName("com.mysql.cj.jdbc.Driver")
                // 数据库schema，POSTGRE_SQL,ORACLE,DB2类型的数据库需要指定
                // .schemaName("myBusiness")
                // 如果需要修改各类生成文件的默认命名规则，可自定义一个NameConverter实例，覆盖相应的名称转换方法：
                .idType(IdType.AUTO)
                .nameConverter(new NameConverter() {
                    /**
                     * 自定义实体类文件的名称规则
                     */
                    public String entityNameConvert(String tableName) {
                        // 去除表名前的前缀
                        /*if (tableName.startsWith("gc_")) {
                            return tableName.substring(3);
                        }*/
                        // 将表名转换为驼峰命名
                        return underlineToCamel(tableName);
                    }
                    /**
                     * 自定义Service类文件的名称规则
                     */
                    public String serviceNameConvert(String tableName) {
                        return this.entityNameConvert(tableName) + "Service";
                    }

                    /**
                     * 自定义Controller类文件的名称规则
                     */
                    public String controllerNameConvert(String tableName) {
                        return this.entityNameConvert(tableName) + "Controller";
                    }
                }).basePackage("cn.stbchina.bmp.mdm.product").port(8068).build();

        MybatisPlusToolsApplication.run(config);

    }

    /**
     * 将下划线命名转换为驼峰命名
     */
    private static String underlineToCamel(String underlineStr) {
    if (underlineStr == null || underlineStr.isEmpty()) {
        return underlineStr;
    }

    StringBuilder camelStr = new StringBuilder();
    boolean nextUpperCase = false;
    boolean firstChar = true;

    for (char c : underlineStr.toCharArray()) {
        if (c == '_') {
            nextUpperCase = true;
        } else {
            if (nextUpperCase) {
                camelStr.append(Character.toUpperCase(c));
                nextUpperCase = false;
            } else {
                if (firstChar) {
                    camelStr.append(Character.toUpperCase(c));
                    firstChar = false;
                } else {
                    camelStr.append(Character.toLowerCase(c));
                }
            }
        }
    }
    return camelStr.toString();
}



}
