package com.zxt.web.container;


import com.alibaba.ttl.TransmittableThreadLocal;
import com.zxt.bean.model.UserInfoVO;

/**
 * 用户容器
 */
public class UserContainer {

    /**
     * 用户信息线程
     */
    public static final TransmittableThreadLocal<UserInfoVO> USER_INFO_VO_THREAD_LOCAL = new TransmittableThreadLocal<>();

    /**
     * 设置用户信息
     *
     * @param userInfoVO
     */
    public static void putUserInfo(UserInfoVO userInfoVO) {
        USER_INFO_VO_THREAD_LOCAL.set(userInfoVO);
    }

    /**
     * 移除用户信息
     */
    public static void removeUserInfo() {
        USER_INFO_VO_THREAD_LOCAL.remove();
    }


}
