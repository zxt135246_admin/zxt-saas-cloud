package com.zxt.web.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.MultipartConfigFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.servlet.MultipartConfigElement;

/**
 * @author zxt
 */
@Configuration
public class ServerConfig {
    @Value("${server.file.tmp.basedir:/data/upload}")
    private String tmp;

    public ServerConfig() {
    }

    @Bean
    MultipartConfigElement multipartConfigElement() {
        MultipartConfigFactory factory = new MultipartConfigFactory();
        factory.setLocation(this.tmp);
        return factory.createMultipartConfig();
    }
}
