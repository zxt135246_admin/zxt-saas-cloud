package com.zxt.web.config;


import com.zxt.web.server.UndertowServerFactoryCustomizer;
import io.undertow.Undertow;
import io.undertow.UndertowOptions;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.web.servlet.ServletWebServerFactoryAutoConfiguration;
import org.springframework.boot.web.embedded.undertow.UndertowBuilderCustomizer;
import org.springframework.boot.web.embedded.undertow.UndertowServletWebServerFactory;
import org.springframework.boot.web.server.WebServerFactoryCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration(
        proxyBeanMethods = false
)
@ConditionalOnClass({Undertow.class})
@AutoConfigureBefore({ServletWebServerFactoryAutoConfiguration.class})
public class UndertowHttp2Configuration {
    public UndertowHttp2Configuration() {
    }

    @Bean
    public WebServerFactoryCustomizer<UndertowServletWebServerFactory> undertowHttp2WebServerFactoryCustomizer() {
        return (factory) -> {
            factory.addBuilderCustomizers(new UndertowBuilderCustomizer[]{(builder) -> {
                builder.setServerOption(UndertowOptions.ENABLE_HTTP2, true);
            }});
        };
    }

    @Bean
    public UndertowServerFactoryCustomizer undertowServerFactoryCustomizer() {
        return new UndertowServerFactoryCustomizer();
    }
}
