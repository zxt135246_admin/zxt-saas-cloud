package com.zxt.web.config;

import com.zxt.bean.constants.ThreadNameConstant;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.ThreadPoolExecutor;

@Configuration
@EnableAsync
@Slf4j
public class ThreadPoolConfig {


    /**
     * 公共线程池,利用系统availableProcessors线程数量进行计算
     */
    @Bean(name = ThreadNameConstant.ASYNC_SERVICE_EXECUTOR)
    public ThreadPoolTaskExecutor asyncServiceExecutor() {
        log.info("init asyncServiceExecutor......");
        ThreadPoolTaskExecutor pool = new ThreadPoolTaskExecutor();
        int processNum = Runtime.getRuntime().availableProcessors(); // 返回可用处理器的Java虚拟机的数量
        // 配置核心线程数 核心池大小
        int corePoolSize = (int) (processNum / (1 - 0.2));
        pool.setCorePoolSize(corePoolSize);
        int maxPoolSize = (int) (processNum / (1 - 0.5));
        // 配置最大线程数 最大线程数
        pool.setMaxPoolSize(maxPoolSize);
        // 队列程度 队列大小
        pool.setQueueCapacity(maxPoolSize * 1000);
        pool.setThreadPriority(Thread.MAX_PRIORITY);
        pool.setDaemon(false);
        pool.setKeepAliveSeconds(300);// 线程空闲时间
        // rejection-policy：当pool已经达到max size的时候，如何处理新任务
        // CALLER_RUNS：不在新线程中执行任务，而是有调用者所在的线程来执行
        pool.setRejectedExecutionHandler(new ThreadPoolExecutor.DiscardPolicy());
        //设置线程池内线程名称的前缀-------阿里编码规约推荐--方便出错后进行调试
        pool.setThreadNamePrefix("async-executor-thread-");
        return pool;
    }
}
