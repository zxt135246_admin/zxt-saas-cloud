package com.zxt.web.config;

import com.zxt.web.interceptor.LoggingInterceptor;
import com.zxt.web.interceptor.RepeatSubmitInterceptor;
import com.zxt.web.interceptor.UserInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.web.servlet.config.annotation.ContentNegotiationConfigurer;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;


/**
 * @author zxt
 */
@Configuration
public class WebConfig implements WebMvcConfigurer {

    @Autowired
    private RepeatSubmitInterceptor repeatSubmitInterceptor;


    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new LoggingInterceptor());
        registry.addInterceptor(new UserInterceptor()).addPathPatterns(new String[]{"/**"}).excludePathPatterns(new String[]{"/doc.html", "/page/**", "/swagger-ui.html", "/swagger-resources/**", "/webjars/**", "/feign/**", "/*/api-docs", "/error"});
        registry.addInterceptor(repeatSubmitInterceptor);
    }

    public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
        configurer.enable();
    }

    public void configureContentNegotiation(ContentNegotiationConfigurer configurer) {
        configurer.defaultContentType(new MediaType[]{MediaType.APPLICATION_JSON});
    }
}