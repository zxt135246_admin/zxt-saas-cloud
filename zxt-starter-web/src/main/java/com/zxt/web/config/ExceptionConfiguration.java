package com.zxt.web.config;


import com.zxt.bean.factory.YamlPropertySourceFactory;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@ComponentScan({"com.zxt.web.handler"})
@PropertySource(
        factory = YamlPropertySourceFactory.class,
        value = {"classpath:mall-error.yml"}
)
public class ExceptionConfiguration {
    public ExceptionConfiguration() {
    }
}

