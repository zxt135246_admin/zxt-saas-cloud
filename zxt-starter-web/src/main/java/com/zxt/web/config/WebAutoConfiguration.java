package com.zxt.web.config;


import org.springframework.context.annotation.Import;


@Import({ExceptionConfiguration.class,
        DateConverterConfiguration.class,
        UndertowHttp2Configuration.class,
        WebConfig.class,
        FilterConfig.class,
        ThreadPoolConfig.class})
public class WebAutoConfiguration {
}
