package com.zxt.web.interceptor;

import com.alibaba.fastjson.JSON;
import com.zxt.bean.annotation.PassLogin;
import com.zxt.bean.model.UserInfoVO;
import com.zxt.bean.result.Result;
import com.zxt.bean.result.ResultCode;
import com.zxt.bean.utils.CollectionsUtil;
import com.zxt.bean.utils.http.HttpHelper;
import com.zxt.bean.utils.StringUtils;
import com.zxt.redis.utils.UserUtil;
import com.zxt.web.container.UserContainer;
import com.zxt.web.filter.RepeatedlyRequestWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Objects;

/**
 * @author zxt
 * 为每次请求设置ThreadLocal
 */
@Component
public class UserInterceptor implements HandlerInterceptor {
    private static final Logger log = LoggerFactory.getLogger(UserInterceptor.class);

    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        try {
            String nowParams = "";
            if (HttpHelper.isJsonRequest(request) && request instanceof RepeatedlyRequestWrapper) {
                nowParams = HttpHelper.getBodyStringByHutool(request);
            } else {
                if (CollectionsUtil.isNotEmpty(request.getParameterMap())) {
                    nowParams = JSON.toJSONString(request.getParameterMap());
                } else {
                    nowParams = "无参数";
                }
            }
            log.info("requestUri：{},contextPath：{},parameterNames：{}", new Object[]{request.getRequestURI(), request.getContextPath(), nowParams});
            /*if (this.supports(handler)) {
                this.setUserInfo(request);
                return true;
            } else {
                return Boolean.TRUE;
            }*/
            this.setUserInfo(request);
            return Boolean.TRUE;
        } catch (Exception var5) {
            log.error(var5.getMessage(), var5);
            this.returnErrorJson(response, JSON.toJSONString(Result.newError(ResultCode.SYS_ERROR)));
            return Boolean.FALSE;
        }
    }

    private void setUserInfo(HttpServletRequest request) {
        String token = UserUtil.getToken();
        if (StringUtils.isNotBlank(token)) {
            UserInfoVO userInfoVO = UserUtil.getUser(token);
            if (Objects.nonNull(userInfoVO)) {
                UserContainer.putUserInfo(userInfoVO);
            }
        }
    }

    private boolean supports(Object object) throws Exception {
        if (object instanceof HandlerMethod) {
            HandlerMethod method = (HandlerMethod) object;
            return method.getMethodAnnotation(PassLogin.class) == null;
        } else {
            return false;
        }
    }

    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
    }

    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        UserContainer.removeUserInfo();
    }

    public void returnErrorJson(HttpServletResponse response, String json) throws Exception {
        PrintWriter writer = null;
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json;charset=utf-8");

        try {
            writer = response.getWriter();
            writer.print(json);
        } catch (IOException var8) {
            log.error("response error", var8);
        } finally {
            if (writer != null) {
                writer.close();
            }

        }

    }

    public UserInterceptor() {
    }
}