package com.zxt.web.interceptor;


import com.zxt.bean.utils.UuIdUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.core.NamedThreadLocal;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class LoggingInterceptor implements HandlerInterceptor {
    private static final Logger LOGGER = LoggerFactory.getLogger(LoggingInterceptor.class);
    private static ThreadLocal<Long> starTimeThreadLocal = new NamedThreadLocal("ThreadLocal StartTime");

    public LoggingInterceptor() {
    }

    public boolean preHandle(HttpServletRequest request, HttpServletResponse httpServletResponse, Object o) throws Exception {
        starTimeThreadLocal.set(System.currentTimeMillis());
        String traceId = request.getHeader("x-traceId-header");
        if (StringUtils.isEmpty(traceId)) {
            traceId = UuIdUtils.randomUUID(Boolean.TRUE).toUpperCase();
        }

        MDC.put("traceId", traceId);
        return true;
    }

    public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, ModelAndView modelAndView) throws Exception {
    }

    public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) throws Exception {
        String ip = httpServletRequest.getRemoteAddr();
        String uri = httpServletRequest.getRequestURI();
        String method = httpServletRequest.getMethod();
        String queryStr = httpServletRequest.getQueryString();
        if (StringUtils.isEmpty(queryStr)) {
            queryStr = "";
        }

        String protocol = httpServletRequest.getProtocol();
        int status = httpServletResponse.getStatus();
        long timeUsed = System.currentTimeMillis() - (Long)starTimeThreadLocal.get();
        LOGGER.info("{} \"{} {} {} {}\" {} {}ms", new Object[]{ip, method, uri, queryStr, protocol, status, timeUsed});
        starTimeThreadLocal.remove();
        MDC.remove("traceId");
    }
}
