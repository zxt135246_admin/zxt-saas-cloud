package com.zxt.web.interceptor.impl;

import com.alibaba.fastjson.JSON;
import com.zxt.bean.constants.ApiConstant;
import com.zxt.bean.constants.CacheConstants;
import com.zxt.bean.utils.CollectionsUtil;
import com.zxt.bean.utils.http.HttpHelper;
import com.zxt.bean.utils.StringUtils;
import com.zxt.redis.utils.RedisUtil;
import com.zxt.web.annotation.RepeatSubmit;
import com.zxt.web.filter.RepeatedlyRequestWrapper;
import com.zxt.web.interceptor.RepeatSubmitInterceptor;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.support.StandardMultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * 判断请求url和数据是否和上一次相同，
 * 如果和上次相同，则是重复提交表单。 有效时间为10秒内。
 *
 * @author zxt
 */
@Component
public class SameUrlDataInterceptor extends RepeatSubmitInterceptor {
    public final String REPEAT_PARAMS = "repeatParams";

    public final String REPEAT_TIME = "repeatTime";

    @SuppressWarnings("unchecked")
    @Override
    public boolean isRepeatSubmit(HttpServletRequest request, RepeatSubmit annotation) {
        String nowParams = "";
        if (HttpHelper.isJsonRequest(request) && request instanceof RepeatedlyRequestWrapper) {
            // application/json 请求方式
            nowParams = HttpHelper.getBodyStringByHutool(request);
        } else if (request instanceof StandardMultipartHttpServletRequest) {
            // 文件请求方式
            Map<String, MultipartFile> fileMap = HttpHelper.getFileMap(request);
            if (CollectionsUtil.isNotEmpty(fileMap)) {
                nowParams = fileMap.values().stream().map(MultipartFile::getOriginalFilename).collect(Collectors.joining(","));
            } else {
                //没有上传文件，以请求头入参为准
                if (CollectionsUtil.isNotEmpty(request.getParameterMap())) {
                    nowParams = JSON.toJSONString(request.getParameterMap());
                } else {
                    nowParams = "无参数";
                }
            }
        } else {
            //取请求头
            if (CollectionsUtil.isNotEmpty(request.getParameterMap())) {
                nowParams = JSON.toJSONString(request.getParameterMap());
            } else {
                nowParams = "无参数";
            }
        }

        Map<String, Object> nowDataMap = new HashMap<>(2);
        nowDataMap.put(REPEAT_PARAMS, nowParams);
        nowDataMap.put(REPEAT_TIME, System.currentTimeMillis());

        // 请求地址（作为存放cache的key值）
        String url = request.getRequestURI();

        // 唯一值（没有消息头则使用请求地址）
        String submitKey = StringUtils.trimToEmpty(request.getHeader(ApiConstant.HEADER_KEY));

        // 唯一标识（指定key + url + 消息头）
        String cacheRepeatKey = CacheConstants.REPEAT_SUBMIT_KEY + url + submitKey;

        Object sessionObj = RedisUtil.get(cacheRepeatKey);
        if (sessionObj != null) {
            Map<String, Object> sessionMap = (Map<String, Object>) sessionObj;
            if (sessionMap.containsKey(url)) {
                Map<String, Object> preDataMap = (Map<String, Object>) sessionMap.get(url);
                if (compareParams(nowDataMap, preDataMap) && compareTime(nowDataMap, preDataMap, annotation.interval())) {
                    return true;
                }
            }
        }

        Map<String, Object> cacheMap = new HashMap<>(1);
        cacheMap.put(url, nowDataMap);
        RedisUtil.set(cacheRepeatKey, cacheMap, TimeUnit.MILLISECONDS, annotation.interval());
        return false;
    }

    /**
     * 判断参数是否相同
     */
    private boolean compareParams(Map<String, Object> nowMap, Map<String, Object> preMap) {
        String nowParams = (String) nowMap.get(REPEAT_PARAMS);
        String preParams = (String) preMap.get(REPEAT_PARAMS);
        return nowParams.equals(preParams);
    }

    /**
     * 判断两次间隔时间
     */
    private boolean compareTime(Map<String, Object> nowMap, Map<String, Object> preMap, int interval) {
        long time1 = (Long) nowMap.get(REPEAT_TIME);
        long time2 = (Long) preMap.get(REPEAT_TIME);
        if ((time1 - time2) < interval) {
            return true;
        }
        return false;
    }
}
