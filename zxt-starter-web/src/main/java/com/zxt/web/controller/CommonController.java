package com.zxt.web.controller;

import com.alibaba.fastjson.JSON;
import com.zxt.bean.model.UserInfoVO;
import com.zxt.bean.utils.StringUtils;
import com.zxt.web.container.UserContainer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Objects;

public class CommonController {
    private static final Logger log = LoggerFactory.getLogger(CommonController.class);

    public CommonController() {
    }

    public HttpServletRequest getRequest() {
        return RequestContextHolder.currentRequestAttributes() == null ? null : ((ServletRequestAttributes)RequestContextHolder.currentRequestAttributes()).getRequest();
    }

    public HttpServletResponse getResponse() {
        return RequestContextHolder.getRequestAttributes() == null ? null : ((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getResponse();
    }

    public HttpSession getSession() {
        HttpServletRequest request = this.getRequest();
        HttpSession session = request == null ? null : request.getSession();
        return session;
    }

    protected UserInfoVO getUser() {
        UserInfoVO userInfoVO = (UserInfoVO) UserContainer.USER_INFO_VO_THREAD_LOCAL.get();
        if (!Objects.isNull(userInfoVO)) {
            return userInfoVO;
        } else {
            String userStr = this.getRequest().getHeader("Zxt-User");
            if (StringUtils.isNotEmpty(userStr)) {
                String decode;
                try {
                    decode = URLDecoder.decode(userStr, "UTF-8");
                } catch (UnsupportedEncodingException var5) {
                    log.error(var5.getMessage(), var5);
                    decode = userStr;
                }

                userInfoVO = (UserInfoVO) JSON.parseObject(decode, UserInfoVO.class);
                UserContainer.putUserInfo(userInfoVO);
                return userInfoVO;
            } else {
                return null;
            }
        }
    }

    protected String getBrowseEnvironment() {
        return this.getRequest().getHeader("Browse-Environment");
    }
}
