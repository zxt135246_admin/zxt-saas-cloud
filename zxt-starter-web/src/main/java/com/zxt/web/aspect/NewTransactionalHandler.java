/*
package com.zxt.web.aspect;

import com.zxt.web.annotation.NewTransactional;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Component;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;

*/
/**
 * @author zxt
 * 事务aop
 *//*

@Aspect
@Component
public class NewTransactionalHandler {

    private static final Logger log = LoggerFactory.getLogger(NewTransactionalHandler.class);

    */
/**
     * jdbc事务管理器
     *//*

    private final DataSourceTransactionManager dataSourceTransactionManager;
    */
/**
     * 定义事务属性
     *//*

    private final TransactionDefinition transactionDefinition;

    @Autowired
    public NewTransactionalHandler(DataSourceTransactionManager dataSourceTransactionManager, TransactionDefinition transactionDefinition) {
        this.dataSourceTransactionManager = dataSourceTransactionManager;
        this.transactionDefinition = transactionDefinition;
    }

    @Around("@annotation(newTransactional)")
    public Object around(ProceedingJoinPoint joinPoint, NewTransactional newTransactional) {
        log.info("[开始]执行NewTransactional环绕通知,开启线程事务");
        //开启事务
        TransactionStatus transactionStatus = dataSourceTransactionManager.getTransaction(transactionDefinition);
        //String value = newTransactional.value();
        try {
            Object result = joinPoint.proceed();
            //提交事务
            dataSourceTransactionManager.commit(transactionStatus);
            return result;
        } catch (Throwable t) {
            //回滚事务
            dataSourceTransactionManager.rollback(transactionStatus);
            log.error("[异常]执行NewTransactional环绕通知,执行失败,回滚线程事务", t);
        } finally {
            log.info("[完成]执行NewTransactional环绕通知,执行完成,关闭线程事务");
        }
        log.info("[结束]执行NewTransactional环绕通知");
        return null;
    }
}*/
