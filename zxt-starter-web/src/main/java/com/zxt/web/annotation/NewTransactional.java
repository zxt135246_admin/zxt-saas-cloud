package com.zxt.web.annotation;

import java.lang.annotation.*;

/**
 * 自定义事务注解
 *
 * @author zxt
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface NewTransactional {
    //String value() default "";
}
