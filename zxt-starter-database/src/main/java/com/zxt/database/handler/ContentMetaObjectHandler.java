package com.zxt.database.handler;


import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.zxt.bean.model.UserInfoVO;
import com.zxt.redis.utils.UserUtil;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.Objects;

@Component
public class ContentMetaObjectHandler implements MetaObjectHandler {

    private final static String CREATE_TIME = "createTime";
    private final static String UPDATE_TIME = "updateTime";
    private final static String CREATE_USER_ID = "createUserId";
    private final static String CREATE_NICK_NAME = "createNickName";

    public ContentMetaObjectHandler() {
    }

    public void insertFill(MetaObject metaObject) {
        LocalDateTime now = LocalDateTime.now();
        this.strictInsertFill(metaObject, CREATE_TIME, LocalDateTime.class, now);
        this.strictInsertFill(metaObject, UPDATE_TIME, LocalDateTime.class, now);
        UserInfoVO user = UserUtil.getUser(UserUtil.getToken());
        if (!Objects.isNull(user)) {
            this.strictInsertFill(metaObject, CREATE_USER_ID, Long.class, user.getId());
            this.strictInsertFill(metaObject, CREATE_NICK_NAME, String.class, user.getNickName());
        }
    }

    public void updateFill(MetaObject metaObject) {
        this.strictUpdateFill(metaObject, UPDATE_TIME, LocalDateTime.class, LocalDateTime.now());
    }
}
