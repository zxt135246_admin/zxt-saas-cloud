package com.zxt.database.config;


import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedJdbcTypes;
import org.apache.ibatis.type.MappedTypes;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Component
@MappedTypes({LocalDateTime.class})
@MappedJdbcTypes(
        value = {JdbcType.TIMESTAMP},
        includeNullJdbcType = true
)
public class CustomLocalDateTimeTypeHandler extends BaseTypeHandler<LocalDateTime> {
    private final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    public CustomLocalDateTimeTypeHandler() {
    }

    public void setNonNullParameter(PreparedStatement ps, int i, LocalDateTime parameter, JdbcType jdbcType) throws SQLException {
        if (parameter != null) {
            ps.setString(i, this.dateTimeFormatter.format(parameter));
        }
    }

    public LocalDateTime getNullableResult(ResultSet rs, String columnName) throws SQLException {
        String target = rs.getString(columnName);
        return ObjectUtils.isEmpty(target) ? null : LocalDateTime.parse(target, this.dateTimeFormatter);
    }

    public LocalDateTime getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
        String target = rs.getString(columnIndex);
        return ObjectUtils.isEmpty(target) ? null : LocalDateTime.parse(target, this.dateTimeFormatter);
    }

    public LocalDateTime getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
        String target = cs.getString(columnIndex);
        return ObjectUtils.isEmpty(target) ? null : LocalDateTime.parse(target, this.dateTimeFormatter);
    }
}
