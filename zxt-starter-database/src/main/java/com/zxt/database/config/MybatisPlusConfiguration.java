package com.zxt.database.config;

import com.baomidou.mybatisplus.autoconfigure.ConfigurationCustomizer;
import com.baomidou.mybatisplus.core.MybatisConfiguration;
import com.baomidou.mybatisplus.core.incrementer.IdentifierGenerator;
import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.BlockAttackInnerInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.OptimisticLockerInnerInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import com.zxt.bean.factory.YamlPropertySourceFactory;
import com.zxt.bean.utils.IDGenerator;
import com.zxt.database.handler.ContentMetaObjectHandler;
import org.apache.ibatis.type.EnumTypeHandler;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;


@Configuration
@PropertySource(
        factory = YamlPropertySourceFactory.class,
        value = {"classpath:mall-db.yml"}
)
@MapperScan(
        basePackages = {"com.zxt.**.dao"}
)
public class MybatisPlusConfiguration {
    private static final Long MAX_LIMIT = 50000L;


    /**
     * 主键生成使用雪花算法
     *
     * @return
     */
    @Bean
    public IdentifierGenerator idGenerator() {
        return new IDGenerator();
    }

    @Bean
    public MybatisPlusInterceptor paginationInterceptor() {
        MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
        // 分页插件
        PaginationInnerInterceptor paginationInnerInterceptor = new PaginationInnerInterceptor();
        paginationInnerInterceptor.setMaxLimit(MAX_LIMIT);
        interceptor.addInnerInterceptor(paginationInnerInterceptor);
        // 防止全表更新与删除
        BlockAttackInnerInterceptor blockAttackInnerInterceptor = new BlockAttackInnerInterceptor();
        interceptor.addInnerInterceptor(blockAttackInnerInterceptor);
        // 乐观锁
        interceptor.addInnerInterceptor(new OptimisticLockerInnerInterceptor());
        return interceptor;
    }

    @Bean
    @ConditionalOnMissingBean({ContentMetaObjectHandler.class})
    public ContentMetaObjectHandler mateMetaObjectHandler() {
        return new ContentMetaObjectHandler();
    }

    @Bean
    public ConfigurationCustomizer configurationCustomizer() {
        return new MybatisPlusCustomizers();
    }

    public MybatisPlusConfiguration() {
    }

    public static class MybatisPlusCustomizers implements ConfigurationCustomizer {
        public MybatisPlusCustomizers() {
        }

        public void customize(MybatisConfiguration configuration) {
            configuration.setDefaultEnumTypeHandler(EnumTypeHandler.class);
        }
    }
}