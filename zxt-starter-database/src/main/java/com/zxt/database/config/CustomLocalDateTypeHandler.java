package com.zxt.database.config;


import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedJdbcTypes;
import org.apache.ibatis.type.MappedTypes;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Component
@MappedTypes({LocalDate.class})
@MappedJdbcTypes(
        value = {JdbcType.DATE},
        includeNullJdbcType = true
)
public class CustomLocalDateTypeHandler extends BaseTypeHandler<LocalDate> {
    private final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    public CustomLocalDateTypeHandler() {
    }

    public void setNonNullParameter(PreparedStatement ps, int i, LocalDate parameter, JdbcType jdbcType) throws SQLException {
        if (parameter != null) {
            ps.setString(i, this.dateTimeFormatter.format(parameter));
        }
    }

    public LocalDate getNullableResult(ResultSet rs, String columnName) throws SQLException {
        String target = rs.getString(columnName);
        return ObjectUtils.isEmpty(target) ? null : LocalDate.parse(target, this.dateTimeFormatter);
    }

    public LocalDate getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
        String target = rs.getString(columnIndex);
        return ObjectUtils.isEmpty(target) ? null : LocalDate.parse(target, this.dateTimeFormatter);
    }

    public LocalDate getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
        String target = cs.getString(columnIndex);
        return ObjectUtils.isEmpty(target) ? null : LocalDate.parse(target, this.dateTimeFormatter);
    }
}
