window.addEventListener('load', function () {
    var focus = document.querySelector('.focus');
    var ul = focus.children[0];
    var ol = focus.children[1];
    var w = focus.offsetWidth;
    var index = 0;
    var timer = setInterval(function () {
        index++;
        var transformX = -index * w;
        ul.style.transition = 'All .3s';
        ul.style.transform = 'translateX(' + transformX + 'px)';
    }, 2000);

    ul.addEventListener('transitionend', function () {
        if (index >= 3) {
            ul.style.transition = 'none';
            index = 0;
            var transformX = -index * w;
            ul.style.transform = 'translateX(' + transformX + 'px)';
        } else if (index < 0) {
            ul.style.transition = 'none';
            index = 2;
            var transformX = -index * w;
            ul.style.transform = 'translateX(' + transformX + 'px)';
        }
        ol.querySelector('.current').classList.remove('current');
        ol.children[index].classList.add('current');
    })
    var startX = 0;
    var moveX = 0;
    var flag = false;
    ul.addEventListener('touchstart', function (e) {
        startX = e.targetTouches[0].pageX;
        clearInterval(timer);
        flag = true;
    })
    ul.addEventListener('touchmove', function (e) {
        moveX = e.targetTouches[0].pageX - startX;
        ul.style.transition = 'none';
        var transformX = -index * w + moveX;
        ul.style.transform = 'translateX(' + transformX + 'px)';
        e.preventDefault();
    })

    ul.addEventListener('touchend', function (e) {
        if (flag == true) {
            if (Math.abs(moveX) > 50) {
                moveX > 0 ? index-- : index++;
                ul.style.transition = 'All .3s';
                var transformX = -index * w;
                ul.style.transform = 'translateX(' + transformX + 'px)';
            } else {
                ul.style.transition = 'All .1s';
                var transformX = -index * w;
                ul.style.transform = 'translateX(' + transformX + 'px)';
            }
            clearInterval(timer);
            timer = setInterval(function () {
                index++;
                var transformX = -index * w;
                ul.style.transition = 'All .3s';
                ul.style.transform = 'translateX(' + transformX + 'px)';
            }, 2000);
        }
        flag = false;
    })
    var nav = document.querySelector('nav');
    var goBack = document.querySelector('.goBack');
    window.addEventListener('scroll', function () {
        if (window.pageYOffset >= nav.offsetTop) {
            goBack.style.display = 'block';
        } else {
            goBack.style.display = 'none';
        }
    })

    goBack.addEventListener('click', function () {
        window.scroll(0, 0);
    })


    //初始化数据
    init();

})

//处理回显数据
function dealData(res) {
    document.getElementById("content-body").innerHTML +=
        "<div style='background: linear-gradient(to right, rgb(221,172,201), rgb(190,159,217));\n" +
        "margin: 15px;' class=\"row\">\n" +
        "                <a href=\"detail.html?id=" + res.id + "\">\n" +
        res.content +
        "                    <br><br>\n" +
        "                    <beizhu>作者:" + res.nickName + "&nbsp;&nbsp;&nbsp;类型:" + res.typeName + "&nbsp;&nbsp;&nbsp;评论数:" + res.commentsNum + "&nbsp;&nbsp;&nbsp;时间:" + res.createTime.substr(0, 10) + "&nbsp;&nbsp;&nbsp;热度:" + res.contentHeat + "🔥</beizhu>\n" +
        "                </a>\n" +
        "            </div>";
}

function init() {


    /*初始化文案*/
    var queryContentDTO = {
        pageNo: 1,
        pageSize: 50000
    }
    $.ajax({
        type: 'post',
        url: "http://101.35.246.250/content/publicContent/query",
        data: JSON.stringify(queryContentDTO),
        async: false,//同步/异步
        contentType: "application/json",
        dataType: 'json', //返回 JSON 数据
        success: function (data, status) {
            if (data.code == "200") {
                for (let inData in data.data.records) {
                    let res = data.data.records[inData];
                    dealData(res);
                }
            } else {
                swal(data.message);
            }
        },
        error: function (data, status, e) {
            alert('接口调用错误！');
        }
    });

    var queryTypeDTO = {
        pageNo: 1,
        pageSize: 6
    }
    /*初始化文案类型*/
    $.ajax({
        type: 'post',
        url: "http://101.35.246.250/content/contentType/query",
        //data: JSON.stringify(opt),
        data: JSON.stringify(queryTypeDTO),
        async: false,//同步/异步
        contentType: "application/json",
        dataType: 'json', //返回 JSON 数据
        success: function (data, status) {
            if (data.code == "200") {
                var html = "";
                var go = true;
                for (let inData in data.data.records) {
                    let res = data.data.records[inData];
                    if (go == true) {
                        html += "<div class=\"nav-items\"><a display:block onclick=\"queryContent('" + res.id + "',null)\" >" + res.typeName + "</a>";
                        go = false;
                    } else {
                        html += "<a display:block onclick=\"queryContent('" + res.id + "',null)\" >" + res.typeName + "</a></div>";
                        document.getElementById("content-type-body").innerHTML += html;
                        html = "";
                        go = true;
                    }
                }
            } else {
                swal(data.message);
            }
        },
        error: function (data, status, e) {
            alert('接口调用错误！');
        }
    });

    var token = sessionStorage.getItem("Authorization");
    if (token != null) {
        /*初始化消息数量*/
        var queryContentDTO = {
            pageNo: 1,
            pageSize: 50000,
            isRead: false
        }
        var zxtUser = sessionStorage.getItem("Zxt-User");
        $.ajax({
            type: 'post',
            url: "http://101.35.246.250/operation/systemNews/query",
            headers: {'Authorization': token, 'Zxt-User': zxtUser},
            data: JSON.stringify(queryContentDTO),
            async: false,//同步/异步
            contentType: "application/json",
            dataType: 'json', //返回 JSON 数据
            success: function (data, status) {
                if (data.code == "601") {
                    sessionStorage.removeItem("Authorization");
                    swal(data.message + ",3秒后自动跳转登录页面");
                    setTimeout(function () {
                        window.location.href = "../login.html"
                    }, 3000);
                } else if (data.code == "200") {
                    var size = data.data.records.length;
                    if (size == 0) {
                        document.getElementById("newsNum").remove();
                    } else {
                        document.getElementById("newsNum").innerText = size;
                    }
                } else {
                    swal(data.message);
                }
            },
            error: function (data, status, e) {
                alert('接口调用错误！');
            }
        });
    }

    $.ajax({
        type: 'post',
        url: "http://101.35.246.250/user/user/getAllUser",
        headers: {'Authorization': token},
        data: '{}',
        async: false,//同步/异步
        contentType: "application/json",
        dataType: 'json', //返回 JSON 数据
        success: function (data, status) {
            if (data.code == "200") {
                console.log(data.data);
                for (let inData in data.data) {
                    let res = data.data[inData];
                    console.log(res);
                    document.getElementById("userBody").innerHTML+="<li><a onclick=\"queryContent(null,'"+res.id+"')\" href=\"#\">\n" +
                        "        <!--<span class=\"subnav-entry-icon-icon1\"></span>-->\n" +
                        "        <img style=\"border-radius:50%\" width=\"30px\"\n" +
                        "             src=\""+res.avatarUrl+"\">\n" +
                        "        <span>"+res.nickName+"</span>\n" +
                        "    </a></li>"

                }
            } else if (data.code == "601") {
                sessionStorage.removeItem("Authorization");
                swal(data.message + ",3秒后自动跳转登录页面");
                setTimeout(function () {
                    window.location.href = "../login.html"
                }, 3000);
            } else {
                swal(data.message);
            }
        },
        error: function (data, status, e) {
            alert('接口调用错误！');
        }
    });
}

function my() {
    if (checkLogin()) {
        //通过校验登录
        window.location.href = "center.html"
    }
}

function goNews() {
    if (checkLogin()) {
        //通过校验登录
        window.location.href = "../news.html"
    }
}

function queryContent(contentTypeId,userId) {
    var queryContentDTO = {
        pageNo: 1,
        pageSize: 50000,
        contentTypeId: contentTypeId,
        userId: userId,
    }
    $.ajax({
        type: 'post',
        url: "http://101.35.246.250/content/publicContent/query",
        data: JSON.stringify(queryContentDTO),
        async: false,//同步/异步
        contentType: "application/json",
        dataType: 'json', //返回 JSON 数据
        success: function (data, status) {
            if (data.code == "200") {

                document.getElementById("content-body").innerHTML = "";
                for (let inData in data.data.records) {
                    let res = data.data.records[inData]
                    dealData(res);
                }
            } else {
                swal(data.message);
            }
        },
        error: function (data, status, e) {
            alert('接口调用错误！');
        }
    });
}

//校验是否登录
function checkLogin() {
    var token = sessionStorage.getItem("Authorization");
    if (token == null) {
        swal("未登录,3秒后自动跳转前往登录");
        setTimeout(function () {
            window.location.href = "../login.html"
        }, 3000);
        return false;
    } else {
        return true;
    }
}

function upload() {
    if (checkLogin()) {
        //通过校验登录
        window.location.href = "upload.html"
    }
}


function privateContent() {
    if (checkLogin()) {
        //通过校验登录
        window.location.href = "privateContent.html"
    }
}

function goImages() {
    if (checkLogin()) {
        //通过校验登录
        window.location.href = "../image/list.html"
    }
}