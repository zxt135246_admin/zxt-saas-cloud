// 动画
const siginBtn = document.getElementById('signin')
const sigupBtn = document.getElementById('signup')
const fistForm = document.getElementById('from1')
const secondForm = document.getElementById('from2')
const container = document.querySelector(".container")


siginBtn.addEventListener('click', (e) => {
    container.classList.remove("right-panel-active")
})

sigupBtn.addEventListener('click', (e) => {
    container.classList.add("right-panel-active")
})
fistForm.addEventListener("submit", (e) => {
    return e.preventDefault()
})
secondForm.addEventListener('submit', (e) => {
    return e.preventDefault()
})

// 表单验证
// 获取输入框
let un = document.getElementById('un')
let ue = document.getElementById('ue')
let upwd = document.getElementById('upwd')

let inun = document.getElementById('inun')
let inupwd = document.getElementById('loginPassword')
// 获取提示文字
let userName = document.getElementById('userName')
let userEmail = document.getElementById('userEmail')
let userPassword = document.getElementById('userPassword')
let inUserName = document.getElementById('inUserName')
let inUserPwd = document.getElementById('inUserPwd')

var formValiad = {
    // 用户名验证  不少于6个字符
    checkUserName: function () {
        var pattern = /^\w{6,}$/
        if (un.value.length == 0) {
            userName.style.visibility = 'visible'
            userName.innerText = '用户名不能为空'
            userName.style.color = 'red'
            return false
        }
        if (!pattern.test(un.value)) {
            userName.style.visibility = 'visible'
            userName.innerText = '用户名不合法'
            userName.style.color = 'red'
            return false
        } else {
            userName.style.visibility = 'visible'
            userName.innerText = '合法'
            userName.style.color = 'green'
            return true
        }
    },
    // 邮箱验证
    checkUserEmail: function () {
        var pattern = /^[0-9a-zA-Z]+([\.\-_]*[0-9a-zA-Z]+)*@([0-9a-zA-Z]+[\-_]*[0-9a-zA-Z]+\.)+[0-9a-zA-Z]{2,6}$/
        if (ue.value.length == 0) {
            userEmail.style.visibility = 'visible'
            userEmail.innerText = '邮箱不能为空'
            userEmail.style.color = 'red'
            return false
        }
        if (!pattern.test(ue.value)) {
            userEmail.style.visibility = 'visible'
            userEmail.innerText = '邮箱不合法'
            userEmail.style.color = 'red'
            return false
        } else {
            userEmail.style.visibility = 'visible'
            userEmail.innerText = '合法'
            userEmail.style.color = 'green'
            return true
        }
    },
    // 密码验证
    checkUserPassword: function () {
        var pattern = /^\w{6,18}$/
        if (upwd.value.length == 0) {
            userPassword.style.visibility = 'visible'
            userPassword.innerText = '密码不能为空'
            userPassword.style.color = 'red'
            return false
        }
        userPassword.style.visibility = 'visible'
        userPassword.innerText = '合法'
        userPassword.style.color = 'green'
        return true
    },
    // 登录框 邮箱验证
    checkInUserEmail: function () {
        var pattern = /^[0-9a-zA-Z]+([\.\-_]*[0-9a-zA-Z]+)*@([0-9a-zA-Z]+[\-_]*[0-9a-zA-Z]+\.)+[0-9a-zA-Z]{2,6}$/
        if (inun.value.length == 0) {
            inUserName.style.visibility = 'visible'
            inUserName.innerText = '邮箱不能为空'
            inUserName.style.color = 'red'
            return false
        }
        if (!pattern.test(inun.value)) {
            inUserName.style.visibility = 'visible'
            inUserName.innerText = '邮箱不合法'
            inUserName.style.color = 'red'
            return false
        } else {
            inUserName.style.visibility = 'visible'
            inUserName.innerText = '合法'
            inUserName.style.color = 'green'
            return true
        }
    },
    // 登录框 密码验证
    checkUserInPassword: function () {
        var pattern = /^\w{2,18}$/
        if (inupwd.value.length == 0) {
            inUserPwd.style.visibility = 'visible'
            inUserPwd.innerText = '密码不能为空'
            inUserPwd.style.color = 'red'
            return false
        }
        inUserPwd.style.visibility = 'visible'
        inUserPwd.innerText = '合法'
        inUserPwd.style.color = 'green'
        return true
    }
}

function login() {

    var username = document.getElementById("loginUsername").value;
    var password = document.getElementById("loginPassword").value;

    var opt = {"username": username, "password": password};
    $.ajax({
        type: 'post',
        /*url: "http://10.130.8.177:12001/login/login",*/
        url: "http://101.35.246.250/user/login/login",
        data: JSON.stringify(opt),
        async: false,//同步/异步
        contentType: "application/json",
        dataType: 'json', //返回 JSON 数据
        success: function (data, status) {
            if (data.code == "200") {
                console.log(data.data);
                sessionStorage.setItem("Authorization", data.data);
                var data = sessionStorage.getItem("Authorization");
                //sessionStorage.setItem("key","value");    //保存数据到sessionStorage
                //var data = sessionStorage.getItem("key");   //获取数据
                //sessionStorage.removeItem("key");                //删除数据
                //sessionStorage.clear();                                  //删除保存的所有数据
                $.ajax({
                    type: 'get',
                    /*url: "http://10.130.8.177:12001/user/get",*/
                    url: "http://101.35.246.250/user/user/get",
                    headers: {'Authorization': data},
                    async: false,//同步/异步
                    contentType: "application/json",
                    dataType: 'json', //返回 JSON 数据
                    success: function (data, status) {
                        console.log(data);
                        if (data.code == "601") {
                            sessionStorage.removeItem("Authorization");
                            //sessionStorage.clear();
                            //swal(data.message);
                            swal(data.message + ",2秒后自动跳转登录页面");
                            setTimeout(function () {
                                window.location.href = "login.html"
                            }, 3000);
                        }
                        sessionStorage.setItem("Zxt-User", JSON.stringify(data.data));
                    },
                    error: function (data, status, e) {
                        alert('接口调用错误！');
                    }
                });

                console.log(data);
                window.location.href = "choose.html"
            } else {
                swal(data.message);
            }
        },
        error: function (data, status, e) {
            alert('接口调用错误！');
        }
    });
};


var name, value;
var str = location.href; //取得整个地址栏
var num = str.indexOf("openId")
var openId = str.substr(num + 7);
openId = decodeURI(openId);
console.log(openId);
if (openId != null && num!=-1) {
    console.log(openId);
    $.ajax({
        type: 'get',
        /*url: "http://10.130.8.177:12001/login/login",*/
        url: "http://101.35.246.250/user/login/quickLogin?openId=" + openId,
        async: false,//同步/异步
        contentType: "application/json",
        dataType: 'json', //返回 JSON 数据
        success: function (data, status) {
            if (data.code == "200") {
                console.log(data.data);
                sessionStorage.setItem("Authorization", data.data);
                var data = sessionStorage.getItem("Authorization");
                //sessionStorage.setItem("key","value");    //保存数据到sessionStorage
                //var data = sessionStorage.getItem("key");   //获取数据
                //sessionStorage.removeItem("key");                //删除数据
                //sessionStorage.clear();                                  //删除保存的所有数据

                console.log(data);
                window.location.href = "choose.html"
            } else {
                swal(data.message);
            }
        },
        error: function (data, status, e) {
            alert('接口调用错误！');
        }
    });
}
